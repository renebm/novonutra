<div class="carousel-divisor"></div>
<div class="footer">
    <div style="width: 100%;height: 1px;margin: 0 0 -1px;clear: both;"></div>
    <div id="institucional-box">
        <div class="title-box-footer">
        	<span class="title-box-footer">Institucional</span>            
      </div>

        <ul class="institucional-lines-box">
		    <li><a href="index.php">Home</a></li>
			<li><a href="sobre.php">Sobre a empresa</a></li>
        </ul>
        
  </div>
    
<div class="footer-boxes">
        <div class="title-boxes-footer">
        	<span class="title-box-footer">Ajuda e suporte</span>
        </div>

        <ul class="institucional-lines-box">
            <li><a href="como-comprar.php">Dúvidas</a></li>
			<li><a href="politica-privacidade.php">Política de privacidade</a></li>
			<li><a href="pagamentos.php">Formas de pagamento</a></li>
			<li><a href="entregas.php">Prazos e Fretes</a></li>
        </ul>
        
  </div>
    
<div class="footer-boxes">
        <div class="title-boxes-footer" >
        	<span class="title-box-footer">Perfil</span>
        </div>

        <ul class="institucional-lines-box">
            <li><a href="minha-conta.php">Sua Conta</a></li>
			<li><a href="meus-pedidos.php">Status do pedido</a></li>
			<li><a href="trocas.php">Trocas e devoluções</a></li>
			<li><a href="fale-conosco.php">Fale conosco</a></li>
        </ul>        
  </div>
<div class="footer-boxes" >
        <div class="title-boxes-footer" >
        	<span class="title-box-footer">Artigos</span>
        </div>

        <ul class="institucional-lines-box">
			<?php
				$departamentos = mysql_query("select ccategoria,clink_seo from tblcategoria_artigo where ccodcategoria = 0 and cativa = -1 and now() between cdata_inicio and cdata_termino;");
				while($departamento = mysql_fetch_array($departamentos)){
			?>
				<li><a href="artigos/<?=$departamento["clink_seo"] ?>"><?=$departamento["ccategoria"] ?></a></li>
			<?php
				}
			?>
        </ul>        
  </div>
  <div class="footer-boxes" >
        <div class="title-boxes-footer" >
        	<span class="title-box-footer">Produtos</span>
        </div>

        <ul class="institucional-lines-box">
			<?php
				$departamentos = mysql_query("select ccategoria,clink_seo from tblcategoria where ccodcategoria = 0 and cativa = -1 and now() between cdata_inicio and cdata_termino;");
				while($departamento = mysql_fetch_array($departamentos)){
			?>
				<li><a href="<?=$departamento["clink_seo"] ?>"><?=$departamento["ccategoria"] ?></a></li>
			<?php
				}
			?>
        </ul>        
  </div>
   <div class="footer-boxes" >
        <div class="title-boxes-footer" >
        	<span class="title-box-footer">Pagamento</span>
        </div>

        <ul class="institucional-lines-box">
            <li><a href="pagamentos.php"><img src="images/bandeiras-cartoes.jpg" alt="Meios de Pagamentos" /></a></li>
        </ul>        
  </div>
	<div style=" height:40px; padding-top:177px;">
    	<div style="float:left; margin-top: 15px;">
			<img src="images/logo.png">
        </div>
		<div style="float:right; margin-top: 29px; position: relative; right: 20px;">
      <span id="sac-text">SAC: 11-3683.0306</span>
      <span id="sac-text">Whatsapp: 11-9.7225.3257</span> 
    </div>
    <div style="float:right; margin-top: 29px; margin-right: 28px; position: relative; right: -10px;">
      <span class="icone"><a href="https://www.facebook.com/nutracorpore" target="_blank"><img src="images/icoFace.jpg" alt="Facebook"></a></span>
      <span class="icone" style="margin-right:15px;"><a href="https://www.youtube.com/user/nutracorpore" target="_blank"><img src="images/icoYouTube.jpg" alt="YouTube"></a></span>
    </div>
		<div style="display:block; margin-top: 29px;clear: both;">
			<div class="hr2"></div>
			<span id="copyright">
        Nutracorpore - CNPJ: 18.572.130/0001-64 -  Rua Dona Primitiva Vianco, 901 - Centro - Osasco - São Paulo - CEP: 06010-000  -  www.nutracorpore.com.br Copyright <?=date("Y") ?> - Todos os direitos reservados
			</span>
			<span id="feeshop">
				Desenvolvimento e projeto: <img src="images/feeshop.png" style="vertical-align: middle; margin-top: -7px;">
			</span>
		</div>
   </div>
</div>
<div id="blue-end-footer-box"></div>

<div class="lightbox">
  <div class="warning big yellow">
    <img src="images/rect-warning.png" alt="">
    <p class="message"></p>
    <p class="alert-sac">SAC: 11 3683.0306</p>
    <p class="alert-sac">WHATSAPP: 11 97225.3257</p>
    <button class="response">OK</button>
  </div>
</div>