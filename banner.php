<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}

	$id = $_REQUEST["id"];	
	if(!is_numeric($id)){
		$id = 0;	
	}
	
	$erro = 0;
	$msg = "";


	/*-----------------------------------------------------------------------
	ajax de excluisão
	------------------------------------------------------------------------*/
	if($_POST && $_REQUEST["action"]=="excluir"){
		
		$ssql = "select bannerid, barquivo from tblbanner where bannerid = '{$id}'";
		$result = mysql_query($ssql);
		if($result){
			while($row = mysql_fetch_assoc($result)){
				$arquivo = "../".$row["barquivo"];	
				if(file_exists($arquivo)){
					unlink($arquivo);
				}
				$arquivo = "../".$row["barquivo"];	
			mysql_free_result($result);
			}
		}
				
		$ssql = "delete from tblbanner where bannerid='{$id}'";
		mysql_query($ssql);
		echo "ok";
		exit();
		
	}




	if($_POST && $_REQUEST["action"]=="gravar"){
		
		$id					= 	addslashes($_REQUEST["id"]);
		$categoria			=	intval($_REQUEST["categoria"]);
		$marca				=	intval($_REQUEST["marca"]);
		$tipo				=	intval($_REQUEST["tipo"]);
		$titulo				=	addslashes($_REQUEST["titulo"]);
		$descricao			=	addslashes($_REQUEST["descricao"]);
		$link				=	addslashes($_REQUEST["link"]);
		$target				=	addslashes($_REQUEST["target"]);
		$ativo				=	intval($_REQUEST["ativo"]);
		$click				= 	0;
		$view				= 	0;
		$contador			= 	0;
		
		
		$data_inicio		=	addslashes($_REQUEST["data_inicio"]);
		$data_termino		=	addslashes($_REQUEST["data_termino"]);
		
		
		if(!is_numeric($categoria)){
			$categoria = 0;	
		}

		if(!is_numeric($tipo)){
			$tipo = 0;	
		}
		
		$data_inicio		=	formata_data_db($data_inicio);
		$data_termino		=	formata_data_db($data_termino);
		
		/*-------------------------------------------------
		zera os contadores
		--------------------------------------------------*/
		$ssql = "update tblbanner set bcontador = 0";
		mysql_query($ssql);
		
		
		
		if($id==0){
			
			//verifica se nao existe ja umbanner com o mesmo nome;
			$ssql = "select bannerid from tblbanner where btitulo='{$banner}' and bcodtipo='{$tipo}'";
			$result = mysql_query($ssql);
				if($result){
					$num_rows = mysql_num_rows($result);
					if($num_rows>0){
						$msg = "Já existe um registro com os mesmos dados digitados";
						$erro = 1;
					}	
				}
				
			//--------------------------------------------------------	
			
			if($num_rows==0){
				$ssql = "insert into tblbanner (bcodtipo, bcodcategoria, bcodmarca, btitulo, bdescricao, blink, btarget, barquivo, bativo, bdata_inicio, bdata_termino, 
												bcontador, bview, bclick, bcodusuario, bdata_alteracao, bdata_cadastro) 
							values('{$tipo}','{$categoria}','{$marca}','{$titulo}','{$descricao}','{$link}','{$target}','{$arquivo}','{$ativo}','{$data_inicio}','{$data_termino}',
								   '{$contador}', '{$view}', '{$click}', '{$usuario}', '{$data_hoje}','{$data_hoje}')";
				$result = mysql_query($ssql);
				if(!$result){
					$msg = "Erro ao gravar registro.<br>".mysql_error($conexao);
					$erro = 1;
				}
				else
				{
					$id = mysql_insert_id();
					$msg = "Registro salvo com sucesso";
				}
				
			}
			
		}
		else
		{
				//edita/atualiza o registro	
				$ssql = "update tblbanner set bcodtipo='{$tipo}', bcodcategoria='{$categoria}', bcodmarca='{$marca}', btitulo='{$titulo}', bdescricao='{$descricao}', blink='{$link}', 
											btarget='{$target}', bativo='{$ativo}', bdata_inicio='{$data_inicio}', bdata_termino='{$data_termino}', 
											bcodusuario='{$usuario}', bdata_alteracao='{$data_hoje}' 
											where bannerid = $id";
				$result = mysql_query($ssql);
				if(!$result){
					$msg = "Erro ao atualizar registro";
					$erro = 1;
				}
				else
				{
					$msg = "Registro atualizado com sucesso";
				}	
			
		}
		
		//echo $ssql;
		
		
		/*---------------------------------------------------------------------
		atualiza a imagem se foi postada
		-----------------------------------------------------------------------*/
	
			if($_FILES){						
				$file_name = $_FILES['arquivo']['name'];
				$file_type = $_FILES['arquivo']['type'];
				$file_size = $_FILES['arquivo']['size'];
				$file_tmp_name = $_FILES['arquivo']['tmp_name'];
				$error = $_FILES['arquivo']['error'];
							
				$file_size =  number_format($file_size/1024,2);
				
				if($file_size>100 && $tipo==1){
					$msg .= "<br /><br />Erro ao gravar imagem: O tamanho do arquivo não pode ser maior que 100KB";
				}				
			}
		
			if($error==0){
				
				$arquivo = "imagem/banner/" . gera_url_seo_imagem($file_name);
				
				$img_hash = mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"));	
				
				if(file_exists("../".$arquivo)){
					$arquivo = left($arquivo,strlen($arquivo)-4)."-".$img_hash.right($arquivo,4);	
				}
				
				//echo $arquivo;
				//move para pasta TMP
				$move = move_uploaded_file($file_tmp_name,"../".$arquivo);
				
				$ssql = "update tblbanner set barquivo='{$arquivo}' where bannerid='{$id}'";
				$result = mysql_query($ssql);
				if(!$result){
					$msg .= "<br />Ocorreu um erro ao atualizar a imagem tente enviar a imagem novamente";
				}
			
			}
		//echo $ssql;
	}




/*---------------------------------------------------------------------------------------------------
carrega os dados do banner
----------------------------------------------------------------------------------------------------*/
	if($id>0){
	
		$ssql = "select bcodtipo, bcodcategoria, bcodmarca, btitulo, bdescricao, blink, btarget, barquivo, bativo, bdata_inicio, bdata_termino, 
												bcontador, bview, bclick, bcodusuario, bdata_alteracao, bdata_cadastro
				from tblbanner where bannerid='{$id}'";
		$result = mysql_query($ssql);
		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				
				$categoria		=	$row["bcodcategoria"];
				$marca			=	$row["bcodmarca"];
				$tipo			=	$row["bcodtipo"];
				$titulo			=	$row["btitulo"];
				$descricao		=	$row["bdescricao"];
				$link			=	$row["blink"];
				$target			=	$row["btarget"];
				
				$ativo			=	$row["bativo"];
				
				$arquivo		=	$row["barquivo"];
				
				$data_inicio	=	formata_data_tela($row["bdata_inicio"]);
				$data_termino	=	formata_data_tela($row["bdata_termino"]);
				
				$usuario		=	get_usuario($row["bcodusuario"],"unome");		
				$data_alteracao	=	formata_data_tela($row["bdata_alteracao"]);
				$data_cadastro	=	formata_data_tela($row["bdata_cadastro"]);
				
			}
			mysql_free_result($result);
		}	
		
	}

	
	if($id==0){
		$data_inicio = date("d/m/Y")." 00:00:00";
		$data_termino = "31/12/2050 23:59:59";
	}

		
		
		
function gera_url_seo_imagem($input){
	$ret = $input;
	
	$extensao = right($input,4);
	$extensao = strtolower($extensao);
	
	$ret = left($input,strlen($input)-4);
	
	$ret = str_replace("à","a",$ret);
	$ret = str_replace("á","a",$ret);
	$ret = str_replace("ã","a",$ret);
	$ret = str_replace("â","a",$ret);
	$ret = str_replace("é","e",$ret);
	$ret = str_replace("ê","e",$ret);
	$ret = str_replace("í","i",$ret);
	$ret = str_replace("ó","o",$ret);
	$ret = str_replace("õ","o",$ret);
	$ret = str_replace("ú","u",$ret);
	$ret = str_replace("ü","u",$ret);
	$ret = str_replace("ç","c",$ret);
	$ret = str_replace("ñ","n",$ret);
	$ret = trim(ereg_replace(' +','-',preg_replace('/[^a-zA-Z0-9-\s]/','',$ret))); 
	$ret = str_replace("--","-",$ret);
	$ret = str_replace("----","---",$ret);
	
	$ret = strtolower($ret);
	
	return $ret.$extensao;
	
}		

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
							   
		$("#data_inicio").mask("99/99/9999 99:99:99");					   	
		$("#data_termino").mask("99/99/9999 99:99:99");	   	
		
		$("#tipo").focus();
		
	});
    
</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form action="banner.php" method="post" enctype="multipart/form-data" name="frm_banner" id="frm_banner" onsubmit="return valida_banner();">
    <input name="action" id="action" type="hidden" value="gravar" />  
    <input name="id" id="id" type="hidden" value="<?php echo $id?>" />
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Banner &raquo;  <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='banner.php';"><?php echo ($id==0) ? "Novo Registro" : $titulo;  ?></span> </span> <a href="banner_consulta.php">Consulta</a>
       	  </div>
            
            <div id="conteudo-interno">
              <table width="98%" border="0" cellspacing="0" cellpadding="2" style="margin:10px;">
                <tr>
                  <td width="130">ID</td>
                  <td id="td_id">#<?php echo $id;?></td>
                  <td width="200" align="center" valign="top">
				  <?php echo $msg;?>
                  <br />
                  <?php 				  
				  	if(file_exists("../".$arquivo) && $tipo>=2 && $tipo<=3){		
						echo "<p align='center'>";
						echo "<img src='../".$arquivo."' />";
						echo "</p>";
					}
				  ?>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>Tipo</td>
                  <td><select name="tipo" class="formulario" id="tipo">
                    <option value="0">Selecione</option>
                    <option value="1" <?php if($tipo==1){echo "selected";}?>>Full Banner</option>
                    <option value="2" <?php if($tipo==2){echo "selected";}?>>SkyScrape</option>
                    <option value="3" <?php if($tipo==3){echo "selected";}?>>Selo</option>
                    <option value="4" <?php if($tipo==4){echo "selected";}?>>Regua</option>
                  </select></td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>Titulo</td>
                  <td><input name="titulo" type="text" class="formulario" id="titulo" onblur="javascript: gera_link_seo(this.value,'link_seo');" value="<?php echo $titulo;?>" size="75" /></td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>Descrição</td>
                  <td><input name="descricao" class="formulario" id="descricao" type="text" size="75" maxlength="250" value="<?php echo $descricao;?>" /></td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>URL / Link</td>
                  <td><input name="link" class="formulario" id="link" type="text" size="75" maxlength="250" value="<?php echo $link;?>" /></td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>Destino</td>
                  <td><select name="target" class="formulario" id="target">
                    <option value="0">Selecione</option>
                    <option value="_self" <?php if($target=="_self"){echo "selected";}?>>Mesma Janela</option>
                    <option value="_blank" <?php if($target=="_blank"){echo "selected";}?>>Nova Janela</option>
                  </select></td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>Categoria</td>
                  <td><select name="categoria" id="categoria" class="formulario">
                    <option value="0" >Selecione</option>
                    <?php
                           $ssql = "select categoriaid, ccategoria from tblcategoria where ccodcategoria = 0 order by ccategoria";
						   $result = mysql_query($ssql);
						   if($result){
								while($row=mysql_fetch_assoc($result)){
									echo '<option value="'.$row["categoriaid"].'"';
									if( $categoria == $row["categoriaid"] ){
										echo ' selected';	
									}
									echo '>'.$row["ccategoria"].'</option>';
								}
								mysql_free_result($result);
						   }
					?>
                  </select></td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>Marca</td>
                  <td><select name="marca" id="marca" class="formulario">
                    <option value="0" >Selecione</option>
                    <?php
                           $ssql = "select marcaid, mmarca from tblmarca order by mmarca";
						   $result = mysql_query($ssql);
						   if($result){
								while($row=mysql_fetch_assoc($result)){
									echo '<option value="'.$row["marcaid"].'"';
									if( $marca == $row["marcaid"] ){
										echo ' selected';	
									}
									echo '>'.$row["mmarca"].'</option>';
								}
								mysql_free_result($result);
						   }
					?>
                  </select></td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>  
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>                                                              
                <tr>
                  <td>Ativo</td>
                  <td><label>
                    <input type="radio" name="ativo" id="ativo" value="-1" <?php echo ($ativo == -1) ? "checked" : "" ;?>  />
                    Sim</label>
                    <label>
                      <input type="radio" name="ativo" id="ativo" value="0" <?php echo ($ativo == 0) ? "checked" : "" ;?>  />
                      Não</label></td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>Data Início</td>
                  <td><input name="data_inicio" class="formulario" id="data_inicio" type="text" size="25" maxlength="20" value="<?php echo $data_inicio;?>" />
                    dd/mm/aaaa hh:mm:ss</td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>Data Término</td>
                  <td><input name="data_termino" class="formulario" id="data_termino" type="text" size="25" maxlength="20" value="<?php echo $data_termino;?>" />
                    dd/mm/aaaa hh:mm:ss</td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>Arquivo</td>
                  <td><input name="arquivo" type="file" id="arquivo" size="75" maxlength="200" /></td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>Dimensão: 1000px x 410px<br />
                    Tamanho máximo: 100KB<br />
                    Formatos aceitos: .jpg, .gif, .png<br /></td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>Usuário</td>
                  <td><?php echo $usuario;?></td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>Data Alteração</td>
                  <td><?php echo $data_alteracao;?></td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>Data Cadastro</td>
                  <td><?php echo $data_cadastro;?></td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><input type="submit" id="btn-cmd-gravar" name="btn-cmd-gravar" value="Gravar" class="btn-gravar" /></td>
                  <td width="200" align="center" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="3"><?php 
				  	if($arquivo!="" && file_exists("../".$arquivo) && $tipo==1){
						echo "<p align='center'>";
						echo "<img src='../".$arquivo."' />";
						echo "</p>";
					}
				  ?></td>
                </tr>
              </table>
            </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>