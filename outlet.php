<?php
	include("include/inc_conexao.php");	
	include_once("include/inc_funcao.php");	
	/*-------------------------------------------------
	//navegação sem ssl
	---------------------------------------------------*/
	if(strpos($_SERVER['SERVER_NAME'],".com")>0){
		if($_SERVER['SERVER_PORT']!=80){
			header("location: http://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
			exit();
		}
	}	
	
	
	//monta a categoria
	$uri = str_replace("/nutraNew/","",$_SERVER['REQUEST_URI']);	

	$pagina = 1;
	$start = 0;
	$limit = 16;
	$ordem = 0;
	
	$canonical = "";
	
	$menor = 0;
	$maior = 999999;
	if(isset($_GET["marca"]) && $_GET["marca"] != ''){
	  $marca = $_GET["marca"];
	  $marca = get_marcaid_by_node($marca);
	  $marca = " and tblmarca.marcaid = $marca ";
	}
	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}

	
	
	if(is_numeric($qs["ordem"])){	
		$ordem = $qs["ordem"];
		if($ordem>8 || $ordem <0){
			$ordem = 0;	
		}
	}	
	
	if(is_numeric($qs["menor"])){	
		$menor = intval($qs["menor"]);
	}	
	
	if(is_numeric($qs["maior"])){	
		$maior = intval($qs["maior"]);
		if($maior==0){
			$maior = 999999;	
		}
	}		
	
	/*--------------------------------------------------------------------------
	remove os parametros de querystring para montar as info de categorias
	---------------------------------------------------------------------------*/
	if(strpos($uri,"?")<>""){
		$uri = substr($uri,0,strpos($uri,"?")-1);
	}

	$nodes = explode("/",$uri);
	$departamentoid = intval(get_categoriaid_by_node($nodes[1],0));
	$categoriaid 	= intval(get_categoriaid_by_node($nodes[2],$departamentoid));
	$subcategoriaid = intval(get_categoriaid_by_node($nodes[3],$categoriaid));
	



	/*-----------------------------------------------------------------------------
	cria a data de lancamento
	-------------------------------------------------------------------------------*/
	$config_qtde_dias_lancamento = intval(get_configuracao("config_qtde_dias_lancamento"));
	if($config_qtde_dias_lancamento==0){
		$config_qtde_dias_lancamento = 30;	
	}
	
	$data = strtotime(date("Y-m-d", strtotime($data_hoje)) . "-$config_qtde_dias_lancamento days") . "";
	$data_lancamento = date("Y-m-d H:i:s",$data);

		switch ($ordem){
			
			case "0":
			$order = " pdata_inicio desc";
			break;				
			
			case "1":
			$order = " pvalor_unitario";
			break;

			case "2":
			$order = " pvalor_unitario desc";
			break;

			case "3":
			$order = " pvalor_unitario desc";	//mais vendidos
			break;

			case "4":
			$order = " pvalor_unitario desc";	//mais bem avaliados
			break;

			case "5":
			$order = " pproduto";
			break;

			case "6":
			$order = " pproduto desc";
			break;

			case "7":
			$order = " pdata_inicio desc";	//lancamento
			break;

			case "8":
			$order = " pdesconto desc";
			break;

		}
		

		//define se ira agrupar os produtos com mesma referencia
		$config_agrupa_por_referencia = get_configuracao("config_agrupa_por_referencia");
		if($config_agrupa_por_referencia==0){
			$agrupa_por_referencia = ", tblproduto.produtoid ";	
		}
		$vitrine_ordem = get_configuracao("vitrine_ordem");
		  if($vitrine_ordem==""){
		    $vitrine_ordem = " RAND() ";
		  }


		$ssql_sale = "select 1 as rank, tblproduto.pdata_alteracao, tblmarca.mmarca, tblproduto.pdescricao, tblmarca.mlink_seo, tblproduto.pdisponivel,tblproduto.produtoid, sum(tblestoque.eestoque) as estoque,tblproduto.pcontrola_estoque, tblproduto.preferencia, tblproduto.pproduto,
			tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, (tblproduto.pvalor_comparativo - tblproduto.pvalor_unitario) as pdesconto,
			tblproduto.plink_seo, tblproduto_midia.marquivo as pimagem
			from tblproduto
			left join tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
			left join tblproduto_midia on tblproduto.produtoid = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal = -1
			left join tblproduto_categoria on tblproduto.produtoid = tblproduto_categoria.pcodproduto
			left join tblmarca on marcaid = tblproduto.pcodmarca
			where tblproduto.pdata_inicio<='{$data_hoje}' and tblproduto.pdata_termino>='{$data_hoje}' $marca
			and tblproduto.pvalor_unitario between '{$menor}' and '{$maior}'";
		
		if($departamentoid > 0 && $categoriaid == 0 && $subcategoriaid == 0){
			$ssql_sale .= " and tblproduto_categoria.pcodcategoria = '{$departamentoid}' ";		
		}

		if($departamentoid > 0 && $categoriaid > 0 && $subcategoriaid == 0){
			$ssql_sale .= " and tblproduto_categoria.pcodcategoria = '{$categoriaid}' ";		
		}

		if($departamentoid > 0 && $categoriaid > 0 && $subcategoriaid > 0){
			$ssql_sale .= " and tblproduto_categoria.pcodcategoria = '{$subcategoriaid}' ";		
		}
		
		/* 24/01/2014 - Busca os IDs dos produtos que são daquela variacao */
		
		if(isset($qs["variacao"])){
			if((int)$qs["variacao"] > 0){
				$variacao = mysql_query("select ccodproduto from tblproduto_caracteristica where ccodpropriedade = ".addslashes((int)$qs["variacao"]));
				$ids_tamanho = "0";
				while($rw = mysql_fetch_array($variacao)){
					$ids_tamanho .= ",".$rw["ccodproduto"];
				}
				$ssql_sale .= " and tblproduto.produtoid in ($ids_tamanho) ";
			}
		}
		$ssql_sale .= " and tblproduto.pvalor_comparativo > tblproduto.pvalor_unitario ";
		$ssql_sale .= " and tblproduto.outlet = -1 ";

		$ssql_sale .= "	
			group by tblproduto.preferencia $agrupa_por_referencia
			having(1=1)
			";
		$ssql_sale = "(".str_replace("having(1=1)","HAVING ((SUM(tblestoque.eestoque) > 0 && tblproduto.pcontrola_estoque = -1 && tblproduto.pdisponivel = -1) || (tblproduto.pdisponivel = -1 && tblproduto.pcontrola_estoque = 0 ))",$ssql_sale).") UNION ALL (".str_replace(array("having(1=1)","1 as rank"),array("HAVING ((SUM(tblestoque.eestoque) = 0 && tblproduto.pcontrola_estoque = -1) || tblproduto.pdisponivel = 0)","2 as rank"),$ssql_sale).")";
		
		//order by $vitrine_ordem, $order
		//echo $ssql_sale;
		//die();
		$ssql_sale  = "SELECT tblproduto.produtoid, tblproduto.pdata_alteracao, tblmarca.mmarca, tblproduto.pdescricao, tblmarca.mlink_seo, tblproduto.pdisponivel,tblproduto.produtoid, sum(tblestoque.eestoque) as estoque,tblproduto.pcontrola_estoque, tblproduto.preferencia, tblproduto.pproduto,
			tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, (tblproduto.pvalor_comparativo - tblproduto.pvalor_unitario) as pdesconto,
			tblproduto.plink_seo, tblproduto_midia.marquivo as pimagem";
		$ssql_sale .= " FROM tblproduto ";
		$ssql_sale .= "left join tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
			left join tblproduto_midia on tblproduto.produtoid = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal = -1
			left join tblproduto_categoria on tblproduto.produtoid = tblproduto_categoria.pcodproduto
			left join tblmarca on marcaid = tblproduto.pcodmarca
			where tblproduto.pdata_inicio <='{$data_hoje}' and tblproduto.pdata_termino >='{$data_hoje}' $marca
			and tblproduto.pvalor_unitario between '{$menor}' and '{$maior}' and tblproduto.outlet = -1 GROUP BY tblproduto.produtoid";

		$result = mysql_query($ssql_sale);					
		if($result){
			$total_registros = mysql_num_rows($result);	
		}



/*-------------------------------------------------------------------
base href
--------------------------------------------------------------------*/
$server = ($_SERVER['SERVER_PORT']==80 ? "http://" : "https://") . $_SERVER['SERVER_NAME'] . str_replace("lancamento.php","",$_SERVER['SCRIPT_NAME']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $categoria?>" />
<meta name="description" content="<?php echo $categoria;?>" />
<meta name="keywords" content="<?php echo $categoria;?>" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $categoria;?>" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?php echo $site_site;?>/categoria.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<?php
include("include/inc_analytics.php");	
?>
</head>

<body>
<div id="global-container">
	<div id="header-content">
	<?php
			include("inc_header.php");
	?>
    </div>
	<div id="main-box-container">
   	  <div id="categoria-container-box"></div>
    	<div id="container-menu-left">
        	<?php
            	include("inc_left_sale.php");
			?>            
        </div>
        <div class="box-products-container">
			<div id="org-sup-box-content">
				<div class="box-sort-by">
					<span class="number-found-itens"><?php echo $total_registros;?> ítens encontrados</span>
					<span class="sort-by">Ordenar por: </span>
					<select name="sort-by-itens" class="sort-by-itens" onchange="javascript:paginacao('ordem',this.value);">
						<?php
							echo combo_ordem_pagina($ordem);
						?>
					</select>
					<span class="itens-por-pagina">Itens por página: </span>
					<select name="qtde-itens-pag" class="qtde-itens" onchange="javascrip:paginacao('limit',this.value);">
						<?php
							echo combo_itens_pagina($limit);
						?>
					</select>
				</div>
				<div class="pagination-box">
					<div class="paginacao">
						<?php
							echo paginacao($pagina, $limit, $total_registros);
						?>                            
					</div>
				</div>
			</div>
            <div id="products-category-box">
                <?php 

					$imagem_x = 260;
					$imagem_y = 260;
					
					$ssql_sale .= " order by tblproduto.pproduto asc,pdata_alteracao desc limit $start, $limit";

					$result = mysql_query($ssql_sale);
			
					if($result){
						
						$count = 0;
						$num_rows = mysql_num_rows($result);
						if($num_rows==0){
							echo '<div style="width: 100%;height: auto;text-align: center;font-family: Arial;position: relative;top:35px;"><strong>Nenhum produto foi localizado.</strong></div>';	
						}
						while($row=mysql_fetch_assoc($result)){
			
							$id = $row["produtoid"];
							$referencia = $row["preferencia"];
							$valor_unitario = $row["pvalor_unitario"];
							$valor_comparativo = number_format($row["pvalor_comparativo"],2,',','.');
							
							$valor = $row["pvalor_unitario"];
							
							if( isset($_SESSION["utm_source"]) ){
								$desconto_utm =  get_desconto_utm_source($id);
								$valor_unitario = number_format($valor_unitario - $desconto_utm,2,",",".");
								
								if(floatval($desconto_utm)>0){
									$valor_comparativo = number_format($row["pvalor_unitario"],2,',','.');
								}
								
							}
							else
							{
								$valor_unitario = number_format($valor_unitario,2,",",".");	
							}

							$imagem = $row["pimagem"];
							$count++;
							?>
							<a href="<?=$row["plink_seo"] ?>">
							<div class="produto">
								<span class="product-thumb"><img src="<?php if(file_exists($imagem)){ echo $imagem; }else{ echo "imagem/produto/tumb-indisponivel.png"; } ?>" alt="produto"></span>
								<span class="product-menu-top20-product-title"><?=$row["pproduto"] ?></span>
								<span class="product-menu-top20-product-mark"><b>+</b> <?=$row["mmarca"] ?></span>
								<span class="product-menu-top20-product-price">R$ <s><?=number_format($row["pvalor_unitario"],2,",",".") ?></s></span>
								<span class="par-velue"><?=get_valor_parcelado_new($row["produtoid"],$row["pvalor_unitario"])?></span>
								<span class="a-vista-price">Ou R$ <?=number_format($row["pvalor_unitario"]-($row["pvalor_unitario"]*0.10),2,",",".")?> à vista</span>
								<span class="boleto-text">via boleto</span>
							</div>
							</a>
							<?php
						}
						mysql_free_result($result);
					}				  
				  
				  ?>

			</div>
			<div id="org-sup-box-content">
				<div class="box-sort-by">
					<span class="itens-por-pagina">Itens por página</span>
					<select name="qtde-itens-pag" class="qtde-itens" onchange="javascrip:paginacao('limit',this.value);">
						<?php
							echo combo_itens_pagina($limit);
						?>
					</select>
					<span class="sort-by">Ordenação</span>
					<select name="sort-by-itens" class="sort-by-itens" onchange="javascript:paginacao('ordem',this.value);">
						<?php
							echo combo_ordem_pagina($ordem);
						?>
					</select>
				</div>
				<div class="pagination-box">
					<div class="paginacao"><span class="paginacao-text">Página:</span></span> 
						<?php
							echo paginacao($pagina, $limit, $total_registros);
						?>                            
					</div>
				</div>
			</div>
        </div>
		<div id="aside-right-bar2">
			<?php carregaBanners(2); ?>
		</div>
	</div>
    
    <div id="footer-container">
    <?php
		include("inc_footer.php");
	?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>