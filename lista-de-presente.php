<?php
	include("include/inc_conexao.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Lista de Presente</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Meus Pedidos" />
<meta name="description" content="<?php echo $site_nome;?> Meus Pedidos. Acompanhe o status e histórico de suas compras." />
<meta name="keywords" content="<?php echo $site_nome;?> Meus Pedidos" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?>  Meus Pedidos" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/lista-presente.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		$("#lista_data_evento").mask("99/99/9999");
		$("#lista_titulo").focus();
    });	
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">
    	
      <div id="containe-lista-main" style="width:980px; margin:10px; float:left; background-color:#fff;">
        	
            <span style="width:980px; float:left; text-align:center;">
            	<img src="images/tit-lista-presente.jpg" alt="Lista de Presentes" width="980" height="75" border="0" />
            </span>
            
            <span style="width:920px; float:left; text-align:center; line-height:25px; padding:20px; font-size:13px;">
                A Lista de Presentes <?php echo $site_nome;?> foi criada para tornar mais simples e especial o momento de presentear noivos e aniversariantes.<br />
                Aqui você vai encontrar qualidade e variedade para montar uma lista de presentes com seu estilo.
                <br /><br />
                Se você foi convidado, aqui você encontra o presente ideal para marcar presença e participar desse momento tão especial. <br>
                Siga um dos caminhos abaixo para começar.            
       	</span>

            
            <div style="width:400px; float:left; margin:20px;"> <a href="criar-lista-presente.php">
            	<img src="images/lista-img-criar.png" alt="Criar lista de presente" width="400" height="267" border="0" /></a>
          	</div>
            
            <div style="float:left; height:200px; margin:40px;">
            &nbsp;
            </div>
            
<div style="width:400px; float:left; margin:20px;"> <a href="lista-presente.php">
            	<img src="images/lista-img-busca.png" alt="Lista de presente" width="400" height="267" border="0" /></a>
       	</div>

            
      </div>
    
    </div>
    
    <div id="footer-container">
		<?php
            include("inc_footer.php");
        ?>
    </div>
</div>
</body>
</html>