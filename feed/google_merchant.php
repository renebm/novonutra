<?php
include("../include/inc_conexao.php");

header("Content-type: text/xml");

	echo '<?xml version="1.0" encoding="UTF-8"?>';
	echo '<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">';
	echo '<channel>';
	echo "<title>".$site_nome."</title>";
	echo "<link>http://".$_SERVER['SERVER_NAME']."</link>";
	echo "<description>".$site_descricao."</description>";
	
	$ssql = "select * from tblproduto 
			join tblmarca on pcodmarca = marcaid where produtoid <> 103 ";
	
	$result = mysql_query($ssql);
	if($result){
		while( $row = mysql_fetch_assoc($result) ){
			
			$id		 	  = $row["produtoid"];
			$codigo		  = $row["pcodigo"];
			$produto 	  = mb_convert_case(($row["pproduto"]), MB_CASE_TITLE, "UTF-8");
			$imagem		  = "http://".$_SERVER['SERVER_NAME']."/" . $row["marquivo"];
			$descricao	  = $row["pdescricao"];
			$valor		  = number_format($row["pvalor_unitario"],2,",",".");
			$marca		  = $row["mmarca"];

			$categoria	  = '';
			
			$cursorCategorias = "SELECT tblcategoria.ccategoria, tblcategoria.clink_seo
			FROM tblcategoria
			JOIN tblproduto_categoria ON tblcategoria.categoriaid = tblproduto_categoria.pcodcategoria
			WHERE tblproduto_categoria.pcodproduto = $id
			ORDER BY tblcategoria.ccodcategoria";
			
			$listaCategorias = mysql_query($cursorCategorias);
			while($cursor=mysql_fetch_assoc($listaCategorias)){
				if($categoria!=""){
					$categoria .= " > ";
				}
				$categoria .= $cursor["ccategoria"];
				$subcategoria = $cursor["clink_seo"];
			}
			
			$link  = "http://".$_SERVER['SERVER_NAME']."/" . $row["plink_seo"] . "?utm_source=google&utm_medium=shopping&utm_campaign=".$subcategoria;
			
			echo "<item>";
			echo "<title><![CDATA[".$produto."]]></title>";
			echo "<link><![CDATA[".$link."]]></link>";
			echo "<description><![CDATA[".$descricao."]]></description>"; 
			echo "<g:id>".$id."</g:id>";
			echo "<g:condition>new</g:condition>";
			echo "<g:price>".$valor."</g:price>";
			echo "<price-no-rebate>".$comdesconto."</price-no-rebate>";
			echo "<month-price-number>".$parcelas."</month-price-number>";
			echo "<month-price-value>".$parcela."</month-price-value>"; 
			echo "<g:availability>in stock</g:availability>";
			echo "<g:image_link><![CDATA[".$imagem."]]></g:image_link>";
	echo "	<g:shipping>
                <g:country>BR</g:country>
                <g:service>Transportadora</g:service>
                <g:price>0.00</g:price>
            </g:shipping>";
			echo "<g:brand><![CDATA[".$marca."]]></g:brand>";
			echo "<g:google_product_category><![CDATA[Saúde e beleza > Cuidados com a saúde > Preparo físico e nutrição]]></g:google_product_category>";
			echo "<g:product_type><![CDATA[".$categoria."]]></g:product_type>";
			echo "<g:mpn><![CDATA[".$codigo."]]></g:mpn>";
			echo "</item>";
			
		}
		mysql_free_result($result);
	}

	echo '</channel>';
	echo '</rss>';

?>