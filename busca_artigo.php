<?php
	include("include/inc_conexao.php");
	
	/*-------------------------------------------------
	//navegação sem ssl
	---------------------------------------------------*/
	if(strpos($_SERVER['SERVER_NAME'],".com")>0){
		if($_SERVER['SERVER_PORT']!=80){
			header("location: http://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
			exit();
		}
	}
	
	$pagina = 1;
	$start = 0;
	$limit = 12;
	$ordem = 0;

	$canonical = "";

	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/

	if(is_numeric($_GET["pagina"])){	
		$pagina = $_GET["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}

	/*----------------------------------------------------------------------
	insere a palavra bucada na tblbusca
	----------------------------------------------------------------------*/
	$ssql = "select btag from tblbusca where btag='{$string}' and bip='{$ip}'";
	$result = mysql_query($ssql);
	if($result){
		if(mysql_num_rows($result)==0){

			$ssql = "insert into tblbusca (btag, borigem, bip, bdata_alteracao, bdata_cadastro)
						values('{$string}','1','{$ip}','{$data_hoje}','{$data_hoje}')";
			mysql_query($ssql);
			//echo $ssql;
		}	
	}
		
	/*------------------------------------------------------------------------
	busca
	-------------------------------------------------------------------------*/

	$string = str_replace(" ","%",$_GET["string"]);
	$categoria = '';
	if(isset($_GET["categoria"])){
		if((int)$_GET["categoria"] > 0){
			$categoria = $_GET["categoria"];
		}
	}
	
	$ssql_busca ="select artigoid,atitulo,adata_cadastro,aimagem,alink_seo,ccategoria from tblartigos
	left join tblcategoria_artigo on acategoria = categoriaid 
	where (atitulo like '%$string%' or adescricao like '%$string%' or apalavra_chave like '%$string%')
	";
	if($categoria != '' && $categoria > 0){
		$ssql_categoria = mysql_query("select if(c.categoriaid is null,0,c.categoriaid) as categoria, if(s.categoriaid is null,0,s.categoriaid) as subcategoria
					from tblcategoria_artigo as c
					left join tblcategoria_artigo as s on s.ccodcategoria = c.categoriaid
					where c.ccodcategoria = $categoria;");
		while($row = mysql_fetch_array($ssql_categoria)){
			$categoria .= ",".$row["categoria"].",".$row["subcategoria"];
		}
		$ssql_busca .= " and acategoria in ($categoria) ";
	}
	$ssql_busca .= " order by adata_cadastro desc";
	
	//echo $ssql_busca;
	//die();
	
	$result = mysql_query($ssql_busca);
	if($result){
		$total_registros = mysql_num_rows($result);	
	}
	
	$ssql_busca .= " limit $start, $limit";
	
	/*-------------------------------------------------------------------
	base href
	--------------------------------------------------------------------*/
	$server = ($_SERVER['SERVER_PORT']==80 ? "http://" : "https://") . $_SERVER['SERVER_NAME'] . str_replace("busca.php","",$_SERVER['SCRIPT_NAME']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $categoria?>" />
<meta name="description" content="<?php echo $categoria;?>" />
<meta name="keywords" content="<?php echo $categoria;?>" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $categoria;?>" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?php echo $site_site;?>/categoria.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<?php
include("include/inc_analytics.php");	
?>
</head>

<body>
<div id="global-container">
	<div id="header-content">
	<?php
			include("inc_header.php");
		?>
    </div>
	<div id="main-box-container">
		<div id="box-sup-artigo">
			<form name="artigobusca"  method="post" action="busca/" onsubmit="return valida_busca_artigo();">
				<input type="text" value="<?=$string ?>" name="string" id="search-artigo">
				<input type="submit" value="" name="enviar" id="search-artigo-btn">
			</form>
		</div>
    	<div id="container-menu-left">
        	<?php
            	include("inc_left_busca_artigo.php");
			?>            
        </div>
        <div class="box-products-container" style="position: relative;top: -20px;">
            <div id="products-category-box">
                  <?php 
						if($total_registros){
							$ssql_busca = mysql_query($ssql_busca);
							while($row = mysql_fetch_array($ssql_busca)){
								$data = explode(" ",$row["adata_cadastro"]);
								$data = explode("-",$data[0]);
								$data = $data[2]."/".$data[1]."/".$data[0];
								//$imagem = str_replace("tumb","big",$row["aimagem"]);
								$imagem = $row["aimagem"];
								if(!file_exists($imagem)){ $imagem = "imagem/produto/med-indisponivel.png"; }
						?>
							<a href="artigo/<?=$row["alink_seo"]."---".$row["artigoid"] ?>" alt="artigo">
								<div class="artigo-box-two">
									<span class="img-prev-two">
										<img src="<?=$imagem ?>" alt="Artigo">
									</span>
									<span class="artigo-rel-date"><?=$data ?></span>
									<span class="artigo-rel-tit">
										<?=$row["atitulo"] ?>
									</span>
								</div>
							</a>
						<?php
							}
						}else{
							echo "<div align='center' style='margin:30px 0;'>Nenhum resgistro encontrado.</div>";
						}
				  ?>
                <div id="org-sup-box-content">
                    <div class="pagination-box">
                    	<div class="paginacao"><span class="paginacao-text">Página:</span></span> 
							<?php
								echo paginacao($pagina, $limit, $total_registros);
							?>                            
                        </div>
                    </div>
                </div>
              
              
			</div>
        </div>
		<div id="aside-right-bar2" style="margin-top:0px;">
			<?php carregaBanners(2); ?>
		</div>
	</div>
    
    <div id="footer-container">
    <?php
		include("inc_footer.php");
	?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>