<?php
	include("include/inc_conexao.php");	
	include("include/inc_frete.php");	

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$titulo;
	$nome;
	$endereco;
	$numero;
	$complemento;
	$bairro;
	$cidade;
	$estado;
	$cep;
	$forma_pagamento;
	$condicao_pagamento;
	$parcelas;
	$frete_tipo;
	$frete_prazo;
	

	/*-------------------------------------------------------------
	verifica se ta logado
	-------------------------------------------------------------*/
	if(isset($_SESSION["cadastro"])){
		$cadastro = $_SESSION["cadastro"];
		if(!is_numeric($cadastro)){
			header("location: login.php?redir=minha-conta.php");
			exit();
		}
	}
	else
	{
		header("location: login.php?redir=confirmacao-compra.php");
		exit();
	}


	/*------------------------------------------------------------------------
	verifica se tem algum orcamento em aberto com base em cookies
	--------------------------------------------------------------------------*/
	if(isset($_COOKIE["orcamento"])){
		$orcamento = $_COOKIE["orcamento"];
		if(!is_numeric($orcamento)){
			$orcamento = 0;	
		}		
	}
	if($orcamento==0){
		header("location: index.php");
		exit();
	}
	
	


	if( !isset($_REQUEST["action"]) || $_REQUEST["action"] == ""){
		header("location: confirmacao-compra.php?action=confirma");
		die();
	}



	/*------------------------------------------------------------------------
	confirma o pedido
	--------------------------------------------------------------------------*/
	if($_REQUEST){
	
		if($_REQUEST["action"]=="confirma" || $_REQUEST["action"]=="ws-confirma"){
			
			$ip = $_SERVER['REMOTE_ADDR'];
			$id = 0;
			
			//verifica se já nao existe o pedido
			$ssql = "select pedidoid, pcodorcamento from tblpedido where pcodorcamento = '{$orcamento}'";
			$result = mysql_query($ssql);
			if($result){
				while($row=mysql_fetch_assoc($result)){
					$id = $row["pedidoid"];	
				}
				mysql_free_result($result);
			}
			
				if($id==0){
					/*---------------------------------------------------------------------------
					// grava os dados do orcamento para pedido e finaliza o orcamento e pedido
					---------------------------------------------------------------------------*/
					$ssql = "insert into tblpedido (pcodorcamento, pcodigo, pcodcadastro, ptitulo, pnome, pendereco, pnumero, pcomplemento, pbairro, pcidade, pestado, pcep, psubtotal, 
													pvalor_desconto, pvalor_frete, pvalor_presente, pvalor_presente_cartao, pvalor_desconto_cupom, pvalor_desconto_troca, pvalor_total, 
													pcodforma_pagamento, pcodcondicao_pagamento, pcodfrete, pcodcupom_desconto, pcodendereco_fatura, pcodstatus, pfinalizado, preferencia, 
													pcodlista_presente, ptexto_presente, pcartao_titular, pcartao_numero, pcartao_validade, pcartao_codseguranca, pcartao_retorno, pip)
											select orcamentoid, ocodigo, ocodcadastro, otitulo, onome, oendereco, onumero, ocomplemento, obairro, ocidade, oestado, ocep, osubtotal, 
													ovalor_desconto, ovalor_frete, ovalor_presente, ovalor_presente_cartao, ovalor_desconto_cupom, ovalor_desconto_troca, ovalor_total, 
													ocodforma_pagamento, ocodcondicao_pagamento, ocodfrete, ocodcupom_desconto, ocodendereco_fatura,  '1' as ocodstatus, '-1' as ofinalizado, oreferencia, 
													ocodlista_presente, otexto_presente, ocartao_titular, ocartao_numero, ocartao_validade, ocartao_codseguranca, ocartao_retorno, oip from tblorcamento 
													where orcamentoid='{$orcamento}'";
													
					$result = mysql_query($ssql);
					$id = mysql_insert_id();
					
					
					/*--------------------------------------------------------------------------
					// grava os itens 
					---------------------------------------------------------------------------*/
					$ssql = "insert into tblpedido_item (pcodpedido, pcodigo, pcodproduto, pcodpropriedade, pcodtamanho, pquantidade, pvalor_unitario, ppeso, pdesconto, 
														 ppresente, pbaixa_estoque)
							select $id , $id, ocodproduto,  ocodpropriedade, ocodtamanho, oquantidade, ovalor_unitario, opeso, odesconto, 
														 opresente, obaixa_estoque	from tblorcamento_item where ocodorcamento = '{$orcamento}'";
					mysql_query($ssql);
					
				}
									
				//atualiza o orcamento
				$ssql = "update tblorcamento set ofinalizado = -1, ocodstatus=1, odata_alteracao='{$data_hoje}' where orcamentoid='{$orcamento}'";			
				mysql_query($ssql);
				
				
				//atualiza o status do cupom de desconto  | 1 = Utilizado se cupom unico por email
				$email = get_cadastro($cadastro,"cemail");
				$ssql = "update tblcupom_desconto set cstatus = 1 where cemail = '{$email}' and cupomid in (select ocodcupom_desconto from tblorcamento where orcamentoid='{$orcamento}')";
				mysql_query($ssql);


				//atualiza o status do cupom de desconto  | 1 = Utilizado se cupom gerado randonicamente
				$email_site = $site_email;
				$ssql = "update tblcupom_desconto set cstatus = 1 where cemail = '{$email_site}' and ctipo = 2 and cupomid in (select ocodcupom_desconto from tblorcamento where orcamentoid='{$orcamento}')";
				mysql_query($ssql);
				
				
				
				
				//atualiza o pedido
				$ssql = "update tblpedido set pip = '{$ip}', pcodigo='{$id}', pdata_alteracao='{$data_hoje}', pdata_cadastro='{$data_hoje}' where pedidoid='{$id}'";			
				mysql_query($ssql);


				//verifica se o pedido foi de lista de presente
				$lista_presente = intval(get_pedido($id,"pcodlista_presente"));

				if( $lista_presente > 0 ){
					$lista_credito		= intval(get_lista_presente($lista_presente,"lcredito"));
				}
		
		
		
				//atualiza o estoque se o pedido não é de lista de presente ou se não é opção de gerar crédito
				if( intval($lista_presente) == 0 || intval($lista_credito) == 0 ){		


					//atualiza o estoque
					$ssql = "select tblpedido_item.itemid, tblpedido_item.pcodproduto, tblpedido_item.pcodtamanho, tblpedido_item.pcodpropriedade, tblpedido_item.pquantidade, 
							tblpedido_item.pbaixa_estoque, tblproduto.pcontrola_estoque
							from tblpedido_item
							inner join tblproduto ON tblpedido_item.pcodproduto = tblproduto.produtoid
							where tblpedido_item.pcodpedido = '{$id}' and tblpedido_item.pquantidade >0 and tblpedido_item.pbaixa_estoque=0 ";			
					$result = mysql_query($ssql);
					
					if($result){
						while($row=mysql_fetch_assoc($result)){
							$baixa_estoque = 1;					// nao controla estoque
							if($row["pcontrola_estoque"]==-1){
								$baixa_estoque = -1;			// controla estoque
							
								$produtoid = $row["pcodproduto"];
								$propriedade = $row["pcodpropriedade"];
								$tamanho = $row["pcodtamanho"];
								$tipo = 2;
								$quantidade = $row["pquantidade"];
								$note = "Pedido #".$id;
								$usuario = "0";
								
								//lanca o historico na tblajuste_estoque
								$ssql = "insert into tblestoque_ajuste (acodproduto, acodpropriedade, acodtamanho, atipo, aquantidade, anote, acodpedido, acodusuario, adata_cadastro) 
											values('{$produtoid}','{$propriedade}','{$tamanho}', '{$tipo}', '{$quantidade}','{$note}','{$id}','{$usuario}','{$data_hoje}') ";	
								mysql_query($ssql);						
								
								
								//atualiza o estoque
								$ssql = "update tblestoque set eestoque=eestoque-'{$quantidade}', ecodusuario='{$usuario}', edata_alteracao='{$data_hoje}' 
												where ecodproduto='{$produtoid}' and ecodtamanho='{$tamanho}' and ecodpropriedade='{$propriedade}' ";
								mysql_query($ssql);									
								//echo $ssql;
								
								//atualiza linha do pedido_item
								$ssql = "update tblpedido_item set pbaixa_estoque='{$baixa_estoque}' where itemid='".$row["itemid"]."'";
								mysql_query($ssql);
								
							}
						}
						mysql_free_result($result);
					}

				}

				
				//echo $ssql;
				
				setcookie("pedido",$id,$expires);
				
				$expires = time()-3600;
				setcookie("orcamento","0",$expires);
				header("location: finaliza-compra.php");
			
		}
	
	}


?>
