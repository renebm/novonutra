<?php
include("../include/inc_conexao.php");

header("Content-type: text/xml");

	echo '<?xml version="1.0" encoding="UTF-8"?>';
	echo '<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">';
	echo '<channel>';
	echo "<title>Style Market</title>";
	echo "<link>http://www.stylemarket.com.br</link>";
	echo "<description>Melhor Seleção de Roupas Acessórios em até 6x s/ juros e Frete Grátis‎</description>";

	$ssql = "select p.produtoid, p.pproduto, p.pcodigo, p.pdescricao, p.plink_seo, p.pvalor_unitario,
			pm.marquivo,
			m.mmarca
			from tblproduto as p
			inner join tblproduto_midia as pm on p.produtoid = pm.mcodproduto and pm.mcodtipo = 1 and pm.mprincipal = -1
			inner join tblmarca as m on p.pcodmarca = m.marcaid
			where p.pdisponivel = -1 and p.pdata_inicio <= '{$data_hoje}' and p.pdata_termino >= '{$data_hoje}' ";
	$result = mysql_query($ssql);
	if($result){
		while( $row = mysql_fetch_assoc($result) ){
			
			$id		 	= $row["produtoid"];
			$codigo		= $row["pcodigo"];
			$produto 	= $row["pproduto"];
			$link		= "http://www.stylemarket.com.br/" . $row["plink_seo"];
			$imagem		= "http://www.stylemarket.com.br/" . $row["marquivo"];
			$descricao	= $row["pdescricao"];
			$valor		= number_format($row["pvalor_unitario"],2,".","");
			$marca		= $row["mmarca"];
			
			
			if(trim($descricao)==""){
				$descricao = $produto;
			}
			
			
			echo "<item>";
			echo "<title><![CDATA[".$produto."]]></title>";
			echo "<link>".$link."</link>";
			echo "<description><![CDATA[".$descricao."]]></description>"; 
			echo "<g:id><![CDATA[".$id."]]></g:id>";
			echo "<g:image_link>".$imagem."</g:image_link>";
			echo "<g:price>".$valor."</g:price>";
			echo "<g:condition>new</g:condition>";
			echo "<g:availability>in stock</g:availability>";
			echo "<g:brand><![CDATA[".$marca."]]></g:brand>";
			echo "<g:google_product_category><![CDATA[Vestuário e acessórios]]></g:google_product_category>";
			echo "<g:product_type><![CDATA[Vestuário e acessórios]]></g:product_type>";
			echo "<g:mpn><![CDATA[".$codigo."]]>></g:mpn>";
			echo "</item>";
			
			
		}
		mysql_free_result($result);
	}	
	
	echo '</channel>';
	echo '</rss>';
	
	
?>