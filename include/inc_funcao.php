<?php

header('Content-Type: text/html; charset=utf-8'); 

/*-----------------------------------------------------

------------------------------------------------------*/


	function gera_log($log){

		if(!file_exists("log")){
			mkdir("log");	
		}

		$arquivo = "log/".date("Ymd").".log";
		
		$fp = fopen($arquivo, "a"); // a=coloca o ponteiro no final do arquivo 
		$escreve = fwrite($fp, $log);
		fclose($fp);

	}


	function formata_valor_tela($valor){
		/*
		$valor = get_only_numbers($valor);
		$real = left($valor,strlen($valor)-2);
		$centavo = right($valor,2);
		if(strlen($real)>3){
			//$real = left($real,strlen($real)-3) . "." . right($real,3);
			if(strlen($real)<=6){
				$real = left($real,strlen($real)-3) . "." . right($real,3);
			}
			else
			{
				$milhao = left($real,strlen($real)-6);
				$real = right($real,6);
				$real = $milhao . "." . left($real,strlen($real)-3) . "." . right($real,3);	
			}
		}*/
		return number_format($valor,2,",",".");
	}
	
	
	function formata_cep_tela($texto){
		$texto = get_only_numbers(right("00000000" . $texto,8));
		return left($texto,5)."-".right($texto,3);
	}	


	function formata_cpf_cnpj_tela($texto){
		$texto = get_only_numbers($texto);
		
		if(strlen($texto)==11){
			return substr($texto,0,3).".".substr($texto,3,3).".".substr($texto,6,3)."-".substr($texto,9,2);
		}

		if(strlen($texto)==14){
			return substr($texto,0,2).".".substr($texto,2,3).".".substr($texto,5,3)."/".substr($texto,8,4)."-".substr($texto,12,2);
		}

	}	
	
	
	
	function formata_cartao_mascara($texto){
		$texto = get_only_numbers($texto);
		
		return "*********".substr($texto,12,6);
	}		


	
	function formata_telefone_tela($texto){
		$texto = get_only_numbers($texto);
		
		
		if(strlen($texto)==10){
			return "( " . substr($texto,0,2) . " ) ". substr($texto,2,4) . "-" . substr($texto,6,4);
		}

		if(strlen($texto)==11){	//novo formato a partir de 29/07/2012
			return "( " . substr($texto,0,2) . " ) ". substr($texto,2,5) . "-" . substr($texto,7,4);
		}

	}		


	function formata_data_tela($texto){

		if(strlen($texto)>=12){
			$data = substr($texto,0,10);
			$hora = substr($texto,10,9);
		}else{
			$data = $texto;
			}
		
		list($ano,$mes,$dia) = explode("-",$data);
		return $dia . "/" . $mes . "/" . $ano . $hora;
	}		


	function formata_data_db($texto){
		$data = $texto;
		
		if(strlen($texto)>=12){
			$data = substr($texto,0,10);
			$hora = substr($texto,10,9);
		}
		
		list($dia,$mes,$ano) = explode("/",$data);
		return $ano . "-" . $mes . "-" . $dia .@$hora;
	}		

	
	
	
	function formata_valor_db($valor){	
		
		//if(gettype($valor)!=double){		
			$val = "";		
			list($real,$decimal) = explode(",",$valor);
						
			$real = str_replace(".","",$real);		
			
			if($decimal=="" || !is_numeric($decimal)){
				$decimal = "00";	
			}
			$val .=$real.".".$decimal;
		//}
		//else
		//{
		//	$val = $valor;	
		//}
		
		return $val;
	}	
	
	
	function iif($condicao,$strue,$sfalse){
		if ($condicao){
			return $strue;
		}else{
			return $sfalse;
		}
	}
	
	
	function right($value, $count){
		return substr($value, ($count*-1));
	}
	
	function left($string, $count){
		return substr($string, 0, $count);
	}
	
	
	function get_only_numbers($num){
		$num = preg_replace("/[^0-9]/","", $num); 		
		return $num;
	}
	
	function get_categoria($id, $campo){
		$ssql_query = "select $campo from tblcategoria where categoriaid = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query[$campo];
			}
			mysql_free_result($result_query);
		}
	}
	
	function get_categoria_artigo($id, $campo){
		$ssql_query = "select $campo from tblcategoria_artigo where categoriaid = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query[$campo];
			}
			mysql_free_result($result_query);
		}
	}
	
	function get_produto($id, $campo){
		$ssql_query = "select $campo from tblproduto where produtoid = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query[$campo];
			}
			mysql_free_result($result_query);
		}
	}	
	
	
	function get_produto_propriedade($id, $campo){
		$ssql_query = "select $campo from tblproduto_propriedade where propriedadeid = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query[$campo];
			}
			mysql_free_result($result_query);
		}
	}		


	function get_produto_condicao_pagamento($id){
		$ssql_query = "select pcodcondicao from tblproduto_condicao_pagamento where pcodproduto = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query["pcodcondicao"];
			}
			mysql_free_result($result_query);
		}
	}		



	function get_cadastro($id, $campo){
		$ssql_query = "select $campo from tblcadastro where cadastroid = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query[$campo];
			}
			mysql_free_result($result_query);
		}
	}		


	function get_frete_tipo($id, $campo){
		$ssql_query = "select $campo from tblfrete_tipo where freteid = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query[$campo];
			}
			mysql_free_result($result_query);
		}
	}		



	
	function get_usuario($id, $campo){
		$ssql_query = "select $campo from tblusuario where usuarioid = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query[$campo];
			}
			mysql_free_result($result_query);
		}
	}		
	

	function get_orcamento($id, $campo){
		$ssql_query = "select $campo from tblorcamento where orcamentoid = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query[$campo];
			}
			mysql_free_result($result_query);
		}
	}
	
	
	function get_pedido($id, $campo){
		$ssql_query = "select $campo from tblpedido where pedidoid = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query[$campo];
			}
			mysql_free_result($result_query);
		}
	}	
	
	
	function get_categoria_by_node($node, $parent = '0'){
		$ssql_query = "select ccategoria from tblcategoria where clink_seo = '{$node}'
						and ccodcategoria='{$parent}'";	
		
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query["ccategoria"];
			}
			mysql_free_result($result_query);
		}
	}	
	function get_categoria_by_node_noparent($node){
		$ssql_query = "select lower(ccategoria) as ccategoria from tblcategoria where clink_seo = '{$node}'";	
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query["ccategoria"];
			}
			mysql_free_result($result_query);
		}
	}	

	function get_categoriaid_by_node($node, $parent = '0'){
		$ssql_query = "select categoriaid, ccategoria from tblcategoria where clink_seo = '{$node}'
			and ccodcategoria='{$parent}' ";	
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query["categoriaid"];
			}
			mysql_free_result($result_query);
		}
	}
	
	function get_categoriaid_artigo_by_node($node, $parent = '0'){
		$ssql_query = "select categoriaid, ccategoria from tblcategoria_artigo where clink_seo = '{$node}'
			and ccodcategoria='{$parent}' ";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query["categoriaid"];
			}
			mysql_free_result($result_query);
		}
	}
	
	function get_categoriaid_by_node_nochild($node){
		$ssql_query = "select categoriaid, ccategoria from tblcategoria where clink_seo = '{$node}'";	
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query["categoriaid"];
			}
			mysql_free_result($result_query);
		}
	}	


	function get_marcaid_by_node($node, $parent = '0'){
		$ssql_query = "select marcaid, mmarca from tblmarca where mlink_seo = '{$node}'";

		
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query["marcaid"];
			}
			mysql_free_result($result_query);
		}
	}	
	
	
	function get_marca($id, $campo){
		$ssql_query = "select $campo from tblmarca where marcaid = '{$id}'";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query[$campo];
			}
			mysql_free_result($result_query);
		}
	}
	function get_marca_nome($sio, $campo){
		$ssql_query = "select $campo from tblmarca where mlink_seo = '{$sio}'";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query[$campo];
			}
			mysql_free_result($result_query);
		}
	}	




	function get_condicao_pagamento($id, $campo){
		$ssql_query = "select $campo from tblorcamento where orcamentoid = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query[$campo];
			}
			mysql_free_result($result_query);
		}
	}



function get_carrinho_subtotal($orcamento){
	$sub_total = 0;
	$ssql = "select oquantidade, ovalor_unitario from tblorcamento_item where ocodorcamento='{$orcamento}' and oquantidade>0";
	$result = mysql_query($ssql);		
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$sub_total = $sub_total + ($row["oquantidade"]*$row["ovalor_unitario"]);
		}
		mysql_free_result($result);
	}
	$sub_total = number_format($sub_total, 2, ".", "");
	return $sub_total;
}


function get_carrinho_peso($orcamento){
		$peso = 0;
		$ssql = "select oquantidade, opeso from tblorcamento_item where ocodorcamento='{$orcamento}' and oquantidade>0";
		$result = mysql_query($ssql);		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$peso = $peso + ($row["oquantidade"]*$row["opeso"]);
			}
			mysql_free_result($result);
		}
		$peso = number_format($peso, 2, ".", "");
		return $peso;
}


function get_valor_parcelado($produto, $valor){
	/*------------------------------------------------------------------------
	carrega o valor de parcela minima
	--------------------------------------------------------------------------*/	
	$valor_minimo_parcela = get_configuracao("valor_minimo_parcela");
	
	if(!is_numeric($valor_minimo_parcela)){
		$valor_minimo_parcela = 50;	
	}
	
	$valor_minimo_parcela	=	number_format($valor_minimo_parcela,2,".","");
	
	/*------------------------------------------------------------------------
	calcula o parcelamento arredondando para baixo
	--------------------------------------------------------------------------*/		
	$parcelado = floor($valor/$valor_minimo_parcela);
	
	if($parcelado < 1){
	//	$parcelado = 1;	
	}		

	
	//verifica se tem regra de condicao de pagamento para o produto
	
		$ssql_regra = "";
		$ssql_query = "select tblproduto_condicao_pagamento.pcodcondicao, tblcondicao_pagamento.cnumero_parcelas 
				from tblproduto_condicao_pagamento 
				left join tblcondicao_pagamento on tblproduto_condicao_pagamento.pcodcondicao = tblcondicao_pagamento.condicaoid
				where tblproduto_condicao_pagamento.pcodproduto  = '{$produto}'
				order by tblcondicao_pagamento.cnumero_parcelas desc
				limit 0,1";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				if($row_query["pcodcondicao"]>0){
					$ssql_regra .= " and tblcondicao_pagamento.condicaoid='".$row_query["pcodcondicao"]."' ";
				}
			}
			mysql_free_result($result_query);
		}
		
		//------------------------------------------------------------------------------------------------------------------------
		
		$ssql_query = "SELECT tblcondicao_pagamento.condicaoid, tblcondicao_pagamento.ccondicao, tblcondicao_pagamento.cnumero_parcelas
				  FROM tblcondicao_pagamento
				  where tblcondicao_pagamento.cativa=-1 
				  and tblcondicao_pagamento.cnumero_parcelas <= '{$parcelado}' 
				  $ssql_regra
				  order by tblcondicao_pagamento.cnumero_parcelas desc limit 0,1
				  ";
		//echo $ssql_query;
		//die();
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				if($row_query["cnumero_parcelas"]>1){
					$parcelado = number_format($valor/$row_query["cnumero_parcelas"],2,",",".");
					return "ou " .$row_query["cnumero_parcelas"]." x de R$ $parcelado";	
				}	
			}
			mysql_free_result($result_query);
		}	
	
	
}

function get_valor_parcelado_new($produto, $valor){
	$valor_minimo_parcela = get_configuracao("valor_minimo_parcela");
	if(!is_numeric($valor_minimo_parcela)){
		$valor_minimo_parcela = 50;	
	}
	$valor_minimo_parcela	=	number_format($valor_minimo_parcela,2,".","");
	$parcelado = floor($valor/$valor_minimo_parcela);
	$parcelaMaxima = mysql_fetch_array(mysql_query("select max(cnumero_parcelas) as max from tblcondicao_pagamento inner join tblforma_pagamento on formapagamentoid = ccodforma_pagamento and fativa = -1 where cativa = -1"), MYSQL_ASSOC);
	if($parcelado > $parcelaMaxima["max"]){ $parcelado = $parcelaMaxima["max"]; }
	if((int)$parcelado < 1){
		$parcelado = 1;	
	}
	if($parcelado > 1){
		return "em at&eacute; ".$parcelado."x de R$ ".number_format($valor/$parcelado,2,",",".");
	}
}

function get_carrinho_parcelado($orcamento){

	/*------------------------------------------------------------------------
	carrega o valor do produto
	--------------------------------------------------------------------------*/	
	$subtotal = get_orcamento($orcamento,"osubtotal");

	/*------------------------------------------------------------------------
	carrega o valor de parcela minima
	--------------------------------------------------------------------------*/	
	$valor_minimo_parcela = get_configuracao("valor_minimo_parcela");
	
	if(!is_numeric($valor_minimo_parcela)){
		$valor_minimo_parcela = 50;	
	}
	
	$valor_minimo_parcela	=	number_format($valor_minimo_parcela,2,".","");
			
		
	/*------------------------------------------------------------------------
	calcula o parcelamento arredondando para baixo
	--------------------------------------------------------------------------*/		
	$parcelado = floor($subtotal/$valor_minimo_parcela);
	
	if($parcelado<1){
		$parcelado = 1;	
	}	



	/*------------------------------------------------------------
	selecione os itens do carrinho
	------------------------------------------------------------*/
	//verifica se tem regra de condicao de pagamento para os produtos do carrinho
		$ssql_regra = "";
		$ssql = "	
				select tblorcamento_item.ocodproduto, tblproduto_condicao_pagamento.pcodcondicao, tblcondicao_pagamento.cnumero_parcelas 
				from tblorcamento_item 
				left join tblproduto_condicao_pagamento on tblorcamento_item.ocodproduto = tblproduto_condicao_pagamento.pcodproduto
				left join tblcondicao_pagamento on tblproduto_condicao_pagamento.pcodcondicao=tblcondicao_pagamento.condicaoid
				where tblorcamento_item.oquantidade > 0 and tblorcamento_item.ocodorcamento = '{$orcamento}'
				order by tblcondicao_pagamento.cnumero_parcelas desc
				limit 0,1
			";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				if($row["pcodcondicao"]>0){
					$ssql_regra .= " and tblcondicao_pagamento.condicaoid='".$row["pcodcondicao"]."' ";
				}
			}
			mysql_free_result($result);
		}
		//echo $ssql_regra;
		//exit();
		
		//------------------------------------------------------------------------------------------------------------------------
		
		$ssql = "SELECT tblcondicao_pagamento.condicaoid, tblcondicao_pagamento.ccondicao, tblcondicao_pagamento.cnumero_parcelas
				  FROM tblcondicao_pagamento
				  where tblcondicao_pagamento.cativa=-1 
				  and tblcondicao_pagamento.cnumero_parcelas <= '{$parcelado}' 
				  $ssql_regra
				  order by tblcondicao_pagamento.cnumero_parcelas desc limit 0,1
				  ";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				if($row["cnumero_parcelas"]>1){
					$parcelado = number_format($subtotal/$row["cnumero_parcelas"],2,",",".");
					return "Sem juros em at&eacute; <strong>" .$row["cnumero_parcelas"]." x de R$ $parcelado</strong>.";	
				}	
			}
			mysql_free_result($result);
		}
		//echo $ssql;
}



	function get_conteudo($id){
		$ssql_query = "select cpagina, ctitulo, cdescricao, cpalavra_chave, ctexto from tblconteudo where conteudoid = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				$GLOBALS["conteudo_titulo"] = $row_query["ctitulo"];
				$GLOBALS["conteudo_descricao"] = $row_query["cdescricao"];
				$GLOBALS["conteudo_texto"] = $row_query["ctexto"];
				$GLOBALS["conteudo_palavra_chave"] = $row_query["cpalavra_chave"];
			}
			mysql_free_result($result_query);
		}
	}	
	
	
	function get_loja($id, $campo){
		$ssql_query = "select $campo from tblloja where lojaid = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query[$campo];
			}
			mysql_free_result($result_query);
		}
	}		
	
	
	/*----------------------------------------------------------------
	//verifica se o produto est� em regra de desconto utm_source
	//somente it� verificar se existir $_SESSION["utm_source"]
	-----------------------------------------------------------------*/
	function get_desconto_utm_source($produtoid){
		
		$utm_source 	= $_SESSION["utm_source"];
		$desconto_utm 	= 0; 
		
		$desconto		= 0;
		
		//$ssql_query = "select dtipo, ddesconto, dutm_source, dcodproduto, dcodmarca, dcodcategoria from tbldesconto where dutm_source='{$utm_source}'";
		$ssql_query = "select dtipo, ddesconto, dutm_source, dcodproduto, dcodmarca, dcodcategoria 
						from  tbldesconto 
						where dutm_source = '{$utm_source}'
						and IF(dcodproduto>0,dcodproduto='{$produtoid}',dcodproduto=0)
						and IF(dcodcategoria > 0,dcodcategoria in(select pcodcategoria from tblproduto_categoria where pcodcategoria=dcodcategoria and pcodproduto='{$produtoid}') ,dcodcategoria=0)
						";
		//echo $ssql_query;
		//die();
		$result_query = mysql_query($ssql_query);
		if($result_query){
			
			if( mysql_num_rows($result_query) > 0 ){
				$valor	= get_produto($produtoid,"pvalor_unitario");	
			}
			//echo $valor;
			while($row_query=mysql_fetch_assoc($result_query)){
				
				$tipo	=	$row_query["dtipo"];
				
				if($tipo==1){
					$desconto_utm	= ( $valor * $row_query["ddesconto"] ) / 100;
				}
				else
				{
					$desconto_utm	= $row_query["ddesconto"];
				}
			}
			mysql_free_result($result_query);
		}
				
		return $desconto_utm;
		
	}
	
	
	
	function get_configuracao($parametro){
		$ret;
		$ssql_query = "select cvalor from tblconfiguracao where cparametro = '{$parametro}'";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				$ret = $row_query["cvalor"];
			}
			mysql_free_result($result_query);
		}
		return @$ret;
	}		
	
	
	
	function get_estoque($id, $tamanho, $propriedade){
		$ret = 0;
		$ssql_query = "select eestoque from tblestoque where ecodproduto = '{$id}' and ecodtamanho='{$tamanho}' and ecodpropriedade='{$propriedade}'";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				$ret = $row_query["eestoque"];
			}
			mysql_free_result($result_query);
		}
		return $ret;
	}		
	
	function get_estoque_por_orcamento($id){
		$ret = 0;
		$ssql_query = "select eestoque from tblorcamento_item right join tblestoque on ocodproduto = ecodproduto where itemid = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				$ret = $row_query["eestoque"];
			}
			mysql_free_result($result_query);
		}
		return $ret;
	}		
	
	function combo_itens_pagina($itens){
		$ret = "";
		if($itens==0){
			$itens=16;
		}
		
		for($i=16;$i<=96;$i++){
			if($i % 16 == 0){
				$ret .= '<option value="'.$i.'"';
				
				if($i==$itens){
					$ret .= " selected";	
				}
				
				$ret .= '>';
				$ret .= $i;
				$ret .= '</option>';
			}	
		}
				
		return $ret;
	}
	
	
	
	function combo_ordem_pagina($ordem){

		$ret = "";
		$ret .= "<option value=''>Selecione</option>";
		
		$ret .= "<option value='1'";
		if($ordem==1) $ret .= " selected";
		$ret .= ">Menor Pre�o</option>";
		
		$ret .= "<option value='2'";
		if($ordem==2) $ret .= " selected";
		$ret .= ">Maior Pre�o</option>";
		
		/*$ret .= "<option value='3'";
		if($ordem==3) $ret .= " selected";
		$ret .= ">Mais Clicados</option>";
		

		$ret .= "<option value='4'";
		if($ordem==4) $ret .= " selected";
		$ret .= ">Mais bem Avaliados</option>";
		*/
		
		$ret .= "<option value='5'";
		if($ordem==5) $ret .= " selected";
		$ret .= ">A - Z</option>";
		
		$ret .= "<option value='6'";
		if($ordem==6) $ret .= " selected";
		$ret .= ">Z - A</option>";
		
		$ret .= "<option value='7'";
		if($ordem==7) $ret .= " selected";
		$ret .= ">Data de Lan�amento</option>";
		
		$ret .= "<option value='8'";
		if($ordem==8) $ret .= " selected";
		$ret .= ">Melhor desconto</option>";
		
		return utf8_encode($ret);
	}


	
	
	function paginacao($pag, $itens, $total){
		$count = 0;
		$start = 1;
		
		$paginas = number_format($total/$itens);
		
		if($paginas < $total/$itens){
			$paginas = number_format($total/$itens)+1;
		}

		if($paginas==0){
			$paginas = 1;	
		}
	
		if( ($pag - 3) > 0  ){
			$start = $pag - 3;	
		}
		
		if( ($paginas - $start) < 6 && ($paginas - 6) > 0 ){
			$start = $paginas - 6;	
		}
		
		if($pag == 1){ $style1 = 'color:gray;'; }
		if($pag == $paginas){ $style2 = 'color:gray;'; }
		
		$ret = '<span style="cursor:pointer;border:0;" class="num-paginacao" onclick="pag_anterior(1);" >primeira</span>';
		$ret .= '<span style="cursor:pointer;border:0;'.$style1.'margin-right:10px;" class="num-paginacao" onclick="pag_anterior('.($pag).');" >anterior</span>';
		$ultimo = $start;
		for($i=$start;$i<=$paginas;$i++){
			$count ++;
			if($i==$paginas && (($i-1) != ($ultimo)) && ($paginas > 6)){ $ret .= "<span style='float:left;padding-top: 5px;padding-left: 3px;'>...</span>" ; }
			if($count == 1 || $count<=6 || ($i==$paginas && $ultimo == ($i-1))){	
				$ret .= '<span id="'.$i.'" class="num-paginacao';
				if($i==$pag){
					$ret .= ' num-paginacao-selected';	
				}
				$ret .= '" onmouseover=this.style.cursor="pointer"; onclick=javascript:paginacao("pagina",'.$i.');>';
				$ret .= $i;
				$ret .= '</span>';
				$ultimo = $i;
			}
		}
		$ret .= '<span style="cursor:pointer;border:0;margin-left:10px;'.$style2.'" class="num-paginacao" onclick="pag_posterior('.$pag.','.$paginas.');" >pr&oacute;xima</span>';
		$ret .= '<span style="cursor:pointer;border:0;" class="num-paginacao" onclick="pag_anterior('.($paginas+1).');" >&uacute;ltima</span>';
		return $ret;
	}


	
	
	
	
	function monta_combo_estado($sigla){
		$ret = "";
		$ssql_query = "select estadoid, esigla, eestado from tblestado order by eestado";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query = mysql_fetch_assoc($result_query)){
				$ret .= '<option value="'.$row_query["esigla"].'"';
				if($sigla==$row_query["esigla"]){
					$ret .= ' selected';
				}
				$ret .= '>';
				$ret .= $row_query["eestado"];
				$ret .= '</option>';
			}
			mysql_free_result($result_query);
		}
		return $ret;
	}
	
	
	
	function monta_combo_marca($marca){
		$ret = "";
		$ssql_query = "select marcaid, mmarca from tblmarca order by mmarca";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query = mysql_fetch_assoc($result_query)){
				$ret .= '<option value="'.$row_query["marcaid"].'"';
				if($marca==$row_query["marcaid"]){
					$ret .= ' selected';
				}
				$ret .= '>';
				$ret .= $row_query["mmarca"];
				$ret .= '</option>';
			}
			mysql_free_result($result_query);
		}
		return $ret;
	}	
	
	
	
	function monta_combo_pedido_status($status){
		$ret = "";
		$ssql_query = "select statusid, sstatus, sdescricao from tblpedido_status order by statusid";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query = mysql_fetch_assoc($result_query)){
				$ret .= '<option value="'.$row_query["statusid"].'"';
				if($status==$row_query["statusid"]){
					$ret .= ' selected';
				}
				$ret .= '>';
				$ret .= $row_query["sstatus"];
				$ret .= '</option>';
			}
			mysql_free_result($result_query);
		}
		return $ret;
	}	



	function get_lista_presente_qtde_comprada($id, $produto, $propriedade, $tamanho){
		$ret = 0;
		
		$ssql_query = "select sum(pi.pquantidade) as total 
					from tblpedido_item as pi
					inner join tblpedido as pe on pi.pcodpedido = pe.pedidoid and pe.pcodlista_presente = '{$id}'
					where pi.pcodproduto='{$produto}' and pi.pcodpropriedade='{$propriedade}' and pi.pcodtamanho='{$tamanho}' and pi.pquantidade > 0 
					and pe.pcodstatus < 6
					";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				$ret = $row_query["total"];
			}
			mysql_free_result($result_query);
		}
		
		
		return $ret;
	}
	
	
	function get_lista_presente($id,$campo){
		$ssql_query = "select $campo from tbllista_presente where listaid = $id";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			while($row_query=mysql_fetch_assoc($result_query)){
				return $row_query[$campo];
			}
			mysql_free_result($result_query);
		}
	}		
	
	
	function get_carrinho_item_presente($id){
		$num_rows = 0;
		$ssql_query = "select ocodproduto from tblorcamento_item where ocodorcamento = '{$id}' and oquantidade > 0 ";
		$result_query = mysql_query($ssql_query);
		if($result_query){
			$num_rows = mysql_num_rows($result_query);
		}
		return $num_rows;
	}
	


function datediff($ret, $data_1, $data_2){

	list($date_1, $time_1) = explode(" ",$data_1);	
	list($dia_1,$mes_1,$ano_1) = explode("-",$date_1);
	list($hora_1,$minuto_1,$segundo_1) = explode(":",$time_1);

	if(strlen($dia_1) == 4){
		$aux = $dia_1;
		$dia_1 = $ano_1;
		$ano_1 = $aux;
	}

	$data_inicio_1 =  mktime($hora_1,$minuto_1,$segundo_1,$mes_1,$dia_1,$ano_1);

	list($date_2, $time_2) = explode(" ",$data_2);	
	list($dia_2,$mes_2,$ano_2) = explode("-",$date_2);
	list($hora_2,$minuto_2,$segundo_2) = explode(":",$time_2);

	if(strlen($dia_2) == 4){
		$aux = $dia_2;
		$dia_2 = $ano_2;
		$ano_2 = $aux;
	}


	$data_termino_2 =  mktime($hora_2,$minuto_2,$segundo_2,$mes_2,$dia_2,$ano_2);

	if($ret=="s"){
		return $data_inicio_1 - $data_termino_2;	
	}
	
	
	if($ret=="h"){
		return ($data_inicio_1 - $data_termino_2)/60;	
	}	


	if($ret=="d"){
		return (($data_inicio_1 - $data_termino_2)/60)/24;	
	}	


}



function addDayIntoDate($date,$days) {
     $thisyear = substr ( $date, 0, 4 );
     $thismonth = substr ( $date, 4, 2 );
     $thisday =  substr ( $date, 6, 2 );
     $nextdate = mktime ( 0, 0, 0, $thismonth, $thisday + $days, $thisyear );
     return strftime("%Y%m%d", $nextdate);
}

function subDayIntoDate($date,$days) {
     $thisyear = substr ( $date, 0, 4 );
     $thismonth = substr ( $date, 4, 2 );
     $thisday =  substr ( $date, 6, 2 );
     $nextdate = mktime ( 0, 0, 0, $thismonth, $thisday - $days, $thisyear );
     //return strftime("%Y%m%d", $nextdate);
	 $nextdate = strftime("%Y%m%d", $nextdate);
	 $nextdate = substr($nextdate,0,4) . "-" . substr($nextdate,4,2) . "-" . substr($nextdate,6,2);
	 return $nextdate;
}


function show_banner($tipo, $departamento = '0', $marca = '0'){
	$ssql_banner = "select bannerid, bcodtipo, btitulo, btarget, barquivo, bview, bcontador from tblbanner where bcodtipo=$tipo 
					and bativo=-1 and bdata_inicio<='".date("Y-m-d Y:i:s")."' and bdata_termino>='".date("Y-m-d Y:i:s")."' 
					and bcodcategoria = '{$departamento}' and bcodmarca = '{$marca}'
					order by bcontador 
					limit 0,2";
					
	$result_banner = mysql_query($ssql_banner);
	if($result_banner){
		
		$num_rows = mysql_num_rows($result_banner);
		if($num_rows==0){
			echo "<script>";
			if($tipo==1){
				echo "$('#box-full-banner').hide();";	
			}
			if($tipo==4){
				echo "$('#banner-regua').hide();";	
			}			
			echo "</script>";
		}
		$cont = 1;
		while($row_banner=mysql_fetch_assoc($result_banner)){
			
			echo "<a href='redir.php?origem=banner&codigo=".$row_banner["bannerid"]."' target='".$row_banner["btarget"]."'>";
			echo "<img src='".$row_banner["barquivo"]."' border='0' style='float: ";
			if($cont == 1){echo "left"; }else{ echo "right";}
			echo ";' />";
			echo "</a>";

			$id = $row_banner["bannerid"];
			$view = $row_banner["bview"];
			$contador = $row_banner["bcontador"];
			
			$view++;
			$contador++;
			$cont++;
			mysql_query("update tblbanner set bview='".$view."', bcontador='".$contador."' where bannerid=".$id);
			
		}	
		mysql_free_result($result_banner);
	}
}





function gera_url_seo($input){
	$ret = $input;
		
	if (preg_match('!\S!u', $ret)) {
		$ret = utf8_decode($ret);
	}	

	$ret = strtolower($ret);
	
	$ret = str_replace("'","-",$ret);
	$ret = str_replace("`","-",$ret);
	$ret = str_replace("#","-",$ret);
	$ret = str_replace("!","-",$ret);
	$ret = str_replace("&","-",$ret);
	$ret = str_replace("@","-",$ret);
	$ret = str_replace("$","-",$ret);
	$ret = str_replace("%","-",$ret);
	$ret = str_replace("^","-",$ret);
	$ret = str_replace("*","-",$ret);
	$ret = str_replace("(","-",$ret);
	$ret = str_replace(")","-",$ret);
	$ret = str_replace("=","-",$ret);
	$ret = str_replace("+","-",$ret);
	$ret = str_replace("`","-",$ret);
	$ret = str_replace("~","-",$ret);
	$ret = str_replace(".","-",$ret);
	$ret = str_replace(",","-",$ret);
	$ret = str_replace("/","-",$ret);
	$ret = str_replace("|","-",$ret);
	$ret = str_replace(":","",$ret);
	
	
	$ret = str_replace("�","a",$ret);
	$ret = str_replace("�","a",$ret);
	$ret = str_replace("�","a",$ret);
	$ret = str_replace("�","a",$ret);
	$ret = str_replace("�","e",$ret);
	$ret = str_replace("�","e",$ret);
	$ret = str_replace("�","i",$ret);
	$ret = str_replace("�","o",$ret);
	$ret = str_replace("�","o",$ret);
	$ret = str_replace("�","o",$ret);
	$ret = str_replace("�","u",$ret);
	$ret = str_replace("�","u",$ret);
	$ret = str_replace("�","c",$ret);
	$ret = str_replace("�","n",$ret);
	$ret = trim(ereg_replace(' +','-',preg_replace('/[^a-zA-Z0-9-:\s]/','',$ret))); 
	$ret = str_replace("--","-",$ret);
	$ret = str_replace("----","---",$ret);
	
	
	$ret = rtrim($ret,"-");
	
	return $ret;
	
}



function envia_email($from, $from_email, $to, $to_email, $subject, $body){
	
	$dados = mysql_fetch_array(mysql_query("select lsmtp,lusuario_smtp,lsenha_smtp from tblloja limit 1;"), MYSQL_ASSOC);
	
	$app_secure		=	'ssl';
	$app_smtp		=	$dados["lsmtp"];
	$app_port		=	"25";
	$app_user		=	$dados["lusuario_smtp"];
	$app_pwd		=	$dados["lsenha_smtp"];
	
	/*
	$cMail = new COM("Persits.MailSender"); 

	// dados para autenticacao no servidor SMTP 
	
	$cMail->Host = $app_smtp; 
	$cMail->Username = "demonstracao@nutracorpore.com.br"; 
	$cMail->Password = "teste123*"; 

	// caso queira enviar o email no formato HTML adicione a linha 
	$cMail->IsHTML = True; 

	// email de origem 
	//$fromemail = $from; 
	$cMail->From = $from_email; 
	$cMail->FromName = $from; 

	//email de destino 
	$cMail->AddAddress($to_email);

	 // assunto da mensagem 
	$cMail->Subject = $subject; 

	// conteudo da mensagem mensagem 
	$cMail->Body = $body;

	// enviar a mensagem com msg de sucesso ou nao 
	if(!$cMail->Send()) { 
		gera_log("Mailer Error: " . $cMail->ErrorInfo);
		return false;
	} 
	else { 
		return true;
	}
	*/
	
	require_once("email/swift_required.php");
	//$transport = Swift_SmtpTransport::newInstance($app_smtp, $app_port, $app_secure)
	$transport = Swift_SmtpTransport::newInstance($app_smtp, $app_port)
		->setUsername("demonstracao@nutracorpore.com.br")
		->setPassword("teste123*");
	$mailer = Swift_Mailer::newInstance($transport);
	$message = Swift_Message::newInstance($subject)
		->setFrom(array($app_user => $from))
		->setTo($to_email)
		->setBody('', 'text/plain')
		->addPart($body, 'text/html');

	if (!$mailer->send($message)){
		gera_log("Mailer Error: " . $mail->ErrorInfo);
		return false;
	}else{
		return true;
	}
	
}

function shortURL($longUrl){

	// Get API key from : http://code.google.com/apis/console/
	$apiKey = 'MyAPIKey';

	$postData = array('longUrl' => $longUrl, 'key' => $apiKey);
	$jsonData = json_encode($postData);

	$curlObj = curl_init();

	curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url');
	curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curlObj, CURLOPT_HEADER, 0);
	curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
	curl_setopt($curlObj, CURLOPT_POST, 1);
	curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);

	$response = curl_exec($curlObj);

	// Change the response json string to object
	$json = json_decode($response);

	curl_close($curlObj);

	return $json->id;
}
$jsArtigosCat = array();
$preload	  = '';
$bannerCont = 0;
function carregaCategorias($tipo){
	//1 = LOJA / 2 = BLOG
	if($tipo == 2){
		$departamentos = mysql_query("select categoriaid,ccategoria,clink_seo from tblcategoria_artigo where ccodcategoria = 0 and cativa = -1 and now() between cdata_inicio and cdata_termino;");
		while($departamento = mysql_fetch_array($departamentos)){
?>
	<span class="item" onmouseover="expandeArtigos(this,<?=$departamento["categoriaid"] ?>)">
		<a href="artigos/<?=$departamento["clink_seo"] ?>"><?=$departamento["ccategoria"] ?></a>
<?php
			$categorias = mysql_query("select categoriaid,ccategoria,clink_seo from tblcategoria_artigo where ccodcategoria = '".$departamento["categoriaid"]."' and cativa = -1 and now() between cdata_inicio and cdata_termino;");
			$i=0;
			while($categoria = mysql_fetch_array($categorias)){
				$GLOBALS["jsArtigosCat"][$departamento["categoriaid"]][$i] = array();
				$GLOBALS["jsArtigosCat"][$departamento["categoriaid"]][$i][0] = "artigos/".$departamento["clink_seo"]."/".$categoria["clink_seo"];
				$GLOBALS["jsArtigosCat"][$departamento["categoriaid"]][$i][1] = $categoria["ccategoria"];
				$GLOBALS["jsArtigosCat"][$departamento["categoriaid"]][$i][2] = $categoria["categoriaid"];
				$i++;
			}
?>
	</span>
<?php
		}
	}else if($tipo == 1){
		$departamentos = mysql_query("select categoriaid,ccategoria,clink_seo from tblcategoria where ccodcategoria = 0 and cativa = -1 and now() between cdata_inicio and cdata_termino and (cmenu = 0 or cmenu = 2);");
		$depCount = 0;
		while($departamento = mysql_fetch_array($departamentos)){
			$depCount++;
			$categorias = mysql_query("select categoriaid,ccategoria,clink_seo from tblcategoria where categoriaid != 29 AND ccodcategoria = '".$departamento["categoriaid"]."' and cativa = -1 and now() between cdata_inicio and cdata_termino and (cmenu = 0 or cmenu = 2) order by ccategoria;");
			if(mysql_num_rows($categorias)){
?>
		<div class="departamento">
			<a href="<?=$departamento["clink_seo"] ?>"><span class="nome"><?=$departamento["ccategoria"] ?></span></a>
<?php
				$catCount = 0;
				while($categoria = mysql_fetch_array($categorias)){
					$catCount++;
					if($catCount > 8){ $class = 'class="categoria_todas categoria_todas_'.$depCount.'"'; }else{ $class = ''; }
?>
			<a href="<?=$departamento["clink_seo"]."/".$categoria["clink_seo"] ?>" <?=$class ?>><span class="categoria"><?=$categoria["ccategoria"] ?></span></a>
<?php
				}
				if($catCount > 8){
?>
			<a href="javascript:void(0)" onclick="$('.categoria_todas_<?=$depCount?>').show(); $(this).hide(); "><span class="categoriaTodas">(<font face="Arial">+</font>) ver todas</span></a>
<?php			}		?>
		</div>
<?php
			}
		}
	}
}
function carregaJs(){
	echo "var catArtigos = new Array();	var previaArtigos = new Array();";
	$preload	= '';
	$hoje = date("Y-m-d");
	foreach($GLOBALS["jsArtigosCat"] as $k => $valor){
		$i=0;
		$ret='';
		echo "catArtigos[$k] = new Array();";
		$artigosPadrao = mysql_query("select artigoid,atitulo,alink_seo,aimagem from tblartigos where (acategoria in(select categoriaid from tblcategoria_artigo where ccodcategoria = $k) or acategoria = $k) and (data_inicio <= '{$hoje}' OR data_inicio IS NULL) order by adata_cadastro desc limit 3");
		while($artigo = mysql_fetch_array($artigosPadrao)){
			$ret.="<div class='artigo-box'><span class='img-prev'><a alt='artigo' href='artigo/".$artigo["alink_seo"]."---".$artigo["artigoid"]."'><img src='".$artigo["aimagem"]."' alt='Artigo'></a></span><span class='prev-tit'><a alt='artigo' href='artigo/".$artigo["alink_seo"]."---".$artigo["artigoid"]."'>".$artigo["atitulo"]."</a></span></div>";
		}
		echo 'previaArtigos['.$k.'] = "'.$ret.'";';
		for($i=0;$i<sizeof($GLOBALS["jsArtigosCat"][$k]);$i++){
			echo "catArtigos[$k][$i] = new Array();";
			echo "catArtigos[$k][$i][0]='".$GLOBALS["jsArtigosCat"][$k][$i][0]."';";
			echo "catArtigos[$k][$i][1]='".$GLOBALS["jsArtigosCat"][$k][$i][1]."';";
			echo "catArtigos[$k][$i][2]='".$GLOBALS["jsArtigosCat"][$k][$i][2]."';";
			$artigosPadrao = mysql_query("select artigoid,atitulo,alink_seo,aimagem from tblartigos where acategoria in(select categoriaid from tblcategoria where ccodcategoria = '".$GLOBALS["jsArtigosCat"][$k][$i][2]."') or acategoria = '".$GLOBALS["jsArtigosCat"][$k][$i][2]."' order by adata_cadastro desc limit 3");
			$ret='';
			while($artigo = mysql_fetch_array($artigosPadrao)){
				$ret.="<div class='artigo-box'><span class='img-prev'><a alt='artigo' href='artigo/".$artigo["alink_seo"]."---".$artigo["artigoid"]."'><img src='".$artigo["aimagem"]."' alt='Artigo'></a></span><span class='prev-tit'><a alt='artigo' href='artigo/".$artigo["alink_seo"]."---".$artigo["artigoid"]."'>".$artigo["atitulo"]."</a></span></div>";
			}
			echo 'previaArtigos['.$GLOBALS["jsArtigosCat"][$k][$i][2].'] = "'.$ret.'";';
		}
	}
	echo str_replace(array("\r\n","  ","\t"),"",$GLOBALS["preload"]);
}
function carregaSlideBanners(){
	$banners = mysql_query("select blink,barquivo from tblBanner where bcodtipo = 1 and now() between bdata_inicio and bdata_termino and bativo = -1 limit 10;");
	$cont = mysql_num_rows($banners);
	echo '<ol class="bolinha_wrapper">';
	for($i=0;$i<$cont;$i++){
		echo '<li name="bolinha" class="bolinhabanner">'.$i.'</li>';
	}
	echo '</ol>';
}
function carregaBanners($tipo){
	//1 = FULL / 2 = LATERAL/ 3 = R�GUA
	if($tipo == 1){
		$banners = mysql_query("select blink,barquivo from tblBanner where bcodtipo = 1 and now() between bdata_inicio and bdata_termino and bativo = -1 ORDER BY ordem ASC limit 10;");
		while($banner = mysql_fetch_array($banners)){
			if($GLOBALS["bannerCont"]){ $class = "off"; }else{ $class = "on"; }
?>
	<span class="<?=$class ?>" title="Banner" <?php if($class == "on"){ ?> style="background:url('<?=$banner["barquivo"] ?>') no-repeat center;background-size: auto 100%;" <?php }else{ ?> lazy="<?=$banner["barquivo"] ?>" <?php } ?> onclick="window.location='<?=$banner["blink"] ?>';" ><?=$GLOBALS["bannerCont"] ?></span>
<?php
		if($GLOBALS["bannerCont"]){ $preload .= "$('<img/>')[0].src = '{$banner["barquivo"]}';"; }
			$GLOBALS["bannerCont"]++;
		}
	}else if($tipo == 2){
		$banners = mysql_query("select blink,barquivo from tblBanner where bcodtipo = 2 and now() between bdata_inicio and bdata_termino and bativo = -1 order by rand() limit 3;");
		while($banner = mysql_fetch_array($banners)){
?>
			<a href="<?=$banner["blink"] ?>"><img src="<?=$banner["barquivo"] ?>" alt="Banner"></a>
<?php
		}
	}else if($tipo == 3){
		$banners = mysql_query("select blink,barquivo from tblBanner where bcodtipo = 3 and now() between bdata_inicio and bdata_termino and bativo = -1 limit 1;");
		while($banner = mysql_fetch_array($banners)){
?>
			<img src="<?=$banner["barquivo"] ?>" alt="Banner">
<?php
		}
	}
	else if($tipo == 4){
		$banners = mysql_query("select blink,barquivo from tblBanner where bcodtipo = 5 and now() between bdata_inicio and bdata_termino and bativo = -1 order by rand() limit 2;");
		while($banner = mysql_fetch_array($banners)){
?>
			<a href="<?=$banner["blink"] ?>"><img src="<?=$banner["barquivo"] ?>" alt="Banner"></a>
<?php
		}
	}
}
?>