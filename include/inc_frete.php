<?php

header('Content-type: text/html; charset=utf-8');


/*-----------------------------------------------------

40010	SEDEX sem contrato
40045	SEDEX a Cobrar, sem contrato
40126	SEDEX a Cobrar, com contrato
40215	SEDEX 10, sem contrato
40290	SEDEX Hoje, sem contrato
40096	SEDEX com contrato
40436	SEDEX com contrato
40444	SEDEX com contrato
40568	SEDEX com contrato
40606	SEDEX com contrato
41106	PAC sem contrato
41068	PAC com contrato

81019	e-SEDEX, com contrato
81027	e-SEDEX Prioritário, com conrato
81035	e-SEDEX Express, com contrato
81868	(Grupo 1) e-SEDEX, com contrato
81833	(Grupo 2) e-SEDEX, com contrato
81850	(Grupo 3) e-SEDEX, com contrato

99999	Retira na loja
99998	Transportadora à Cobrar
99997	Motoboy (Somente São Paulo)
99990	Frete Grátis
99996	Carta Comercial Registrada

"PAC||41106","SEDEX||40010"

------------------------------------------------------*/

class Frete{


	//GLOBAL VARIABLES
	var $erro=0;
	var $erro_msg;


	//seleciona o codigo do servico
	var $codservico= array();     
	var $codempresa;//get_configuracao("correios_codempresa");
	var $senha;//get_configuracao("correios_senha");
	var $cep_origem="04534011";
	var $cep_destino="";
	var $peso=0;
	var $formato = 1;
	var $comprimento=16;
	var $altura=2;
	var $largura=11;
	var $diametro=0;
	var $mao_propria=0;
	var $valor_declarado=0;
	var $aviso_recebimento=0;
	var $retorno_formato = "xml";
	var $exibe_html = 1;
	
	var $logradouro;
	var $bairro;
	var $cidade;
	var $estado;
	var $freteid="";
	
	//variaveis de retorno
	var $valor_frete;
	
	
	
	function doCalculaFrete(){

		$html_h = '<table width="100%" border="0" cellspacing="0" cellpadding="3" class="table-calcula-frete">
		<tr>
		<td height=20><strong>Valor do Frete</strong></td>
		<td><strong>Disponibilidade</strong></td>';
		
		$html_f = '</tr>
		</table>';
		
		$correios_valor_declarado = intval(get_configuracao("correios_valor_declarado"));
		
		if( $correios_valor_declarado == 0 ){
			$this->valor_declarado = 0;			
		}
		
		
		$this->codempresa = get_configuracao("correios_codempresa");
		$this->senha = get_configuracao("correios_senha");
		

		for($i=0;$i<count($this->codservico);$i++){
			
			list($servico_nome,$servico_codigo) = explode("||",$this->codservico[$i]);

			$peso = $this->peso;
			

			//webservice padrao = correios
			$ws = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?";
			$ws .= "ncdempresa=".$this->codempresa."&sdssenha=".$this->senha."&sceporigem=".$this->cep_origem."&scepdestino=".$this->cep_destino."";
			$ws .= "&nvlpeso=".$peso."&ncdformato=".$this->formato."&nvlcomprimento=".$this->comprimento."&nvlaltura=".$this->altura."&nvllargura=".$this->largura."";
			$ws .= "&scdmaopropria=".$this->mao_propria."&nvlvalordeclarado=".$this->valor_declarado."&scdavisorecebimento=".$this->aviso_recebimento."";
			$ws .= "&ncdservico=".$servico_codigo."&nvldiametro=".$this->diametro."&strretorno=".$this->retorno_formato;

			$valor_frete = $this->CalculaFrete($ws);

			if($this->exibe_html==0){
				$this->valor_frete .= $servico_nome . "|" . $valor_frete. "|".CalculaPrazo($ws)."##";
			}else{
				//$valor_frete = explode('||', $valor_frete);
				if($i==0){
					$this->valor_frete = $html_h;	
				}
				
				$color = "#fff";
				
				if($i % 2==0){
					$color = "#F2F2F2";	
				}
				
				$cep = substr($this->cep_destino,0,5)."-".substr($this->cep_destino,-3,3);
				
				$this->valor_frete .= "<tr>";
				$this->valor_frete .= "<td  height=20 style=background-color:".$color.";> ";
				
				if($valor_frete>0){
					$this->valor_frete .= "R$ ";	
				}
				else
				{
					$valor_frete = "frete grátis";
					$this->valor_frete .= "* ";	
				}
				
				if($this->erro!=0){
					$this->valor_frete = "* não calculado";
				}
				
				
				$this->valor_frete .= "" . $valor_frete['values'] ."</td>";	
				$this->valor_frete .= "<td style=background-color:".$color.";>" . $servico_nome . " para o CEP ".$cep.""; 
				
				if($valor_frete>0){
					$this->valor_frete .= " em até ".$this->CalculaPrazo($ws)." dia(s).";
				}
				
				$this->valor_frete .= "</td>";
				
				$this->valor_frete .= "</tr>";

				if($i==count($this->codservico)-1){
					$this->valor_frete .= $html_f;	
				}
				
				
			}
		}
			 
	}
	
	
	
	function caCalculaFrete(){
		
		$correios_valor_declarado = intval(get_configuracao("correios_valor_declarado"));
		
		if( $correios_valor_declarado == 0 ){
			$this->valor_declarado = 0;			
		}		
		
		$this->codempresa = get_configuracao("correios_codempresa");
		$this->senha = get_configuracao("correios_senha");
		
		
		//print_r($this->codservico);
		for($i=0;$i<count($this->codservico);$i++){
			
			$valor_frete = 0;
			$peso = $this->peso;	
			$peso_extra = 0;
				
			list($servico_nome,$servico_codigo,$servico_calcular) = explode("||",$this->codservico[$i]);
				//echo $servico_nome;
				//ve se o peso não passou de 30KG para servicos do correio
				$servicos_correio = array("40010","40045","40126","40215","40290","40096","40436","40444","40568","40606","41106","41068","81019","81027","81035","81868","81833","81850");
				if( in_array($servico_codigo, $servicos_correio) && $this->peso > 30 ){
					$peso_extra = $peso -30;
					$peso = 30;
				}				
						
						
				//webservice padrao = correios
				$ws  = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?";
				$ws .= "ncdempresa=".$this->codempresa."&sdssenha=".$this->senha."&sceporigem=".$this->cep_origem."&scepdestino=".$this->cep_destino."";
				$ws .= "&nvlpeso=".$peso."&ncdformato=".$this->formato."&nvlcomprimento=".$this->comprimento."&nvlaltura=".$this->altura."&nvllargura=".$this->largura."";
				$ws .= "&scdmaopropria=".$this->mao_propria."&nvlvalordeclarado=".$this->valor_declarado."&scdavisorecebimento=".$this->aviso_recebimento."";
				$ws .= "&ncdservico=".$servico_codigo."&nvldiametro=".$this->diametro."&strretorno=".$this->retorno_formato;

				$fp = fopen('log.txt', 'w');
				fwrite($fp, $ws);
				fclose($fp);
				//echo $ws;
				//die();

				$valor_frete = $this->CalculaFrete($ws);
			
				if($servico_nome == "PAC"){
					$valor = $this->CalculaFrete($ws);
					$valoraux = $valor['values'];
					$_SESSION['valoraux'] =$valoraux;
					//echo "valoraux: $valoraux";
				}

				$freteid = '';
				if($this->freteid != ''){
					$freteid = $this->freteid;
				}
				//echo $freteid;
				//die();

				// if( $this->is_frete_gratis($freteid, $servico_codigo) == -1 ){
				// 	$valor_frete = 0;
				// }

				
				//ve se o peso não tem teso extra por passar de 30 kilos do correio
				if( $peso_extra > 0 ){
					$valor_frete = $valor_frete + ( ($valor_frete/30) * $peso_extra );
					$valor_frete = number_format($valor_frete,2,".","");
				}

				$this->valor_frete .= $valor_frete['values']."||".$valor_frete['status']."||";
			
		}

		return $this->valor_frete;
	}

	
	
	
	function CalculaFrete($ws){
		//carrega o xml de retorno
		$xml = simplexml_load_file($ws);
		// verifica se não há erros
		$valid = array('0', '010', '009', '011');
		$alert = array('-3', '-6', '-888', '007', '008', '7', '99');
		if(in_array($xml->cServico->Erro, $valid)){
			$response = array('status' => 999, 'values' => formata_valor_db($xml->cServico->Valor));
		} elseif(in_array($xml->cServico->Erro, $alert)) {
			$response = array('status' => 1000, 'values' => $xml->cServico->MsgErro);
		} else {
			$response = array('status' => 1001, 'values' => $xml->cServico->Erro, 'message' => $xml->cServico->MsgErro);
		}

		return $response;
	}
	
	
	
	function CalculaPrazo($ws){
			
			$correios_prazo_add = intval(get_configuracao("correios_prazo_add"));
		
			 //carrega o xml de retorno
			 $xml = simplexml_load_file($ws);
			 // verifica se não há erros
			 if($xml->cServico->Erro == '0'){
				 return intval($xml->cServico->PrazoEntrega) + $correios_prazo_add; 
			 }else{
				$this->erro = $xml->cServico->Erro;
				$this->erro_msg = $xml->cServico->MsgErro;
				return 0; 
				break;
			 }		
	}	
	
	
	
	function CalculaPrazoEntrega($servico_codigo){
		
			$correios_prazo_add = intval(get_configuracao("correios_prazo_add"));
		
			$servicos_correio = array("40010","40045","40126","40215","40290","40096","40436","40444","40568","40606","41106","41068","81019","81027","81035","81868","81833","81850");
			if( in_array($servico_codigo, $servicos_correio) && $this->peso > 30 ){
				$this->peso = 30;
			}	
		
			$ws = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?";
			$ws .= "ncdempresa=".$this->codempresa."&sdssenha=".$this->senha."&sceporigem=".$this->cep_origem."&scepdestino=".$this->cep_destino."";
			$ws .= "&nvlpeso=".$this->peso."&ncdformato=".$this->formato."&nvlcomprimento=".$this->comprimento."&nvlaltura=".$this->altura."&nvllargura=".$this->largura."";
			$ws .= "&scdmaopropria=".$this->mao_propria."&nvlvalordeclarado=".$this->valor_declarado."&scdavisorecebimento=".$this->aviso_recebimento."";
			$ws .= "&ncdservico=".$servico_codigo."&nvldiametro=".$this->diametro."&strretorno=".$this->retorno_formato;		

			 //carrega o xml de retorno
			 $xml = simplexml_load_file($ws);
			 // verifica se não há erros
			 if($xml->cServico->Erro == '0'){
				 return ($xml->cServico->PrazoEntrega) + $correios_prazo_add; 
			 }else{
				$this->erro = $xml->cServico->Erro;
				$this->erro_msg = $xml->cServico->MsgErro;
				return 0; 
				break;
			 }		
	}		
		
	
	function doEndereco(){
		$ws = file_get_contents('http://m.correios.com.br/movel/buscaCepConfirma.do?cepEntrada='.$this->cep_destino.'&tipoCep=&cepTemp=&metodo=buscarCep');
		$len = strlen($ws);
		$start = strpos($ws,'<span class="respostadestaque">');
		$stop = strpos($ws,'<span class="resposta">CEP:');
		
		$html = trim(substr($ws,$start,($stop-$start)));
		
		$remove = array('\r','\n',chr(13),'<span class="respostadestaque">','<span class="resposta">','<br/>','  ');
		$html = str_replace($remove,'',$html);
		$html = str_replace('</span>','||',$html);
		
		$html = utf8_encode($html);
		
		$dados = explode("||",$html);
		$x = explode("/",$dados[4]);
		
		$this->logradouro = trim($dados[0]);
		$this->bairro = $dados[2];
		$this->cidade = trim($x[0]);
		$this->estado = trim($x[1]);
		
	} 
	

	function is_frete_gratis($freteid, $codigo_servico = ''){
		$ret = 9;
		$orcamento = $_COOKIE["orcamento"];
		$items = 0;
		$subtotal = 0;
		


		/*---------------------------------------------------------------------------------
		//verifica se so tem item com frete gratis no carrinho
		---------------------------------------------------------------------------------*/
		$ssql = "select tblorcamento_item.itemid, tblorcamento_item.ocodproduto, tblorcamento_item.ocodpropriedade, tblorcamento_item.ocodtamanho, tblorcamento_item.oquantidade, 
				tblorcamento_item.ovalor_unitario, tblorcamento_item.opeso, tblproduto.pfrete_gratis
				from tblorcamento_item
				inner join tblproduto on tblorcamento_item.ocodproduto = tblproduto.produtoid
				where tblorcamento_item.ocodorcamento='{$orcamento}' and tblorcamento_item.oquantidade > 0 ";
			$result = mysql_query($ssql);		
			if($result){
				while($row=mysql_fetch_assoc($result)){			
					if($row["pfrete_gratis"]==0){
						$ret = 0;
					}
					if($row["pfrete_gratis"]==-1 && $ret==9){
						$ret = -1;
					}
				}
				mysql_free_result($result);
			}		

		if($ret==9){
			$ret = 0;	
		}


		/*---------------------------------------------------------------------------------
		//seleciona os totais
		---------------------------------------------------------------------------------*/
		$ssql = "select tblorcamento_item.oquantidade, tblorcamento_item.ovalor_unitario
			from tblorcamento_item
			where tblorcamento_item.ocodorcamento='{$orcamento}' and tblorcamento_item.oquantidade > 0";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$items = $items + $row["oquantidade"];
				$subtotal = $subtotal + ($row["oquantidade"]*$row["ovalor_unitario"]);
			}
			mysql_free_result($result);
		}
		$subtotal = number_format($subtotal,2,".",",");
		
		if(!is_numeric($items)){
			$items = 0;	
		}
		
		$cep_destino = get_only_numbers($GLOBALS["cep_destino"]);
		
		$ssql_codservico = "";
		
		if($freteid != "" && is_numeric($freteid) && $freteid > 0){
			$ssql_codservico = " and tbldesconto.dcodfrete='{$freteid}' ";
		}
		
		/*---------------------------------------------------------------------------------
		//ve se exite o desconto
		---------------------------------------------------------------------------------*/
		$ssql = "SELECT tbldesconto.dcodtipo, tbldesconto.dtipo, tbldesconto.dvalor, tbldesconto.ddesconto, tbldesconto.dquantidade,  tbldesconto.dcodfrete
				FROM tbldesconto
				WHERE tbldesconto.dcodtipo=2 and tbldesconto.dativo=-1  AND tbldesconto.dvalor <='{$subtotal}' and tbldesconto.dquantidade <='{$items}' 				
				and tbldesconto.dcep_inicial<='".$cep_destino."' and tbldesconto.dcep_final>='".$cep_destino."'
				$ssql_codservico
				limit 0,1
				";		
		//echo $ssql;
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)>0){
				$ret = -1;	
			}	
		}
		return $ret;
	}
}
?>