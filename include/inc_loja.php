<?php


	/*------------------------------------------------------------------
		variaveis globais
	-------------------------------------------------------------------*/

	$site_nome;
	$site_email;
	$site_site;
	$site_descricao;
	$site_palavra_chave;	

	if(isset($_COOKIE["site_nome"])){
		$GLOBALS["site_nome"] = $_COOKIE["site_nome"];
		$GLOBALS["site_email"] = $_COOKIE["site_email"];
		$GLOBALS["site_site"] = $_COOKIE["site_site"];
		@$GLOBALS["site_descricao"] = $_COOKIE["site_descricao"];
		@$GLOBALS["site_palavra_chave"] = $_COOKIE["site_palavra_chave"];
		
	}
	else
	{	
		
		$ssql = "select lojaid, lrazaosocial, lnome, lemail, lsite, lendereco, lnumero, lcomplemento, lbairro, lcidade, lestado, lcep, ltelefone, latendimento, ldescricao, lpalavra_chave from tblloja where lojaid = 1";
		$result = mysql_query($ssql);
		
		if($result){	
		
			while($row=mysql_fetch_assoc($result)){	
				
				//$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie
				$expires = time()+ 60 * 60 * 1; // 1 hora de cookie				
				
				
				$GLOBALS["site_nome"] = $row["lnome"];
				$GLOBALS["site_email"] = $row["lemail"];
				$GLOBALS["site_site"] = $row["lsite"];
				$GLOBALS["site_descricao"] = $row["ldescricao"];
				$GLOBALS["site_palavra_chave"] = $row["lpalavra_chave"];	
				
				
				setcookie("site_nome", $GLOBALS["site_nome"], $expires);
				setcookie("site_email", $site_email, $expires);
				setcookie("site_site", $site_site, $expires);
				setcookie("site_descricao", $site_descricao, $expires);
				setcookie("site_palavra_chave",  $site_palavra_chave, $expires);
				
			}
			mysql_free_result($result);
		}
		
	}

	/*------------------------------------------------------------------------------------*/

	function getEstoque($id){

		$sql = mysql_query('SELECT * FROM tblestoque WHERE ecodproduto = '.$id);

		if(count($sql) >= 1){
			while ($list = mysql_fetch_assoc($sql)) {
				if($list['eestoque'] > 0){
					$estoque[] = 1;
				}else{
					$estoque[] = 0;
				}
			}
		}else{
			$estoque[] = 1;
		}

		return $estoque;
	}

	/*------------------------------------------------------------------------------------*/


	function get_carrinho_totals(){
		if(isset($_COOKIE["orcamento"])){
			$orcamento = $_COOKIE["orcamento"]; 	
		}else{
		    $orcamento = 0;
		}
		$ret = "";
		$ssql = "select count(tblorcamento_item.ocodproduto) as items,  
				tblorcamento.ovalor_total, tblorcamento.osubtotal, tblorcamento.ovalor_frete, tblorcamento.ovalor_desconto, tblorcamento.ovalor_desconto_cupom, sum(tblorcamento_item.oquantidade*tblorcamento_item.opeso) as peso
				from tblorcamento_item
				left join tblorcamento on tblorcamento_item.ocodorcamento=tblorcamento.orcamentoid
				where ocodorcamento = $orcamento and tblorcamento_item.oquantidade > 0
				group by tblorcamento.ovalor_total, tblorcamento.osubtotal, tblorcamento.ovalor_frete, tblorcamento.ovalor_desconto, tblorcamento.ovalor_desconto_cupom	";
		//echo $ssql;		
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$ret .=  $row["items"] . "||" . $row["ovalor_total"] . "||" . $row["osubtotal"] . "||" . $row["ovalor_frete"] . "||" . $row["ovalor_desconto"] . "||" . $row["ovalor_desconto_cupom"] . "||" . $row["peso"] . "||";	
			}
			mysql_free_result($result);
		}
		return $ret;
	}
	
	
	
	function get_welcome_message(){
		if(isset($_COOKIE["apelido"])){
			return '<strong>Olá ' . $_COOKIE["apelido"] . '</strong> não é você? <a href="javascript:logout();"><u>clique aqui</u></a>';  
		}else{
			return 'Já é cadastrado? <a href="login.php"><u>clique aqui</u></a>';	
		}	
	}
	
	
	function get_tag_cloud(){
		$ret;
		$ssql = "select count( buscaid ) as total, btag from tblbusca where bativa=-1 group by btag order by total desc, btag limit 0,30";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)==0){
				$ret = "Nenhuma palavra cadastrada ainda.";	
			}
			
			while($row=mysql_fetch_assoc($result)){
				$ret .=  $row["btag"] . ",";	
			}
			mysql_free_result($result);
		}
		return $ret;
	}	
	
	
	
	/*-----------------------------------------------------------------------------------------
	
	------------------------------------------------------------------------------------------*/
	
	function monta_vitrine(){


		//define a qtde de produtos exibir na vitrine
		$vitrine_qtde_item = get_configuracao("vitrine_qtde_item");
		if(!is_numeric($vitrine_qtde_item)){
			$vitrine_qtde_item = 16;	
		}
		
		//define a ordem da vitrine
		$vitrine_ordem = get_configuracao("vitrine_ordem");
		if($vitrine_ordem==""){
			$vitrine_ordem = " RAND() ";
		}
		
		
		//define se ira agrupar os produtos com mesma referencia
		$config_agrupa_por_referencia = get_configuracao("config_agrupa_por_referencia");
		if($config_agrupa_por_referencia==0){
			$agrupa_por_referencia = ", tblproduto.produtoid ";	
		}		
		
		$ret="";
		$data_hoje = date("Y-m-d H:i:s");			
		$ssql = "select SUM(tblestoque.eestoque),tblproduto.pdescricao, tblproduto.plink_seo,tblproduto.pcontrola_estoque,tblproduto.pdisponivel,tblproduto.produtoid as produtoid, tblmarca.mmarca, tblmarca.mlink_seo, tblproduto.preferencia,
		tblproduto.pproduto, tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, 
		tblproduto.plink_seo, tblproduto_midia.marquivo as pimagem
		from tblproduto
		left join tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
		left join tblproduto_midia on tblproduto.produtoid = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal = -1
		left join tblmarca on marcaid = tblproduto.pcodmarca
		where tblproduto.pdisponivel=-1 and tblproduto.pvitrine=-1 and tblproduto.pdata_inicio<='{$data_hoje}' and tblproduto.pdata_termino>='{$data_hoje}' and pcodmarca = marcaid
		group by tblproduto.preferencia ".@$agrupa_por_referencia."
		HAVING ((SUM(tblestoque.eestoque) > 0 && tblproduto.pcontrola_estoque = -1 && tblproduto.pdisponivel = -1) || (tblproduto.pdisponivel = -1 && tblproduto.pcontrola_estoque = 0 ))
		order by $vitrine_ordem";
		//echo $ssql;
		//die();
		$result = mysql_query($ssql);

		if($result){	
			$num_rows = mysql_num_rows($result);
			if($num_rows==0){
				echo "Nenhum produto disponível no momento.";	
			}
			$cnt = 0;
			while($row=mysql_fetch_assoc($result)){
				$estoque = getEstoque($row['produtoid']);
				if(!in_array('0', $estoque)){
					$imagem = $row["pimagem"];
					if(!file_exists($imagem)){
						$imagem = "imagem/produto/tumb-indisponivel.png";		
					}
					$compara = strtolower($row["pproduto"]);
					if(!strstr($compara,"creatina") && !strstr($compara,"carnetina") && !strstr($compara,"carnitine") && !strstr($compara,"polifenol de alcachofra")){
						$cnt++;
					?>
					<a href="<?=$row["plink_seo"] ?>">
					
					<div class="produto">
						<span class="product-thumb"><img src="<?=$imagem?>" alt="produto"></span>
						<span class="product-menu-top20-product-title"><?=$row["pproduto"] ?></span>
						<span class="product-menu-top20-product-mark"><b>+</b> <?=$row["mmarca"] ?></span>
						<span class="product-menu-top20-product-price">R$ <s><?=number_format($row["pvalor_unitario"],2,",",".") ?></s></span>
						<span class="par-velue"><?=get_valor_parcelado_new($row["produtoid"],$row["pvalor_unitario"])?></span>
						<span class="a-vista-price">Ou R$ <?=number_format($row["pvalor_unitario"]-($row["pvalor_unitario"]*0.10),2,",",".")?> à vista</span>
						<span class="boleto-text">via boleto</span>
					</div>
					</a>
					<?php
					}
					if($cnt == $vitrine_qtde_item){
						break;
					}
				}
			}
		}
	}
	
	
	
	
	function monta_vitrine_referencia($id, $referencia, $vitrine_ordem){
		$data_hoje = date("Y-m-d H:i:s");		

		$ssql1 = "select tblproduto.produtoid, tblproduto.preferencia, tblproduto.pproduto, tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, 
					tblproduto.plink_seo, (tblproduto_midia.marquivo) as pimagem 				
					from tblproduto
					left join tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
					left join tblproduto_midia on tblproduto.produtoid=tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal=-1 					 
					where tblproduto.produtoid <> '{$id}' and tblproduto.preferencia = '{$referencia}' 
					and tblproduto.pdisponivel=-1 
					and tblproduto.pdata_inicio<='{$data_hoje}' and tblproduto.pdata_termino>='{$data_hoje}' 
					HAVING ((SUM(tblestoque.eestoque) > 0 && tblproduto.pcontrola_estoque = -1 && tblproduto.pdisponivel = -1) || (tblproduto.pdisponivel = -1 && tblproduto.pcontrola_estoque = 0 ))
					order by $vitrine_ordem limit 0, 5";
		$result1 = mysql_query($ssql1);
		
		if($result1){
			echo '<ul>';
				while($row1 = mysql_fetch_assoc($result1)){	
					$count++;

					$link_seo1 	= $row1["plink_seo"];
					$imagem1 	= $row1["pimagem"];					
					
					if($count <=4){
						echo 
							'<li>	
							<a href="'.$link_seo1.'">
							<img src="'.$imagem1.'" />
							</a>
							</li>';
					}
					else
					{
						echo '<li>	
							<a href="'.$link_seo1.'">
							<img src="images/ico-mais-referencias.png" />
							</a>
							</li>';						
					}
					
					
				}
			echo '</ul>';
			mysql_free_result($result1);
		}
		
		
		
	}
	
	
	
	
	function monta_produtos_relacionados($id,$cat){
		
		//define se ira agrupar os produtos com mesma referencia
		$config_agrupa_por_referencia = get_configuracao("config_agrupa_por_referencia");
		if($config_agrupa_por_referencia==0){
			$agrupa_por_referencia = ", tblproduto.produtoid ";	
		}
		
		$imagem_x = 260;
		$imagem_y = 260;
		
		
		//define a qtde de produtos relacionados a exibir
		$produto_qtde_item_relacionado = get_configuracao("produto_qtde_item_relacionado");
		if($produto_qtde_item_relacionado==0 || $produto_qtde_item_relacionado==""){
			$produto_qtde_item_relacionado = 6;	
		}			

		
		$ret="";
		$data_hoje = date("Y-m-d H:i:s");
		$ssql = "select SUM(tblestoque.eestoque),tblproduto.pcontrola_estoque,tblproduto.pdisponivel, tblproduto.produtoid, tblproduto.recomendados, tblproduto.pdisponivel, tblproduto.pdescricao,
					tblproduto.pcontrola_estoque, tblmarca.mmarca, tblmarca.mlink_seo,tblproduto.preferencia, tblproduto.pproduto,
					tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, (tblproduto.pvalor_comparativo - tblproduto.pvalor_unitario) as pdesconto,
					tblproduto.plink_seo, tblproduto_midia.marquivo as pimagem, tblproduto_midia2.marquivo as pimagem2
					from tblproduto
					left join tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
					left join tblproduto_midia on tblproduto.produtoid = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal = -1
					left join tblproduto_midia as tblproduto_midia2  on tblproduto.produtoid = tblproduto_midia2.mcodproduto and tblproduto_midia2.mprincipal = 2
					left join tblproduto_categoria on tblproduto.produtoid = tblproduto_categoria.pcodproduto 
					left join tblmarca on marcaid = tblproduto.pcodmarca
					where tblproduto.produtoid <> $id and tblproduto.produtoid in (select pcodproduto from tblproduto_categoria
					where pcodcategoria=$cat) and tblproduto.recomendados = 1
					group by tblproduto.preferencia $agrupa_por_referencia
					HAVING ((SUM(tblestoque.eestoque) > 0 && tblproduto.pcontrola_estoque = -1 && tblproduto.pdisponivel = -1) || (tblproduto.pdisponivel = -1 && tblproduto.pcontrola_estoque = 0 ))
					order by rand()";

		$result = mysql_query($ssql);

		if($result){	
			$count = 0;
			$num_rows = mysql_num_rows($result);
			if($num_rows==0){
				$ssql = "select SUM(tblestoque.eestoque),tblproduto.pcontrola_estoque,tblproduto.pdisponivel, tblproduto.produtoid, tblproduto.pdisponivel, tblproduto.pdescricao,
					tblproduto.pcontrola_estoque, tblmarca.mmarca, tblmarca.mlink_seo,tblproduto.preferencia, tblproduto.pproduto,
					tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, (tblproduto.pvalor_comparativo - tblproduto.pvalor_unitario) as pdesconto,
					tblproduto.plink_seo, tblproduto_midia.marquivo as pimagem, tblproduto_midia2.marquivo as pimagem2
					from tblproduto
					left join tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
					left join tblproduto_midia on tblproduto.produtoid = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal = -1
					left join tblproduto_midia as tblproduto_midia2  on tblproduto.produtoid = tblproduto_midia2.mcodproduto and tblproduto_midia2.mprincipal = 2
					left join tblproduto_categoria on tblproduto.produtoid = tblproduto_categoria.pcodproduto 
					left join tblmarca on marcaid = tblproduto.pcodmarca
					where tblproduto.produtoid <> $id group by tblproduto.preferencia $agrupa_por_referencia
					HAVING ((SUM(tblestoque.eestoque) > 0 && tblproduto.pcontrola_estoque = -1 && tblproduto.pdisponivel = -1) || (tblproduto.pdisponivel = -1 && tblproduto.pcontrola_estoque = 0 ))
					order by rand()";
				$result = mysql_query($ssql);
				$num_rows = mysql_num_rows($result);
			}
			if($num_rows==0){
				echo "Nenhum produto disponível no momento.";	
			}
			$cnt = 0;
			while($row=mysql_fetch_assoc($result)){	
				
				$id = $row["produtoid"];
				$valor_unitario = $row["pvalor_unitario"];
				$valor = number_format($row["pvalor_unitario"],2,",",".");
				$valor_comparativo = number_format($row["pvalor_comparativo"],2,',','.');
				
				if( isset($_SESSION["utm_source"]) ){
					$desconto_utm =  get_desconto_utm_source($id);
					$valor_unitario = number_format($valor_unitario - $desconto_utm,2,",",".");
					
					if(floatval($desconto_utm)>0){
						$valor_comparativo = number_format($row["pvalor_unitario"],2,',','.');
					}
					
				}
				else
				{
					$valor_unitario = number_format($valor_unitario,2,",",".");	
				}				
				
				$imagem = str_replace("-tumb","-med",$row["pimagem"]);
				$imagem2 = str_replace("-tumb","-med",$row["pimagem2"]);
				
				if(!file_exists($imagem)){
					$imagem = "imagem/produto/med-indisponivel.png";		
				}
				if(!file_exists($imagem2)){
					$imagem2 = $imagem;
				}
				$count++;
				if($count != $produto_qtde_item_relacionado){ $style="style='margin-right:11.5px;'"; }else{ $style=''; }
				$compara = strtolower($row["pproduto"]);
				if(!strstr($compara,"creatina") && !strstr($compara,"carnetina") && !strstr($compara,"carnitine") && !strstr($compara,"polifenol de alcachofra")){
					$cnt++;
				?>
				<a href="<?=$row["plink_seo"] ?>">
				<div class="produto" <?=$style?>>
					<span class="product-thumb"><img src="<?=$imagem?>" alt="produto"></span>
					<span class="product-menu-top20-product-title"><?=$row["pproduto"] ?></span>
					<span class="product-menu-top20-product-mark"><b>+</b> <?=$row["mmarca"] ?></span>
					<span class="product-menu-top20-product-price">R$ <s><?=number_format($row["pvalor_unitario"],2,",",".") ?></s></span>
					<span class="par-velue"><?=get_valor_parcelado_new($row["produtoid"],$row["pvalor_unitario"])?></span>
					<span class="a-vista-price">Ou R$ <?=number_format($row["pvalor_unitario"]-($row["pvalor_unitario"]*0.10),2,",",".")?> à vista</span>
					<span class="boleto-text">via boleto</span>
				</div>
				</a>
				<?php
				}
				if($cnt == 6){
					break;
				}
			}
			mysql_free_result($result);
		}
	}
	
	
	function monta_link_categoria_produto($id){
		$ret = "";
		$ssql = "
			select tblcategoria.categoriaid, tblcategoria.ccodcategoria, LOWER(tblcategoria.ccategoria) as ccategoria, tblcategoria.clink_seo
			from tblcategoria
			inner join tblproduto_categoria on tblcategoria.categoriaid = tblproduto_categoria.pcodcategoria 
			where tblproduto_categoria.pcodproduto = $id and tblcategoria.ccodcategoria <> 1 and tblcategoria.ccodcategoria <> 2
			order by tblcategoria.ccodcategoria		
		";								
		//echo $ssql;
		$result = mysql_query($ssql);
		if($result){
			$cont = 1;
			while($row=mysql_fetch_assoc($result)){
					$GLOBALS["categoriaid"] = $row["categoriaid"];
					
					$link = $row["clink_seo"];
					
					if( $row["ccodcategoria"] > 0  ){
						$link = get_categoria($row["ccodcategoria"],"clink_seo") . "/" . $link;
						
						$cod = intval(get_categoria($row["ccodcategoria"],"ccodcategoria"));
						
						if( $cod > 0 ){
							$link = get_categoria($cod,"clink_seo") . "/" . $link;
						}
						
					}
					$style = '';
					if($cont == 1){ $style="class='blue'"; }
					$ret .= '<a href="'.$link.'" '.$style.' >'.$row["ccategoria"] . '</a> / ' ;
					$cont++;
				}
			mysql_free_result($result);
		}
		return $ret;
	}
		

?>