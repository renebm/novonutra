<!-- BEGIN GOOGLE ANALYTICS CODEs -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-48816816-1']);
  _gaq.push(['_trackPageview']);
  
  (function(i,s,o,g,r,a,m){
	i['GoogleAnalyticsObject']=r;
	i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();
		a=s.createElement(o),m=s.getElementsByTagName(o)[0];
		a.async=1;
		a.src=g;
		m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-48816816-1', 'nutracorpore.com.br');
  ga('send', 'pageview');

<?php
	$url = $_SERVER['PHP_SELF']; 
	$pos = strrpos($url,"/")+1;
	$url = substr( $url, $pos, strlen($url) );
	if( $url == "finaliza-compra.php" || strpos($url,"finaliza-compra.php") ){

	$i = 0;
	echo "\r\n";
	
	echo '_gaq.push(["_addTrans","'.$pedido.'","'.$site_nome.'","'.$valor_subtotal.'","0,00","'.number_format($valor_frete, 2, ',', '').'","'.$cidade.'","'.$estado.'","Brasil"]);';
	
	echo "\r\n";

		$ssql = "select tblpedido_item.itemid, tblpedido_item.pcodproduto, tblpedido_item.pcodpropriedade, tblpedido_item.pcodtamanho, tblpedido_item.pquantidade, 
				tblpedido_item.pvalor_unitario, tblpedido_item.ppeso, tblproduto.pcodigo, tblproduto.pproduto, tblproduto.psubtitulo
				from tblpedido_item
				inner join tblproduto on tblpedido_item.pcodproduto = tblproduto.produtoid
				where tblpedido_item.pcodpedido='{$pedido}' and tblpedido_item.pquantidade > 0";
		$result = mysql_query($ssql);
		if($result){ 
			
			while($row = mysql_fetch_assoc($result) ){	

				$sku	 	= $row["pcodigo"];
				$produto 	= $row["pproduto"];
				$subtitulo 	= $row["psubtitulo"];
				$valor 		= number_format($row["pvalor_unitario"],2,",","");
				$quantidade	= $row["pquantidade"];

				echo '_gaq.push(["_addItem",';				
					echo '"'.$pedido.'",';
					echo '"'.$sku.'",';					
					echo '"'.$produto.'",';
					echo '"'.$subtitulo.'",';
					echo '"'.$valor.'",';
					echo '"'.$quantidade.'"';
				echo ']);';					
				echo "\r\n";
			}
			mysql_free_result($result);
		}

	echo '_gaq.push(["_trackTrans"]);';
	echo "\r\n";

	}
?>
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- END GOOGLE ANALYTICS CODEs -->