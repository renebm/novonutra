<?php
	include("include/inc_conexao.php");	
	
	//monta a categoria
	$uri = str_replace("/evoluamoda/","",$_SERVER['REQUEST_URI']);	

	$pagina = 1;
	$start = 0;
	$limit = 12;
	$ordem = 0;
	
	$canonical = "";


	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}

	
	
	if(is_numeric($qs["ordem"])){	
		$ordem = $qs["ordem"];
		if($ordem>8 || $ordem <0){
			$ordem = 0;	
		}
	}	
	
	
	/*--------------------------------------------------------------------------
	remove os parametros de querystring para montar as info de categorias
	---------------------------------------------------------------------------*/
	if(strpos($uri,"?")<>""){
		$uri = substr($uri,0,strpos($uri,"?")-1);
	}

	$nodes = explode("/",$uri);
	$child=0;



		switch ($ordem){
			
			case "0":
			$order = " pdata_inicio desc";
			break;				
			
			case "1":
			$order = " pvalor_unitario";
			break;

			case "2":
			$order = " pvalor_unitario desc";
			break;

			case "3":
			$order = " pvalor_unitario desc";	//mais vendidos
			break;

			case "4":
			$order = " pvalor_unitario desc";	//mais bem avaliados
			break;

			case "5":
			$order = " pproduto";
			break;

			case "6":
			$order = " pproduto desc";
			break;

			case "7":
			$order = " pdata_inicio desc";	//lancamento
			break;

			case "8":
			$order = " pdesconto desc";
			break;

		}


		//define se ira agrupar os produtos com mesma referencia
		$config_agrupa_por_referencia = get_configuracao("config_agrupa_por_referencia");
		if($config_agrupa_por_referencia==0){
			$agrupa_por_referencia = ", tblproduto.produtoid ";	
		}


		$data = subDayIntoDate(date("Y-m-d"),30);

		$ssql_oferta = "select tblproduto_categoria.pcodproduto, 
				tblproduto.produtoid, tblproduto.preferencia, tblproduto.pproduto, tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, 
				(tblproduto.pvalor_comparativo - tblproduto.pvalor_unitario) as pdesconto, 
				tblproduto.plink_seo, (tblproduto_midia.marquivo) as pimagem 
				from tblproduto_categoria 
				left join tblproduto on tblproduto_categoria.pcodproduto = tblproduto.produtoid
				left join tblproduto_midia on tblproduto.produtoid = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal=-1
				where tblproduto.pvalor_comparativo > tblproduto.pvalor_unitario and tblproduto.pdisponivel=-1
				group by tblproduto.preferencia $agrupa_por_referencia
				order by $order";

		//echo $ssql_lancamento;
		//echo "<br/><br/>";
		$result = mysql_query($ssql_oferta);					
		if($result){
			$total_registros = mysql_num_rows($result);	
		}



/*-------------------------------------------------------------------
base href
--------------------------------------------------------------------*/
$server = ($_SERVER['SERVER_PORT']==80 ? "http://" : "https://") . $_SERVER['SERVER_NAME'] . str_replace("lancamento.php","",$_SERVER['SCRIPT_NAME']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Ofertas <?php echo $site_nome;?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Lançamentos" />
<meta name="description" content="<?php echo $site_nome;?> . <?php echo $conteudo_descricao;?>" />
<meta name="keywords" content="<?php echo $site_palavra_chave;?>" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?>  Lançamentos" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?php echo $site_site;?>/oferta.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {	

	});
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">
    	<div id="categoria-container-box">
        	<div class="categoria-menu"><span class="cat-menu-left">Menu</span></div>
            <a href="index.php?ref=categoria"><?php echo $site_nome;?></a><span style="float:left; margin: 0 2px 0 2px; padding:5px 0 0 0; color:#666;">&nbsp;/&nbsp;</span><span id="id-pag-atual"><?php echo $categoria?></span>
        </div>
    	<div id="container-menu-left">
        	<?php
            	include("inc_left_lancamento.php");
			?>
        </div>
        <div class="box-products-container">
	        <h1><?php echo $categoria;?></h1>
                <div id="org-sup-box-content">
                	<div class="box-sort-by">
                    	<span class="number-found-itens">Foram encontrados: <?php echo $total_registros;?></span>
                        <span class="itens-por-pagina">Itens por página</span>
                        <select name="qtde-itens-pag" class="qtde-itens" onchange="javascrip:paginacao('limit',this.value);">
                        	<?php
                            	echo combo_itens_pagina($limit);
							?>
                        </select>
                        <span class="sort-by">Ordenação</span>
                        <select name="sort-by-itens" class="sort-by-itens" onchange="javascript:paginacao('ordem',this.value);">
							<?php
                            	echo combo_ordem_pagina($ordem);
							?>
                        </select>
                    </div>
                    <div class="pagination-box">
                    	<div class="paginacao"><span class="paginacao-text">Página:</span></span> 
							<?php
                            	echo paginacao($pagina, $limit, $total_registros);
							?>                            
                        </div>
                    </div>
                </div>
            <div id="products-category-box">
              <div style="width:100%; height:auto; margin:20px 0 20px 0; text-align:center;">
              	<!--strong>Nenhum produto foi localizado.</strong-->
              </div>
              
                <?php 

					
					$ssql_oferta .= " limit $start, $limit";
					
					//echo $ssql_lancamento;
						
					$result = mysql_query($ssql_oferta);
			
					if($result){
						
						$count = 0;
						$num_rows = mysql_num_rows($result);
						if($num_rows==0){
							echo '<div style="width:100%; height:auto; margin:20px 0 20px 0; text-align:center;"><strong>Nenhum produto foi localizado.</strong></div>';	
						}
						while($row=mysql_fetch_assoc($result)){
							$valor_unitario = number_format($row["pvalor_unitario"],2,',','.');
							$valor_comparativo = number_format($row["pvalor_comparativo"],2,',','.');
							$imagem = $row["pimagem"];
							$imagem = str_replace("-tumb","-med",$imagem);
							if(!file_exists($imagem)){
								$imagem = "imagem/produto/tumb-indisponivel.png";		
							}
							$count++;
							$ret = '
							  <div class="item-vitrine" id="item-'.$row["produtoid"].$count.'" onmouseover="javascript:item_vitrine_over(this);" onmouseout="javascript:item_vitrine_out(this);">
								<div class="imagem-item">
								<a href="'.$row["plink_seo"].'">
								<img src="'.$imagem.'" alt="'.$row["pproduto"].'" title="'.$row["pproduto"].'" border="0" width="157" height="258"  />
								</a>
								</div>
								<div class="titulo-item"><span class="desc-item">'.$row["pproduto"].'</span></div>
								<div class="valores-item">';
									if($valor_comparativo > 0){
										$ret .= '<span class="valor-comp">&nbsp;';
										$ret .= 'De:<em> R$ '.$valor_comparativo.'</em>';
										$ret .= '</span>';
									}
									
									$ret .= '<span class="valor-real">Por:<strong> R$ '.$valor_unitario.'</strong></span>
								</div>
							  </div>		
							  ';
							  
							  echo $ret;
						}
						mysql_free_result($result);
					}				  
				  
				  ?>
                <div id="org-sup-box-content">
                	<div class="box-sort-by">
                    	<span class="number-found-itens">Foram encontrados: <?php echo $total_registros;?></span>
                        <span class="itens-por-pagina">Itens por página</span>
                        <select name="qtde-itens-pag" class="qtde-itens" onchange="javascrip:paginacao('limit',this.value);">
                        	<?php
                            	echo combo_itens_pagina($limit);
							?>
                        </select>
                        <span class="sort-by">Ordenação</span>
                        <select name="sort-by-itens" class="sort-by-itens" onchange="javascript:paginacao('ordem',this.value);">
							<?php
                            	echo combo_ordem_pagina($ordem);
							?>
                        </select>
                    </div>
                    <div class="pagination-box">
                    	<div class="paginacao"><span class="paginacao-text">Página:</span></span> 
							<?php
                            	echo paginacao($pagina, $limit, $total_registros);
							?>                            
                        </div>
                    </div>
                </div>
              
              
			</div>
        </div>
	</div>
    
    <div id="footer-container">
    <?php
		include("inc_footer.php");
	?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>