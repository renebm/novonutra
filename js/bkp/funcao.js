﻿var dominio = "";
var intervalo = "";
var base_url = document.location.host;
var index = "";
var tempoFullBanner = 6;
var temporizador = '';
$(document).ready(function(){
	$("#top20-carousel").owlCarousel({
		autoPlay: 5000,
		lazyLoad : true,
		items : 5
	});
	$("#kits-carousel").owlCarousel({
		autoPlay: 5000,
		lazyLoad : true,
		items : 5
	});
	$("#marcas-carousel").owlCarousel({
		autoPlay: 5000,
		lazyLoad : true,
		items : 8
	});
	if($("span[title$='Banner']").size() > 1){
		index = $("span[title$='Banner'][class='on']").html();
		rotacionaBanner();
	}
	if($("span[title$='BannerArtigo']").size() > 1){
		rotacionaBannerArtigo(15);
	}
	$("#baseArtigos").bind("mouseleave",function(){
		$("span[class$='item'], span[class$='item itemHover']").attr('class','item');
		$("#baseArtigos").attr('style','display:none');
	});
	$("#produto-menu-box").bind("mouseleave",function(){
		$(this).removeAttr('style');
		$('#produtos-menu').removeAttr('style');
	});
	$("#fabricantes-menu-box").bind("mouseleave",function(){
		$(this).removeAttr('style');
		$('#fabricantes-menu').removeAttr('style');
	});
	$("#objetivos-menu-box").bind("mouseleave",function(){
		$(this).removeAttr('style');
		$('#objetivos-menu').removeAttr('style');
	});
	$("#top20-menu-box").bind("mouseleave",function(){
		$(this).removeAttr('style');
		$('#top20-menu').removeAttr('style');
	});
	$("#kits-menu-box").bind("mouseleave",function(){
		$(this).removeAttr('style');
		$('#kits-menu').removeAttr('style');
	});
	
	$("#marcas-carousel").bind("mouseleave",function(){
		var owl = $("#marcas-carousel");
		owl.trigger('owl.play',5000);
	});
	$("#marcas-carousel").bind("mouseover",function(){
		var owl = $("#marcas-carousel");
		owl.trigger('owl.stop');
	});
});
function rotacionaBanner(){
	temporizador = setInterval(function(){
		if(index == ($("span[title$='Banner']").size() - 1)){
			$("span[title$='Banner']").fadeOut();
			$("span[title$='Banner']").attr('class','off');
			index = 0;
			$("span[title$='Banner']:contains('"+index+"')").fadeIn();
			$("span[title$='Banner']:contains('"+index+"')").attr('class','on');
		}else{
			$("span[title$='Banner']").fadeOut();
			index = eval(index+1);
			$("span[title$='Banner']:contains('"+index+"')").fadeIn();
			$("span[title$='Banner']:contains('"+index+"')").attr('class','on');
		}
	}, (tempoFullBanner*1000));
}

function trocaBanner(acao){
	clearTimeout(temporizador);
	$("span[title$='Banner']").fadeOut();
	$("span[title$='Banner']").attr('class','off');
	if(acao == 'n'){
		if(index == ($("span[title$='Banner']").size() - 1)){
			index = 0;
		}else{
			index = eval(index+1);
		}
	}else{
		if(index == 0){
			index = ($("span[title$='Banner']").size() - 1);
		}else{
			index = eval(index-1);
		}
	}
	$("span[title$='Banner']:contains('"+index+"')").fadeIn();
	$("span[title$='Banner']:contains('"+index+"')").attr('class','on');
	rotacionaBanner();
}

function rotacionaBannerArtigo(tempo){
	index = $("span[title$='BannerArtigo'][class='on']").attr('rel');
	setInterval(function(){
		if(index == ($("span[title$='BannerArtigo']").size() - 1)){
			index = 0;
		}else{
			index = eval(index+1);
		}
		ativaBArtigo(index);
	}, (tempo*1000));
}
function ativaBArtigo(id){
	var estilo = $(".botaoBanner").attr("rel");
	$(".botaoBanner").attr("style",estilo);
	$(".botaoBanner:contains('"+id+"')").attr("style",estilo+";background:#2578c8");
	$("span[title$='BannerArtigo']").fadeOut();
	$("span[title$='BannerArtigo'][rel='"+id+"']").fadeIn();
}
function expandeArtigos(obj,dep){
	if(!(dep in catArtigos)){
		return false;
	}
	$("#menuArtigos").html('');
	$.each(catArtigos[dep], function(index){
		$("#menuArtigos").append('<a href="'+catArtigos[dep][index][0]+'" class="artigoCategoria" onmouseover="trocaArtigos(this,'+catArtigos[dep][index][2]+')">'+catArtigos[dep][index][1]+'</a>');
	});
	$("#previaArtigos").html(previaArtigos[dep]);
	$("span[class$='item itemHover']").attr('class','item');
	$(obj).attr('class','item itemHover');
	$("#baseArtigos").attr('style','display:block');
}
function trocaArtigos(obj,dep){
	$("#previaArtigos").html('');
	$("#previaArtigos").html(previaArtigos[dep]);
	$("a[class$='artigoCategoria artigoCategoriaHover']").attr('class','artigoCategoria');
	$(obj).attr('class','artigoCategoria artigoCategoriaHover');
}
(base_url.indexOf(dominio)==-1) ? base_url = "http://"+base_url+"/"+dominio+"/" : base_url = "http://"+base_url+"/";

function valida_busca(tipo){

	if(tipo==1){
		var string = trim(document.getElementById('busca').value);
	}
	else
	{
		var string = trim(document.getElementById('str-busca').value);
	}
	if(string=='DIGITE AQUI O QUE DESEJA'){
		alert('Digite o que deseja buscar.');
		return false;	
	}
	if(string.length<2){
		alert('Digite o que deseja buscar com no mínimo 2 caracteres.');
		return false;		
	}

	string = string.toLowerCase();
	string = string.replace("(^[a-z0-9])$","");

	string =  base_url + "busca.php?string="+string;

	window.location.href = string;	
	return false;
}

function valida_busca_artigo(){

	var string = trim(document.getElementById('search-artigo').value);

	if(string.length<2){
		alert('Digite o que deseja buscar com no mínimo 2 caracteres.');
		return false;		
	}

	string = string.toLowerCase();
	string = string.replace("(^[a-z0-9])$","");

	string =  base_url + "busca_artigo.php?string="+string;

	window.location.href = string;	
	return false;
}


function valida_cadastro_news(){
	
	var nome = document.getElementById('nome');
	var email = document.getElementById('email')

	var tipo = new String(nome.type);


	if(tipo=="hidden"){
		//nao valida o campo
	}else{
		if(trim(nome.value)=='DIGITE SEU NOME' || trim(nome.value)==''){
			alert('Digite seu nome.');
			nome.focus();
			return false;	
		}
	}
	
	if(!ValidaEmail(email.value)){
		alert('Digite seu e-mail.');
		email.focus();
		return false;	
	}

	
	action = 'news';
	$.ajax({
        type: "POST",
        url: "ajax_news.php",
        dataType: "html",
		timeout: 30000,
        data: {action: action, nome: nome.value, email: email.value},
        success: function(data)
        {
			var ret = data.split("||");
			if(ret[0]=="ok"){
        		//$('#frm-boletim-ofertas').html(ret[1]);
				alert(ret[1]);
			}
			else
			{
				alert("Ocorreu um erro ao tentar gravar, tente mais tarde.");	
				//$('#frm-boletim-ofertas').html("Ocorreu um erro ao tentar gravar, tente mais tarde.");
			}
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
				alert("Ocorreu um erro ao tentar gravar, tente mais tarde.");	
				//$('#frm-boletim-ofertas').html("Ocorreu um erro ao tentar gravar, tente mais tarde.");
        }
    });	
	
	
	return false;
		
}



function valida_cadastro_news_footer(){
	
	var nome = "";
	var email = document.getElementById('cad-email-ftr');

	if(!ValidaEmail(email.value)){
		alert('Digite seu e-mail.');
		email.focus();
		return false;	
	}

	action = 'news';
	$.ajax({
        type: "POST",
        url: "ajax_news.php",
        dataType: "html",
		timeout: 30000,
        data: {action: action, nome: nome.value, email: email.value},
        success: function(data)
        {
			var ret = data.split("||");
			
			if(ret[0]=="ok"){
					alert(ret[1]);
			}
			else
			{
				alert("Ocorreu um erro ao tentar gravar, tente novamente mais tarde.");
			}
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
				alert("Ocorreu um erro ao tentar gravar, tente novamente mais tarde.");
        }
    });	
		
}




function cadastro_tipo(t){

	$("#div-txt-cnpj").html('<input name="txt-cnpj" type="text" class="campos-cadastro" id="txt-cnpj" value="" />');
	var m = "";
	
	if(t==1){
		$("#txt-cnpj").attr("value","");		
		m = "999.999.999-99";
		$("#lbl-razao").html("Nome*");
		$("#lbl-nome").html("Sobrenome*");
		$("#lbl-cnpj").html("CPF*");
		$("#lbl-ie").html("RG*");
	}	

	if(t==2){
		$("#txt-cnpj").attr("value","");
		m = "99.999.999/9999-99";
		$("#lbl-razao").html("Razão social*");
		$("#lbl-nome").html("Nome fantasia*");
		$("#lbl-cnpj").html("CNPJ*");
		$("#lbl-ie").html("IE*");
	}	
	
	$("#txt-cnpj").mask(m);

}


function valida_cadastro(){
	var tipo;
	var razao = document.getElementById("txt-razao-social");
	var nome = document.getElementById("txt-nome");
	var apelido = document.getElementById("txt-apelido");
	var cnpj = document.getElementById("txt-cnpj");
	var ie = document.getElementById("txt-ie");
	var nascimento = document.getElementById("txt-data-nascimento");
	var telefone = document.getElementById("txt-telefone");
	var celular = document.getElementById("txt-celular");
	
	var email = document.getElementById("txt-email");
	var email2 = document.getElementById("txt-email2");
	
	var senha = document.getElementById("txt-senha");
	var senha2 = document.getElementById("txt-senha2");


	$.each($('.radio-tipo-cadastro'), function(key, value) { 
		var $this = $( this ); 
		if($this.attr("checked")){
			tipo = $this.attr("value");
		}
	});	


	if(trim(razao.value)==""){
		if(tipo==1){
			alert("O nome deve ser informado");
		}

		if(tipo==2){
			alert("A razão social deve ser informada");
		}
		razao.focus();
		return false;
	}
	
	if(trim(nome.value)==""){
		if(tipo==1){
			alert("O sobre nome deve ser informada");
		}

		if(tipo==2){
			alert("O nome fantasia deve ser informado");
		}
		nome.focus();
		return false;
	}	


	if(trim(apelido.value)==""){
		alert("Informe como gostaria de ser chamado");
		apelido.focus();
		return false;
	}

	if(trim(cnpj.value)==""){
		alert("Informe o número do documento");
		cnpj.focus();
		return false;
	}

	if(trim(ie.value)==""){
		alert("Informe o número do documento");
		ie.focus();
		return false;
	}


	if(trim(nascimento.value)==""){
		alert("Informe a data de nascimento");
		nascimento.focus();
		return false;
	}
	
	
	if(trim(telefone.value)==""){
		alert("O número do telefone deve ser informado");
		telefone.focus();
		return false;
	}
	
	if(trim(celular.value)==""){
		alert("O número de celular deve ser informado");
		celular.focus();
		return false;
	}
	
	if(!ValidaEmail(email.value)){
		alert("O e-mail deve ser informado");
		email.focus();
		return false;
	}
	
	if(email.value!=email2.value){
		alert("O e-mail deve ser confirmado");
		email.focus();
		return false;
	}
	

	if(trim(senha.value)==""){
		alert("A senha deve ser informada");
		senha.focus();
		return false;
	}

	if(trim(senha.value).length<4){
		alert("A senha deve ser informada com mínimo de 4 caracteres");
		senha.focus();
		return false;
	}

	if(senha.value!=senha2.value){
		alert("A senha deve ser informada e confirmada");
		senha.focus();
		return false;
	}
	
}



function valida_cadastro_senha(){

	var senha = document.getElementById("txt-senha");
	var senha2 = document.getElementById("txt-senha2");

	if(trim(senha.value)==""){
		alert("A senha deve ser informada");
		senha.focus();
		return false;
	}

	if(trim(senha.value).length<4){
		alert("A senha deve ser informada com mínimo de 4 caracteres");
		senha.focus();
		return false;
	}

	if(senha.value!=senha2.value){
		alert("A senha deve ser informada e confirmada");
		senha.focus();
		return false;
	}
	
}





function logout(){
	action = 'logout';
	$.ajax({
        type: "POST",
        url: "ajax.php",
        dataType: "html",
		timeout: 30000,
        data: {action: action},
        success: function(data)
        {
			var ret = data.split("||");
			if(ret[0]=="ok"){
        		$('#txt-cadastrar').html('<em class="em-cinza">'+ret[1]+'</em>');
			}else{
				window.location.reload();	
			}
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	//
        }
    });	
}

function marca_banner(){
	action = 'marca-banner';
	$.ajax({
        type: "POST",
        url: "ajax.php",
        dataType: "html",
		timeout: 30000,
        data: {action: action},
        success: function(data)
        {
			
			var ret = data.split("||");
			if(ret[0]=="ok"){
				$('#box-meio-carrossel').html(ret[1]);
				$('#box-carrossel').show();
				//setTimeout('marca_banner_carrossel()',300);
			}else{
				$('#box-meio-carrossel').hide();	
			}
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	$('#box-meio-carrossel').hide();
        }
    });		
}

function marca_banner_carrossel(){
      $("#box-meio-carrossel").jCarouselLite({
            btnPrev : '.prev',
            btnNext : '.next',
            auto    : 0,
            speed   : 500,
            visible : 9,
			scroll : 4
      });
}




function full_banner(){
	action = 'full-banner';
	$.ajax({
        type: "POST",
        url: "ajax.php",
        dataType: "html",
		timeout: 30000,
        data: {action: action},
        success: function(data)
        {
			var ret = data.split("||");
			if(ret[0]=="ok"){
				$('#box-full-banner').html(ret[1]);
				$('#box-full-banner').show();
				full_banner_slideshow();
			}else{
				$('#box-full-banner').hide();	
			}
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	$('#box-full-banner').hide();
        }
    });		
}




function skyscrape_banner(){
	action = 'skyscrape-banner';
	$.ajax({
        type: "POST",
        url: "ajax.php",
        dataType: "html",
		timeout: 30000,
        data: {action: action},
        success: function(data)
        {
			var ret = data.split("||");
			if(ret[0]=="ok"){
				$('#box-banner-skyscrape').html(ret[1]);
				$('#box-banner-skyscrape').show();
			}else{
				$('#box-banner-skyscrape').hide();	
			}
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	$('#box-banner-skyscrape').hide();
        }
    });		
}




function full_banner_slideshow(){
	$('.slideshow').cycle({
		fx: 'fade',
		speed: 1000,
		timeout: 5000,
		pause: true,
		next: null
	});	
}


function banner_regua(){
	action = 'banner-regua';
	$.ajax({
        type: "POST",
        url: "ajax.php",
        dataType: "html",
		timeout: 10000,
        data: {action: action},
        success: function(data)
        {
			var ret = data.split("||");
			if(ret[0]=="ok"){
				$('#box-banner-skyscrape').html(ret[1]);
				$('#box-banner-skyscrape').show();
			}else{
				$('#box-banner-skyscrape').hide();	
			}
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	$('#box-banner-skyscrape').hide();
        }
    });		
}



function popula_carrinho(qtd){
  clearInterval(intervalo);
  if(document.getElementById('container-aba-carrinho').style.display == 'none'){
	if(parseInt(qtd)<1){
		return false;
	}
	
   	$('#box-aba-carrinho').html('<div id="box-aba-carrinho-loading"><img src="images/loading.gif" /><br />Caregando dados, aguarde...</div>');
	action = 'populate';
	$('#container-aba-carrinho').show();
	$.ajax({
        type: "POST",
        url: "ajax_popup_carrinho.php",
        dataType: "html",
		timeout: 10000,
        data: {action: action},
        success: function(data)
        {
			$('#box-aba-carrinho').html(data);			
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	$('#container-aba-carrinho').hide();
        }
    });	
  }
}

function layer_carrinho_off(){
  clearInterval(intervalo);
  intervalo = window.setInterval(function() {$('#container-aba-carrinho').hide();}, 500);
}


function busca_auto_completa(){
	var str = document.getElementById("busca");
	if(str.value.length<2){
		return false;	
	}
	
	$.ajax({
        type: "GET",
        url: "ajax_busca.php",
        dataType: "html",
		timeout: 30000,
        data: {string: str.value},
        success: function(data){
			str = data.split(",");
			busca_auto_completa_show(str);		
        }
    });		
}

function busca_auto_completa_show(tags){
	$("input#busca").autocomplete({									
		source: tags,
		minLength: 0,
		autoFocus: false,
		delay: 0
	});
	$('.ui-menu-item').css('width', '195px');
	$('#ui-id-1').css('width', '199px');
}






function ajax_calcula_frete(){
	var action = "docalculafrete";

	var cep = $('#pref-cep').val() + $('#sulf-cep').val();
	
	// campo cep separado em 2 
	if(cep==undefined){
		cep = $("#pref-cep").attr("value") + '' + $("#sulf-cep").attr("value");
	}

	var scep = get_only_numbers(cep);
	var peso = document.getElementById("peso").value;
	var valor = document.getElementById("valor_unitario").value;
	var frete_gratis = document.getElementById("frete_gratis").value;
	
	if(scep.length<8){
		alert("Cep inválido");
		return;
	}
	
	$("#box-frete").html('<img src="images/spinner.gif" /> Calculando frete, aguarde...');
	
	//produto com frete gratis
	if(frete_gratis==-1){
		
		var xhtml = '<table width="100%" border="0" cellspacing="0" cellpadding="3" style="border:#CCC solid 1px; font-family:Verdana, Geneva, sans-serif; font-size:11px; color:#666; margin-top:5px;">';
		xhtml =  xhtml + '<tr>';
		xhtml =  xhtml + '<td><strong>Valor do Frete</strong></td>';
		xhtml =  xhtml + '<td><strong>Disponibilidade</strong></td>';		
		xhtml =  xhtml + '</tr>';
		
		xhtml =  xhtml + '<tr>';
		xhtml =  xhtml + '<td colspan="2" style="background-color:#F2F2F2;">* Frete Grátis para o CEP '+cep+'</td>';		
		xhtml =  xhtml + '</tr>';

		xhtml =  xhtml + '</table>';		
		
		$("#box-frete").html(xhtml);
		
		return;
	}
	
	
	//claculo de frete
	$.ajax({
		type: "POST",
		url: "ajax_frete.php",
		dataType: "html",
		timeout: 8000,
		data: {action: action, cep: scep, peso: peso, valor: valor, frete_gratis: frete_gratis},
		success: function(data)
		{
			var ret = data.split("||");
			if(ret[0]=="ok"){
				$("#box-frete").html(ret[1]);
			}else{
				alert(ret[2]);
					$("#box-frete").html('');
			}
		},
		error: function (jqXHR, textStatus, errorThrown)
		{	
			$("#box-frete").html("Erro ao calcular o frete tente mais tarde.");
			//alert(textStatus);
		}
	});			
	
}


function carrinho_calcula_frete(){
	var action = "calculafrete";
	
	var cep = document.getElementById("cep-carrinho").value;
	var scep = get_only_numbers(cep);	
	
	if(scep.length<8){
		alert("Cep inválido");
		return;
	}

	$("#btn-calcula-frete").attr('src','images/spinner.gif');
	
	document.getElementById("cep").value = cep;
	document.getElementById("action").value = action;	
	setTimeout('document.getElementById("frm_carrinho").submit()',500);
	
	
}


function carrinho_calcula_cupom_desconto(){
	var action = "calculacupomdesconto";
	
	var cupom = document.getElementById("cupom-desconto-field").value;
	
	if(cupom.length<6){
		alert("Cupom inválido");
		return;
	}
	
	$("#btn-calcula-desconto").attr('src','images/spinner.gif');

	document.getElementById("cupom").value = cupom;
	document.getElementById("action").value = action;	
	setTimeout('document.getElementById("frm_carrinho").submit()',500);
	
}




function item_vitrine_over(obj){
  jQuery(obj).animate({
    'opacity' : 0
  },150);
}


function item_vitrine_out(obj){
  jQuery(obj).animate({
    'opacity' : 100
  },150);
}



function show_layer_calcula_cep(){
	/*$('#txt-cep').mask('99999-999');*/
	$("#box-calcula-cep").show("fade");	
	$("#txt-cep").focus();
}



function show_layer_avaliacao(){
	$('#box-container-avaliacao').show();
	setTimeout('$("#avaliacao-titulo").focus()',100);		
}


function show_layer(obj){
	$('#'+obj).show();	
}


function hide_layer(obj){
	$('#'+obj).hide();
}





function seta_field_valor(campo, valor){
	
	if(trim(campo.value)==""){
		campo.value = valor;
	}	
}

function clear_field(campo, valor){
	if(campo.value==valor){
		campo.value = "";
	}
}


function goto(url){
	window.open(url, "_parent");
}


function go_to(url,target,parameter){
	window.open(url, target, paremeter);
}





function paginacao(parametro, valor){
	filtro_valor(parametro, valor);
}



function filtro_valor(parametro1, valor1,parametro2, valor2){

	try{
		var url = decodeURIComponent(document.URL);
	}
	catch(err){
	  var url = document.URL;
	}
	
	var pos = url.indexOf("?");
	
	if(pos==-1){
		url = url.replace(/\/$/, "");
		url += "/";	
	}else{
		url = url.substr(0,pos);
	}
	
	url += "?"+parametro1+"="+valor1;
	if(typeof(parametro2) != 'undefined'){
		url += "&"+parametro2+"="+valor2;
	}

	if(variaveis!=""){
		for(i=0;i<variaveis.length;i++){
			nvar=variaveis[i].split("=")
			if( nvar[0] != parametro1 && nvar[0] != parametro2 ){
				url += "&" + nvar[0]+"="+nvar[1];
			}
		}
	}
	
	window.location.href = url;
	
}




function troca_produto_cor(id){
	window.location.href = String(id);	
}



/*--------------------------------------------------------------------------*/

function valida_login(){
	var email = document.getElementById("txt-email");
	var senha = document.getElementById("txt-senha");
	
	if(!ValidaEmail(email.value)){
		alert("O e-mail deve ser informado");
		email.focus();
		return false;
	}


	if(trim(senha.value)==""){
		alert("A senha deve ser informada");
		senha.focus();
		return false;
	}

}


function valida_esqueci_senha(){
	var email = document.getElementById("txt-email");
	
	if(!ValidaEmail(email.value)){
		alert("O e-mail deve ser informado");
		email.focus();
		return false;
	}


}





/*------------------------------------------------------------------*/

function load_botoes_redes_sociais(){
/*	var link_facebook = document.getElementById("link_facebook");

	action = 'load_botoes_redes_sociais';
	$.ajax({
        type: "POST",
        url: "ajax_produto.php",
        dataType: "html",
		timeout: 10000,
        data: {action: action, link_facebook:link_facebook.value},
        success: function(data)
        {						
			$('#social').html(data);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
			setTimeout("load_botoes_redes_sociais()",5000);
        }
    });	
*/
}



function valida_avisa_indisponivel(){
	var produtoid = document.getElementById("produtoid");
	var propriedade = document.getElementById("propriedade");
	var tamanho = document.getElementById("tamanho");
	var nome = document.getElementById("txt_nome");
	var email = document.getElementById("txt_email");
	
	if( trim(nome.value)=="" || trim(nome.value)=="Digite seu nome" ){
		alert("O nome deve ser informado");
		nome.focus();
		return false;
	}
	
	if(!ValidaEmail(email.value)){
		alert("O e-mail deve ser informado");
		email.focus();
		return false;
	}

	
	if(tamanho.value=="-1"){
		alert("Selecione o tamanho desejado");
		return false;
	}
	
	
	if(propriedade.value=="-1"){
		alert("Selecione o tamanho desejado");
		return false;
	}
	
	
	$('#campos-aviseme').html('<img src="images/spinner.gif" /> Gravando dados, aguarde...');
	
	action = 'aviseme';
	$.ajax({
        type: "POST",
        url: "ajax_produto.php",
        dataType: "html",
		timeout: 10000,
        data: {action: action, produtoid:produtoid.value, propriedade:propriedade.value, tamanho:tamanho.value, nome: nome.value, email: email.value},
        success: function(data)
        {
			//alert(data);			
			var ret = data.split("||");
			if(ret[0]=="ok"){
        		$('#txt-avise-me').html(ret[1]);
				$('#campos-aviseme').hide();
			}
			else
			{
				$('#txt-avise-me').html(ret[1]);
			}
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
				$('#txt-avise-me').html("Erro ao gravar, tente novamente mais tarde.");
        }
    });	
	
	setTimeout('$("#campos-aviseme").hide()',3000);
	return false;
	
}



function produto_disponivel(status){
	
	if(status==0){
		$("#box-comprar").hide();
		$("#valorde").hide();		
		$("#valorpor").hide();
		$("#valorparcelado").hide();
		$("#box-btn-comprar").hide();			
		$("#box-avise-me").show();
	}
	
	if(status==-1){
		$("#box-comprar").show();
		$("#valorde").show();		
		$("#valorpor").show();
		$("#valorparcelado").show();
		$("#box-btn-comprar").show();			
		$("#box-avise-me").hide();
	}	
	
}



function produto_indique(id){	

	var action = "load";
	$.ajax({
		type: "POST",
		url: "produto-indique.php",
		dataType: "html",
		timeout: 30000,
		data: {action: action, produtoid: id},
		success: function(data)
		{
			$('#lb').show();
			$('#lb').html(data);
			setTimeout('$("#i-nome-de").focus();',300);
		},
		error: function (jqXHR, textStatus, errorThrown)
		{	
			//alert(textStatus);
		}
	});				

}

function artigo_indique(id){	

	var action = "load";
	$.ajax({
		type: "POST",
		url: "artigo-indique.php",
		dataType: "html",
		timeout: 30000,
		data: {action: action, produtoid: id},
		success: function(data)
		{
			$('#lb').show();
			$('#lb').html(data);
			setTimeout('$("#i-nome-de").focus();',300);
		},
		error: function (jqXHR, textStatus, errorThrown)
		{	
			//alert(textStatus);
		}
	});				

}

function guia_tamanho(id){	

	var action = "load_guia_tamanho";
	$.ajax({
		type: "POST",
		url: "guia-tamanho.php",
		dataType: "html",
		timeout: 30000,
		data: {action: action, id: id},
		success: function(data)
		{
			//alert(data);
			
			$('#lb').show();
			$('#lb').html(data);
		},
		error: function (jqXHR, textStatus, errorThrown)
		{	
			//alert(textStatus);
		}
	});				

}


function opcao_pagamento(id){	

	var action = "load";
	$.ajax({
		type: "POST",
		url: "ajax_opcao_pagamento.php",
		dataType: "html",
		timeout: 30000,
		data: {action: action, id: id},
		success: function(data)
		{
			//alert(data);
			
			$('#lb').show();
			$('#lb').html(data);
		},
		error: function (jqXHR, textStatus, errorThrown)
		{	
			//alert(textStatus);
		}
	});				

}


function valida_indique(){
	var produtoid = document.getElementById("produtoid");
	var nome_de = document.getElementById("i-nome-de");
	var email_de = document.getElementById("i-email-de");

	var nome_para = document.getElementById("i-nome-para");
	var email_para = document.getElementById("i-email-para");

	var mensagem = document.getElementById("i-mensagem");
	
	if(trim(nome_de.value)==""){
		alert("O nome deve ser informado");
		nome_de.focus();
		return false;
	}

	if(!ValidaEmail(email_de.value)){
		alert("O e-mail deve ser informado");
		email_de.focus();
		return false;
	}


	if(trim(nome_para.value)==""){
		alert("O nome deve ser informado");
		nome_para.focus();
		return false;
	}

	if(!ValidaEmail(email_para.value)){
		alert("O e-mail deve ser informado");
		email_para.focus();
		return false;
	}
	
	
	if(trim(mensagem.value)==""){
	//	alert("A mensagem deve ser informada");
	//	mensagem.focus();
	//	return false;
	}

	
	var action = "enviar";


	$("#lb-mensagem").html('<img src="images/spinner.gif" border="0" /> Processando, aguarde...');
	$("#cmd-continuar").attr("disabled","1");
	
	$.ajax({
		type: "POST",
		url: "produto-indique.php",
		dataType: "html",
		timeout: 30000,
		data: {action: action, produtoid: produtoid.value, nome_de: nome_de.value, email_de: email_de.value, nome_para: nome_para.value, email_para: email_para.value, mensagem: mensagem.value},
		success: function(data)
		{
			$("#lb-mensagem").html(data);
			$("#cmd-continuar").attr("disabled",0);
			setTimeout('$("#lb").hide("slow")',2000);
		},
		error: function (jqXHR, textStatus, errorThrown)
		{	
			$("#lb-mensagem").html("Erro ao enviar sua indicação, tente novamente mais tarde.");
				$("#cmd-continuar").attr("disabled",0);
		}
	});		
	

	return false;
	
}


function valida_avaliacao(){
	
	var action = "avaliacao";
	var titulo = document.getElementById("avaliacao-titulo");
	var texto = document.getElementById("avaliacao-texto");
	var avaliacao = document.getElementById("avaliacao");
	var produto = document.getElementById("produtoid");
	var autoriza = document.getElementById("avaliacao-autoriza");
	var interesse = 1;
	
	e = document.frm_avaliacao.elements;	//elements sao os itens dentro do formulario
	for (i=0; i<e.length; i++) {
		if (e[i].type == "radio" && e[i].id == "avaliacao-opt" && e[i].checked){
			interesse = e[i].value;
		}
	}	
	
	if(trim(titulo.value)==""){
		alert("O título da sua avaliação deve ser informado");
		titulo.focus();
		return false;
	}
	
	if(trim(texto.value)==""){
		alert("O texto da sua avaliação deve ser informado");
		texto.focus();
		return false;
	}
	
	if(avaliacao.value==0){
		alert("Você deve selecionar uma nota.\r\nPasse o mouse e clique nas estrelas");
		return false;
	}
	
	
	if(autoriza.checked==true){
		autoriza = -1;	
	}
	else
	{
		autoriza = 0;	
	}
	
	
	$("#cmd_enviar").html('<img src="images/spinner.gif" border="0" /> Processando, aguarde...');
	
	$.ajax({
		type: "POST",
		url: "ajax_produto.php",
		dataType: "html",
		timeout: 30000,
		data: {action: action, produto: produto.value, titulo: titulo.value, texto: texto.value, interesse: interesse, autoriza: autoriza, nota: avaliacao.value},
		success: function(data)
		{
			//alert(data);
			var ret = data.split("||");
			if(ret[0]=="ok"){				
				$("#cmd_enviar").html(ret[1]);
			}else{
				$("#cmd_enviar").html("Erro ao processar sua avaliação, tente mais tarde.");
			}
		},
		error: function (jqXHR, textStatus, errorThrown)
		{	
			$("#cmd_enviar").html("Erro ao processar sua avaliação, tente novamente mais tarde.");
			//alert(textStatus);
		}
	});		
	
	
	return false;	
}





/*----------------------------------------------------------------------------*/

function add_carrinho(){
	
	var id = document.getElementById("produtoid");
	var tamanho = document.getElementById("tamanho");
	var propriedade = document.getElementById("propriedade");
	var qtde = document.getElementById("qtde");


	if(parseInt(id.value)==0){
		alert("Ocorreu um erro ao processar seu produto.");
		window.location.href = "index.php?ref=produto-erro";
		return false;
	}
	
	if(tamanho.value==-1){
		alert("Selecione o tamanho desejado");
		return false;
	}

	if(propriedade.value==-1){
		alert("Selecione uma opção");
		return false;
	}

	if(estoque[tamanho.value+":"+propriedade.value] < qtde.value){
		alert('A quantidade definida excedeu estoque.\nMáximo de '+qtde.value+' unidade(s)');
		return false;
	}
	
	//envia o post para o carrinho
	document.getElementById("frm_produto").submit();

	
}



function excluir_item_carrinho(id, produto){
	
	if(confirm("Deseja exlcuir o produto selecionado ?")){
		document.getElementById("action").value="excluir";
		document.getElementById("id").value=id;
		document.getElementById("produtoid").value=produto;
		document.getElementById("frm_carrinho").submit();
	}
	
}

function excluir_item_ajax(id){
  if(confirm("Deseja exlcuir o produto selecionado ?")){
    $(".carrinho-item-"+id).remove();
	$.ajax({
      async: true,
      type: "POST",
	  url: "ajax_zera_item_qtd.php",
	  dataType: "html",
	  timeout: 30000,
	  data: {idproduto: id}
    });
  }
}

function atualizar_qtde_carrinho(id, produto){
	var srefresh = $("#produto-refresh-"+produto).html();
	
	var qt = parseInt(document.getElementById("qtde-produto-carrinho-"+id).value);
	
	if(isNaN(qt)){
		qt = "1";	
	}
	
	if(qt<0){
		qt = 0;
	}


	$("#produto-refresh-"+produto).html("<img src='images/spinner.gif' />");	
	$("#qtde-produto-carrinho-"+id).attr("value",qt);
	
	var action = 'get_estoque_carrinho_refresh';
	
		$.ajax({
		type: "POST",
		url: "ajax_produto.php",
		dataType: "html",
		timeout: 30000,
		data: {action: action, id: id, produto: produto},
		success: function(data)
		{

			var ret = data.split("||");
			
			if(ret[0]=="ok"){
				
				if(parseFloat(ret[1]) < qt){
					alert("Este produto possui apenas "+ret[1]+" em estoque.");
					$("#qtde-produto-carrinho-"+id).attr("value",ret[1]);	
				}
				else
				{
					document.getElementById("action").value="atualizar-item";
					document.getElementById("id").value=id;
					document.getElementById("produtoid").value=produto;
					document.getElementById("qtde").value = qt;
					document.getElementById("frm_carrinho").submit();							
				}
				
				$("#produto-refresh-"+produto).html("");
			}
			else
			{
				alert("Erro ao atualizar quantidade tente novamente mais tarde.");
				$("#produto-refresh-"+produto).html("");
			}			
		},
		error: function (jqXHR, textStatus, errorThrown)
		{	
			alert(textStatus + " " + errorThrown);
			$("#produto-refresh-"+produto).html("");
		}
	});	
		
}



function atualizar_carrinho_item_presente(id, tipo){
	
		var action = 'carrinho_item_presente';
	
		$.ajax({
		type: "POST",
		url: "ajax_produto.php",
		dataType: "html",
		timeout: 30000,
		data: {action: action, id: id, tipo: tipo},
		success: function(data)
		{
				
			var ret = data.split("||");
			
			if(ret[0]=="ok"){
				
			}
			else
			{
				alert("Erro " + ret[1]);
			}			
		},
		error: function (jqXHR, textStatus, errorThrown)
		{	
			alert(textStatus + " " + errorThrown);
		}
	});	
}


/*----------------------------------------------------------------------------*/
//Endereço
/*----------------------------------------------------------------------------*/

function valida_endereco(){
	var titulo = document.getElementById("endereco_titulo");	
	var nome = document.getElementById("endereco_nome");	
	
	var cep = document.getElementById("endereco_cep");	
	var endereco = document.getElementById("endereco_endereco");	
	var numero = document.getElementById("endereco_numero");	
	var bairro = document.getElementById("endereco_bairro");	
	var cidade = document.getElementById("endereco_cidade");	
	var estado = document.getElementById("endereco_estado");	
	var referencia = document.getElementById("endereco_referencia");	
	
	
	if(trim(titulo.value)==""){
		alert("O tipo de endereço deve ser informado");
		titulo.focus();
		return false;
	}

	if(trim(endereco.value)==""){
		alert("O endereço deve ser informado");
		endereco.focus();
		return false;
	}

	if(trim(numero.value)==""){
		alert("O número deve ser informado");
		numero.focus();
		return false;
	}
	
	if(trim(bairro.value)==""){
		alert("O bairro deve ser informado");
		bairro.focus();
		return false;
	}
	
	if(trim(cidade.value)==""){
		alert("A cidade deve ser informada");
		cidade.focus();
		return false;
	}
	
	if(trim(estado.value)==""){
		alert("O estado deve ser informado");
		estado.focus();
		return false;
	}
	
	if(trim(referencia.value)==""){
		//alert("O ponto de referência deve ser informado");
		refrencia.value = "";
		referencia.focus();
		return false;
	}

	
}


function endereco_editar(id){
	document.getElementById("action").value = "editar";
	document.getElementById("enderecoid").value = id;
	document.getElementById("frm_endereco").submit();
}


function endereco_excluir(id){
	if(confirm("Deseja realmente excluir o endereço selecionado?")){		
		document.getElementById("action").value = "excluir";
		document.getElementById("enderecoid").value = id;
		document.getElementById("frm_endereco").submit();
	}
}


function endereco_frete(id){
	document.getElementById("action").value = "endereco";
	document.getElementById("enderecoid").value = id;
	$("#frm_endereco").attr('action','frete.php');
	document.getElementById("frm_endereco").submit();
}


function endereco_load(cep){

	scep = get_only_numbers(cep);
	var action = "endereco";
	
	$("#spinner").show();
	
	setTimeout('$("#spinner").hide()',8000);
	
	$.ajax({
		type: "POST",
		url: "ajax_frete.php",
		dataType: "html",
		timeout: 30000,
		data: {action: action, cep: scep},
		success: function(data)
		{
			//alert(data);
			
			var ret = data.split("||");
			if(ret[0]=="ok"){
				
				$("#endereco_endereco").attr("value",ret[1]);
				$("#endereco_bairro").attr("value",ret[2]);
				$("#endereco_cidade").attr("value",ret[3]);
				$("#endereco_estado").attr("value",ret[4]);
				$("#endereco_estado>option[value='"+ret[4]+"']").attr('selected','selected')
				
				$("#spinner").hide();
			}
		},
		error: function (jqXHR, textStatus, errorThrown)
		{	
			$("#spinner").hide();
			//alert(textStatus);
		}
	});	

}







/*---------------------------------------------------------------*/

function frete_pagamento(){
	document.getElementById("frm_frete").submit();	
}







/*---------------------------------------------------------------*/
function valida_pagamento(){
	
	//disabilita botão continuar
	//evita clicar duas vezes
	$("#cmd_continuar").attr("disabled", "disabled");
	
	
	var titular = document.getElementById("cartao-titular");
	var numero = document.getElementById("cartao-numero");
	var validade = document.getElementById("cartao-validade");
	var codseguranca = document.getElementById("cartao-codseguranca");
	var parcelas = document.getElementById("parcelas");
	var endereco_fatura = document.getElementById("endereco-fatura");
	
	
	if(pgto_tipo>=2 && pgto_tipo<=4){		// 2-CIELO 	3-REDECARD	4-POS
	
		if(trim(titular.value)==""){
			alert("O titular do cartão deve ser informado");
			$("#cmd_continuar").removeAttr("disabled");
			titular.focus();
			return false;
		}
	
		if(trim(numero.value)==""){
			alert("O número do cartão deve ser informado");
			$("#cmd_continuar").removeAttr("disabled");
			numero.focus();
			return false;
		}
		
		if(trim(validade.value)==""){
			alert("A validade do cartão deve ser informada");
			$("#cmd_continuar").removeAttr("disabled");
			validade.focus();
			return false;
		}
		
		if(trim(codseguranca.value)==""){
			alert("O código de segurança do cartão deve ser informado");
			$("#cmd_continuar").removeAttr("disabled");
			codseguranca.focus();
			return false;
		}	
		
		
		if(parcelas.value==""){
			alert("O número de parcelas deve ser informado");
			$("#cmd_continuar").removeAttr("disabled");
			parcelas.focus();
			return false;
		}
		
		
		if(endereco_fatura.value==""){
			alert("O endereço de fatura do cartão deve ser informado");
			$("#cmd_continuar").removeAttr("disabled");
			endereco_fatura.focus();
			return false;
		}
		
		//chama funcao que posta os dados do cartão para webservice;
		
		$('#msg-cartao').html('<img src="images/spinner.gif" /> Processando condição de pagamento, aguarde...');
		
		if(pgto_tipo==2){
			cielo_ws_getauthorized();
			return false;
		}

		if(pgto_tipo==3){
			redecard_ws_getauthorized();
			return false;
		}
		
		if(pagto_tipo==4){
			$("#frm_pagamento").submit();	
		}
		
		return false;		
			
	}		
	
	
}


function get_condicao_pagamento(id){
	var action = "load-condicao-pagamento"; 
	
	$.ajax({
        type: "POST",
        url: "ajax.php",
        dataType: "html",
		timeout: 10000,
        data: {action: action, forma_pagamento: id},
        success: function(data)
        {
			$('#parcelas').html(data);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
			$('#parcelas').html('<option value="">Selecione</option>');
        }
    });	
}


/*-----------------------------------------------------------------------
Lista de Presente
------------------------------------------------------------------------*/
function valida_lista_presente(){
	var titulo = document.getElementById("lista_titulo");
	var nome = document.getElementById("lista_nome");
	var data_evento = document.getElementById("lista_data_evento");
	var link_seo = document.getElementById("lista_link_seo");
	var endereco = document.getElementById("lista_endereco");
	
	if(trim(titulo.value)==""){
		alert("O nome da lista deve ser informado");
		titulo.focus();
		return false;
	}

	if(trim(nome.value)==""){
		alert("O nome dos presenteados deve ser informado");
		nome.focus();
		return false;
	}

	if(trim(data_evento.value)==""){
		alert("A data do evento deve ser informada");
		data_evento.focus();
		return false;
	}

	if(trim(link_seo.value)==""){
		alert("O link da lista deve ser informado");
		link_seo.focus();
		return false;
	}

	if(endereco.value=="0" || endereco.value==""){
		alert("O endereço de entrega deve ser informado");
		endereco.focus();
		return false;
	}

	
}


function add_lista_presente(){
	var id = document.getElementById("produtoid");
	var tamanho = document.getElementById("tamanho");
	var propriedade = document.getElementById("propriedade");


	if(parseInt(id.value)==0){
		alert("Ocorreu um erro ao processar seu produto.");
		window.location.href = "index.php?ref=produto-erro";
		return;
	}
	
	if(tamanho.value==-1){
		alert("Selecione o tamanho desejado");
		return;
	}

	if(propriedade.value==-1){
		alert("Selecione uma opção");
		return;
	}


	if($("#box-avise-me").is(":visible")){
		alert("Produto indisponível");
		return;
	}
	
	
	var scontent = $("#box-lista-presente").html();
	//scontent = (scontent + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
	
	$("#box-lista-presente").html('<img src="images/spinner.gif" border="0" /> Processando, aguarde...');
	
	//envia o post para a lista de presente
	//listaid vai pela sessão intval($_SESSION["lista_presente"])
	var action = "add_lista_presente";
	$.ajax({
		type: "POST",
		url: "ajax_produto.php",
		dataType: "html",
		timeout: 30000,
		data: {action: action, produto: id.value, tamanho: tamanho.value, propriedade: propriedade.value},
		success: function(data)
		{
			var ret = data.split("||");
			if(ret[0]=="ok"){			
				$("#box-lista-presente").html("Produto adicionado a lista com sucesso");
				//$("#box-lista-presente").html(scontent);
			}
			else
			{
				$("#box-lista-presente").html(ret[1]);	
			}
			
			
		},
		error: function (jqXHR, textStatus, errorThrown)
		{	
			alert(textStatus + "  " + errorThrown);
			$("#box-lista-presente").html(scontent);
		}
	});		
	

}


/*-----------------------------------------*/
function gera_link_seo(texto, destino){
	var action = "url_seo";
	
	$.ajax({
        type: "POST",
        url: "ajax_produto.php",
        dataType: "html",
		timeout: 30000,
        data: {action: action, string: texto},
        success: function(data)
        {
			data = data.toLowerCase();
			$("#"+destino).attr("value",data);		
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	//alert(textStatus);
        }
    });	
}



/*---------------------------------------------------------------*/
function cielo_ws_getauthorized(){
	var cartao_titular	= document.getElementById("cartao-titular");
	var cartao_numero	= document.getElementById("cartao-numero");	
	var cartao_validade	= document.getElementById("cartao-validade");
	var cartao_codseguranca	= document.getElementById("cartao-codseguranca");
	var parcelas 	= document.getElementById("parcelas"); 
	var endereco_fatura = document.getElementById("endereco-fatura");
	
	var pagamento = 0;

	$.each($('.radio-pagamento'), function(key, value) { 
		var $this = $( this ); 
		if($this.attr("checked")){
			pagamento = $this.attr("value");
		}
	});	

	var action = "ws-gravar";
	var hash_cartao_numero = ""+right(cartao_numero.value,4);
	
	var scartao_titular	= cartao_titular.value;
	var scartao_numero	= cartao_numero.value;	
	var scartao_validade	= cartao_validade.value;
	var scartao_codseguranca	= cartao_codseguranca.value;
	var sparcelas 	= parcelas.value; 
	var sendereco_fatura 	= endereco_fatura.value;
		
	//limpa os dados do cartao da tela		
	clear_dados_cartao();
	
	
	$.ajax({
        type: "POST",
        url: "pagamento.php",
        dataType: "html",
		timeout: 30000,
        data: {action: action, pagamento: pagamento, 'cartao-titular':scartao_titular, 'cartao-numero': hash_cartao_numero, 'parcelas': sparcelas, 'endereco-fatura': sendereco_fatura},
        success: function(data)
        {
			
			if(data=="ok"){
				$('#msg-cartao').html('<img src="images/spinner.gif" /> Validando cartão de crédito, aguarde...');
				
				/*--------------------------------------------------*/
				action = "requisicao-transacao"; //cielo webservice
				
				$.ajax({
					type: "POST",
					url: "ajax_webservice_gateway.php",
					dataType: "html",
					timeout: 30000,
					data: {action: action, cartao_titular: scartao_titular, cartao_numero: scartao_numero, cartao_validade:scartao_validade, cartao_codseguranca:scartao_codseguranca},
					success: function(data)
					{
						
						var ret = data.split("||");
						if(ret[0]=="0"){
							$('#msg-cartao').html('<img src="images/spinner.gif" /> Transação autorizada');
							window.location.href = "confirmacao-compra.php";
						}else{
							$('#msg-cartao').html(String(ret[1]));
							$("#cmd_continuar").removeAttr("disabled");
						}
						
						
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						$('#msg-cartao').html('Erro: Ocorreu um erro ao processar sua solicitação tente novamente mais tarde. Descrição do erro: '+ errorThrown);
						$("#cmd_continuar").removeAttr("disabled");
					}
				});	
				
				
				
				//$("#frm_pagamento").submit();
				/*--------------------------------------------------*/
					
						
			}else{
				$('#cartao-msg').html(data);
				$("#cmd_continuar").removeAttr("disabled");
				return false;
			}
			
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
			$('#cartao-msg').html('Erro ao processar, tente novamente.');
			$("#cmd_continuar").removeAttr("disabled");
			return false;
        }
    });	
	
	
}


function clear_dados_cartao(){
	$("#cartao-titular").attr("value","");
	$("#cartao-numero").attr("value","");
	$("#cartao-validade").attr("value","");
	$("#cartao-codseguranca").attr("value","");
	
	//$("#dados-cart").hide();		
}


/*---------------------------------------------------------------*/
function redecard_ws_getauthorized(){
	
	var cartao_titular	= document.getElementById("cartao-titular");
	var cartao_numero	= document.getElementById("cartao-numero");	
	var cartao_validade	= document.getElementById("cartao-validade");
	var cartao_codseguranca	= document.getElementById("cartao-codseguranca");
	var parcelas 	= document.getElementById("parcelas"); 
	var endereco_fatura = document.getElementById("endereco-fatura");
	
	
	var pagamento = 0;

	$.each($('.radio-pagamento'), function(key, value) { 
		var $this = $( this ); 
		if($this.attr("checked")){
			pagamento = $this.attr("value");
		}
	});	
	
	var action = "ws-gravar";
	var hash_cartao_numero = ""+right(cartao_numero.value,4);
	$.ajax({
        type: "POST",
        url: "pagamento.php",
        dataType: "html",
		timeout: 30000,
        data: {action: action, pagamento: pagamento, 'cartao-titular':cartao_titular.value, 'cartao-numero': hash_cartao_numero, parcelas: parcelas.value, 'endereco-fatura': endereco_fatura.value},
        success: function(data)
        {
			if(data=="ok"){
				$('#msg-cartao').html('<img src="images/spinner.gif" /> Validando cartão de crédito, aguarde...');
				
				
				/*--------------------------------------------------*/
				action = "getauthorized";
				
				$.ajax({
					type: "POST",
					url: "ajax_webservice_gateway.php",
					dataType: "html",
					timeout: 30000,
					data: {action: action, cartao_titular: cartao_titular.value, cartao_numero: cartao_numero.value, cartao_validade:cartao_validade.value, cartao_codseguranca:cartao_codseguranca.value},
					success: function(data)
					{
						//alert(data);
						var ret = data.split("||");
						if(ret[0]=="0"){
							$('#msg-cartao').html('<img src="images/spinner.gif" /> Cartão de crédito validado com sucesso, aguarde...');
							window.location.href = "confirmacao-compra.php";
						}else{
							$('#msg-cartao').html(ret[1]);
							$("#cmd_continuar").removeAttr("disabled");
						}
						
						
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						$('#msg-cartao').html('Erro: Ocorrer um erro ao processar sua solicitação tente novamente mais tarde. Descrição do erro: '+ errorThrown);
						$("#cmd_continuar").removeAttr("disabled");
					}
				});	
				
				
				
				//$("#frm_pagamento").submit();
				/*--------------------------------------------------*/
					
						
			}else{
				$('#cartao-msg').html(data);
				$("#cmd_continuar").removeAttr("disabled");
			}
			
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
			$('#cartao-msg').html('Erro ao processar, tente novamente.');
			$("#cmd_continuar").removeAttr("disabled");
        }
    });	
	
	
}



function valida_confirmacao_compra(tipo){
	//webservice
	if(tipo>=2 && tipo<=3){
		
		$('#msg-cartao-meio').html('<img src="images/spinner.gif" /> Processando dados, aguarde...');
		
		action = "confirmtxn";
		$.ajax({
			type: "POST",
			url: "ajax_webservice_gateway.php",
			dataType: "html",
			timeout: 30000,
			data: {action: action},
			success: function(data)
			{
				//alert(data);
				var ret = data.split("||");
				if(parseInt(ret[0])==0 || parseInt(ret[0])==1){
					//$('#msg-cartao-meio').html("");
					$("#frm_confirmacao").attr('action','confirmacao-compra.php');
					document.getElementById("frm_confirmacao").submit();
				}else{
					$('#msg-cartao-meio').html(ret[1]);
				}
				
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				$('#msg-cartao-meio').html('Erro: Ocorrer um erro ao processar sua solicitação, reinicie o processo de pagamento. Descrição do erro: '+ errorThrown);
			}
		});			
		
		return false;
	}
	$("#frm_confirmacao").submit();
}



/*-----------------------------------------------------------------*
  Descricao   : Query String
*-----------------------------------------------------------------*/

qs=new Array();
variaveis=location.search.replace(/\x3F/,"").replace(/\x2B/g," ").split("&");
if(variaveis!=""){
	for(i=0;i<variaveis.length;i++){
		nvar=variaveis[i].split("=");
		qs[nvar[0]]=unescape(nvar[1]);
	}
}

function query_string(variavel){
	return qs[variavel];
}



/*-----------------------------------------------------------------*
  Descricao   : Tira brancos à esquerda e à direita
 *-----------------------------------------------------------------*/
function trim(s)
{
    return s.replace (/^\s+/,'').replace (/\s+$/,'');
}




/*-----------------------------------------------------------------------------------------*
 Descricao...: pega X caratres a esquerda
 Paramentros.: data: 
 Retorno.....: string
 *-----------------------------------------------------------------------------------------*/
function left(texto, posicao){
	
	return texto.substr(0,posicao);

}




/*-----------------------------------------------------------------------------------------*
 Descricao...: pega X caratres a direita
 Retorno.....: string
 *-----------------------------------------------------------------------------------------*/
function right(texto, posicao){
	
	return texto.substr(texto.length-posicao , posicao);

}




/*-----------------------------------------------------------------------------------------*
 Descricao...: pega X caratres da posicao informada
 Retorno.....: string
 *-----------------------------------------------------------------------------------------*/
function mid(texto, inicio, tamanho){
	
	return texto.substr(inicio , tamanho);

}


function get_only_numbers(texto){
	var numero = "";

	valor = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

	for (n = 0; n < texto.length; n++)
	{

		for (a = 0; a < 10; a++)
		{
			if (texto.substr(n, 1) == valor[a])
			{
				numero = numero + texto.substr(n, 1);
				break;
			}
		}
	}

	return numero;
}


/*-----------------------------------------------------------------------------------------*
 Descricao...: Verifica se o E-mail é válido
 Paramentros.: email: String de E-mail
 Retorno.....: true(ok) ou false(nok)
 *-----------------------------------------------------------------------------------------*/
function ValidaEmail(email)
{
   var aux;
   //checando se a string não é vazia
   if((email.replace (/^\s+/,'').replace (/\s+$/,'') == ""))
   {
      return false;
   }
   //checando se existe pelo menos uma arroba e pelo menos algum ponto
   if((email.indexOf("@") == -1)||(email.indexOf(".") == -1))
   {
      return false;
   }
   //checando se a string tem pelo menos 5 caracteres
   if(email.length<5)
   {
      return false;
   }
   //checando se existe brancos
   if(email.indexOf(" ") != -1)
   {
      return false;
   }
   //checando se depois de . não tem outra @, ou um ponto ou espaço
   if((email.substr(email.lastIndexOf(".")+1,1) == "")||(email.substr(email.indexOf(".")+1,1) == "@")||(email.substr(email.indexOf(".")+1,1) == "."))
   {
      return false;
   }
   //checando se depois de @ não tem outra @, ou um ponto ou espaço
   if((email.substr(email.lastIndexOf("@")+1,1) == "")||(email.substr(email.indexOf("@")+1,1) == "@")||(email.substr(email.indexOf("@")+1,1) == "."))
   {
      return false;
   }
   //procurando por mais de uma @
   aux = email.substr(email.indexOf("@")+1);
   if(aux.indexOf("@") != -1)
   {
      return false;
   }
   //checando se o primeiro caracter é @
   if(email.substr(0, 1) == "@")
   {
      return false;
   }
   return true;
}
  function replace_param(theURL, paramName, newValue){
    var uri_array = theURL.split('?');
    try{
	  var params_array = uri_array[1].split('&');
	}catch(e){
	  if (theURL.substring(theURL.length-1) == "/"){
	    theURL = theURL+'?'+paramName+'=1';
	  }else{
	    theURL = theURL+'/?'+paramName+'=1';
	  }
	  var uri_array = theURL.split('?');
	  var params_array = uri_array[1].split('&');
	}
	var achou = 0;
    var items_array = new Array;
    for (i=0; i<params_array.length; i++){
      items_array = params_array[i].split('='); 
      if (items_array[0] == paramName){
        params_array[i] = items_array[0] + '=' + newValue;
		achou = 1;
      }
    }
	var link = uri_array[0] + '?' + params_array.join('&');
	if(achou == 0){
	  link = link+'&'+paramName+'='+newValue;
	}
    return link;
  }
  function pag_anterior(pagina){
	if(pagina > 0){
		var url = replace_param(document.URL, 'pagina', parseInt(pagina-1));
		window.location = url;
	}
  }
  function pag_posterior(pagina,limite){
  	if(pagina < limite){
		var url = replace_param(document.URL, 'pagina', parseInt(pagina*1+1));
		window.location = url;
	}
  }
  
  function remove_atributos_menu(){
	$('.item,.drop-down').removeAttr('style');
  }
  
  function produtos_menu(obj){
	remove_atributos_menu();
	$(obj).attr('style','background-color: #fff;color: #2578c7 !important;');
	$('#produto-menu-box').attr('style','display:block !important;');
  }
  function fabricantes_menu(obj){
	remove_atributos_menu();
	$(obj).attr('style','background-color: #fff;color: #2578c7 !important;');
	$('#fabricantes-menu-box').attr('style','display:block !important;');
  }
  function kits_menu(obj){
	remove_atributos_menu();
	$(obj).attr('style','background-color: #fff;color: #2578c7 !important;');
	$('#kits-menu-box').attr('style','display:block !important;');
  }
  function objetivos_menu(obj){
	remove_atributos_menu();
	$(obj).attr('style','background-color: #fff;color: #2578c7 !important;');
	$('#objetivos-menu-box').attr('style','display:block !important;');
  }
  function top20_menu(obj){
	remove_atributos_menu();
	$(obj).attr('style','background-color: #fff;color: #2578c7 !important;');
	$('#top20-menu-box').attr('style','display:block !important;');
  }