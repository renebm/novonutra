<?php

include("include/inc_conexao.php");

$action = $_REQUEST["action"];


	/*------------------------------------------------------------------------
	verifica se ta logado
	--------------------------------------------------------------------------*/
	if(!isset($_SESSION["cadastro"])){
		exit();
	}else{
		$cadastro = $_SESSION["cadastro"];
		if(!is_numeric($cadastro)){
			exit();
		}
	}


	/*------------------------------------------------------------------------
	verifica se tem algum orcamento em aberto com base em cookies
	--------------------------------------------------------------------------*/
	if(isset($_COOKIE["orcamento"])){
		$orcamento = $_COOKIE["orcamento"];
		if(!is_numeric($orcamento)){
			$orcamento = 0;	
		}		
	}
	if($orcamento==0){
		exit();
	}	



/*--------------------------------------------------------------------------------------------------------------------------------------------
	CIELO	- 	WEBSERVICE
---------------------------------------------------------------------------------------------------------------------------------------------*/
if($action=="requisicao-transacao"){
	
	/*------------------------------------------------------------------------
	resgata os dados do orcamento/pedido
	--------------------------------------------------------------------------*/
	$total = 0;
	$parcelas = 0; 
	$ssql = "select tblorcamento.orcamentoid, tblorcamento.ovalor_total, tblcondicao_pagamento.cnumero_parcelas, tblforma_pagamento.fbandeira
				from tblorcamento
				inner join tblcondicao_pagamento on tblorcamento.ocodcondicao_pagamento=tblcondicao_pagamento.condicaoid
				inner join tblforma_pagamento on tblorcamento.ocodforma_pagamento=tblforma_pagamento.formapagamentoid
				where tblorcamento.orcamentoid='{$orcamento}'";
	//echo $ssql."<br />";

	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$total		=	number_format($row["ovalor_total"],2,"","");
			$parcelas	=	$row["cnumero_parcelas"];
			$bandeira	=	$row["fbandeira"];
		}
		mysql_free_result($result);
	}
	
	
	if($parcelas=="1"){
		$produto = "1";		//a vista
	}
	else
	{
		$produto = "2";		//parcelado estabelecimento
	}
	
	
	if(get_cadastro($cadastro,"cemail")=="fabiocalixto@uol.com.br"){
		//$total = "100";	
	}
	

	/*------------------------------------------------------------------------
	resgata código de fialiacao
	--------------------------------------------------------------------------*/
	$filiacao = get_configuracao("cielo_filiacao");
	$chave = get_configuracao("cielo_chave");
	
	$ip = left($_SERVER['REMOTE_ADDR'],20);
	
	if($filiacao=="1006993069" || $chave=="25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3"){	// TESTES E HOMOLOGACAO
		gera_log("\r\n\r\n".$xml_msg."\r\n\r\n");
		$ws 	= "https://qasecommerce.cielo.com.br/servicos/ecommwsec.do";
		$total 	= left($total,strlen($total)-2) . "00";
	}else{
		
		if($_SERVER['SERVER_NAME']=="servidor"){
			gera_log("\r\n\r\n".$xml_msg."\r\n\r\n");	
		}
		
		$ws = "https://ecommerce.cielo.com.br/servicos/ecommwsec.do";
	}	
	
	/*------------------------------------------------------------------------
	web service
	--------------------------------------------------------------------------*/	
	$cartao_titular		=	addslashes($_REQUEST["cartao_titular"]);
	$cartao_titular		=	urlencode($cartao_titular);
	
	$cartao_numero		=	addslashes($_REQUEST["cartao_numero"]);
	$cartao_numero		=	get_only_numbers($cartao_numero);
	
	$cartao_validade	=	addslashes($_REQUEST["cartao_validade"]);
	list($mes,$ano)		=	explode("/",$cartao_validade);
	$cartao_validade	= 	"20".$ano.$mes;
	
	$cartao_codseguranca=	addslashes($_REQUEST["cartao_codseguranca"]);	
	
	$cartao_bin			=	left($cartao_numero,6);
	
$xml_msg = '<?xml version="1.0" encoding="ISO-8859-1"?>
		<requisicao-transacao id="'.$orcamento.'" versao="1.2.0">
		<dados-ec>
		<numero>'.$filiacao.'</numero>
		<chave>'.$chave.'</chave>
		</dados-ec>
		<dados-portador>
		<numero>'.$cartao_numero.'</numero>
		<validade>'.$cartao_validade.'</validade>
		<indicador>1</indicador>
		<codigo-seguranca>'.$cartao_codseguranca.'</codigo-seguranca>
		</dados-portador>
		<dados-pedido>
		<numero>'.$orcamento.'</numero>
		<valor>'.$total.'</valor>
		<moeda>986</moeda>
		<data-hora>'.date("Y-m-d").'T'.date("H:i:s").'</data-hora>
		<descricao>[origem:'.$ip.']</descricao>
		<idioma>PT</idioma>
		<soft-descriptor></soft-descriptor>
		</dados-pedido>
		<forma-pagamento>
		<bandeira>'.$bandeira.'</bandeira>
		<produto>'.$produto.'</produto>
		<parcelas>'.$parcelas.'</parcelas>
		</forma-pagamento>
		<url-retorno>confirmacao-compra.php</url-retorno>
		<autorizar>3</autorizar>
		<capturar>false</capturar>
		<campo-livre>'.$orcamento.'</campo-livre>
		<bin>'.$cartao_bin.'</bin>
		</requisicao-transacao>';
			
		
		
		$xml_string = httprequest($ws, "mensagem=" . $xml_msg);

		$xml = simplexml_load_string($xml_string);
			
		
		$codigo = $xml->{'autorizacao'}->codigo;
		$lr = $xml->{'autorizacao'}->lr;			// codigo == 00  autorizado
		$msg = $xml->{'autorizacao'}->mensagem;	
		$arp = $xml->{'autorizacao'}->arp;
		$nsu = $xml->{'autorizacao'}->nsu;
		$tid = $xml->tid;
		$pan = $xml->pan;
		
		
				
		if(count($xml) <= 1 || $lr != "00"){
			
			echo "erro||".$msg."";	
			
			$xml_string = utf8_encode($xml_string);
			
			$log = date("Y-m-d H:i:s") . " - Erro pedido # ".$orcamento." na autorização do cartão de crédito:\r\n".$xml_string."\r\n";
			$log = utf8_decode($log);
			gera_log($log);
			
			/*------------------------------------------------------------------------
			atualiza retorno do cartao no orcamento
			--------------------------------------------------------------------------*/	
			$xml_string = '<?xml version="1.0" encoding="ISO-8859-1"?>';
			$xml_string .= '<autorizacao>';
			$xml_string .= '<tid>'.$tid.'</tid>';
			$xml_string .= '<pan>'.$pan.'</pan>';
			$xml_string .= '<codigo>'.$codigo.'</codigo>';
			$xml_string .= '<lr>'.$lr.'</lr>';
			$xml_string .= '<mensagem>'.$msg.'</mensagem>';
			$xml_string .= '<arp>'.$arp.'</arp>';
			$xml_string .= '<nsu>'.$nsu.'</nsu>';
			$xml_string .= '</autorizacao>';
			
			$ssql = "update tblorcamento set ocartao_retorno='{$xml_string}', odata_alteracao='{$data_hoje}' where orcamentoid='{$orcamento}'";
			mysql_query($ssql);
			exit();
		}	
				
		if($lr=="00"){
			/*------------------------------------------------------------------------
			atualiza retorno do cartao no orcamento
			--------------------------------------------------------------------------*/	
			$xml_string = '<?xml version="1.0" encoding="ISO-8859-1"?>';
			$xml_string .= '<autorizacao>';
			$xml_string .= '<tid>'.$tid.'</tid>';
			$xml_string .= '<pan>'.$pan.'</pan>';			
			$xml_string .= '<codigo>'.$codigo.'</codigo>';
			$xml_string .= '<lr>'.$lr.'</lr>';
			$xml_string .= '<mensagem>'.$msg.'</mensagem>';
			$xml_string .= '<arp>'.$arp.'</arp>';
			$xml_string .= '<nsu>'.$nsu.'</nsu>';
			$xml_string .= '</autorizacao>';
			
			$ssql = "update tblorcamento set ocartao_retorno='{$xml_string}', odata_alteracao='{$data_hoje}' where orcamentoid='{$orcamento}'";
			mysql_query($ssql);
			
			echo "0||Transação autorizada.";
		}
}









function httprequest($paEndereco, $paPost){
	
	
	$sessao_curl = curl_init();
	curl_setopt($sessao_curl, CURLOPT_URL, $paEndereco);
	
	curl_setopt($sessao_curl, CURLOPT_FAILONERROR, true);

	//  CURLOPT_SSL_VERIFYPEER
	//  verifica a validade do certificado
	curl_setopt($sessao_curl, CURLOPT_SSL_VERIFYPEER, 0);
	//  CURLOPPT_SSL_VERIFYHOST
	//  verifica se a identidade do servidor bate com aquela informada no certificado
	//curl_setopt($sessao_curl, CURLOPT_SSL_VERIFYHOST, 2);

	//  CURLOPT_SSL_CAINFO
	//  informa a localização do certificado para verificação com o peer
	/*
	curl_setopt($sessao_curl, CURLOPT_CAINFO, getcwd() .
			"/include/VeriSignClass3PublicPrimaryCertificationAuthority-G5.crt");
	*/
	/*
	curl_setopt($sessao_curl, CURLOPT_CAINFO, getcwd() .
			"/include/nutracorporecombr_cert2015.cer");
	*/
	//curl_setopt($sessao_curl, CURLOPT_SSLVERSION, 3);

	//  CURLOPT_CONNECTTIMEOUT
	//  o tempo em segundos de espera para obter uma conexão
	curl_setopt($sessao_curl, CURLOPT_CONNECTTIMEOUT, 10);

	//  CURLOPT_TIMEOUT
	//  o tempo máximo em segundos de espera para a execução da requisição (curl_exec)
	curl_setopt($sessao_curl, CURLOPT_TIMEOUT, 40);

	//  CURLOPT_RETURNTRANSFER
	//  TRUE para curl_exec retornar uma string de resultado em caso de sucesso, ao
	//  invés de imprimir o resultado na tela. Retorna FALSE se há problemas na requisição
	curl_setopt($sessao_curl, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($sessao_curl, CURLOPT_POST, true);
	curl_setopt($sessao_curl, CURLOPT_POSTFIELDS, $paPost );

	$resultado = curl_exec($sessao_curl);
	
	curl_close($sessao_curl);

	if ($resultado)
	{
		return $resultado;
	}
	else
	{
		return curl_error($sessao_curl);
	}
}








/*--------------------------------------------------------------------------------------------------------------------------------------------
	REDECARD	- WEBSERVICE
---------------------------------------------------------------------------------------------------------------------------------------------*/

if($action=='getauthorized'){

	/*------------------------------------------------------------------------
	resgata os dados do orcamento/pedido
	--------------------------------------------------------------------------*/
	$total = 0;
	$parcelas = 0; 
	$ssql = "select tblorcamento.orcamentoid, tblorcamento.ovalor_total, tblcondicao_pagamento.cnumero_parcelas 
				from tblorcamento
				inner join tblcondicao_pagamento on tblorcamento.ocodcondicao_pagamento=tblcondicao_pagamento.condicaoid
				where tblorcamento.orcamentoid='{$orcamento}'";
	//echo $ssql."<br />";
	
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$total		=	number_format($row["ovalor_total"],2,".","");
			$parcelas	=	$row["cnumero_parcelas"];
		}
		mysql_free_result($result);
	}
	
	if($parcelas<10){
		$parcelas = "0".$parcelas;	
	}
	
	if($parcelas=="01"){
		$parcelas = "00";
		$transacao = "04";		//a vista	
	}
	else{
		$transacao = "08";		//parcelado estabelecimento
	}
	
	
	if(get_cadastro($cadastro,"cemail")=="fabiocalixto@uol.com.br"){
		$total = "1.00";	
	}
	
	
	
	/*------------------------------------------------------------------------
	resgata código de fialiacao
	--------------------------------------------------------------------------*/
	$filiacao = get_configuracao("redecard_filiacao");
	
	//echo $filiacao."";
	
	/*------------------------------------------------------------------------
	web service
	--------------------------------------------------------------------------*/	
	$cartao_titular		=	addslashes($_REQUEST["cartao_titular"]);
	$cartao_titular		=	urlencode($cartao_titular);
	
	$cartao_numero		=	addslashes($_REQUEST["cartao_numero"]);
	$cartao_numero		=	get_only_numbers($cartao_numero);
	$cartao_validade	=	addslashes($_REQUEST["cartao_validade"]);
	list($mes,$ano)		=	explode("/",$cartao_validade);
	
	$cartao_codseguranca=	addslashes($_REQUEST["cartao_codseguranca"]);
	
	//$ws = "https://ecommerce.redecard.com.br/pos_virtual/wskomerci/cap_teste.asmx/GetAuthorizedTst?TOTAL=".$total."&TRANSACAO=".$transacao."&PARCELAS=".$parcelas."&FILIACAO=".$filiacao."&NUMPEDIDO=".$orcamento."&NRCARTAO=".$cartao_numero."&CVC2=".$cartao_codseguranca."&MES=".$mes."&ANO=".$ano."&PORTADOR=".$cartao_titular."&IATA=&DISTRIBUIDOR=&CONCENTRADOR=&TAXAEMBARQUE=&ENTRADA=&NUMDOC1=&NUMDOC2=&NUMDOC3=&NUMDOC4=&PAX1=&PAX2=&PAX3=&PAX4=&CONFTXN=&ADDData=&ADD_Data=";
	$ws = "https://ecommerce.redecard.com.br/pos_virtual/wskomerci/cap.asmx/GetAuthorized?TOTAL=".$total."&TRANSACAO=".$transacao."&PARCELAS=".$parcelas."&FILIACAO=".$filiacao."&NUMPEDIDO=".$orcamento."&NRCARTAO=".$cartao_numero."&CVC2=".$cartao_codseguranca."&MES=".$mes."&ANO=".$ano."&PORTADOR=".$cartao_titular."&IATA=&DISTRIBUIDOR=&CONCENTRADOR=&TAXAEMBARQUE=&ENTRADA=&NUMDOC1=&NUMDOC2=&NUMDOC3=&NUMDOC4=&PAX1=&PAX2=&PAX3=&PAX4=&CONFTXN=&ADD_Data=";
	
	
	$xml_string = file_get_contents($ws);
	$xml = simplexml_load_string($xml_string);
	
	if(count($xml)<=1){
		echo "Erro:WS-001 Erro ao processar sua solicitação, tente novamente mais tarde. <br />Se o erro persistir entre em contato conosco pelo e-mail: $site_email.";	
		exit();
	}
	
	$codret	= $xml->CODRET;
	$msgret	=  utf8_encode(urldecode($xml->MSGRET));

	/*------------------------------------------------------------------------
	atualiza retorno do cartao no orcamento
	--------------------------------------------------------------------------*/	
	$ssql = "update tblorcamento set ocartao_retorno='{$xml_string}', odata_alteracao='{$data_hoje}' where orcamentoid='{$orcamento}'";
	mysql_query($ssql);


	if($codret!=0){
		
		$log = $data_hoje . " - Erro ao validar cartão de crédito pedido # ".$orcamento."  Retorno: [ ".$codret."||".$msgret." ] \r\n";
		gera_log($log);
		
		echo $codret."||".$msgret." Confira todos os dados digitado e tente novamente mais tarde. <br />Se o erro persistir entre em contato conosco pelo e-mail: $site_email.";	
		exit();		
	}
	
	$log = $data_hoje . " - Cartão de crédito aprovado pedido # ".$orcamento."  Retorno: [ ".$codret."||".$msgret." ] \r\n";
	gera_log($log);	

	echo $codret."||".$msgret;

}


/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/

if($action=='confirmtxn'){
	
	
	/*------------------------------------------------------------------------
	resgata código de fialiacao
	--------------------------------------------------------------------------*/
	$filiacao = get_configuracao("redecard_filiacao");	
	
	/*------------------------------------------------------------------------
	resgata os dados do orcamento/pedido
	--------------------------------------------------------------------------*/
	$xml_string;
	$total = 0;
	$parcelas = 0; 
	$ssql = "select tblorcamento.orcamentoid, tblorcamento.ovalor_total, tblorcamento.ocartao_retorno, tblcondicao_pagamento.cnumero_parcelas 
				from tblorcamento
				inner join tblcondicao_pagamento on tblorcamento.ocodcondicao_pagamento=tblcondicao_pagamento.condicaoid
				where tblorcamento.orcamentoid='{$orcamento}'";
	//echo $ssql."<br />";
	
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$xml_string	=	$row["ocartao_retorno"];
			$total		=	number_format($row["ovalor_total"],2,".","");
			$parcelas	=	$row["cnumero_parcelas"];
		}
		mysql_free_result($result);
	}
	
	if($parcelas<10){
		$parcelas = "0".$parcelas;	
	}

	
	if($parcelas=="01"){
		$parcelas = "00";
		$transacao = "04";		//a vista	
	}
	else{
		$transacao = "08";		//parcelado estabelecimento
	}
	
	if(get_cadastro($cadastro,"cemail")=="fabiocalixto@uol.com.br"){
		$total = "1.00";	
	}	
	
	
	$xml = simplexml_load_string($xml_string);
	if(count($xml)<=1){
		echo "Erro:WS-002 Erro ao processar sua solicitação, tente novamente mais tarde. <br />Se o erro persistir entre em contato conosco pelo e-mail: $site_email.";	
		exit();
	}

	//echo $xml_string;
	//exit();

	$data		= $xml->DATA;
	$numsqn		= $xml->NUMSQN;
	$numcv		= $xml->NUMCV;
	$numautor	= $xml->NUMAUTOR;
	
	/*------------------------------------------------------------------------
	web service
	--------------------------------------------------------------------------*/	
	//$ws = "https://ecommerce.redecard.com.br/pos_virtual/wskomerci/cap_teste.asmx/ConfirmTxnTst?DATA=".$data."&NUMSQN=".$numsqn."&NUMCV=".$numcv."&NUMAUTOR=".$numautor."&PARCELAS=".$parcelas."&TRANSORIG=".$transacao."&TOTAL=".$total."&FILIACAO=".$filiacao."&DISTRIBUIDOR=&NUMPEDIDO=".$orcamento."&NUMDOC1=&NUMDOC2=&NUMDOC3=&NUMDOC4=&PAX1=&PAX2=&PAX3=&PAX4=&CONFTXN=&ADDData=&ADD_Data=";
	$ws = "https://ecommerce.redecard.com.br/pos_virtual/wskomerci/cap.asmx/ConfirmTxn?DATA=".$data."&NUMSQN=".$numsqn."&NUMCV=".$numcv."&NUMAUTOR=".$numautor."&PARCELAS=".$parcelas."&TRANSORIG=".$transacao."&TOTAL=".$total."&FILIACAO=".$filiacao."&DISTRIBUIDOR=&NUMPEDIDO=".$orcamento."&NUMDOC1=&NUMDOC2=&NUMDOC3=&NUMDOC4=&PAX1=&PAX2=&PAX3=&PAX4=&CONFTXN=&ADDData=&ADD_Data=";

	//echo $ws;
	//exit();
	
	$xml_string = file_get_contents($ws);
	$xml = simplexml_load_string($xml_string);
	
	if(count($xml)<=1){
		echo "Erro:WS-003 Erro ao processar sua solicitação, tente novamente mais tarde. <br />Se o erro persistir entre em contato conosco pelo e-mail: $site_email.";	
		exit();
	}
	
	$codret	= $xml->CODRET;
	$msgret	=   utf8_encode(urldecode($xml->MSGRET));


	/*------------------------------------------------------------------------
	atualiza retorno do cartao no orcamento
	--------------------------------------------------------------------------*/	
	//$ssql = "update tblorcamento set ocartao_retorno='{$xml_string}', odata_alteracao='{$data_hoje}' where orcamentoid='{$orcamento}'";
	//mysql_query($ssql);


	if($codret>=2){
		
		$log = $data_hoje . " - Erro ao confirmar cartão de crédito pedido # ".$orcamento."  Retorno: [ ".$codret."||".$msgret." ] \r\n";
		gera_log($log);
		
		echo $codret."||".$msgret.", reinicie o processo de pagamento clicano em voltar. <br />Se o erro persistir entre em contato conosco pelo e-mail: $site_email. ";	
		exit();		
	}
	
	$log = $data_hoje . " - Cartão de crédito aprovado e confirmado pedido # ".$orcamento."  Retorno: [ ".$codret."||".$msgret." ] \r\n";
	gera_log($log);	

	echo $codret."||".$msgret;

}
?>
