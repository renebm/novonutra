<?php
	include("include/inc_conexao.php");

	/*-------------------------------------------------------------
	verifica se ta logado
	-------------------------------------------------------------*/
	if(isset($_SESSION["cadastro"])){
		$cadastro = $_SESSION["cadastro"];
		if(!is_numeric($cadastro)){
			header("location: login.php?redir=criar-lista-presente.php");
			exit();
		}
	}
	else
	{
		header("location: login.php?redir=criar-lista-presente.php");
		exit();
	}
		
	$id		= intval($_REQUEST['id']);
	$msg 	= "Criar Lista de Presente";
	$erro	= 0;

	if( $_REQUEST['action']=='gravar' ){
		
		$id 			=	intval($_REQUEST['id']);
		
		$titulo			=	addslashes(trim($_REQUEST['lista_titulo']));
		$nome			=	addslashes(trim($_REQUEST['lista_nome']));
		$data_evento	=	formata_data_db(addslashes($_REQUEST['lista_data_evento']));
		
		$link_seo		=	"lista-presente/".addslashes(trim($_REQUEST['lista_link_seo']));
		$link_seo		=	str_replace("lista-presente/lista-presente/","lista-presente/",$link_seo);
		
		$texto			=	addslashes(trim($_REQUEST['lista_texto']));
		$imagem			= 	"";
		$endereco		=	intval($_REQUEST['lista_endereco']);
		
		$credito		=	intval($_REQUEST['lista_credito']);
		$notificacao	=	intval($_REQUEST['lista_notificacao']);
		
		
		//verifica se já não existe a lista
		if($id == 0 ){
			$ssql = "select listaid from tbllista_presente where ltitulo='{$titulo}'";
			$result = mysql_query($ssql);
			$num_rows = mysql_num_rows($result);
			
			if( $num_rows > 0 ){
				
				$msg 	= "O nome da lista já existe escolha outro nome.";
				$erro 	= 1;
				
			}
			else
			{
				
				$ssql = "insert into tbllista_presente (lcodcadastro, ltitulo, lnome, ldata_evento, llink_seo, ltexto, limagem, lcodendereco, lcredito, lnotificacao, ldata_alteracao, ldata_cadastro)
						VALUES('{$cadastro}','{$titulo}', '{$nome}','{$data_evento}','{$link_seo}','{$texto}','{$imagem}','{$endereco}','{$credito}','{$notificacao}','{$data_hoje}','{$data_hoje}');
						";
				$result = mysql_query($ssql);
				if(!$result){
					$msg  = "Erro ao incluir lista. " . mysql_error($conexao);	
				}
				else
				{
					$msg  = "Lista criada com sucesso.";	
				}
				
				$id	= mysql_insert_id();
			}
			
		}
		else
		{
				
				//verifica se já não existe a lista
				$ssql = "select listaid from tbllista_presente where ltitulo='{$titulo}' and listaid <> '{$id}' ";
				$result = mysql_query($ssql);
				$num_rows = mysql_num_rows($result);
				
				if( $num_rows > 0 ){
					$msg 	= "O nome da lista já existe escolha outro nome.";
					$erro 	= 1;
				}
				else
				{
				
				
					
					$tipo = intval(get_lista_presente($id,"lcredito"));
					
					if( $credito != $tipo){
						
						$ssql1 = "select pcodlista_presente from tblpedido where pcodlista_presente = '{$id}' ";
						$result1 = mysql_query($ssql1);
						if( mysql_num_rows($result1) > 0 ){
							$erro = 1;
							$msg = "Você não pode mais mudar o tipo de lista: ";
							
							if( $tipo == 0 ){
								$msg .= "Você optou por receber os produtos.";
							}
							if( $tipo == -1 ){
								$msg .= "Você optou por receber em crédito.";
							}
							
							
						}
						
						
					}
				
					
					if($erro==0){
				
						$ssql = "update tbllista_presente set ltitulo='{$titulo}', lnome='{$nome}', ldata_evento='{$data_evento}', llink_seo='{$link_seo}', ltexto='{$texto}',
								lcredito='{$credito}', lnotificacao='{$notificacao}'
								where listaid = '{$id}' and lcodcadastro = '{$cadastro}'
								";
						$result = mysql_query($ssql);
						if(!$result){
							$msg  = "Erro ao atualizar lista. " . mysql_error($conexao);	
						}
						else
						{
							$msg  = "Lista atualizada com sucesso.";			
						}
						
					}
					
				}
			
		}
		
	}

	
	
	if( $id > 0 ){
		$ssql = "select listaid, lcodcadastro, ltitulo, lnome, ldata_evento, llink_seo, ltexto, limagem, lcodendereco, lcredito, lnotificacao,
				ldata_alteracao, ldata_cadastro
				from tbllista_presente where lcodcadastro = '{$cadastro}' and listaid = '{$id}'
				";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$id				= 	$row["listaid"];
				$titulo			=	$row["ltitulo"];
				$nome			=	$row["lnome"];
				$data_evento	=	formata_data_tela($row["ldata_evento"]);
				$link_seo		=	$row["llink_seo"];
				$texto			=	$row["ltexto"];
				$endereco		=	$row["lcodendereco"];
				
				$credito		=	$row["lcredito"];
				$notificacao	=	$row["lnotificacao"];
			}
			mysql_free_result($result);
		}	
	}
	
		

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Lista de Presente</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Meus Pedidos" />
<meta name="description" content="<?php echo $site_nome;?> Meus Pedidos. Acompanhe o status e histórico de suas compras." />
<meta name="keywords" content="<?php echo $site_nome;?> Meus Pedidos" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?>  Meus Pedidos" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/lista-presente.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		$("#lista_data_evento").mask("99/99/9999");
		$("#lista_titulo").focus();
		
		$(".ico-refresh").live('click',function(){
					
			var _loading	= '<img src="images/spinner.gif" border="0" />';		
			
			var listaid		= "<?php echo intval($id);?>";
			var produto 	= $(this).attr("produto");
			var propriedade = $(this).attr("propriedade");
			var tamanho 	= $(this).attr("tamanho");
			var qtde		= $("#qtde-"+produto).val();
			
			var id			= String(produto)+"-"+String(propriedade)+"-"+String(tamanho);
			
			var _content	= $("#"+id).html();
			
			$("#"+id).html(_loading);
		
			var action = "update_lista_presente_produto";
			$.ajax({
				type: "POST",
				url: "ajax_produto.php",
				dataType: "html",
				timeout: 10000,
				data: {action: action, listaid: listaid, produto: produto, tamanho: tamanho, propriedade: propriedade, qtde: qtde},
				success: function(data)
				{	
					var ret = data.split("||");
					if(ret[0]=="ok"){			
						$("#"+id).html(_content);
						if(qtde==0){
							$("#tr-"+id).hide();	
						}
					}
					else
					{
						$("#"+id).html(_content);
						alert(ret[1]);
					}
				},
				error: function (jqXHR, textStatus, errorThrown)
				{	
					alert(textStatus + "  " + errorThrown);
				}
			});
			
		
		});
		
		
		$(".class_qtde").keydown(function(){
			if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
				(event.keyCode == 65 && event.ctrlKey === true) || 
				(event.keyCode >= 35 && event.keyCode <= 39)) {
					 return;
			}
			else {
				if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
					event.preventDefault(); 
				}   
			}										  
		});
		
		
    });	
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">


    <div id="menu-conta-left">
        <?php include("inc_menu_mc.php");?>
    </div>
    
    <div id="box-meio-minha-conta">
    	<div id="box-meus-dados">
        	<h2 class="h2-pg-meus-pedidos">Lista de Presente</h2>
            
            <h4 class="h4-minha-conta"><?php echo $msg;?></h4>
            
       	  <form name="frm_pedidos" id="frm_pedidos" method="post" action="criar-lista-presente.php" onsubmit="return valida_lista_presente();" >
            <input type="hidden" name="action" id="action" value="gravar" />
            <input type="hidden" name="id" id="id" value="<?php echo intval($id);?>" />          
            
            <div class="box-minha-lista"><span class="dados-minha-lista">Nome da Lista:</span> <input type="text" id="lista_titulo" name="lista_titulo" class="filtro-minha-lista" value="<?php echo $titulo;?>" maxlength="100" onblur="javascript:gera_link_seo(this.value,'lista_link_seo');" /></div>
            <div class="box-minha-lista"><span class="dados-minha-lista">Nome dos Presenteados:</span> <input name="lista_nome" type="text" class="filtro-minha-lista" id="lista_nome" value="<?php echo $nome;?>" maxlength="100" /></div>
            <div class="box-minha-lista"><span class="dados-minha-lista">Data do Evento:</span> <input name="lista_data_evento" type="text" class="filtro-meus-pedidos" id="lista_data_evento" value="<?php echo $data_evento;?>" /></div>
            <div class="box-minha-lista">
            	<span class="dados-minha-lista">URL da Lista:</span> <input name="lista_link_seo" type="text" class="filtro-minha-lista" id="lista_link_seo" value="<?php echo $link_seo;?>" readonly="readonly" />
            </div>
			<?php
                if(trim($link_seo)!=""){
                    echo '<div class="box-minha-lista">';
                    echo '<a href="'.$site_site . "/" . $link_seo.'">';
                    echo $site_site . "/" . $link_seo;	
                    echo '</a>';
                    echo '</div>';
                }
            ?>                          
            <div class="box-minha-lista"><span class="dados-minha-lista">Endereço de Entrega:</span>
          	<select name="lista_endereco" id="lista_endereco" class="filtro-minha-lista">
            	<option value="0">Selecione o endereço de entrega</option>
                <?php
                	$ssql = "select enderecoid, etitulo from tblcadastro_endereco where ecodcadastro='{$cadastro}'";
					$result = mysql_query($ssql);
					if($result){
						while($row=mysql_fetch_assoc($result)){
							echo '<option value="'.$row["enderecoid"].'"';
							if( intval($endereco) == intval($row["enderecoid"]) ){
								echo ' selected ';	
							}
							echo '>';
							echo $row["etitulo"];
							echo '</option>';
						}
						mysql_free_result($result);
					}					
				?>
            </select>
			
            &nbsp;&nbsp;[ <a href="cadastro-endereco.php">Cadastrar novo endereço</a> ]
            </div>
            <div class="box-minha-lista">
            	<span class="dados-minha-lista">Mensagem:</span>
              	<textarea name="lista_texto" class="filtro-minha-lista-textarea" id="lista_texto"><?php echo $texto;?></textarea>
            </div>
            
            <div class="box-minha-lista">&nbsp;</div>
            
			<div class="box-minha-lista">
            	<span class="dados-minha-lista"><strong>Entrega ou Crédito:</strong></span> 
         		<label style="width:95%; height:35px; float:left; margin:10px;"><input name="lista_credito" id="lista_credito" type="radio" value="-1" <?php if($credito==-1){echo "checked";}?> /> Quero receber em crédito o valor dos presentes comprados, e após o evento - em até 30 dias corridos - selecionar os presentes que quero receber no endereço de entrega cadastrado (nesta opção, enviar email para contato@popdecor.com.br após o evento para solicitar o crédito e trocar os presentes).</label>
                <label style="width:95%;  height:25px; float:left; margin:10px;"><input name="lista_credito" id="lista_credito" type="radio" value="0" <?php if($credito==0){echo "checked";}?> /> Quero receber os presentes comprados antes do evento no endereço de entrega cadastrado.</label>
			</div>  

            <div class="box-minha-lista">
            &nbsp;
            </div>

            
			<div class="box-minha-lista">
            	<span class="dados-minha-lista"><strong>Notificação de compra:</strong></span> 
   		    	<label style="width:100%; height:15px; float:left; margin:10px;"><input name="lista_notificacao" id="lista_notificacao" type="radio" value="-1" <?php if($notificacao==-1){echo "checked";}?> /> Quero receber por e-mail notificação dos presentes comprados.</label>
                <label style="width:100%; height:15px; float:left; margin:10px;"><input name="lista_notificacao" id="lista_notificacao" type="radio" value="0" <?php if($notificacao==0){echo "checked";}?> /> Não quero receber e-mail notificação dos presentes comprados.</label>
			</div>        
            
                            
            
		  <div class="lista-box-btns-conta">
                <div id="lista-box-btn-buscar">
                  <input type="image" name="cmd_buscar" id="cmd_buscar" src="images/btn-criar-lista.png" />
                </div>
          </div>
            


          </form>
          
            
        </div>
        
    	
        
        <?php
			$ssql = "select p.produtoid, p.pproduto, p.pdescricao, p.pvalor_unitario,
					sum(li.lquantidade) as lquantidade, li.lcodpropriedade, li.lcodtamanho,
					pm.marquivo as pimagem
					from tblproduto as p
					inner join tbllista_presente_item  as li on p.produtoid = li.lcodproduto 
					left join tblproduto_midia as pm on li.lcodproduto = pm.mcodproduto and pm.mprincipal = -1
					inner join tbllista_presente on li.lcodlista = tbllista_presente.listaid
					where li.lcodlista = '{$id}' and tbllista_presente.lcodcadastro = '{$cadastro}' and li.lquantidade > 0
					group by p.produtoid, li.lcodpropriedade, li.lcodtamanho";      
			$result = mysql_query($ssql);
			if(!$result){
				echo mysql_error($conexao);	
			}
			if($result){
				
				$num_rows = mysql_num_rows($result);
				
				if($num_rows>0){
					echo '<h4 class="h4-minha-conta">Meus Produtos - Lista de Presente</h4>';
					echo	'<div id="box-header-lista">
							<span id="tit-produto-lista" class="tit-bar-text-cart-box">Produto</span>
							<span id="qtde-comprada-lista" class="tit-bar-text-cart-box">Qtde Comprada</span>
							<span id="preco-produto-lista" class="tit-bar-text-cart-box">Preço</span>
							<span id="qtde-produto-lista" class="tit-bar-text-cart-box">Qtde</span>
							<span id="total-produto-lista" class="tit-bar-text-cart-box">Atualizar</span>
							</div>';
				}
						
				while($row=mysql_fetch_assoc($result)){
				
						$produtoid	= $row["produtoid"];
						$produto	= $row["pproduto"];
						$descricao	= $row["pdescricao"];
						
						$propriedade= intval($row["lcodpropriedade"]);
						$tamanho	= intval($row["lcodtamanho"]);
						
						$quantidade	= intval($row["lquantidade"]);
						$quantidade_comprada	= intval(get_lista_presente_qtde_comprada($listaid, $produtoid, $propriedade, $tamanho));
						
						$valor_unitario = $row["pvalor_unitario"];
						$valor_comparativo = number_format($row["pvalor_comparativo"],2,',','.');
						
						if( isset($_SESSION["utm_source"]) ){
							$desconto_utm =  get_desconto_utm_source($id);
							$valor_unitario = number_format($valor_unitario - $desconto_utm,2,",",".");
							
							if(floatval($desconto_utm)>0){
								$valor_comparativo = number_format($row["pvalor_unitario"],2,',','.');
							}
						}
						else
						{
							$valor_unitario = number_format($valor_unitario,2,",",".");	
						}
				
						$imagem = $row["pimagem"];
						if(!file_exists($imagem)){
							$imagem = "imagem/produto/tumb-indisponivel.png";		
						}				
				
					echo '
						<div class="item-lista-presente" id="tr-'.$produtoid.'-'.$propriedade.'-'.$tamanho.'">
					
							<div class="img-produto-lista">
								<img src="'.$imagem.'" border="0" width="40" height="40" />
							</div>
							
							<div class="desc-lista-presente">
								<span class="nome-lista-presente">'.$produto.'</span>
								<span class="desc-listas">'.$descricao.'</span>
							</div>
							
							<div class="comprada-lista-presente">
								<span class="desc-qtde-lista">'.$quantidade_comprada.'</span>                        	
							</div>                        
							
							<div class="preco-lista-presente">
								<span class="preco-qtde-lista">R$ '.$valor_unitario.'</span>                        	
							</div>
							
							<div class="qtde-lista-presente">
								<span class="desc-qtde-lista"><input type="text" class="class_qtde" name="qtde-'.$produtoid.'" id="qtde-'.$produtoid.'" value="'.$quantidade.'" maxlength="3" style="width:20px; height:20px; text-align:center; border:#ccc solid 1px;"></span>                        	
							</div>
							
							<div class="btn-lista-presente" id="'.$produtoid.'-'.$propriedade.'-'.$tamanho.'">                        	
								<img src="images/ico-refresh.png" border="0" class="ico-refresh" produto="'.$produtoid.'" propriedade="'.$propriedade.'" tamanho="'.$tamanho.'" />
							</div>                                                							
							
						</div>				
					';
					
				}
				mysql_free_result($result);
			}
					
		
		?>
        
        
    </div>   
	</div>
    
    <div id="footer-container">
		<?php
            include("inc_footer.php");
        ?>
    </div>
</div>
</body>
</html>