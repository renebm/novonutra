<?php
	include("include/inc_conexao.php");	
	include_once("include/inc_funcao.php");
	$top20 = '-1';
	/*-------------------------------------------------
	//navegação sem ssl
	---------------------------------------------------*/
	if(strpos($_SERVER['SERVER_NAME'],".com")>0){
		if($_SERVER['SERVER_PORT']!=80){
			header("location: http://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
			exit();
		}
	}	
	
	$server = ($_SERVER['SERVER_PORT']==80 ? "http://" : "https://") . $_SERVER['SERVER_NAME'] . str_replace("categoria.php","",$_SERVER['SCRIPT_NAME']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="language" content="pt-br" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?php echo $site_site;?>/categoria.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<?php
include("include/inc_analytics.php");	
?>
</head>

<body>
<div id="global-container">
	<div id="header-content">
	<?php
			include("inc_header.php");
		?>
    </div>
	<div id="main-box-container">
   	  <div id="categoria-container-box"></div>
        <div class="box-products-container" style="width:820px;">
            <div id="products-category-box">
				<?php
					$produtos = mysql_query("(select 1 as rank, SUM(tblestoque.eestoque),tblproduto.pdescricao, tblproduto.plink_seo,tblproduto.pcontrola_estoque,tblproduto.pdisponivel,tblproduto.produtoid as produtoid, tblmarca.mmarca, tblmarca.mlink_seo, tblproduto.preferencia,
					tblproduto.pproduto, tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, 
					tblproduto.plink_seo, tblproduto_midia.marquivo as pimagem
					from tblproduto
					left join tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
					left join tblproduto_midia on tblproduto.produtoid = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal = -1
					left join tblmarca on marcaid = tblproduto.pcodmarca
					where tblproduto.pdisponivel=-1 and tblproduto.pvitrine=-1 and now() between tblproduto.pdata_inicio and tblproduto.pdata_termino and pcodmarca = marcaid and ptop20 = -1
					group by tblproduto.preferencia 
					HAVING ((SUM(tblestoque.eestoque) > 0 && tblproduto.pcontrola_estoque = -1 && tblproduto.pdisponivel = -1) || (tblproduto.pdisponivel = -1 && tblproduto.pcontrola_estoque = 0 )))union(
					select 2 as rank, SUM(tblestoque.eestoque),tblproduto.pdescricao, tblproduto.plink_seo,tblproduto.pcontrola_estoque,tblproduto.pdisponivel,tblproduto.produtoid as produtoid, tblmarca.mmarca, tblmarca.mlink_seo, tblproduto.preferencia,
					tblproduto.pproduto, tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, 
					tblproduto.plink_seo, tblproduto_midia.marquivo as pimagem
					from tblproduto
					left join tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
					left join tblproduto_midia on tblproduto.produtoid = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal = -1
					left join tblmarca on marcaid = tblproduto.pcodmarca
					where tblproduto.pdisponivel=-1 and tblproduto.pvitrine=-1 and now() between tblproduto.pdata_inicio and tblproduto.pdata_termino and pcodmarca = marcaid
					group by tblproduto.preferencia 
					HAVING ((SUM(tblestoque.eestoque) > 0 && tblproduto.pcontrola_estoque = -1 && tblproduto.pdisponivel = -1) || (tblproduto.pdisponivel = -1 && tblproduto.pcontrola_estoque = 0 ))) order by rank limit 20");
					while($produto = mysql_fetch_array($produtos)){
				?>
				<a href="<?=$produto["plink_seo"] ?>">
				<div class="produto">
					<span class="product-thumb"><img src="<?php if(file_exists($produto["pimagem"])){ echo $produto["pimagem"]; }else{ echo "imagem/produto/med-indisponivel.png"; } ?>" alt="produto"></span>
					<span class="product-menu-top20-product-title"><?=$produto["pproduto"] ?></span>
					<span class="subtituloProduto"><?=$produto["pdescricao"] ?></span>
					<span class="precoProduto">R$ <?=number_format($produto["pvalor_unitario"],2,",",".") ?></span>
					<span class="parceladoProduto"><?php echo get_valor_parcelado($produto["produtoid"],$produto["pvalor_unitario"]); ?></span>
				</div>
				<?php
					}
				?>
			</div>
        </div>
		<div id="aside-right-bar2" style="margin-top: 0;">
			<?php carregaBanners(2); ?>
		</div>
	</div>
    
    <div id="footer-container">
    <?php
		include("inc_footer.php");
	?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>