<?php
	include("include/inc_conexao.php");
	$telaProduto = '-1';
	/*-------------------------------------------------
	//navegação sem ssl
	---------------------------------------------------*/
	if(strpos($_SERVER['SERVER_NAME'],".com")>0){
		if($_SERVER['SERVER_PORT']!=80){
			header("location: http://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
			exit();
		}
	}
	/*
	if(isset($_COOKIE["orcamento"])){
		echo $_COOKIE["orcamento"];
	}
	*/
	$erro = 0;
	$id = addslashes(strtolower($_REQUEST["id"]));
	
	$tamanho = 0;
	$propriedade = 0;
	
	$categoriaid = 0;	//monta produtos relacionados
	
	if(!is_numeric($id)){
		$link_seo = addslashes(strtolower($_SERVER['REQUEST_URI']));
		$link_seo = explode("---",$link_seo);
		$id = $link_seo[1];
	}
	
	if(!is_numeric($id) || $id < 1){	
		$id = get_only_numbers($id);
	}
	
	
	$ssql = "select tblproduto.produtoid, tblproduto.preferencia, tblproduto.pcodigo, tblproduto.pproduto, tblproduto.psubtitulo,  tblproduto.pdescricao,  
				tblproduto.pdescricao_detalhada, tblproduto.ppalavra_chave,	tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, tblproduto.pcodmarca, pficha_tecnica,
				tblproduto.pdisponivel, tblproduto.pcontrola_estoque,  tblproduto.pfrete_gratis,  tblproduto.ppeso,  tblproduto.pminimo, tblproduto.plink_seo,
				(tblproduto_midia.marquivo) as pimagem, tblproduto_midia.midiaid, tblmarca.mguia, tblmarca.mmarca, tblmarca.mlink_seo
				from tblproduto 
				left join tblproduto_midia on tblproduto_midia.mcodproduto=tblproduto.produtoid and tblproduto_midia.mprincipal=-1
				left join tblmarca on tblproduto.pcodmarca=tblmarca.marcaid
				";
	//if(is_numeric($id) && $id>0){
		$ssql .= " where tblproduto.produtoid='{$id}'
				and tblproduto.pdata_inicio<='{$data_hoje}' and tblproduto.pdata_termino>='{$data_hoje}'
		";	
		//echo $ssql;
		//die();
	//}else{
	//	$ssql .= " where tblproduto.plink_seo='{$link_seo}'";
	//}
	
	//if()
	//echo $ssql;
	//exit();
	
	$result = mysql_query($ssql);
	if($result){
		//echo mysql_num_rows($result);
		
		if(mysql_num_rows($result)==0){
			header("location: index.php?refer=produto");
			exit();
		}
		
		while($row = mysql_fetch_assoc($result)){
			$id 				= $row["produtoid"];
			
			if(!is_numeric($id)){
				header("Location: index.php?refer=produto");
				exit();
			}


			$id = $row["produtoid"];
			$valor_remarketing = number_format($row["pvalor_unitario"], 2, ".", ".");
			$valor_unitario = $row["pvalor_unitario"];
			$valor = number_format($row["pvalor_unitario"],2,",","");
			$valor_comparativo = number_format($row["pvalor_comparativo"],2,',','.');
			
			if( isset($_SESSION["utm_source"]) ){
				$desconto_utm =  get_desconto_utm_source($id);
				$valor_unitario = number_format($valor_unitario - $desconto_utm,2,",",".");
				
				if(floatval($desconto_utm)>0){
					$valor_comparativo = number_format($row["pvalor_unitario"],2,',','.');
				}
				
			}
			else
			{
				$calc_desconto  = $valor_unitario;
				$valor_unitario = number_format($valor_unitario,2,",",".");	
				
			}


			$referencia 		= $row["preferencia"];
			$codigo				= $row["pcodigo"];	//SKU
			$produto_nome		= $row["pproduto"];

			$subtitulo			= $row["psubtitulo"];
			$descricao			= $row["pdescricao"];
			$descricao_detalhada = $row["pdescricao_detalhada"];
			$ficha_tecnica 		= $row["pficha_tecnica"];
			$palavra_chave		= $row["ppalavra_chave"];


			$codmarca 			= $row["pcodmarca"];
			$marca				= $row["mmarca"];
			$marca_link			= $row["mlink_seo"];
			$guia_tamanho		= $row["mguia"];
			
			$disponivel 		= $row["pdisponivel"];
			$controla_estoque	= $row["pcontrola_estoque"];
			$frete_gratis		= $row["pfrete_gratis"];
			$peso				= number_format($row["ppeso"],2,',','.');
			$minimo				= $row["pminimo"];
			$link_seo			= $row["plink_seo"];
			$imagem_tumb		= $row["pimagem"];	
			$imagem_med			= str_replace("-tumb","-med",$row["pimagem"]);
			$imagem_big_zoom	= str_replace("-tumb","-big",$row["pimagem"]);	
			
			$link_facebook		= "//" . urldecode($_SERVER['SERVER_NAME'] . "/" . $link_seo);
			
			//($_SERVER['SERVER_PORT']==80 ? "http://" : "https://")
			if(!file_exists($imagem_tumb)){
				$imagem_tumb = "imagem/produto/tumb-indisponivel.png";		
			}
	
			if(!file_exists($imagem_med)){
				$imagem_med = "imagem/produto/med-indisponivel.png";		
			}
			
			if(!file_exists($imagem_big)){
				$imagem_big = "imagem/produto/big-indisponivel.png";		
			}			
			
			$link_mapeamento = trim(monta_link_categoria_produto($id)," / ");
		}
		mysql_free_result($result);
	}
/*-------------------------------------------------------------------
estoque
--------------------------------------------------------------------*/
$estoque = "";
if($controla_estoque==-1 && $disponivel ==-1){
	$disponivel = 0;
	$ssql = "SELECT tblestoque.ecodproduto, pai.ppropriedade AS propriedade_pai, tblestoque.ecodtamanho, tblproduto_propriedade.ppropriedade AS ptamanho, 
				tblestoque.ecodpropriedade, proper.ppropriedade AS ppropriedade, tblestoque.eestoque, tblestoque.edata_alteracao
				FROM tblestoque
				LEFT JOIN tblproduto_propriedade ON tblestoque.ecodtamanho = tblproduto_propriedade.propriedadeid
				LEFT JOIN tblproduto_propriedade AS proper ON tblestoque.ecodpropriedade = proper.propriedadeid
				LEFT JOIN tblproduto_propriedade AS pai ON tblproduto_propriedade.pcodpropriedade = pai.propriedadeid
				WHERE tblestoque.ecodproduto = $id";	
	$result = mysql_query($ssql);
	if($result){
		$num_rows = mysql_num_rows($result);
		if($num_rows==0){
			$disponivel=0;	
		}
		while($row=mysql_fetch_assoc($result)){			
			/*--------------------------------------------
			campos : 1=tamanho | 2=propriedade | 3=estoque
			---------------------------------------------*/			
			$estoque_array .= 'estoque["'. $row["ecodtamanho"] . ':' .$row["ecodpropriedade"] . '"]='.$row["eestoque"] . ";\r\n";			
			$tamanhos_box[$row["ecodtamanho"]] = "1";

				if( $row["eestoque"] > 0 ){
					$disponivel = -1;	
				}
			
		}
		mysql_free_result($result);
	}
					
}
	//verifica se tem propriedade ou tamanho
	$t = 0;
	$produto_tamanho;
		$ssql  = "select tblproduto_caracteristica.caracteristicaid,tblestoque.eestoque,tblproduto_caracteristica.ccodpropriedade, tblproduto_caracteristica.ccodproduto, 
				tblproduto_propriedade.ppropriedade, tblproduto_propriedade.pcodtipo 
				from tblproduto_caracteristica
				left join tblproduto_propriedade on tblproduto_caracteristica.ccodpropriedade = tblproduto_propriedade.propriedadeid
				left join tblestoque on ecodproduto = ccodproduto and ecodtamanho = propriedadeid
				where tblproduto_caracteristica.ccodproduto= $id  order by tblproduto_propriedade.pordem";
				
				//echo $ssql;
				
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			if($row["pcodtipo"]==1){	// tamanaho
				$tamanho =-1;			//precisa informar o tamanho pra por no carrinho
				if($row["ccodpropriedade"]>0){
					$t++;
					if($t==1){
						$produto_tamanho .= '<ol id="selectable">';
					}
						$class = "";
						$disabled = "";
						if($t==1){
						//	$class = "disabled";
						//	$disabled = ' disabled="disabled"';
						}
						//echo $row["ppropriedade"];
						if($row["eestoque"] == null || $row["eestoque"] < 1){ $class="nostock"; }
						$produto_tamanho .= '<li class="ui-state-default '.$class.'" '.$disabled.' rel="'.$row["ccodpropriedade"].'"><span>'.$row["ppropriedade"].'</span></li>';
				}
				
			}
			else
			{
				$propriedade = -1;		//precisa informar a propriedade pra por no carrinho
				$produto_propriedade .= '<label>';
				$produto_propriedade .= '<input type="radio" id="produto-propriedade" name="produto-propriedade" value="'.$row["ccodpropriedade"].'" onclick="javascript:set_value1('.$row["ccodpropriedade"].');">' . $row["ppropriedade"] . "<br />";				
				$produto_propriedade .= '</label>';
			}
		}
		
					if($t>=1){
						$produto_tamanho .= '</ol>';
					}		
		
		mysql_free_result($result);
	}
	
	
	
/*------------------------------------------------------------------
avaliacao // nota
------------------------------------------------------------------*/
	$avaliacao_media = 0;
	
	$ssql = "select count(avaliacaoid) as total, sum(anota) as nota from tblproduto_avaliacao where acodproduto =$id and astatus=-1";
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$avaliacoes		=	$row["total"];
			$votos			=	$row["nota"];
		}
		mysql_free_result($result);
	}
	
	if($votos>0 && $avaliacoes>0){
		$avaliacao_media	=	number_format($votos/$avaliacoes,0);
	}


/*-------------------------------------------------------------------
atualiza o view e click
---------------------------------------------------------------------*/
if($_COOKIE["produto_view"]!=$id){
	setcookie("produto_view",$id,time()+36000);
	$ssql = "update tblproduto set pview=pview+1, pclick=pclick+1 where produtoid = '{$id}'";
	mysql_query($ssql);
}


/*-------------------------------------------------------------------
Lista de presente
--------------------------------------------------------------------*/
$lista_presente 	= 	intval($_SESSION["lista_presente"]);
$lista_titulo 		=	($_SESSION["lista_presente_titulo"]);


	
/*-------------------------------------------------------------------
base href
--------------------------------------------------------------------*/
$server = ($_SERVER['SERVER_PORT']==80 ? "//" : "//") . $_SERVER['SERVER_NAME'] . str_replace("produto.php","",$_SERVER['SCRIPT_NAME']);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="<?php echo $produto_nome . ' ' . $subtitulo;?>" property="og:title"></meta>
<meta content="<?php echo $site_nome;?>" property="og:site_name"></meta>
<meta content="http:<?php echo $server . $imagem_med;?>" property="og:image"></meta>

<title><?php echo $produto_nome;?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $produto_nome?>" />
<meta name="description" content="<?php echo $descricao;?>" />
<meta name="keywords" content="<?php echo $palavra_chave;?>" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $produto_nome;?>" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/<?php echo $link_seo;?>" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-jqzoom.css" />


<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-jqzoom.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {			
				
		$("#pref-cep").mask("99999");
		$("#sulf-cep").mask("999");
		
		<?php
		if($tamanho==0){
			echo '$("#box-tamanhos").parent(".campos-detalhe").hide();'."\r\n";
		}
	
		if($propriedade==0){
			echo '$("#box-propriedades").hide();'."\r\n";	
		}
		
		if($lista_presente==0){
			echo '$("#box-lista-presente").hide();';
		}
		

		$count = 1;
		if($tamanho!=0){
			$count++;
			echo '$("#tamanho-bullet").html("'.$count.'");';
		}
	
		if($propriedade!=0){
			$count++;
			echo '$("#propriedade-bullet").html("'.$count.'");';			
		}
		
		$count++;
		echo '$("#cart-bullet").html("'.$count.'");';			


		if($disponivel==0){
			echo 'produto_disponivel(0)';
		}
		
		?>
		
		marca_banner();
		
		$('#pref-cep').keyup(function(e){ 
			ccep = $('#pref-cep').val();
			if(ccep.length == 5){ 
				$("#sulf-cep").focus();
			} 
		}); 		

		$('#sulf-cep').keyup(function(e){ 
			ccep = $('#sulf-cep').val();
			if(ccep.length == 3){ 
				ajax_calcula_frete();
			} 
		}); 		


		
		//star rating		
		$('.avaliar-prod li').mouseover(function(){  
			var $this = $( this );  
			var rating = $this.index()+1;  
			var y = "0px " + (-34 * rating)+"px";
			
			$(".avaliar-prod").css({
				 backgroundPosition: y
			 })
			//seta o valor do voto mesmo sem clicar
			//$("#avaliacao").attr("value",rating);
		}); 		


		//star rating click // touch
		$('.avaliar-prod li').click(function(){  
			var $this = $( this );  
			var rating = $this.index()+1;  
			var y = "0px " + (-34 * rating)+"px";
			
			$(".avaliar-prod").css({
				 backgroundPosition: y
			 })
			
			//seta o valor do voto touch
			$("#avaliacao").attr("value",rating);
		}); 		


		//rating
		$(".media-avaliacoes").css({
			backgroundPosition: "0px -<?php echo $avaliacao_media*34;?>px"					   
		})	
		
		//carrega a funcao de zoom da imagem
		setTimeout("image_zoom()",500);
		
		//carrega os botoes de redes sociais
		setTimeout("load_botoes_redes_sociais()",1000);
		
    });


	


	/*-----------------------------------------------------
	estoque -> variaveis
	------------------------------------------------------*/
	var estoque = new Array();
	<?php echo $estoque_array;?>
	
	
	/*-----------------------------------------------------
	check -> tamanhos
	------------------------------------------------------*/
	$(function() {
		$( "#selectable").selectable({
			stop: function() {
				if($(".ui-selected").attr("disabled")){
					//chama o avise-me
					set_value('0');
					produto_disponivel(0);
				}
				else
				{
					produto_disponivel(1);
					set_value(	$( ".ui-selected").attr("rel") );
				}
			}
		});

	});	
	
	function set_value(valor){
		document.getElementById("tamanho").value = valor;
		checa_estoque();
	}		
	
	/*-----------------------------------------------------
	check -> opcoes propriedade
	------------------------------------------------------*/
	function set_value1(valor){
		document.getElementById("propriedade").value = valor;
		checa_estoque();
	}		
		
		
		
	function checa_estoque(){
		if($("#controla_estoque").attr("value")==0){
			return;	
		}		
		var tamanho = eval(document.getElementById("tamanho").value);
		var propriedade = eval(document.getElementById("propriedade").value);

		var key = tamanho + ":" + propriedade;

		if(estoque[key]<=0 || estoque[key]==undefined){
			produto_disponivel(0);
			$("#disponivel").attr("value","0");
		}else{
			produto_disponivel(-1);	
			$("#disponivel").attr("value","-1");
		}		
		
	}	
	
	
	function image_zoom(){	
		$('.jqzoom').jqzoom({
            zoomType: 'standard',
            lens:true,
            preloadImages: false,
            alwaysOn:false,
            zoomWidth: 430,  
            zoomHeight: 430, 
			imageOpacity: 0.4,
			showEffect: 'show', /*fadein*/
			hideEffect: 'hide', /*fadeout*/
			fadeinSpeed: '0', /*'fast','slow',number*/
			preloadText: "Carregando..."
        });			
	}
		
	
</script>

<?php
include("include/inc_analytics.php");	
?>
<style type="text/css">
	.produto{ padding: 0 9px 30px 0; margin-top: 25px; }
</style>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="lb"></div>

<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">
        <div id="mapeamento-pagina"><?php echo $link_mapeamento;?></div>
        
        <form name="frm_produto" id="frm_produto" method="post" action="carrinho.php">
            <input type="hidden" id="action" name="action" value="incluir" />
            
            <input type="hidden" id="produtoid" name="produtoid" value="<?php echo $id;?>" />
            <input type="hidden" id="tamanho" name="tamanho" value="<?php echo $tamanho;?>" />
            <input type="hidden" id="propriedade" name="propriedade" value="<?php echo $propriedade;?>" />
            <input type="hidden" id="valor_produto" name="valor_produto" value="<?php echo $valor_remarketing;?>" />
            <input type="hidden" id="qtde" name="qtde" value="1" />
            
            <input type="hidden" id="produto-cp" name="produto-cp" value="0" />
            <input type="hidden" id="tamanho-cp" name="tamanho-cp" value="0" />
            <input type="hidden" id="propriedade-cp" name="propriedade-cp" value="0" />            
            
            <input type="hidden" id="disponivel" name="disponivel" value="<?php echo $disponivel;?>" />
            <input type="hidden" id="controla_estoque" name="controla_estoque" value="<?php echo $controla_estoque;?>" />
        </form>
        
        <div id="box-produto">
            <div id="box-imagens-prod">
            
            
              
                <div id="prod-img-m">
                    <a href="<?php echo $imagem_big_zoom;?>" class="jqzoom" rel='gal1' title="<?php echo $produto_nome;?>">
                    <img src="<?php echo $imagem_med;?>" width="430" height="430" alt="<?php echo $produto_nome;?>" title="<?php echo $produto_nome;?>" border="0">
                    </a>
                </div>
                
                <div id="box-thumbs">
                    <div class="clearfix" >
                     <?php
                        echo "<ul id=\"thumblist\"  class=\"clearfix\" >";
                        
						//echo "<li>";
                        //echo "<a class=\"zoomThumbActive\" href='javascript:void(0);' rel=\"{gallery: 'gal1', smallimage: '".$imagem_med."',largeimage: '".$imagem_big."'}\" >";
                        //echo "<img src='".$imagem_med."' title='".$produto_nome."' width=\"50\" height=\"50\">";
                        //echo '</a>';
                        //echo '</li>';
                    
                        
                        $ssql = "select marquivo from tblproduto_midia where mcodproduto = $id and mcodtipo=1";
                        //echo $ssql;
                        $result = mysql_query($ssql);
                        if($result){
                            while($row=mysql_fetch_assoc($result)){
                                $tumb = $row["marquivo"];
                                $imagem = str_replace("-tumb","-med",$tumb);//str_replace("-tumb","-med",$tumb);
                                $imagem_big = str_replace("-tumb","-big",$tumb);
								if(!file_exists($tumb)){
									$tumb = "imagem/produto/tumb-indisponivel.png";		
								}
								if(!file_exists($imagem)){
									$imagem = "imagem/produto/med-indisponivel.png";	
								}
								if(!file_exists($imagem_big)){
									$imagem_big = "imagem/produto/med-indisponivel.png";		
								}
                                echo "<li><a href='javascript:void(0);' rel=\"{gallery: 'gal1', smallimage: '".$imagem."',largeimage: '".$imagem_big."'}\" >";
                                echo "<img src='".$tumb."' title='".$produto_nome."' width=\"70\" height=\"70\">";
                                echo "</a></li>";				
                            }
                            mysql_free_result($result);
                        }		
                        
                       echo '</ul>'; 
                      ?>
                    </div>
                </div>
            </div>
            
            <div id="box-dados-prod">
            	<div class="campos-detalhe">
                    <div id="box-tit-prod">
                        <span id="codigo">Código: <?php echo $codigo; ?></span>
						<h1><?php echo $produto_nome; ?></h1>
                        <h2>Mais produtos <a href="fabricante/<?php echo $marca_link;?>"><?php echo mb_convert_case(($marca), MB_CASE_TITLE, "UTF-8"); ?></a></h2>
						<div id="box-pagamento">
							<?php
								if ( floatval(str_replace(",",".",str_replace(".","",$valor_comparativo))) > floatval(number_format($valor_unitario, 2, '.', '')) ){
									echo '<span id="valorde"><em>de R$ '.$valor_comparativo.'</em></span>';	
								}
							?>
							<div class="preco-produto">
								<div id="valorpornew">Por R$ <span><?=$valor_unitario;?></span></div>
								<?php
									if(strlen(get_valor_parcelado_new($id,$valor)) > 0){
								?>
										<div id="valorparceladonew"><?php echo get_valor_parcelado_new($id,$valor); ?></div>
								<?php
									}
								?>
								<div class="precoPornew" style="margin-top: 5px;">ou <span>R$ <?=number_format($calc_desconto-($calc_desconto*0.10),2,",",".")?></span></div>
								<div class="descritivoDescontonew">à vista no boleto ou transferência</div>					
							</div>
						</div>           
                    </div>
					
				
			
					
					
		
					
			
					
					
					<div class="descricao-produto-curta">
						<h3 class="informacoes-produto">
							<?php echo str_replace("\n","<br/>",$descricao);?>
							
						</h3>
					</div>
				</div>
                
                <div class="campos-detalhe">                
                    <div id="box-tamanhos">
                        <div class="add-to-cart">
                            <span class="tit-aba-prod">VARIAÇÃO</span>
                        </div>
                        <div id="box-cores-items">
							<?php
                                echo $produto_tamanho;
                            ?>
                        </div>                        
                    </div>
						<?php
        
                        $ssql = "select tblproduto.produtoid, tblproduto.preferencia, tblproduto.pproduto, tblproduto.plink_seo, 
                                    (tblproduto_midia.marquivo) as pimagem, tblproduto_midia.midiaid
                                    from tblproduto 
                                    left join tblproduto_midia on tblproduto_midia.mcodproduto=tblproduto.produtoid and tblproduto_midia.mprincipal=-1
                                    where tblproduto.preferencia='{$referencia}'
									order by tblproduto.pordem
									";	
                        $result = mysql_query($ssql);
                        if(mysql_num_rows($result) > 1){
						?>
						<div id="box-cores">
							<div class="add-to-cart">
								<span class="tit-aba-prod">SEMELHANTES</span>
							</div>
							<div id="box-cores-items">
							<?php
								while($row = mysql_fetch_assoc($result)){
									$imagem = $row["pimagem"];
									if(!file_exists($imagem)){
										$imagem = "imagem/produto/tumb-indisponivel.png";		
									}							
									
									if($id==$row["produtoid"]){
										$class_box = 'box-cores-item-selected';
									}
									else
									{
										$class_box = 'box-cores-item';
									}
									echo '<div class="'.$class_box.'">';
									echo '<a href="javascript:void(0);" onclick=javascript:troca_produto_cor("'.$row["plink_seo"].'");>';
									echo '<img src="'.$imagem.'" width="50" height="50" border="0" />';
									echo '</a>';
									echo '</div>';
								}
								mysql_free_result($result);	
							?>
							</div>                
						</div>
						<?php
						}
                        ?>
                </div>

				<div class="campos-detalhe" id="campo-propriedades">                
                    <div id="box-propriedades">
                        <div class="add-to-cart">OPÇÕES</div>
                        <div id="box-cores-items">
						<?php
                            echo $produto_propriedade;
                        ?>
                        </div>
                    </div>            
                </div>
				<!--<span class="tit-aba-prod">CALCULE O FRETE</span>
				<div id="box-calcula-cep">
					<form name="calculofrete" onsubmit="return false;">
						<input type="hidden" id="frete_gratis" value="<?php echo $frete_gratis;?>" />
						<input type="hidden" id="peso" value="<?php echo $peso;?>" />
						<input type="hidden" id="valor_unitario" value="<?php echo $valor_unitario;?>" />
							<span id="digiteCEP">Digite seu cep</span>
							<input type="text" name="pref-cep" id="pref-cep" maxlength="5" />
							<input type="text" name="sulf-cep" id="sulf-cep" maxlength="3" />
							<img src="images/ico-calculadora.png" border="0" alt="calcular frete" title="calcular frete" id="icone-calculadora" onmouseover="this.style.cursor='pointer';"  onclick="javascript:ajax_calcula_frete();" />
					</form>              
				</div>
				<div id="complemento-calculo-cep">
					<span id="link-consulta-cep"> <a href="http://www.buscacep.correios.com.br/servicos/dnec/menuAction.do?Metodo=menuLogradouro" target="_blank" id="consultar-cep">Não sabe seu cep?</a></span>
				</div>                    
				<div id="box-frete"></div>-->
				
                <div id="box-add-to-cart">
    				<div id="box-comprar">
						<div id="box-comprar-qtde">
							<em>Quantidade:</em>
							<input name="produto-qtde" type="text" id="produto-qtde" value="1" maxlength="3" onkeydown="$('#qtde').val($(this).val().replace(/\D/g, ''))" onkeyup="$('#qtde').val($(this).val().replace(/\D/g, ''))"/>
						</div>
						<img src="images/setacarrinho.jpg" style="float: left;margin: 5px 20px;">
						<div id="box-btn-comprar" onmouseover="this.style.cursor='pointer';" onclick="javascript:add_carrinho();"></div>            
					</div>						
                </div>
				<div id="box-add-to-cart">
					<div id="box-avise-me">
						<span id="avise-me-bullet">PRODUTO ESGOTADO</span>
						<span id="txt-avise-me">Avise-me quando chegar</span>
						<div id="campos-aviseme">
						<form name="aviseme" method="post" onsubmit="return valida_avisa_indisponivel();">
							<input type="text" name="txt_nome" id="txt_nome" class="formulario-aviseme" width="20" value="Digite seu nome" onfocus="javascript:clear_field(this,'Digite seu nome');" onblur="javascript:seta_field_valor(this,'Digite seu nome');" />
							<input type="text" name="txt_email" id="txt_email" class="formulario-aviseme" width="20" value="Digite seu e-mail" onfocus="javascript:clear_field(this,'Digite seu e-mail');" onblur="javascript:seta_field_valor(this,'Digite seu e-mail');" />
							<input name="Submit" type="submit" id="btn-ok-aviseme" value="OK" />
						</form>
						</div>
					</div>
				</div>
            </div>
        </div>
				

		<div id="box-produtos-relacionados">
				<div id="products-title"><span>PODERÁ GOSTAR TAMBÉM DE</span></div>
				<div class="box-semelhantes">          
				
				<?php monta_produtos_relacionados($id,$categoriaid);?>   
				
			  </div>
			
			</div>
		</div>
		<div class="carousel-container">
			<div id="box-caracteristicas-prod">
			<span class="tit-produto-descricao">Características</span>
				<div class="descricao-produto">
					<p class="informacoes-produto">
						<?php echo $descricao_detalhada; ?>
					</p>
				</div>
			</div>
			<?php
				if($ficha_tecnica != ''){
			?>
			<div id="box-caracteristicas-prod">
			<span class="tit-produto-descricao">Ficha Técnica</span>
				<div class="descricao-produto">
					<p class="informacoes-produto">
						<?php echo $ficha_tecnica; ?>
					</p>
				</div>
			</div>
			<?php
				}
			?>
			<div id="box-caracteristicas-prod" style="margin-top:10px;">
				<span class="tit-produto-descricao">Comente sobre este produto</span>
					<div class="descricao-produto" >
						<p class="informacoes-produto">
							<div class="fb-comments" data-href="http:<?php echo $link_facebook;?>" data-numposts="5" data-width="1000px" style="margin-top: 10px;"></div>
						</p>
					</div>
			</div>
		</div>
	
    <div id="footer-container">
		<?php
            include("inc_footer.php");
        ?>
    </div>
</div>

<script type="text/javascript">
var google_tag_params = {
	ecomm_prodid: <?php echo $id; ?>,
	ecomm_pagetype: 'product',
	ecomm_totalvalue: <?php echo $valor_remarketing;?>,
};
</script>


<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '570933616448411');
fbq('track', "PageView");
fbq('track', 'ViewContent', {
   	content_type: 'product',
	content_ids: [<?php echo $id; ?>],
	value: <?php echo $valor_remarketing;?>,
	currency: 'BRL'
});
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=570933616448411&ev=PageView&noscript=1"/></noscript>

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 970176820;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

<noscript>
<div style="display:inline;">
	<img height="1" width="1" style="border­style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/970176820/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>