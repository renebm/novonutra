<?php
	include("include/inc_conexao.php");	
	
	
	/*-------------------------------------------------
	//navegação sem ssl
	---------------------------------------------------*/
	if(strpos($_SERVER['SERVER_NAME'],".com")>0){
		if($_SERVER['SERVER_PORT']!=80){
			header("location: http://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
			exit();
		}
	}	
	
	$uri = $_SERVER['REQUEST_URI'];

	/*-------------------------------------------------
	//pega o uri para pegar os valores da lista 
	---------------------------------------------------*/
	$lista_link_seo = addslashes($_REQUEST['id']);
	
	if( strpos($lista_link_seo,"/") ){
		$lista_link_seo = left($lista_link_seo,strpos($lista_link_seo,"/"));	
	}
	
	$lista_link_seo = 'lista-presente/'.$lista_link_seo;
	
	$ssql = "select tbllista_presente.listaid, tbllista_presente.ltitulo, tbllista_presente.ltexto, tbllista_presente.lcodcadastro, tbllista_presente.lcredito 
			from tbllista_presente
			inner join tblcadastro on tbllista_presente.lcodcadastro = tblcadastro.cadastroid
			and tbllista_presente.llink_seo = '{$lista_link_seo}'
			limit 0,1
			";
	//echo $ssql;
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$listaid		=	$row["listaid"];
			$cadastro		=	$row["lcodcadastro"];
			$lista_titulo	=	$row["ltitulo"];
			$lista_texto	=	$row["ltexto"];
			$lista_credito	=	$row["lcredito"];
		}
		mysql_free_result($result);
	}


	$pagina = 1;
	$start = 0;
	$limit = 96;
	$ordem = 0;
	
	$canonical = $lista_lik_seo . "/" . $lista_link_seo;

	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}

	
	
	if(is_numeric($qs["ordem"])){	
		$ordem = $qs["ordem"];
		if($ordem>8 || $ordem <0){
			$ordem = 0;	
		}
	}	
	
	
	/*--------------------------------------------------------------------------
	remove os parametros de querystring para montar as info de categorias
	---------------------------------------------------------------------------*/
	if(strpos($uri,"?")<>""){
		$uri = substr($uri,0,strpos($uri,"?")-1);
	}
	
	
	if(substr($uri,0,1)=="/"){
		$uri = substr($uri,1,strlen($uri));	
	}
	
	
		switch ($ordem){
			
			case "0":
			$order = " pdata_inicio desc";
			break;				
			
			case "1":
			$order = " pvalor_unitario";
			break;

			case "2":
			$order = " pvalor_unitario desc";
			break;

			case "3":
			$order = " pvalor_unitario desc";	//mais vendidos
			break;

			case "4":
			$order = " pvalor_unitario desc";	//mais bem avaliados
			break;

			case "5":
			$order = " pproduto";
			break;

			case "6":
			$order = " pproduto desc";
			break;

			case "7":
			$order = " pdata_cadastro desc";	//lancamento
			break;

			case "8":
			$order = " pdesconto desc";
			break;

		}	


	/*--------------------------------------------------------------------------
	nodes de categoria
	---------------------------------------------------------------------------*/
	$nc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	$nc = substr($nc, strpos($nc,$lista_link_seo) + strlen($lista_link_seo) + 1 ,200);
	$nc = addslashes($nc);
	
	if($nc){
		$ssql = "select categoriaid from tblcategoria where ccategoria = '{$nc}'";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$categorias = $row["categoriaid"].",";
			}
			$categorias = rtrim($categorias,',');
			mysql_free_result($result);
		}
	}
	
	/*--------------------------------------------------------------------------
	query de consulta
	---------------------------------------------------------------------------*/
	$ssql_lista = "select p.produtoid, p.pproduto, p.pdescricao, p.pvalor_unitario, p.pdisponivel, p.pcontrola_estoque,
		( p.pvalor_comparativo - p.pvalor_unitario) as pdesconto,
		sum(li.lquantidade) as lquantidade, li.lcodpropriedade, li.lcodtamanho,
		pm.marquivo as pimagem, pp.ppropriedade as ptamanho
		from tblproduto as p
		inner join tbllista_presente_item  as li on p.produtoid = li.lcodproduto 
		left join tblproduto_midia as pm on li.lcodproduto = pm.mcodproduto and pm.mprincipal = -1
		left join tblproduto_propriedade as pp on li.lcodpropriedade = pp.propriedadeid and pp.pcodtipo = 1
		where li.lcodlista = '{$listaid}' and li.lquantidade > 0
		";
	
	if($categorias){
		$ssql_lista .= " and p.produtoid in($categorias)"; 	
	}
	
	$ssql_lista .= " group by p.produtoid, li.lcodpropriedade, li.lcodtamanho
		order by $order
		";
	$result = mysql_query($ssql_lista);
	if($result){
		$total_registros = mysql_num_rows($result);	
	}
	
/*-------------------------------------------------------------------
base href
--------------------------------------------------------------------*/
$server = ($_SERVER['SERVER_PORT']==80 ? "http://" : "https://") . $_SERVER['SERVER_NAME'] . str_replace("categoria.php","",$_SERVER['SCRIPT_NAME']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $categoria;?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $categoria?>" />
<meta name="description" content="<?php echo $categoria;?>" />
<meta name="keywords" content="<?php echo $categoria;?>" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $categoria;?>" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?php echo $site_site;?>/<?php echo $canonical;?>" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript">
	$(document).ready(function(){
		
		$(".btn-add-carrinho").click(function(){
			var id 			= $(this).attr("rel");
			var tamanho 	= $(this).attr("tamanho");
			var propriedade = $(this).attr("propriedade");
			var qtde 		= $(this).attr("quantidade");
		
			$("#produtoid").attr("value", id );
			$("#tamanho").attr("value", tamanho );
			$("#propriedade").attr("value", propriedade );
			$("#qtde").attr("value", qtde );
		
			$("#frm_produto").submit();
		
		});
		
		
	});

</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">
    	<div id="categoria-container-box">
        	<div class="categoria-menu"><span class="cat-menu-left">Menu</span></div>
            <a href="index.php?ref=categoria">Lista de Presente</a>
            <span style="float:left; margin: 0 2px 0 2px; padding:5px 0 0 0; color:#666;">&nbsp;/&nbsp;</span><span id="id-pag-atual"><?php echo $lista_titulo;?></span>
            <span style="width:790px; float:right; margin: 0 2px 0 2px; padding:5px 0 0 0; color:#666;">
            <?php echo $lista_texto;?>
            </span>
        </div>
    	<div id="container-menu-left">
        	<?php
            	include("inc_left_lista_presente.php");
			?>
        </div>
        <div class="box-products-container">
	        <h1><?php echo $categoria;?></h1>
                <div id="org-sup-box-content">
                	<div class="box-sort-by">
                    	<span class="number-found-itens">Foram encontrados: <?php echo $total_registros;?></span>
                        <span class="itens-por-pagina">Itens por página</span>
                        <select name="qtde-itens-pag" class="qtde-itens" onchange="javascrip:paginacao('limit',this.value);">
                        	<?php
                            	echo combo_itens_pagina($limit);
							?>
                        </select>
                        <span class="sort-by">Ordenação</span>
                        <select name="sort-by-itens" class="sort-by-itens" onchange="javascript:paginacao('ordem',this.value);">
							<?php
                            	echo combo_ordem_pagina($ordem);
							?>
                        </select>
                    </div>
                    <div class="pagination-box">
                    	<div class="paginacao"><span class="paginacao-text">Página:</span></span> 
							<?php
                            	echo paginacao($pagina, $limit, $total_registros);
							?>                            
                        </div>
                    </div>
                </div>
            <div id="products-category-box">
              <div style="width:100%; height:auto; margin:20px 0 20px 0; text-align:center;">
              	<!--strong>Nenhum produto foi localizado.</strong-->
              </div>
              
              
                <div class="lista-item-vitrine">    
                
                    <form name="frm_produto" id="frm_produto" method="post" action="carrinho.php">
                        <input type="hidden" id="action" name="action" value="incluir" />
                        <input type="hidden" id="lista_presente" name="lista_presente" value="<?php echo $listaid;?>" />
                        <input type="hidden" id="produtoid" name="produtoid" value="0" />
                        <input type="hidden" id="tamanho" name="tamanho" value="0" />
                        <input type="hidden" id="propriedade" name="propriedade" value="0" />
                        <input type="hidden" id="qtde" name="qtde" value="1" />
                        <input type="hidden" id="disponivel" name="disponivel" value="0" />
                        <input type="hidden" id="controla_estoque" name="controla_estoque" value="0" />
                    </form> 
                                               
                    <div id="box-header-lista">
                        <span id="tit-produto-lista" class="tit-bar-text-cart-box">Produto</span>
                        <span id="qtde-produto-lista" class="tit-bar-text-cart-box">Qtde</span>
                        <span id="qtde-comprada-lista" class="tit-bar-text-cart-box">Qtde Comprada</span>
                        <span id="preco-produto-lista" class="tit-bar-text-cart-box">Preço</span>
                        <span id="total-produto-lista" class="tit-bar-text-cart-box">Comprar</span>
                    </div>                

              
					<?php 
                    
                    $ssql_lista .= " limit $start, $limit";
                        
                    $result = mysql_query($ssql_lista);
                    
                    if($result){
                        
                        $count = 0;
                        $num_rows = mysql_num_rows($result);
                        if($num_rows==0){
                            echo '<div style="width:100%; height:auto; margin:20px 0 20px 0; text-align:center;"><strong>Nenhum produto foi localizado.</strong></div>';	
                        }
                        while($row=mysql_fetch_assoc($result)){
                    
                            $id 		= $row["produtoid"];
							$produto	= $row["pproduto"];
							$descricao	= $row["pdescricao"];
							
							$propriedade	= $row["lcodpropriedade"];
							$tamanho		= $row["lcodtamanho"];
							
							$disponivel			= intval($row["pdisponivel"]);
							$controla_estoque 	= intval($row["pcontrola_estoque"]);
							
							$tamanho_produto	= $row["ptamanho"];
							
							$quantidade_desejada = intval($row["lquantidade"]);
							
							$quantidade	= intval($row["lquantidade"]);
							$quantidade_comprada	= intval(get_lista_presente_qtde_comprada($listaid, $id, $propriedade, $tamanho));
							
                            $valor_unitario = $row["pvalor_unitario"];
                            $valor_comparativo = number_format($row["pvalor_comparativo"],2,',','.');
                            
							$display_comprar = 'style="display:none;"';
							
							$estoque = get_estoque($id, $tamanho, $propriedade);
							
							
							if($quantidade_comprada < $quantidade || $lista_credito == -1){
								$display_comprar = "";
							}

							if( $estoque < ( $quantidade - $quantidade_comprada ) ){
								$quantidade = $estoque;	
							}

							$comprar = '<img src="images/btn-lista-comprar.png" class="btn-add-carrinho" rel="'.$id.'" propriedade="'.$propriedade.'" tamanho="'.$tamanho.'" quantidade="'. ($quantidade - $quantidade_comprada) .'" />';


							if( $estoque <= 0 && $controla_estoque == -1 || $disponivel == 0 ){
								$comprar = "indiponível";	
							}

							
                            if( isset($_SESSION["utm_source"]) ){
                                $desconto_utm =  get_desconto_utm_source($id);
                                $valor_unitario = number_format($valor_unitario - $desconto_utm,2,",",".");
                                
                                if(floatval($desconto_utm)>0){
                                    $valor_comparativo = number_format($row["pvalor_unitario"],2,',','.');
                                }
                                
                            }
                            else
                            {
                                $valor_unitario = number_format($valor_unitario,2,",",".");	
                            }
                    
                            $imagem = $row["pimagem"];
                            if(!file_exists($imagem)){
                                $imagem = "imagem/produto/tumb-indisponivel.png";		
                            }
                            
                            $count++;
                            $ret = '
                                
                                <div class="item-lista-presente">
                        
                                    <div class="img-produto-lista">
                                        <img src="'.$imagem.'" border="0" width="40" height="66" />
                                    </div>
                                    
                                    <div class="desc-lista-presente">
                                        <span class="nome-lista-presente">'.$produto.'&nbsp;&nbsp;'.$tamanho_produto.'</span>
                                        <span class="desc-listas">'.$descricao.'</span>
                                    </div>
                                    
                                    <div class="qtde-lista-presente">
                                        <span class="desc-qtde-lista">'.$quantidade_desejada.'</span>                        	
                                    </div>
                                    
                                    <div class="comprada-lista-presente">
                                        <span class="desc-qtde-lista">'.$quantidade_comprada.'</span>                        	
                                    </div>                        
                                    
                                    <div class="preco-lista-presente">
                                        <span class="preco-qtde-lista">R$ '.$valor_unitario.'</span>                        	
                                    </div>
                                    
                                    <div class="btn-lista-presente" '.$display_comprar.'>                        	
                                        '.$comprar.'                        	
                                    </div>                                                							
                                
                                </div>
                    
                              ';
                              
                              echo $ret;
                        }
                        mysql_free_result($result);
                    }				  
                    
                    ?>
                  
                  
                </div>
                                  

                <div id="org-sup-box-content">
                	<div class="box-sort-by">
                    	<span class="number-found-itens">Foram encontrados: <?php echo $total_registros;?></span>
                        <span class="itens-por-pagina">Itens por página</span>
                        <select name="qtde-itens-pag" class="qtde-itens" onchange="javascrip:paginacao('limit',this.value);">
                        	<?php
                            	echo combo_itens_pagina($limit);
							?>
                        </select>
                        <span class="sort-by">Ordenação</span>
                        <select name="sort-by-itens" class="sort-by-itens" onchange="javascript:paginacao('ordem',this.value);">
							<?php
                            	echo combo_ordem_pagina($ordem);
							?>
                        </select>
                    </div>
                    <div class="pagination-box">
                    	<div class="paginacao"><span class="paginacao-text">Página:</span></span> 
							<?php
                            	echo paginacao($pagina, $limit, $total_registros);
							?>                            
                        </div>
                    </div>
                </div>
              
              
			</div>
        </div>
	</div>
    
    <div id="footer-container">
    <?php
		include("inc_footer.php");
	?>
    </div>
</div>
</body>
</html>