<?php
	include("include/inc_conexao.php");
	include("include/inc_cadastro.php");
	include("include/inc_frete.php");
	include("include/inc_orcamento.php");
	

	/*-------------------------------------------------------------
	verifica se ta logado
	-------------------------------------------------------------*/
	if(isset($_SESSION["cadastro"])){
		$cadastro = $_SESSION["cadastro"];
		if(!is_numeric($cadastro)){
			header("location: login.php?redir=minha-conta.php");
			exit();
		}
	}
	else
	{
		header("location: login.php?redir=frete.php");
		exit();
	}

	/*-------------------------------------------------------------------	
	navegação com ssl
	---------------------------------------------------------------------*/
	$config_certificado_instalado = get_configuracao("config_certificado_instalado");
	if($config_certificado_instalado==-1){
		if(strpos($_SERVER['SERVER_NAME'],".com")>0){
			if($_SERVER['SERVER_PORT']==80){
				header("location: https://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
				exit();
			}
		}	
	}

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie
	

	$orcamento = 0;
	$cadastro = 0;
	$freteid = 0;

	$cadastro = 0;
	$enderecoid = 0;
	
	$titulo;
	$nome;
	$endereco;
	$numero;
	$complemento;
	$bairro;
	$cidade;
	$estado;
	$cep;
	$referencia;	
	$cep_destino;
	

	/*------------------------------------------------------------------------
	verifica se tem algum orcamento em aberto com base em cookies
	--------------------------------------------------------------------------*/
	if(isset($_COOKIE["orcamento"])){
		$orcamento = $_COOKIE["orcamento"];
		if(!is_numeric($orcamento)){
			$orcamento = 0;	
		}
	}
	if($orcamento==0){
		header("location: index.php");
		exit();
	}		
	
	
	/*------------------------------------------------------------------------
	verifica se está logado
	--------------------------------------------------------------------------*/
	if(isset($_SESSION["cadastro"])){
		$cadastro = $_COOKIE["cadastro"];
		if(!is_numeric($cadastro) || $cadastro <= 0){
			$cadastro = 0;	
		}
	}
	

	/*------------------------------------------------------------------------
	verifica se ja tem cookie com o cep de destino
	--------------------------------------------------------------------------*/	
	if(isset($_COOKIE["cep_destino"])){
		$cep_destino = 	$_COOKIE["cep_destino"];			  
	}
	
	
	
	
	/*------------------------------------------------------------------------
	frete
	--------------------------------------------------------------------------*/
	if(isset($_COOKIE["frete_codigo"])){
		$freteid = $_COOKIE["frete_codigo"];
		if(!is_numeric($freteid)){
			$freteid = 0;	
		}
	}	
	
	
	/*-----------------------------------------------------------------
	
	-----------------------------------------------------------------*/
	if($_POST){
	
		if($_REQUEST["action"]=="endereco"){
			$enderecoid = $_POST["enderecoid"];			
			get_endereco($enderecoid);
					
			//grava o cep de destino se nao tiver sido informado;			
			$cep_destino = $cep;
			//echo $cep;
			setcookie("cep_destino",$cep,$expires);			
			orcamento_grava_endereco($orcamento);
		}
		
		

		if($_REQUEST["action"]=="gravar"){
			$freteid = $_REQUEST["frete"];			
			
			setcookie("frete_codigo",$freteid,$expires);
			orcamento_atualiza_frete_codigo($orcamento);	
			
			header("location: pagamento.php");
		}


		
	}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Finalizar Pedido Frete</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Finalizar Pedido Frete" />
<meta name="description" content="<?php echo $site_nome;?> Finalizar Pedido Frete. Escolha o meio de entrega para seu pedido" />
<meta name="keywords" content="<?php echo $site_nome;?> Finalizar Pedido Endereço" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?>  Finalizar Pedido Endereço" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/frete.php" />

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		$("#endereco_cep").mask("99999-999");
		
		$('#endereco_cep').keyup(function(e){ 
			var t = document.getElementById("endereco_cep").value;
			t = get_only_numbers(t);
			if(t.length==8){
				endereco_load(t);
			}
		}); 		
		
    });	
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">



	<div id="header-content">

        <?php
			include("inc_headerSTEP.php");
		?>

    </div>
    
	<div id="main-box-container">
		<div id="andamento">
			<span class="passox" style="margin-left:0;">1 - Identificação <img src="images/setaSTEP.jpg"/></span>
			<span class="passox ativo" style="margin-left: 162px;">2 - Entrega</span>
			<span class="passox ">3 - Pagamento</span>
			<span class="passox ">4 - Confirmação</span>
		</div>
  
  <div id="enderecos-cadastrados" style="padding: 0px;">
    <div class="campo-cadastro">
                <span class="tit-cat-cadastro">Opções de envio <span style="font-weight:normal; color:#F00"><?php echo $msg;?></span></span>
                <span class="sub-tit-cat-cadastro">Escolha uma forma de envio <span style="font-weight:normal; color:#F00"><?php echo $msg;?></span></span>
    </div>

			<form name="frm_frete" id="frm_frete" action="frete.php" method="post">
            <input type="hidden" name="action" id="action" value="gravar" />
			<?php
			
				$cart_peso = get_carrinho_peso($orcamento);
			
				$ssql = "select freteid, fcodigo, fdescricao, ftexto, fprazo, fcalcular, fvalor_fixo, icone
						from tblfrete_tipo where fativo=-1 order by fordem";
						
				$result = mysql_query($ssql);
				if($result){
					$alert = '';
					$error = '';
					$success = false;
					while($row=mysql_fetch_assoc($result)){
						if(!(($cep_destino >= "01000001" && $cep_destino <= "05999999" ) || ($cep_destino >= "08000000" && $cep_destino <= "08499999" )) && $row["fcodigo"] == "99997"){
							continue;
						}
						$frete = new Frete();
						$frete->codservico = array($row["fdescricao"].'||'.$row["fcodigo"]."||".$row["fcalcular"]);
						$frete->valor_declarado = $cart_subtotal;
						$frete->peso = $cart_peso;
						$frete->cep_destino = $cep_destino;
						$frete->freteid = $row["freteid"];
						
						$frete->caCalculaFrete();						
						
						$valor_frete = $frete->valor_frete;
						//$valor_frete = str_replace("||","",$valor_frete);
						$aux = explode('||', $valor_frete);
						$valor_frete = number_format($aux[0],2,",",".");

						if ($aux[1] == 999) {
							$success = true;
							//$frete = new Frete();
							$frete_gratis = $frete->is_frete_gratis($row["freteid"], $row["fcodigo"]);
							// echo ($cart_subtotal > 149 && $row["freteid"] == 2);
							
							

							if( intval($frete_gratis) == -1 || ($cart_subtotal > 149 && $row["freteid"] == 2)) {
								$valor_frete = "*FRETE GRÁTIS";//"* frete grátis";	
								if(($cep_destino >= "69301000" && $cep_destino <= "69318736") || ($cep_destino >= "76801000" && $cep_destino <= "78979335")) {
									$valor_frete = $aux[0];
								} else {
									$valor_frete = "*FRETE GRÁTIS";//"* frete grátis";	
								}
							} else {
								$valor_frete = $aux[0];
							}
							//$frete = new Frete();
							$frete_prazo = $frete->CalculaPrazoEntrega($row["fcodigo"]);
							
							$frete_prazo = ($frete_prazo == 0 ) ? "" : " até $frete_prazo dias úteis";						
							// echo $frete_prazo;
							
							//echo $frete_gratis;
							
							
							if($row["fcalcular"]==0){
								$valor_frete = number_format($row["fvalor_fixo"],2,",",".");
							}

							//print_r($frete->erro);

							//if(count($frete->erro) == 0){
								$i++;
								echo '	<div class="box-frete" style="font-family:arial">
										<label style="font-family:arial;">
										<div class="entrega-logo-content"><img src="'.$row["icone"].'" /></div>
										<span class="prazo-frete"><b>'.$frete_prazo.'</b></span>
										<input type="radio" id="frete" name="frete" value="'.$row["freteid"].'"';

								if($i==1 && $freteid==0){
									echo ' checked ';
								}
								else
								{
									if($freteid==$row["freteid"]){
										echo ' checked '; 	
									}	
								}
								echo '	/>&nbsp;';
								// echo $row["fdescricao"];
								
								// if($frete_prazo != ""){
								// 	echo '< '.$frete_prazo.' > ';
								// }
								// else
								// {
								// 	echo ' > ';	
								// }
								
										// if($valor_frete>0){
										// 	echo ' R$ ';	
										// }
										// echo $valor_frete . ". ";	
										echo ' <div class="text-box">';
								
										echo "<strong>(R$ ".$valor_frete.")</strong>&nbsp;";
										echo $row["ftexto"];
										echo '</label>';
										echo '</div>';
										echo '</div>';
						} elseif ($aux[1] == 1000) {
							$alert = $aux[0];
						} elseif ($aux[1] == 1001) {
							$error = $aux[0];
						}
					}
					mysql_free_result($result);
					if (! $success && strlen($alert) > 0) {
			?>
						<script>
							$(document).ready(function() {
								$('.lightbox').fadeIn();
								$('.warning .message').html("<?php echo $alert; ?>");
								$('.response').on('click', function() {
									$('.lightbox').fadeOut();
									window.location = 'endereco.php'
								});
							});
						</script>
			<?php
					} elseif (! $success && strlen($error) > 0) {
			?>
						<script>
							$(document).ready(function() {
								$('.lightbox').fadeIn();
								$('.warning .message').html("Ocorreu um erro ao calcular o frete. Entre em contato conosco através de um de nossos canais abaixo e informe o código do erro: "+<?php echo $error; ?>+". Obrigado.");
								$('.response').on('click', function() {
									$('.lightbox').fadeOut();
									window.location = 'endereco.php'
								});
							});
						</script>	
			<?php			
					}
				}
			?>
		</form>
            
        </div>
		



        <div id="container-cadastro" style="min-height:0px;">
            <div id="btns-navegacao">
            <div id="btn-voltar">
	            <a href='endereco.php'>Voltar</a>
            </div>
                <div id="btn-continuar">
                	<a href="javascript:void(0);" onclick="javascript:frete_pagamento();">
					<img src="images/btn-continuar.jpg" border="0" align="Continuar" title="Continuar" />
                    </a>
                </div>
            </div>
        </div>
  </div>   

    
    <div id="footer-container">
		<?php
            include("inc_footerSTEP.php");
        ?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>