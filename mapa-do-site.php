<?php
	include("include/inc_conexao.php");	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> - Mapa do Site</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> - Mapa do Site" />
<meta name="description" content="<?php echo $conteudo_descricao;?>" />
<meta name="keywords" content="<?php echo $conteudo_palavra_chave;?>" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $conteudo_titulo;?>" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?php echo $site_site;?>/mapa-do-site.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<?php
include("include/inc_analytics.php");	
?>

</head>


<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">
    	<h1 class="h1-mapa-site">Categorias</h1>
        <div class="box-mapa-site">
            <DL>
                <?php
                    $ssql = "select categoriaid, ccategoria, clink_seo from tblcategoria where cativa=-1 and ccodcategoria=0 order by cordem, ccategoria";
                    //echo $ssql;
                    $result = mysql_query($ssql);
                    if($result){
                        while($row=mysql_fetch_assoc($result)){
                            echo '<DT>';
                                echo '<a href='.$row[clink_seo].' alt='.$row["ccategoria"].' title='.$row["ccategoria"].'>'.$row["ccategoria"].'</a>';
							echo '</DT>';
                                
                            $ssql = "select categoriaid, ccategoria, clink_seo from tblcategoria where cativa=-1 and ccodcategoria=".$row['categoriaid']." order by cordem, ccategoria";
                            $result1 = mysql_query($ssql);
                            if($result1){
                                while($row1=mysql_fetch_assoc($result1)){
                                    echo '<DD>';
                                        echo '<a href="'.$row["clink_seo"].'/'.$row1["clink_seo"].'" alt="'.$row1["ccategoria"].'" title="'.$row1["ccategoria"].'">'.$row1["ccategoria"].'</a>';
									echo '</DD>';
                                } mysql_free_result($result1);
                            }
                        }mysql_free_result($result);
                    }
                    ?>
            </DL>
    	</div>
        <div class="box-mapa-site">
        	<h1 class="h1-mapa-site">Institucional</h1>
            <!--//a href="programa-afiliados.php">Afiliados</a//-->
            <a href="central-relacionamento.php">Central de Relacionamento</a>
            <a href="como-comprar.php">Como Comprar</a>
            <a href="entregas.php">Entregas</a>
            <a href="pagamentos.php">Formas de Pagamento</a>
            <!--//a href="lojas.php">Nossas Lojas</a//-->
            <a href="politica-privacidade.php">Politica de Privacidade</a>
            <a href="sobre.php">Sobre a <?php echo $site_nome;?></a>
            <a href="trabalhe-conosco.php">Trabalhe Conosco</a>
            <a href="trocas.php">Trocas e Devoluções</a>
        </div>
    </div>
</div>
</body>
</html>