<?php
	include("include/inc_conexao.php");
	
	/*-------------------------------------------------
	//navegação sem ssl
	---------------------------------------------------*/
	if(strpos($_SERVER['SERVER_NAME'],".com")>0){
		if($_SERVER['SERVER_PORT']!=80){
			header("location: http://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
			exit();
		}
	}
	
	
	//monta a categoria
	$uri = str_replace("/stylemarket/","",$_SERVER['REQUEST_URI']);	

	$pagina = 1;
	$start = 0;
	$limit = 16;
	$ordem = 0;
	
	$canonical = "";
	$menor = 0;
	$maior = 999999;
	

	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}

	
	
	if(is_numeric($qs["ordem"])){	
		$ordem = $qs["ordem"];
		if($ordem>8 || $ordem <0){
			$ordem = 0;	
		}
	}	
	
	
	/*--------------------------------------------------------------------------
	remove os parametros de querystring para montar as info de categorias
	---------------------------------------------------------------------------*/
	if(strpos($uri,"?")<>""){
		$uri = substr($uri,0,strpos($uri,"?")-1);
	}

	$nodes = explode("/",$uri);
	$child=0;

	$codcategoria_left 	= 0;	//paretro paara inc_left_categoria.php
	$codmarca          	= 0;
	$string 		   	= addslashes($_REQUEST["string"]);
	$ip					= $_SERVER['REMOTE_ADDR'];

	
	
	
	//veio do link anterior do site
	if( strpos($string,".html") || strpos($_SERVER['REQUEST_URI'],".html") ){
		$string = str_replace(".html","",$string);
		$string = str_replace(".","",$string);
		$string = str_replace("-"," ",$string);
		$string = left($string,20);
	}
	
	

	if(isset($_REQUEST["marca"])){
		$codmarca = $_REQUEST["marca"];
		if(!is_numeric($codmarca)){
			$codmarca=0;	
		}
	}
	



		switch ($ordem){
			
			case "0":
			$order = " pdescricao ";
			break;				
			
			case "1":
			$order = " pvalor_unitario";
			break;

			case "2":
			$order = " pvalor_unitario desc";
			break;

			case "3":
			$order = " pclick desc";	//mais clicado
			break;

			case "4":
			$order = " pvalor_unitario desc";	//mais bem avaliados
			break;

			case "5":
			$order = " pproduto";
			break;

			case "6":
			$order = " pproduto desc";
			break;

			case "7":
			$order = " pdata_cadastro desc";	//lancamento
			break;

			case "8":
			$order = " pdesconto desc";
			break;

		}

		/*----------------------------------------------------------------------
		insere a palavra bucada na tblbusca
		----------------------------------------------------------------------*/
		$ssql = "select btag from tblbusca where btag='{$string}' and bip='{$ip}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)==0){

				$ssql = "insert into tblbusca (btag, borigem, bip, bdata_alteracao, bdata_cadastro)
							values('{$string}','1','{$ip}','{$data_hoje}','{$data_hoje}')";
				mysql_query($ssql);
				//echo $ssql;
			}	
		}
		
		
		//define se ira agrupar os produtos com mesma referencia
		$config_agrupa_por_referencia = get_configuracao("config_agrupa_por_referencia");
		if($config_agrupa_por_referencia==0){
			$agrupa_por_referencia = ", tblproduto.produtoid ";	
		}


		/*------------------------------------------------------------------------
		busca
		-------------------------------------------------------------------------*/

		IF(isset($_REQUEST["menor"])){$menor = $_REQUEST["menor"];}
		IF(isset($_REQUEST["maior"])){$maior = $_REQUEST["maior"];}

		if(!is_numeric($menor)){
			$menor = 0;
		}

		if(!is_numeric($maior)){
			$maior = 99999;
		}




		$ssql_busca = "select 1 as rank, tblproduto.produtoid, tblproduto.pdescricao, tblproduto.pdisponivel, sum(tblestoque.eestoque) as estoque,tblproduto.pcontrola_estoque, tblmarca.mmarca, tblmarca.mlink_seo, tblproduto.preferencia, tblproduto.pproduto,
					tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, (tblproduto.pvalor_comparativo - tblproduto.pvalor_unitario) as pdesconto,
					tblproduto.plink_seo, tblproduto_midia.marquivo as pimagem, tblproduto_midia2.marquivo as pimagem2
					from tblproduto
					left join tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
					left join tblproduto_midia on tblproduto.produtoid = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal = -1
					left join tblproduto_midia as tblproduto_midia2  on tblproduto.produtoid = tblproduto_midia2.mcodproduto and tblproduto_midia2.mprincipal = 2
					left join tblproduto_categoria on tblproduto.produtoid = tblproduto_categoria.pcodproduto 
					left join tblmarca on marcaid = tblproduto.pcodmarca";

		if($codmarca==0){
			$ssql_busca .= " where (tblproduto.pproduto like '%{$string}%' or tblproduto.ppalavra_chave like '%{$string}%') and tblproduto.pvalor_unitario between '{$menor}' and '{$maior}' and tblproduto.pdata_inicio<='{$data_hoje}' and tblproduto.pdata_termino>='{$data_hoje}'
			and tblproduto.pvalor_unitario between '{$menor}' and '{$maior}' and tblproduto.pdata_inicio<='{$data_hoje}' and tblproduto.pdata_termino>='{$data_hoje}'";	
		}

		if($codmarca>0){
			$ssql_busca .= " where (tblproduto.pproduto like '%{$string}%' or tblproduto.ppalavra_chave like '%{$string}%') and tblproduto.pvalor_unitario between '{$menor}' and '{$maior}' and tblproduto.pcodmarca='{$codmarca}'  and tblproduto.pdata_inicio<='{$data_hoje}' and tblproduto.pdata_termino>='{$data_hoje}'";	
		}
		
		if(isset($_GET["variacao"])){
			if((int)$_GET["variacao"] > 0){
				$variacao = mysql_query("select ccodproduto from tblproduto_caracteristica where ccodpropriedade = ".addslashes((int)$_GET["variacao"]));
				$ids_tamanho = "0";
				while($rw = mysql_fetch_array($variacao)){
					$ids_tamanho .= ",".$rw["ccodproduto"];
				}
				$ssql_busca .= " and tblproduto.produtoid in ($ids_tamanho) ";
			}
		}
		
		$ssql_busca .= " group by  tblproduto.preferencia $agrupa_por_referencia
						 having(1=1)
						 order by $order ";
		//echo $ssql_busca;
		//die();

		//echo $ssql_busca;
		//die();
		


		$result = mysql_query($ssql_busca);
		if($result){
			$total_registros = mysql_num_rows($result);	
		}
						
		
		
		

/*-------------------------------------------------------------------
base href
--------------------------------------------------------------------*/
$server = ($_SERVER['SERVER_PORT']==80 ? "http://" : "https://") . $_SERVER['SERVER_NAME'] . str_replace("busca.php","",$_SERVER['SCRIPT_NAME']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $categoria?>" />
<meta name="description" content="<?php echo $categoria;?>" />
<meta name="keywords" content="<?php echo $categoria;?>" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $categoria;?>" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?php echo $site_site;?>/categoria.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<?php
include("include/inc_analytics.php");	
?>
</head>

<body>
<div id="global-container">
	<div id="header-content">
	<?php
			include("inc_header.php");
		?>
    </div>
	<div id="main-box-container">
   	  <div id="categoria-container-box"></div>
    	<div id="container-menu-left">
        	<?php
            	include("inc_left_busca.php");
			?>            
        </div>
        <div class="box-products-container">
			<div id="org-sup-box-content">
				<div class="box-sort-by">
					<span class="number-found-itens"><?php echo $total_registros;?> ítens encontrados</span>
					<span class="sort-by">Ordenar por: </span>
					<select name="sort-by-itens" class="sort-by-itens" onchange="javascript:paginacao('ordem',this.value);">
						<?php
							echo combo_ordem_pagina($ordem);
						?>
					</select>
					<span class="itens-por-pagina">Itens por página: </span>
					<select name="qtde-itens-pag" class="qtde-itens" onchange="javascrip:paginacao('limit',this.value);">
						<?php
							echo combo_itens_pagina($limit);
						?>
					</select>
				</div>
				<div class="pagination-box">
					<div class="paginacao"></span> 
						<?php
							echo paginacao($pagina, $limit, $total_registros);
						?>                            
					</div>
				</div>
			</div>
            <div id="products-category-box">
                  <?php 

					$imagem_x = 260;
					$imagem_y = 260;
					$ssql_busca = "(".str_replace("having(1=1)","HAVING ((SUM(tblestoque.eestoque) > 0 && tblproduto.pcontrola_estoque = -1 && tblproduto.pdisponivel = -1) || (tblproduto.pdisponivel = -1 && tblproduto.pcontrola_estoque = 0 ))",$ssql_busca).") UNION ALL (".str_replace("having(1=1)","HAVING (((SUM(tblestoque.eestoque) = 0 || SUM(tblestoque.eestoque) is null) && tblproduto.pcontrola_estoque = -1) || tblproduto.pdisponivel = 0)",str_replace(" 1 as rank"," 2 as rank",$ssql_busca)).")";
					$vitrine_ordem = str_replace("tblproduto.","",$vitrine_ordem);
					$order = str_replace("tblproduto.","",$order);
					$ssql_busca .= " order by $order limit $start, $limit";
						
					$result = mysql_query($ssql_busca);
			
					if($result){
						
						$count = 0;
						$num_rows = mysql_num_rows($result);
						if($num_rows==0){
							echo '<div style="width:100%; height:auto; margin:20px 0 20px 0; text-align:center;"><strong>Nenhum produto foi localizado.</strong></div>';	
						}
						$products = array();
						$valor_total = 0;
						while($row=mysql_fetch_assoc($result)){
							$valor_total += $row["pvalor_unitario"];
							$products[] = $row["produtoid"];
							$id = $row["produtoid"];
							$referencia = $row["preferencia"];
							$valor_unitario = $row["pvalor_unitario"];
							$valor 			= number_format($row["pvalor_unitario"],2,",",".");
							$valor_comparativo = number_format($row["pvalor_comparativo"],2,',','.');
							
							if( isset($_SESSION["utm_source"]) ){
								$desconto_utm =  get_desconto_utm_source($id);
								$valor_unitario = number_format($valor_unitario - $desconto_utm,2,",",".");
								
								if(floatval($desconto_utm)>0){
									$valor_comparativo = number_format($row["pvalor_unitario"],2,',','.');
								}
								
							}
							else
							{
								$valor_unitario = number_format($valor_unitario,2,",",".");	
							}

							$imagem = $row["pimagem"];
							$imagem2 = $row["pimagem2"];

							if(file_exists($imagem) && !file_exists($imagem2)){
								$imagem2 = $imagem;		
							}
							if(!file_exists($imagem) && file_exists($imagem2)){
								$imagem2 = $imagem;		
							}
							if(!file_exists($imagem)){
								$imagem = "imagem/produto/med-indisponivel.png";		
							}
							if(!file_exists($imagem2)){
								$imagem2 = "imagem/produto/med-indisponivel.png";		
							}
							$count++;
							?>
							<a href="<?=$row["plink_seo"] ?>">
							<div class="produto">
								<span class="product-thumb"><img src="<?=$imagem?>" alt="produto"></span>
								<span class="product-menu-top20-product-title"><?=$row["pproduto"] ?></span>
								<span class="product-menu-top20-product-mark"><b>+</b> <?=$row["mmarca"] ?></span>
								<span class="product-menu-top20-product-price">R$ <s><?=number_format($row["pvalor_unitario"],2,",",".") ?></s></span>
								<span class="par-velue"><?=get_valor_parcelado_new($row["produtoid"],$row["pvalor_unitario"])?></span>
								<span class="a-vista-price">Ou R$ <?=number_format($row["pvalor_unitario"]-($row["pvalor_unitario"]*0.10),2,",",".")?> à vista</span>
								<span class="boleto-text">via boleto</span>
							</div>
							</a>
							<?php
						}
						mysql_free_result($result);
					}				  
				  
				  ?>
			</div>
			<div id="org-sup-box-content">
				<div class="box-sort-by">
					<span class="number-found-itens"><?php echo $total_registros;?> ítens encontrados</span>
					<span class="sort-by">Ordenar por: </span>
					<select name="sort-by-itens" class="sort-by-itens" onchange="javascript:paginacao('ordem',this.value);">
						<?php
							echo combo_ordem_pagina($ordem);
						?>
					</select>
					<span class="itens-por-pagina">Itens por página: </span>
					<select name="qtde-itens-pag" class="qtde-itens" onchange="javascrip:paginacao('limit',this.value);">
						<?php
							echo combo_itens_pagina($limit);
						?>
					</select>
				</div>
				<div class="pagination-box">
					<div class="paginacao"></span> 
						<?php
							echo paginacao($pagina, $limit, $total_registros);
						?>                            
					</div>
				</div>
			</div>
              
        </div>
              
		<div id="aside-right-bar2">
			<?php carregaBanners(2); ?>
		</div>
	</div>
    
    <div id="footer-container">
    <?php
		include("inc_footer.php");
	?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>

<script type="text/javascript">
var google_tag_params = {
	ecomm_prodid: <?php echo json_encode($products); ?>,
	ecomm_pagetype: 'searchresults',
	ecomm_totalvalue: <?php echo number_format($valor_total, 2, ".", ".");?>,
};
</script>

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 970176820;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

<noscript>
<div style="display:inline;">
	<img height="1" width="1" style="border­style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/970176820/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</body>
</html>