<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie
	
	if($_POST){
		$data1 	= addslashes($_REQUEST["data1"]);
		$data2 	= addslashes($_REQUEST["data2"]);
		$status = addslashes($_REQUEST["status"]); 
		$codigo = addslashes($_REQUEST["codigo"]); 
	
		if($data1!= null && $data1 != ""){
			list($dia,$mes,$ano) = explode("/",$data1);
			$data_db1 = $ano."-".$mes."-".$dia;
		}
		
		if($data2!= null && $data2 != ""){
			list($dia,$mes,$ano) = explode("/",$data2);
			$data_db2 = $ano."-".$mes."-".$dia;
		}	
	
	}
	
	if(!$_POST || strlen($data1)<10 || strlen($data2)<10){
	
		$data1 	= "01/".date("m/Y");
		$data2 	= date("d/m/Y");
		
		$data_db1 = formata_data_db($data1);
		$data_db2 = formata_data_db($data2);	
		
		$codigo = 0;
		$status	= 0;

	}


	$prazo = intval(get_configuracao("admin_pedido_historico_dias"));
	if($prazo==0){
		$prazo = 30;	
	}
	
	
	$data = strtotime(date("Y-m-d", strtotime($data_hoje)) . "-$prazo days") . "";
	$data_tela = date("d/m/Y",$data);
	$data = date("Y-m-d H:i:s",$data);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
	
	$(document).ready(function() {	

    });	
	
	//click na linha do pedido
	$('.tr_lista').live('click', function(){
		var $this = $( this );  
		var id = $this.attr("rel");  
		window.location.href = "pedido_consulta.php?status="+id+"&data1=<?php echo $data_tela;?>";		
	})	
	
</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">
    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
	?>
        
    </div>
    
    <div id="content">
    
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Informações gerais sobre o sistema</span>
            </div>
            <div id="conteudo-interno">
            	<table width="98%" border="0" style="margin: 10px; float:left;">
                <tr>
                	<td height="25">Pedidos no sistema desde :<?php echo $data_tela;?></td>
               	</tr>				
                <?php
					$ssql = mysql_query("SELECT * FROM tblpedido ORDER BY pedidoid DESC LIMIT 1 ");
					while($ln = mysql_fetch_assoc($ssql)){
						$data_aux = explode(" ",$ln["pdata_cadastro"]);
						$data_aux2 = explode("-", $data_aux[0]);
						$data = $data_aux2[2]."/".$data_aux2[1]."/".$data_aux2[0];
						echo '<tr class="tr_lista"><td height="25">Último Pedido: '.$data." ".$data_aux[1].'</td></tr>';
					}
                $ssql = "SELECT tblpedido_status.statusid, tblpedido_status.sstatus,tblpedido_status.sdescricao, count( tblpedido.pedidoid ) AS total
							FROM tblpedido_status
							LEFT JOIN tblpedido ON tblpedido_status.statusid = tblpedido.pcodstatus
							where tblpedido.pdata_cadastro >='{$data}' and tblpedido.pfinalizado=-1							
							GROUP BY tblpedido_status.sstatus
							order by tblpedido_status.statusid
							";	
				//echo $ssql;
				
				$result = mysql_query($ssql);
				if($result){
					while($row=mysql_fetch_assoc($result)){
						echo '  <tr class="tr_lista" rel="'.$row["statusid"].'">
								<td height="25">Status: '.$row["sstatus"].' : '.$row["total"].'</td>
								</tr>';
					}	
					mysql_free_result($result);
				}
					
				?>
                <tr>
                	<td height="25">&nbsp;</td>
               	</tr>
                <tr>
                	<td height="25">[ <a href="pedido_consulta.php">Listar todos pedidos</a> ]</td>
               	</tr>
                <tr>
                  <td height="25">&nbsp;</td>
                </tr>
                <tr>
                  <td height="25">&nbsp;</td>
                </tr>
                <tr>
                  <td height="25"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:#CCC solid 1px;">
                    <tr>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="10%" align="center"><a href="banner.php"><img src="images/ico_conteudo.gif" alt="Banner" width="32" height="32" border="0" /></a></td>
                      <td width="10%" align="center"><a href="desconto.php"><img src="images/ico_conteudo.gif" alt="Banner" width="32" height="32" border="0" /></a></td>
                      <td width="10%" align="center"><a href="cupom_desconto.php"><img src="images/ico_conteudo.gif" alt="Banner" width="32" height="32" border="0" /></a></td>
                      <td width="10%" align="center">&nbsp;</td>
                      <td width="10%" align="center"><a href="forma_pagamento.php"><img src="images/ico_conteudo.gif" alt="Forma de Pagamento" width="32" height="32" border="0" /></a></td>
                      <td width="10%" align="center"><a href="condicao_pagamento.php"><img src="images/ico_conteudo.gif" alt="Condição de Pagamento" width="32" height="32" border="0" /></a></td>
                      <td width="10%" align="center">&nbsp;</td>
                      <td width="10%" align="center">&nbsp;</td>
                      <td width="10%" align="center">&nbsp;</td>
                      <td width="10%" align="center">&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="25" align="center"><a href="banner_consulta.php">Banners</a></td>
                      <td align="center"><a href="desconto_consulta.php">Regra de Desconto</a></td>
                      <td align="center"><a href="cupom_desconto_consulta.php">Cupom de Desconto</a></td>
                      <td align="center">&nbsp;</td>
                      <td align="center"><a href="forma_pagamento_consulta.php">F. Pagamento</a></td>
                      <td align="center"><a href="condicao_pagamento_consulta.php">Condição Pagto</a></td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td height="25">&nbsp;</td>
                </tr>
                <tr>
                  <td height="25">&nbsp;</td>
                </tr>
                <tr>
                  <td height="25"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:#CCC solid 1px;">
                    <tr>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="10%" align="center"><a href="busca_consulta.php"><img src="images/ico_conteudo.gif" alt="Banner" width="32" height="32" border="0" /></a></td>
                      <td width="10%" align="center">&nbsp;</td>
                      <td width="10%" align="center"><a href="produto_aviseme_consulta.php"><img src="images/ico_conteudo.gif" alt="Banner" width="32" height="32" border="0" /></a></td>
                      <td width="10%" align="center">&nbsp;</td>
                      <td width="10%" align="center">&nbsp;</td>
                      <td width="10%" align="center">&nbsp;</td>
                      <td width="10%" align="center">&nbsp;</td>
                      <td width="10%" align="center">&nbsp;</td>
                      <td width="10%" align="center">&nbsp;</td>
                      <td width="10%" align="center">&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="25" align="center"><a href="busca_consulta.php">+ Buscados</a></td>
                      <td align="center">&nbsp;</td>
                      <td align="center"><p><a href="produto_aviseme_consulta.php">Produtos Indisponíveis</a></p></td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td height="25">&nbsp;</td>
                </tr>
                <tr>
                  <td height="25">&nbsp;</td>
                </tr>
                <tr>
                  <td height="25">&nbsp;</td>
                </tr>                
                </table>
            </div>
        </div>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>