<?php
include("../include/inc_conexao.php");	

$action = $_REQUEST["action"];

$ip 		= $_SERVER['REMOTE_ADDR'];
$usuario 	= intval($_SESSION["usuarioid"]);
$status		= 1;


if($action=="cancelar-pagamento"){
	
	
	gera_log("\r\n\r\n".$_REQUEST["valor"]."\r\n\r\n");

	$id 	= intval(trim($_REQUEST["id"]));
	$tid	= addslashes(trim($_REQUEST["tid"]));
	$valor	= addslashes($_REQUEST["valor"]);
	
	gera_log("\r\n\r\n".$valor."\r\n\r\n");
	
	$valor_formatado = str_replace(",","",$valor);
	$valor_formatado = str_replace(".","",$valor_formatado);
	
	gera_log("\r\n\r\n".$valor_formatado."\r\n\r\n");
	
	$ssql = "select p.pedidoid, p.pvalor_total, p.pcartao_retorno, p.pcodstatus 
				from tblpedido as p
				where p.pedidoid = '{$id}'
			";
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$status			= intval($row["pcodstatus"]);
			$cartao_retorno = $row["pcartao_retorno"];
		}
		mysql_free_result($result);
	}
	
	$xml = simplexml_load_string($cartao_retorno);
	
	
	if( $xml->tid != $tid ){
		echo utf8_encode("erro||O tid informado n�o confere");	
		die();
	}
	
	if($status <= 1){
		echo utf8_encode("erro||O pedido $id deve ser cancelado antes de estornar o valor do pagamento");	
		die();		
	}
	
	
	$filiacao = get_configuracao("cielo_filiacao");
	$chave = get_configuracao("cielo_chave");


	$xml_msg = '<?xml version="1.0" encoding="ISO-8859-1"?>';
	$xml_msg .= '<requisicao-cancelamento id="'.$id.'" versao="1.2.1">';
	$xml_msg .= '<tid>'.$tid.'</tid>';
	$xml_msg .= '<dados-ec>';
	$xml_msg .= '<numero>'.$filiacao.'</numero>';
	$xml_msg .= '<chave>'.$chave.'</chave>';
	$xml_msg .= '</dados-ec>';
	$xml_msg .= '<valor>'.$valor_formatado.'</valor>';
	$xml_msg .= '</requisicao-cancelamento>';
	
	//gera_log("\r\n\r\n".$xml_msg."\r\n\r\n");

	
	if($filiacao=="1006993069" || $chave=="25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3"){	// TESTES E HOMOLOGACAO
		//gera_log("\r\n\r\n".$xml_msg."\r\n\r\n");
		$ws 	= "https://qasecommerce.cielo.com.br/servicos/ecommwsec.do";
	}else{
		if($_SERVER['SERVER_NAME']=="servidor"){
			//gera_log("\r\n\r\n".$xml_msg."\r\n\r\n");	
		}
		$ws = "https://ecommerce.cielo.com.br/servicos/ecommwsec.do";
	}	
			
	$xml_string =  httprequest($ws, "mensagem=".$xml_msg);
	
	$xml = simplexml_load_string($xml_string);

	$codigo = intval($xml->codigo);
	if($codigo>0){
		echo "erro||".utf8_decode(utf8_encode($xml->mensagem));
		die();
	}
	else
	{
		$codigo = $xml->cancelamentos->cancelamento->codigo;
		$mensagem = utf8_decode(utf8_encode($xml->cancelamentos->cancelamento->mensagem));
		$mensagem .= ". R$ " . $valor;
	}
	
	
	//atualiza o retorno do cartao
	$ssql = "update tblpedido set pcartao_retorno='{$xml_string}' where pedidoid = '{$id}'";
	mysql_query($ssql);
	
	//inserre o historico no pedido 	
	$ssql = "insert into tblpedido_historico (hcodpedido, hcodigo, hcodstatus, htexto, hip, hemail, hcodusuario, hdata_alteracao, hdata_cadastro) ";
	$ssql .= " values('{$id}','{$id}','{$status}','{$mensagem}','{$ip}','-1','{$usuario}','{$data_hoje}','{$data_hoje}')";
	mysql_query($ssql);		


	echo "ok||".$mensagem;
	die();

}



if($action=="consultar-transacao"){
	
	$id 		= intval(trim($_REQUEST["id"]));
	$tid		= addslashes(trim($_REQUEST["tid"]));
	
	$filiacao 	= get_configuracao("cielo_filiacao");
	$chave 		= get_configuracao("cielo_chave");	

	$xml_msg = '<?xml version="1.0" encoding="ISO-8859-1"?>';
	$xml_msg .= '<requisicao-consulta id="'.$id.'" versao="1.2.1">';
	$xml_msg .= '<tid>'.$tid.'</tid>';
	$xml_msg .= '<dados-ec>';
	$xml_msg .= '<numero>'.$filiacao.'</numero>';
	$xml_msg .= '<chave>'.$chave.'</chave>';
	$xml_msg .= '</dados-ec>';
	$xml_msg .= '</requisicao-consulta>';	
	
	if($filiacao=="1006993069" || $chave=="25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3"){	// TESTES E HOMOLOGACAO
		//gera_log("\r\n\r\n".$xml_msg."\r\n\r\n");
		$ws 	= "https://qasecommerce.cielo.com.br/servicos/ecommwsec.do";
	}else{
		//if($_SERVER['SERVER_NAME']=="servidor"){
			gera_log("\r\n\r\n".$xml_msg."\r\n\r\n");	
		//}
		$ws = "https://ecommerce.cielo.com.br/servicos/ecommwsec.do";
	}	
	
	$xml_string =  httprequest($ws, "mensagem=".$xml_msg);
	
	$xml = simplexml_load_string($xml_string);	
	
	gera_log("\r\n\r\n".$xml_string."\r\n\r\n");
	
	echo "ok||";

	//dados da transa��o
	echo "TID: " . $xml->{'tid'}  . "<br>";
	echo "PAN: " . $xml->{'pan'}  . "<br>";
	echo "<br>";
	
	//dados do pedido
	echo "<strong>DADOS</strong><br>";
	foreach ($xml->{'dados-pedido'} as $dados){
		echo "NUMERO : " . $dados->{'numero'}  . "<br>";
		echo "VALOR : " . $dados->{'valor'}  . "<br>";
		echo "DATA : " . $dados->{'data-hora'}  . "<br>";
		echo "DESCRICAO : " . $dados->{'descricao'}  . "<br>";
		echo "<br>";
	}

	//forma de pagto
	echo "<strong>PAGAMENTO</strong><br>";
	foreach ($xml->{'forma-pagamento'} as $dados){
		echo "BANDEIRA : " . $dados->{'bandeira'}  . "<br>";
		echo "PRODUTO : " . $dados->{'produto'}  . "<br>";
		echo "PARCELAS : " . $dados->{'parcelas'}  . "<br>";
		echo "<br>";
	}
	
	//autenticacao
	echo "<strong>AUTENTICACAO</strong><br>";
	foreach ($xml->{'autenticacao'} as $dados){
		echo "DATA : " . $dados->{'data-hora'}  . "<br>";
		echo "CODIGO : " . $dados->{'codigo'}  . "<br>";
		echo "MENSAGEM : " . $dados->{'mensagem'}  . "<br>";
		echo "VALOR : " . $dados->{'valor'}  . "<br>";
		echo "ECI : " . $dados->{'eci'}  . "<br>";
		echo "<br>";
	}	
	
	//autorizacao
	echo "<strong>AUTORIZACAO</strong><br>";
	foreach ($xml->{'autorizacao'} as $dados){
		echo "DATA : " . $dados->{'data-hora'}  . "<br>";
		echo "CODIGO : " . $dados->{'codigo'}  . "<br>";
		echo "VALOR : " . $dados->{'valor'}  . "<br>";
		echo "LR : " . $dados->{'lr'}  . "<br>";
		echo "ARP : " . $dados->{'arp'}  . "<br>";
		echo "NSU : " . $dados->{'nsu'}  . "<br>";
		echo "<br>";
	}	
	
	
	//captura
	echo "<strong>CAPTURA</strong><br>";
	foreach ($xml->{'captura'} as $dados){
		echo "DATA : " . $dados->{'data-hora'}  . "<br>";
		echo "CODIGO : " . $dados->{'codigo'}  . "<br>";
		echo "VALOR : " . $dados->{'valor'}  . "<br>";
		echo "MENSAGEM : " . $dados->{'mensagem'}  . "<br>";
		echo "<br>";
	}		


	//captura
	echo "<strong>CANCELAMENTOS</strong><br>";
	foreach ($xml->{'cancelamentos'}->{'cancelamento'} as $dados){
		echo "DATA : " . $dados->{'data-hora'}  . "<br>";
		echo "CODIGO : " . $dados->{'codigo'}  . "<br>";
		echo "VALOR : " . $dados->{'valor'}  . "<br>";
		echo "MENSAGEM : " . $dados->{'mensagem'}  . "<br>";
		echo "<br>";
	}


	
}


function httprequest($paEndereco, $paPost){

	$sessao_curl = curl_init();
	curl_setopt($sessao_curl, CURLOPT_URL, $paEndereco);
	
	curl_setopt($sessao_curl, CURLOPT_FAILONERROR, true);

	//  CURLOPT_SSL_VERIFYPEER
	//  verifica a validade do certificado
	curl_setopt($sessao_curl, CURLOPT_SSL_VERIFYPEER, false);
	//  CURLOPPT_SSL_VERIFYHOST
	//  verifica se a identidade do servidor bate com aquela informada no certificado
	//curl_setopt($sessao_curl, CURLOPT_SSL_VERIFYHOST, 2);

	//  CURLOPT_SSL_CAINFO
	//  informa a localiza��o do certificado para verifica��o com o peer
	//curl_setopt($sessao_curl, CURLOPT_CAINFO, "../include/VeriSignClass3PublicPrimaryCertificationAuthority-G5.crt");
	
	//$ws = tempnam(sys_get_temp_dir(), 'Certificado');
	//file_put_contents($ws, file_get_contents("../include/VeriSignClass3PublicPrimaryCertificationAuthority-G5.crt"));
	
	//curl_setopt($sessao_curl, CURLOPT_CAINFO, $ws);
	//curl_setopt($sessao_curl, CURLOPT_SSLVERSION, 3);

	//  CURLOPT_CONNECTTIMEOUT
	//  o tempo em segundos de espera para obter uma conex�o
	curl_setopt($sessao_curl, CURLOPT_CONNECTTIMEOUT, 10);

	//  CURLOPT_TIMEOUT
	//  o tempo m�ximo em segundos de espera para a execu��o da requisi��o (curl_exec)
	curl_setopt($sessao_curl, CURLOPT_TIMEOUT, 40);

	//  CURLOPT_RETURNTRANSFER
	//  TRUE para curl_exec retornar uma string de resultado em caso de sucesso, ao
	//  inv�s de imprimir o resultado na tela. Retorna FALSE se h� problemas na requisi��o
	curl_setopt($sessao_curl, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($sessao_curl, CURLOPT_POST, true);
	curl_setopt($sessao_curl, CURLOPT_POSTFIELDS, $paPost );

	$resultado = curl_exec($sessao_curl);

	$response = curl_getinfo($sessao_curl, CURLINFO_HTTP_CODE);

	//curl_close($sessao_curl);

	if ($resultado)
	{
		return $resultado;
	}
	else
	{
		return curl_error($sessao_curl);
	}

}


?>