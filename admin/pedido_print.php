<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie
	
	
	
	/*-------------------------------------------------------------
	pega número do pedido para detalhar
	--------------------------------------------------------------*/
	$id = $_REQUEST["id"];
	
	$ssql = "select tblpedido.pedidoid, tblpedido.pcodorcamento, tblpedido.pcodigo, tblpedido.pcodcadastro, tblpedido.ptitulo, tblpedido.pnome, 
			tblpedido.pendereco, tblpedido.pnumero, tblpedido.pcomplemento, tblpedido.pbairro, tblpedido.pcidade, tblpedido.pestado, tblpedido.pcep, 
			tblpedido.psubtotal, tblpedido.pvalor_desconto, tblpedido.pvalor_desconto_cupom, tblpedido.pvalor_frete, tblpedido.pvalor_presente, tblpedido.pvalor_total, 
			tblpedido.pcodforma_pagamento, tblpedido.pcodcondicao_pagamento, tblpedido.pcodfrete, tblpedido.pcodcupom_desconto, 
			tblpedido.pcodigo_rastreamento, tblpedido.pcodstatus, tblpedido.pdata_cadastro, 
			tblpedido.pcartao_numero, tblpedido.pcartao_retorno, 
	
			tblcadastro_endereco.etitulo, tblcadastro_endereco.enome, tblcadastro_endereco.eendereco, tblcadastro_endereco.enumero, tblcadastro_endereco.ecomplemento,
			tblcadastro_endereco.ebairro, tblcadastro_endereco.ecidade, tblcadastro_endereco.eestado, tblcadastro_endereco.ecep, tblcadastro_endereco.ereferencia,
	
			tblforma_pagamento.fdescricao, tblcondicao_pagamento.ccondicao,
			tblcadastro.cadastroid, tblcadastro.crazao_social, tblcadastro.cnome, tblcadastro.ccodtipo, tblcadastro.ccpf_cnpj, tblcadastro.crg_ie, tblcadastro.cdata_nascimento, tblcadastro.cemail,
			tblcadastro.ctelefone, tblcadastro.ccelular, tblcadastro_tipo.ttipo, 
			tblpedido_status.statusid, tblpedido_status.sdescricao, tb_cadastro_entrega.ereferencia as referencia_entrega,
			tblcupom_desconto.ccupom
	
			from tblpedido 
	
			left join tblcadastro_endereco on tblpedido.pcodendereco_fatura=tblcadastro_endereco.enderecoid 
			left join tblcadastro_endereco as tb_cadastro_entrega on tblpedido.pcep=tb_cadastro_entrega.ecep 

			left join tblcadastro on tblpedido.pcodcadastro=tblcadastro.cadastroid 
			left join tblcadastro_tipo on tblcadastro.ccodtipo=tblcadastro_tipo.tipoid
			left  join tblforma_pagamento on tblpedido.pcodforma_pagamento=tblforma_pagamento.formapagamentoid 
			left join tblcondicao_pagamento on tblpedido.pcodcondicao_pagamento=tblcondicao_pagamento.condicaoid 
			left join tblpedido_status on tblpedido.pcodstatus=tblpedido_status.statusid 
			left join tblcupom_desconto on tblpedido.pcodcupom_desconto=tblcupom_desconto.cupomid 
			
			where tblpedido.pedidoid='{$id}' 
			limit 0,1";
	
	//echo $ssql;
	
	$result = mysql_query($ssql);
	if($result){
		
		while($row=mysql_fetch_assoc($result)){
			
			$orcamento			= $row["pcodorcamento"];
			
			$codigo_pedido	 	= "000000".$row["pcodigo"];
			$codigo_pedido	 	= substr($codigo_pedido, strlen($codigo_pedido)-6,6);
			$codigo_cliente		= $row["pcodcadastro"];
			$razao_social		= $row["crazao_social"] . "&nbsp;" . $row["cnome"];
			$tipo_cadastro		= $row["ttipo"];
			$cnpj_cpf			= $row["ccpf_cnpj"];
			$ie_rg				= $row["crg_ie"];
			$data_nascimento 	= formata_data_tela($row["cdata_nascimento"]);
			$telefone			= $row["ctelefone"];
			$celular			= $row["ccelular"];
			$data_pedido     	= formata_data_tela($row["pdata_cadastro"]);
			$identificacao	 	= $row["ptitulo"];
			$nome 			 	= $row["pnome"];
			$email 			 	= $row["cemail"];
			$endereco		 	= $row["pendereco"]. ", " . $row["pnumero"]. " - " . $row["pcomplemento"]. " - " . $row["pbairro"];
			$cidade 		 	= $row["pcidade"];
			$estado			 	= $row["pestado"];
			$cep 			 	= $row["pcep"];
			$cep			 	= substr($cep, 0,5). "-" . substr($cep, 5,3);
			$referencia 	 	= $row["referencia_entrega"];
			$codigo_rastreamento= $row["pcodigo_rastreamento"];
			
			if($row["etitulo"]!=""){
				$endereco_fatura	=  $row["etitulo"]." - ".$row["eendereco"].",&nbsp;".$row["enumero"]."&nbsp;".$row["ecomplemento"]." - ".$row["ebairro"]." - ".$row["ecidade"]." - ".$row["eestado"]." - ".$row["ecep"];
			}
			
			$subtotal		 	= formata_valor_tela($row["psubtotal"]);
			$valor_desconto	 	= formata_valor_tela($row["pvalor_desconto"]);
			$valor_presente		= formata_valor_tela($row["pvalor_presente"]);
			$valor_frete	 	= formata_valor_tela($row["pvalor_frete"]);
			$valor_total	 	= formata_valor_tela($row["pvalor_total"]);
			$forma_pagamento 	= $row["fdescricao"];
			$cod_condicao_pgto 	= $row["pcodcondicao_pagamento"];
			$condicao_pagamento = $row["ccondicao"];
			
			$status				= $row["statusid"];
			$status_pedido 		= $row["sdescricao"];
			
			$tipo_frete 		= $row["pcodfrete"];
			$cupom_desconto		= $row["ccupom"];
			$valor_cupom_desconto= number_format($row["pvalor_desconto_cupom"],2,",",".");
			
			$cartao_numero		= "**** ".right("".$row["pcartao_numero"],4);
			$cartao_retorno		= utf8_decode($row["pcartao_retorno"]);	
			
		}
		mysql_free_result($result);
	}
	
	// Nova busca para pegar tipo do frete - Nome coluna igual a de outra tabela
	
	$ssql = "select tblfrete_tipo.fdescricao from tblfrete_tipo where freteid=" . $tipo_frete;
	$result = mysql_query($ssql);
	if ($result){
		while($row=mysql_fetch_assoc($result)){
			
			$tipo_frete = $row["fdescricao"];
		}
		mysql_free_result($result);
	} 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		window.print();
    });		
</script>
</head>

<body>

<div id="global-container">

    
    <div id="content" style="width:800px;">
    
    	<div id="conteudo">
            <div id="conteudo-interno">
            	<table width="98%" border="0" style="margin: 10px; float:left;">
                <tr>
               	  <td height="25">Detalhes do Pedido n&deg;: <?php echo $codigo_pedido; ?></td>
                	<td width="500" align="right">Impresso em <?php echo date("d/m/Y H:i:s");?></td>
               	</tr>				
                	<tr>
                	  <td height="25">Data do Pedido: </span> <span class="txt-detalhe-pedido"><?php echo $data_pedido; ?></td>
                	  <td>&nbsp;</td>
                </table>
            </div>
        </div>
    	
        <div class="box-botoes-home">
        	<table width="98%" border="0" style="margin: 5px 0 5px 10px; text-align:center">
                <tr>
                    <td colspan="2" align="center"><strong>Dados do cliente</strong></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                	<td width="15%" align="left"><strong>Pessoa:</strong></td>
                   	<td width="85%" align="left"><?php echo $tipo_cadastro; ?></td>
              	</tr>
                <tr>
                	<td align="left"><strong>Razão Social:</strong></td>
                    <td align="left"><?php echo $razao_social; ?></td>
              	</tr>
                <tr>
                    <td align="left"><strong>CPF / CNPJ:</strong></td>
                    <td align="left"><?php echo $cnpj_cpf; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>IE / RG:</strong></td>
                    <td align="left"><?php echo $ie_rg; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Data Nascimento:</strong></td>
                    <td align="left"><?php echo $data_nascimento; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Telefone:</strong></td>
                    <td align="left"><?php echo $telefone; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Celular:</strong></td>
                    <td align="left"><?php echo $celular; ?></td>
                </tr>
                <tr>
                  <td align="left"><strong>E-mail:</strong></td>
                  <td align="left"><?php echo $email; ?></td>
                </tr>
                <tr>
                  <td align="left">&nbsp;</td>
                  <td align="left">&nbsp;</td>
                </tr>
                <tr>
                  <td align="left"><strong>Endereço Fatura:</strong></td>
                  <td align="left"><?php echo $endereco_fatura; ?></td>
                </tr>
            </table>
  </div>
        
        <div class="box-botoes-home">
        	<table width="98%" border="0" style="margin: 5px 0 5px 10px; text-align:center">
                <tr>
                    <td colspan="2" align="center"><strong>Dados da entrega</strong></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                	<td width="15%" align="left"><strong>Identificação:</strong></td>
                   	<td width="85%" align="left"><?php echo $identificacao; ?></td>
              	</tr>
                <tr>
                	<td align="left"><strong>Destinatário:</strong></td>
                    <td align="left"><?php echo $nome; ?></td>
              	</tr>
                <tr>
                    <td align="left"><strong>Endereço:</strong></td>
                    <td align="left"><?php echo $endereco; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Cidade:</strong></td>
                    <td align="left"><?php echo $cidade; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Estado:</strong></td>
                    <td align="left"><?php echo $estado; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>CEP:</strong></td>
                    <td align="left"><?php echo $cep; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Ponto de Referência:</strong></td>
                    <td align="left"><?php echo $referencia; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="left"><strong>Status:</strong></td>
                    <td align="left"><?php echo $status_pedido; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="left"><strong>Frete:</strong></td>
                    <td align="left"><?php echo $tipo_frete; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Tipo Frete:</strong></td>
                    <td align="left">Normal</td>
                </tr>
            </table>
      </div>
      <div class="box-botoes-home">
        	<table width="98%" border="0" style="margin: 5px 0 5px 10px; text-align:center">
                <tr><td><strong>Itens do Pedido</strong></td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td><table width="100%" border="0" cellpadding="2" cellspacing="2">
                      <tr bgcolor="#eeeeee"> <strong>
                        <td align="left">Descrição do produto</td>
                        <td align="center">Quantidade</td>
                        <td align="center">Serviços</td>
                        <td align="center">Preço Unitário</td>
                        <td align="center">Valor Total</td>
                      </strong> </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <?php

								
								$ssql = "SELECT tblproduto.produtoid, tblproduto.pcodigo, tblproduto.pproduto, tblproduto.pdescricao, tblproduto.psubtitulo, tblproduto.pdisponivel, 
								tblpedido_item.pquantidade, tblpedido_item.pvalor_unitario,  tblpedido_item.pcodproduto, pai.ppropriedade AS propriedade_pai, tblpedido_item.pcodtamanho, tblproduto_propriedade.ppropriedade AS ptamanho, 
								tblpedido_item.pcodpropriedade, proper.ppropriedade AS ppropriedade
								FROM tblproduto
								
								INNER JOIN tblpedido_item ON tblproduto.produtoid = tblpedido_item.pcodproduto
								LEFT JOIN tblproduto_propriedade ON tblpedido_item.pcodtamanho = tblproduto_propriedade.propriedadeid
								LEFT JOIN tblproduto_propriedade AS proper ON tblpedido_item.pcodpropriedade = proper.propriedadeid
								LEFT JOIN tblproduto_propriedade AS pai ON tblproduto_propriedade.pcodpropriedade = pai.propriedadeid
								where tblpedido_item.pcodpedido=$id and tblpedido_item.pquantidade>0
								";
								
								//echo $ssql;
				
								$result = mysql_query($ssql);
								if($result){
									$contador = 1;
									while($row=mysql_fetch_assoc($result)){
										$produto			= $row["pcodigo"] . " - " . $row["pproduto"];
										
										if( get_configuracao("admin_pedido_item_subtitulo") == -1 ){
											$produto .= "&nbsp;" . $subtitulo;
										}
										
										$produto			.= "&nbsp;&nbsp;" . $row["ppropriedade_pai"] . "&nbsp;&nbsp;" . $row["ptamanho"];
										$produto_qtde 	 	= $row["pquantidade"];
										$valor_unitario 	= $row["pvalor_unitario"];
										$produto_desconto 	= formata_valor_tela($row["pdesconto"]);
										$valor_final 		= $produto_qtde * $valor_unitario;
										$valor_final 		= formata_valor_tela($valor_final);
										
										if ((round($contador/2)) <> ($contador/2)){
											echo '<tr bgcolor="#eeeeee">';
										}else{
											echo '<tr bgcolor="#ffffff">';
										}
										
										echo '
													<td align="left">'. $produto .'</td>
													<td align="center">'. $produto_qtde .'</td>
													<td align="right">0,00</td>
													<td align="right">'. formata_valor_tela($valor_unitario) .'</td>
													<td align="right">'. $valor_final .'</td>
												</tr>';
												$contador ++;
									}
								}mysql_free_result($result);
							?>
                      <tr>
                        <td align="left">&nbsp;</td>
                        <td align="center">&nbsp;</td>
                        <td align="right">&nbsp;</td>
                        <td align="right">&nbsp;</td>
                        <td align="right">&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="right" colspan="4"><strong>Subtotal:</strong></td>
                        <td align="right">R$ <?php echo $subtotal; ?></td>
                      </tr>
                      <tr>
                        <td align="right" colspan="4"><strong>Desconto:</strong></td>
                        <td align="right">R$ <?php echo $valor_desconto; ?></td>
                      </tr>
                      <tr>
                        <td colspan="4" align="right"><strong>Cupom de Desconto: <?php if($cupom_desconto!=""){echo "".$cupom_desconto."";}?></strong></td>
                        <td align="right">R$ <?php echo $valor_cupom_desconto; ?></td>
                      </tr>                        
                      <tr>
                        <td align="right" colspan="4"><strong>Frete:</strong></td>
                        <td align="right">R$ <?php echo $valor_frete; ?></td>
                      </tr>
                      <tr>
                        <td align="right" colspan="4"><strong>Total:</strong></td>
                        <td align="right">R$ <?php echo $valor_total; ?></td>
                      </tr>
                    </table></td>
                </tr>
            </table>
      </div>
      
      <div class="box-botoes-home">
        	<table width="98%" border="0" style="margin: 5px 0 5px 10px; text-align:center">
                <tr><td><strong>Dados do Pagamento</strong></td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td><table width="100%" border="0" cellpadding="3" cellspacing="0">
                      <tr>
                        <td width="120" align="left"><strong>Pagamento:</strong></td>
                        <td align="left"><strong><?php echo $forma_pagamento; ?></strong></td>
                      </tr>
                      <tr>
                        <td align="left"><strong>Valor:</strong></td>
                        <td align="left">R$ <?php echo $valor_total; ?></td>
                      </tr>
                      <?php 
								$ssql = "select * from tblcondicao_pagamento where condicaoid=" . $cod_condicao_pgto;
								
									$result = mysql_query($ssql);
									if($result){
									while($row=mysql_fetch_assoc($result)){
										$qtde_parcelas = $row["cnumero_parcelas"];
									}
								}mysql_free_result($result);
							?>
                      <tr>
                        <td align="left"><strong>Parcelas:</strong></td>
                        <td align="left"><?php echo $qtde_parcelas; ?></td>
                      </tr>
                      <tr>
                        <td align="left"><strong>Status:</strong></td>
                        <td align="left"><?php echo $status_pedido; ?></td>
                      </tr>
                    </table></td>
                </tr>
            </table>
      </div>
      <div class="box-botoes-home">
        	<table width="98%" border="0" style="margin: 5px 0 5px 10px; text-align:center">
                <tr><td><strong>Histórico do Pedido</strong></td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                    	<table width="100%" border="0" cellpadding="0" cellspacing="2">
                        	<tr bgcolor="#eeeeee">
                            	<strong>
                                <td width="150" align="center">Data</td>
                                <td width="250" align="center">Status</td>
                                <td align="center"><strong>Texto</strong></td>
                                <td width="150" align="center"><strong>Usuário</strong></td>
                                </strong>
                            </tr>
                            
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            
                            <?php
								$ssql = " select tblpedido_historico.historicoid, tblpedido_historico.hcodpedido, tblpedido_historico.hcodstatus, tblpedido_historico.htexto, ";
								$ssql .= " tblpedido_historico.hcodusuario,";
								$ssql .= " tblpedido_historico.hdata_alteracao, tblusuario.usuarioid, tblusuario.unome, tblpedido_status.sstatus ";
								$ssql .= " FROM tblpedido_historico, tblusuario, tblpedido_status";
								$ssql .= " where tblpedido_historico.hcodpedido=" . $id;
								$ssql .= " and tblpedido_historico.hcodstatus=tblpedido_status.statusid";								
								$ssql .= " and tblusuario.usuarioid=tblpedido_historico.hcodusuario";
								$ssql .= " order by hdata_alteracao";
																
								//echo $ssql;
				
								$result = mysql_query($ssql);
								if($result){
									$contador = 1;
									while($row=mysql_fetch_assoc($result)){
										$data_status 	= $row["hdata_alteracao"];
										$status_pedido 	= $row["sstatus"];
										$usuario		= $row["unome"];
										$texto			= $row["htexto"];
										
										if ((round($contador/2)) <> ($contador/2)){
											echo '<tr bgcolor="#eeeeee">';
										}else{
											echo '<tr bgcolor="#ffffff">';
										}
										
										echo '
													<td align="left">'. $data_status .'</td>
													<td align="center">'. $status_pedido .'</td>
													<td>'. $texto .'</td>
													<td>'. $usuario .'</td>
												</tr>';
												$contador ++;
									}
								}mysql_free_result($result);
							?>
                            <tr>
                                <td align="left">&nbsp;</td>
                                <td align="center">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                	<td>&nbsp;</td>
                </tr>
                <tr>
                	<td>&nbsp;</td>
                </tr>
            </table>
      </div>
      
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>