<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");
	
	
	/*-------------------------------------------------------------
	pega número do pedido para detalhar
	--------------------------------------------------------------*/
	$id = intval($_REQUEST["id"]);
	
	$codigo_integracao 	= get_configuracao("clear_sale_codigo_integracao");
	$ip					= $_SERVER['REMOTE_ADDR'];
	
	if($codigo_integracao==""){
		die("Erro de integração, falta codigo de integração");	
	} 
	
	$ssql = "select tblpedido.pedidoid, tblpedido.pcodorcamento, tblpedido.pcodigo, tblpedido.pcodcadastro, tblpedido.ptitulo, tblpedido.pnome, 
			tblpedido.pendereco, tblpedido.pnumero, tblpedido.pcomplemento, tblpedido.pbairro, tblpedido.pcidade, tblpedido.pestado, tblpedido.pcep, 
			tblpedido.psubtotal, tblpedido.pvalor_desconto, tblpedido.pvalor_desconto_cupom, tblpedido.pvalor_frete, tblpedido.pvalor_presente, tblpedido.pvalor_total, 
			tblpedido.pcodforma_pagamento, tblpedido.pcodcondicao_pagamento, tblpedido.pcodfrete, tblpedido.pcodcupom_desconto, 
			tblpedido.pcodigo_rastreamento, tblpedido.pcodstatus, tblpedido.pdata_cadastro, 
			tblpedido.pcartao_numero, tblpedido.pcartao_retorno, tblpedido.ptexto_presente, tblpedido.preferencia,
	
			tblcadastro_endereco.etitulo, tblcadastro_endereco.enome, tblcadastro_endereco.eendereco, tblcadastro_endereco.enumero, tblcadastro_endereco.ecomplemento,
			tblcadastro_endereco.ebairro, tblcadastro_endereco.ecidade, tblcadastro_endereco.eestado, tblcadastro_endereco.ecep, tblcadastro_endereco.ereferencia,
	
			tblforma_pagamento.fdescricao, tblcondicao_pagamento.ccondicao, tblcondicao_pagamento.cnumero_parcelas,
			tblcadastro.cadastroid, tblcadastro.crazao_social, tblcadastro.cnome, tblcadastro.ccodtipo, tblcadastro.ccpf_cnpj, tblcadastro.crg_ie, tblcadastro.cdata_nascimento, tblcadastro.cemail,
			tblcadastro.ctelefone, tblcadastro.ccelular, tblcadastro_tipo.ttipo, 
			tblpedido_status.statusid, tblpedido_status.sdescricao, tb_cadastro_entrega.ereferencia as referencia_entrega,
			tblcupom_desconto.ccupom,
			tbllista_presente.ltitulo, tbllista_presente.lcredito 
	
			from tblpedido 
	
			left join tblcadastro_endereco on tblpedido.pcodendereco_fatura=tblcadastro_endereco.enderecoid 
			left join tblcadastro_endereco as tb_cadastro_entrega on tblpedido.pcep=tb_cadastro_entrega.ecep 

			left join tblcadastro on tblpedido.pcodcadastro=tblcadastro.cadastroid 
			left join tblcadastro_tipo on tblcadastro.ccodtipo=tblcadastro_tipo.tipoid
			left  join tblforma_pagamento on tblpedido.pcodforma_pagamento=tblforma_pagamento.formapagamentoid 
			left join tblcondicao_pagamento on tblpedido.pcodcondicao_pagamento=tblcondicao_pagamento.condicaoid 
			left join tblpedido_status on tblpedido.pcodstatus=tblpedido_status.statusid 
			left join tblcupom_desconto on tblpedido.pcodcupom_desconto=tblcupom_desconto.cupomid 
			left join tbllista_presente on tblpedido.pcodlista_presente=tbllista_presente.listaid 
			
			where tblpedido.pedidoid='{$id}' 
			limit 0,1";
	
	$result = mysql_query($ssql);
	if($result){
		
		while($row=mysql_fetch_assoc($result)){
			
			$codigo_pedido	 	= "000000".$row["pcodigo"];
			$codigo_pedido	 	= substr($codigo_pedido, strlen($codigo_pedido)-6,6);
			
			
			$razao_social		= $row["crazao_social"] . "&nbsp;" . $row["cnome"];
			$tipo_cadastro		= $row["ttipo"];
			$cnpj_cpf			= $row["ccpf_cnpj"];
			$ie_rg				= $row["crg_ie"];
			$data_nascimento 	= formata_data_tela($row["cdata_nascimento"]);
			
			if(strlen($data_nascimento)<10){
				$data_nascimento = "";	
			}
			
			$telefone			= $row["ctelefone"];
			$telefone_ddd		= substr($telefone,0,2);
			$telefone			= substr($telefone,2,11);
			
			$celular			= $row["ccelular"];
			$celular_ddd		= substr($celular,0,2);
			$celular			= substr($celular,2,11);			
			
			
			$data_pedido     	= formata_data_tela($row["pdata_cadastro"]);
			
			$identificacao	 	= $row["ptitulo"];
			$nome 			 	= $row["pnome"];
			$email 			 	= $row["cemail"];
			
			$endereco		 	= $row["pendereco"];
			$numero				= $row["pnumero"];
			$complemento		= $row["pcomplemento"];
			$bairro				= $row["pbairro"];
			$cidade 		 	= $row["pcidade"];
			$estado			 	= $row["pestado"];
			$cep 			 	= $row["pcep"];
			$cep			 	= substr($cep, 0,5). "-" . substr($cep, 5,3);
			
			
			$valor_total	 	= number_format($row["pvalor_total"],2,".","");
			
			$cartao_numero		= right("".$row["pcartao_numero"],4);
			
			$numero_parcelas	= $row["cnumero_parcelas"];
			
		}
		mysql_free_result($result);
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript" src="js/jquery.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		$("#frm_clearsale").submit();
    });	
</script>

</head>
    <form method="post" id="frm_clearsale" target="iFrameStart" action="http://www.clearsale.com.br/start/Entrada/EnviarPedido.aspx">
    <input type=hidden name=CodigoIntegracao value="<?php echo $codigo_integracao;?>">
    <input type=hidden name=PedidoID value="<?php echo $codigo_pedido;?>">
    <input type=hidden name=Data value="<?php echo $data_pedido;?>">
    <input type=hidden name=IP value="<?php echo $ip;?>">
    <input type=hidden name=Total value="<?php echo $valor_total;?>">
    <input type=hidden name=TipoPagamento value="1">
    <input type=hidden name=Parcelas value="<?php echo $numero_parcelas;?>">
    <!--DADOS DA COMPRA -->
    <input type=hidden name=Cobranca_Nome value="<?php echo $nome; ?>">
    <input type=hidden name=Cobranca_Nascimento value="<?php echo $data_nascimento;?>">
    <input type=hidden name=Cobranca_Email value="<?php echo $email;?>">
    <input type=hidden name=Cobranca_Documento value="<?php echo $cnpj_cpf;?>">
    <input type=hidden name=Cobranca_Logradouro value="<?php echo $endereco;?>">
    <input type=hidden name=Cobranca_Logradouro_Numero value="<?php echo $numero;?>">
    <input type=hidden name=Cobranca_Logradouro_Complemento value="<?php echo $complemento;?>">
    <input type=hidden name=Cobranca_Bairro value="<?php echo $bairro;?>">
    <input type=hidden name=Cobranca_Cidade value="<?php echo $cidade;?>">
    <input type=hidden name=Cobranca_Estado value="<?php echo $estado;?>">
    <input type=hidden name=Cobranca_CEP value="<?php echo $cep;?>">
    <input type=hidden name=Cobranca_Pais value="BRA">
    <input type=hidden name=Cobranca_DDD_Telefone_1 value="<?php echo $telefone_ddd;?>">
    <input type=hidden name=Cobranca_Telefone_1 value="<?php echo $telefone;?>">
    <input type=hidden name=Cobranca_DDD_Celular value="<?php echo $celular_ddd;?>">
    <input type=hidden name=Cobranca_Celular value="<?php echo $celular;?>">
    <!--PRODUTOS -->
    <?php
								
		$i = 0;						
		$ssql = "SELECT tblproduto.produtoid, tblproduto.pcodigo, tblproduto.pproduto, tblproduto.psubtitulo, tblproduto.pvalor_unitario, tblproduto.pdisponivel, tblproduto.ppeso, 
		tblpedido_item.pquantidade, tblpedido_item.pcodproduto, tblpedido_item.ppresente, pai.ppropriedade AS propriedade_pai, tblpedido_item.pcodtamanho, tblproduto_propriedade.ppropriedade AS ptamanho, 
		tblpedido_item.pcodpropriedade, proper.ppropriedade AS ppropriedade
		FROM tblproduto
		INNER JOIN tblpedido_item ON tblproduto.produtoid = tblpedido_item.pcodproduto
		LEFT JOIN tblproduto_propriedade ON tblpedido_item.pcodtamanho = tblproduto_propriedade.propriedadeid
		LEFT JOIN tblproduto_propriedade AS proper ON tblpedido_item.pcodpropriedade = proper.propriedadeid
		LEFT JOIN tblproduto_propriedade AS pai ON tblproduto_propriedade.pcodpropriedade = pai.propriedadeid
		where tblpedido_item.pcodpedido='{$id}' and tblpedido_item.pquantidade > 0
		";  
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$i++;
				echo '	<input type="hidden" name="Item_ID_'.$i.'" value="'.$row["produtoid"].'">' . "\n";
				echo '	<input type="hidden" name="Item_Nome_'.$i.'" value="'.$row["pproduto"].'">' . "\n";
    			echo '	<input type="hidden" name="Item_Qtd_'.$i.'" value="'.$row["pquantidade"].'">' . "\n";
			    echo '	<input type="hidden" name="Item_Valor_'.$i.'" value="'.number_format($row["pvalor_unitario"],2,".","").'">' . "\n";				
			}
			mysql_free_result($result);
		}
	?>

    </form>
<iframe id="iFrameStart" src="" frameborder="0" width="300" height="100">
<P>Seu Browser não suporta iframes</P>
</iframe>   
</body>
</html>    