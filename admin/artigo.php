<?php
	include("../include/inc_conexao.php");
	include("../include/inc_image_resize.php");
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;
	}

	$id = $_REQUEST["id"];	
	if(!is_numeric($id)){
		$id = 0;
	}

	if($_POST && $_REQUEST["action"]=="gravar"){
		$id					= 	addslashes($_REQUEST["id"]);
		$titulo				=	addslashes($_REQUEST["titulo"]);
		$categoria			=	(int) $_REQUEST["codcategoria"];
		$link_seo			=	addslashes($_REQUEST["link"]);
		$palavra_chave		=	addslashes($_REQUEST["palavra_chave"]);
		$descricao			=	addslashes($_REQUEST["editor1"]);
		$ensaio				=	(int)$_REQUEST["ensaio"];
		$ensaio_pasta		=	addslashes($_REQUEST["ensaio_pasta"]);
		$data_inicio        =   $_REQUEST["data_inicio"];
		$aux = explode("/", $_REQUEST["data_inicio"]);		
		$data_inicio = $aux[2]."-".$aux[1]."-".$aux[0];
		

		if(isset($_FILES['arquivo'])){
			$file_name = $_FILES['arquivo']['name'];
			$file_tmp_name = $_FILES['arquivo']['tmp_name'];
			$extensao = right($file_name,4);
			$imagem = gera_url_seo($titulo.date("Ymdhis"))."-big".$extensao;
			$imagem = strtolower("imagem/artigo/".$imagem);
			$move = move_uploaded_file($file_tmp_name,"../imagem/tmp/".$file_name);
			$image = new SimpleImage();
			$image->load("../imagem/tmp/".$file_name);
			$image->resize(659,331);
			$image->save("../".$imagem);
			$imagem = str_replace("big","tumb",$imagem);
			$image->resize(200,115);
			$image->save("../".$imagem);
			$upload = -1;
		}
		if($id>0){
			if($upload){
				$ssql = "update tblartigos set atitulo='{$titulo}', adescricao='{$descricao}', apalavra_chave='{$palavra_chave}', alink_seo='{$link_seo}', aimagem='{$imagem}', acategoria='{$categoria}',	acodusuario='{$usuario}', adata_alteracao='{$data_hoje}', aensaio='{$ensaio}', aensaio_pasta='{$ensaio_pasta}', data_inicio='{$data_inicio}' where artigoid=$id";
			}else{
				$ssql = "update tblartigos set atitulo='{$titulo}', adescricao='{$descricao}', apalavra_chave='{$palavra_chave}', alink_seo='{$link_seo}', acategoria='{$categoria}', acodusuario='{$usuario}', adata_alteracao='{$data_hoje}', aensaio='{$ensaio}', aensaio_pasta='{$ensaio_pasta}', data_inicio='{$data_inicio}' where artigoid=$id";
			}
		}else{
			if($upload){
				$ssql = "insert into tblartigos (atitulo,acategoria,adescricao,apalavra_chave,aimagem,acodusuario,adata_alteracao,adata_cadastro,alink_seo,aensaio,aensaio_pasta,data_inicio)
				values ('{$titulo}', '{$categoria}', '{$descricao}', '{$palavra_chave}', '{$imagem}', '{$usuario}', '{$data_hoje}', '{$data_hoje}','{$link_seo}','{$ensaio}','{$ensaio_pasta}','{$data_inicio}')";
			}else{
				$ssql = "insert into tblartigos (atitulo,acategoria,adescricao,apalavra_chave,acodusuario,adata_alteracao,adata_cadastro,alink_seo,aensaio,aensaio_pasta,data_inicio)
				values ('{$titulo}', '{$categoria}', '{$descricao}', '{$palavra_chave}', '{$usuario}', '{$data_hoje}', '{$data_hoje}','{$link_seo}','{$ensaio}','{$ensaio_pasta}','{$data_inicio}')";
			}
			
		}
		$result = mysql_query($ssql);
		$id_insert = mysql_insert_id();

		if(!$result){
			$msg = "Erro ao gravar registro: ".mysql_error();
			$erro = 1;
		}else{
			$msg = "Registro atualizado com sucesso";
		}
	}

	if($_POST && $_REQUEST["action"]=="excluir"){
		mysql_unbuffered_query("delete from tblartigos where artigoid = '".$_POST["id"]."' limit 1;");
		die("ok");
	}

	if($id>0){
		$ssql = "select atitulo,adata_cadastro,adata_alteracao,acodusuario,acategoria,apalavra_chave,aimagem,adescricao,categoriaid,alink_seo,aensaio,aensaio_pasta,data_inicio
		from tblartigos
		left join tblcategoria_artigo on acategoria = categoriaid
		where artigoid = '$id';";
		
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$titulo				=	$row["atitulo"];
				$palavra_chave		=	$row["apalavra_chave"];
				$imagem				=	$row["aimagem"];
				$descricao			=	$row["adescricao"];
				$categoria			=	$row["categoriaid"];
				$link_seo			=	$row["alink_seo"];
				$usuario			=	get_usuario($row["acodusuario"],"unome");
				$data_cadastro		=	formata_data_tela($row["adata_cadastro"]);
				$data_alteracao		=	formata_data_tela($row["adata_alteracao"]);
				$ensaio				=	$row["aensaio"];
				$ensaio_pasta		=	$row["aensaio_pasta"];
				$aux 		        =   explode("-",$row["data_inicio"]);
				$data_inicio = $aux[2]."/".$aux[1]."/".$aux[0];
			}
			mysql_free_result($result);
		}	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$("#data_inicio").mask("99/99/9999");					   	
		$("#data_termino").mask("99/99/9999 99:99:99");	   	
	});
</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="geral">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form method="post" name="frm_conteudo" id="frm_conteudo" enctype="multipart/form-data">
    <input name="action" id="action" type="hidden" value="gravar" />  
    <input name="id" id="id" type="hidden" value="<?php echo $id?>" />
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Artigo &raquo;  <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='conteudo.php';"><?php echo ($id==0) ? "Novo Registro" : $pagina;  ?></span> </span> <a href="artigo_consulta.php">Consulta</a>
       	  </div>
            
            <div id="conteudo-interno">
				<table width="98%" border="0" cellspacing="0" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="130">ID</td>
                <td>#<?php echo $id;?></td>
                <td width="200" rowspan="19" align="center" valign="top"><?php echo $msg;?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Título</td>
                <td><input name="titulo" type="text" class="formulario" id="titulo" value="<?php echo $titulo;?>" size="75" maxlength="100" onkeyup="javascript: gera_link_seo(this.value,'link_seo');" onblur="javascript: gera_link_seo(this.value,'link_seo');"></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Link SEO</td>
                <td><input name="link" class="formulario" id="link_seo" type="text" size="75" maxlength="250" value="<?php echo $link_seo;?>" readonly /></td>
              </tr>
			  <?php
				if(strlen($link_seo) > 0){
					$id_esc = ($id_insert > 0) ? $id_insert : $id;
			  ?>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><?php echo "http://nutracorpore.com.br/artigo/".$link_seo."---".$id_esc; ?></td>
					</tr>
			  <?php
				}
			  ?>
			  <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Palavra Chave</td>
                <td><input name="palavra_chave" class="formulario" id="palavra_chave" type="text" size="75" maxlength="250" value="<?php echo $palavra_chave;?>" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
			  <tr>
                <td>Categorias</td>
                <td>
					<select name="codcategoria" class="formulario" id="codcategoria">
						<?php
							$ssql = "select categoriaid, ccategoria from tblcategoria_artigo where ccodcategoria = 0 order by ccategoria";
							$result = mysql_query($ssql);
							if($result){
								while($row=mysql_fetch_assoc($result)){
										$ssql1 = "select categoriaid, ccategoria from tblcategoria_artigo where ccodcategoria = ".$row["categoriaid"]." order by ccategoria";
										$result1 = mysql_query($ssql1);
										if($result1){
											while($row1=mysql_fetch_assoc($result1)){
												echo '<option value="'.$row1["categoriaid"].'"';
												if($categoria==$row1["categoriaid"]){
													echo " selected";	
												}
												echo '>';
												echo $row["ccategoria"].'&nbsp;-->&nbsp;'.$row1["ccategoria"];
												echo '</option>';										
											}
											mysql_fetch_assoc($result1);
										}
									
										
									
									
								}
								mysql_free_result($result);
							}
						?>
					</select>
				</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Texto</td>
                <td><textarea name="editor1" class="ckeditor" id="editor1"><?php echo $descricao;?></textarea></td>
                </tr>
			  <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
			  </tr>
			  
              <tr>
                <td>Banner</td>
                <td><input name="arquivo" type="file" class="formulario" id="arquivo" size="75" maxlength="250" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td colspan="2"><?php if(file_exists("../".$imagem) && $id > 0){ echo "<img src='../".str_replace("tumb","big",$imagem)."' >"; } ?>&nbsp;</td>
                </tr>
			  <tr>
                <td>Data de Início:</td>
                <td><input name="data_inicio" type="text" class="formulario" id="data_inicio" size="20" maxlength="20" value="<?php echo (strlen($data_inicio) > 0) ? $data_inicio : date("d/m/Y");?>" /></td>
                </tr>
				<tr>
				<tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
				<td>Habilitar SLIDESHOW</td>
				<td>
					<label>
						<input type="radio" name="ensaio" id="ensaio" value="-1" <?php if($ensaio == -1){ echo "checked"; } ?>>Sim
					</label>
					<label>
						<input type="radio" name="ensaio" id="ensaio" value="0"  <?php if($ensaio ==  0){ echo "checked"; } ?>>Não
					</label>
				</td>
			  </tr>
			  <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
			  </tr>
			  <tr>
                <td>Pasta de imagens</td>
                <td><input name="ensaio_pasta" class="formulario" id="ensaio_pasta" type="text" size="75" maxlength="250" value="<?php echo $ensaio_pasta;?>" /></td>
			  </tr>
			  <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
			  </tr>
              <tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Usuário</td>
                <td><?php echo $usuario;?></td>
                </tr>
              <tr>
                <td>Data Alteração</td>
                <td><?php echo $data_alteracao;?></td>
                </tr>
              <tr>
                <td>Data Cadastro</td>
                <td><?php echo $data_cadastro;?></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
				
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" id="btn-cmd-gravar" name="btn-cmd-gravar" value="Gravar" class="btn-gravar" /></td>
                </tr>
              </table>
          </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>