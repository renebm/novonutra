<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}

	$id = $_REQUEST["id"];	
	if(!is_numeric($id)){
		$id = 1;	
	}
	
	/*--------------------------------------------------------
	//verifica se existe algum dado na tblloja
	--------------------------------------------------------*/
	$ssql = "select lojaid from tblloja where lojaid=1";
	$result = mysql_query($ssql);
	if($result){
		$num_rows = mysql_num_rows($result);
	}



	if($_POST && $_REQUEST["action"]=="gravar"){
		$id					= 	addslashes($_REQUEST["id"]);
		
		$razaosocial			=	addslashes($_REQUEST["razaosocial"]);
		$nome					=	addslashes($_REQUEST["nome"]);
		$email					=	addslashes($_REQUEST["email"]);
		$site					=	addslashes($_REQUEST["site"]);
		$endereco				=	addslashes($_REQUEST["endereco"]);
		$numero					=	addslashes($_REQUEST["numero"]);
		$complemento			=	addslashes($_REQUEST["complemento"]);
		$bairro					=	addslashes($_REQUEST["bairro"]);
		$cidade					=	addslashes($_REQUEST["cidade"]);
		$estado					=	addslashes($_REQUEST["estado"]);
		$cep					=	addslashes($_REQUEST["cep"]);
		$telefone				=	addslashes($_REQUEST["telefone"]);
		$atendimento			=	addslashes($_REQUEST["atendimento"]);
		$descricao				=	addslashes($_REQUEST["descricao"]);
		$palavra_chave			=	addslashes($_REQUEST["palavra_chave"]);
		$smtp					=	addslashes($_REQUEST["smtp"]);
		$usuario_smtp			=	addslashes($_REQUEST["usuario_smtp"]);
		$senha_smtp				=	addslashes($_REQUEST["senha_smtp"]);
		
		$ssql = "update tblloja set lrazaosocial='{$razaosocial}', lnome='{$nome}', lemail='{$email}', lsite='{$site}', lendereco='{$endereco}', lnumero='{$numero}', 
				lcomplemento='{$complemento}', lbairro='{$bairro}', lcidade='{$cidade}', lestado='{$estado}', lcep='{$cep}', ltelefone='{$telefone}', 
				latendimento='{$atendimento}', ldescricao='{$descricao}', lpalavra_chave='{$palavra_chave}', lsmtp='{$smtp}', lusuario_smtp='{$usuario_smtp}', 
				lsenha_smtp='{$senha_smtp}', lcodusuario='{$usuario}', ldata_alteracao='{$data_hoje}'
				where lojaid=1";
		$result = mysql_query($ssql);
		
		if($result){
			$msg = "Registro atualizado com sucesso.";	
		}else{
			$msg = "Erro ao atualizar registro.";	
		}
		
		
	
	}




	if($id>0){
	
		$ssql = "select lojaid, lrazaosocial, lnome, lemail, lsite, lendereco, lnumero, lcomplemento, lbairro, lcidade, lestado, lcep, ltelefone, 
				latendimento, ldescricao, lpalavra_chave, lsmtp, lusuario_smtp, lsenha_smtp, 
				lcodusuario, ldata_alteracao, ldata_cadastro
				from tblloja where lojaid=1
				";
		$result = mysql_query($ssql);
		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				
				$id						=	$row["lojaid"];
				$razaosocial			=	$row["lrazaosocial"];
				$nome					=	$row["lnome"];
				$email					=	$row["lemail"];
				$site					=	$row["lsite"];
				$endereco				=	$row["lendereco"];
				$numero					=	$row["lnumero"];
				$complemento			=	$row["lcomplemento"];
				$bairro					=	$row["lbairro"];
				$cidade					=	$row["lcidade"];
				$estado					=	$row["lestado"];
				$cep					=	$row["lcep"];
				$telefone				=	$row["ltelefone"];
				$atendimento			=	$row["latendimento"];
				$descricao				=	$row["ldescricao"];
				$palavra_chave			=	$row["lpalavra_chave"];
				$smtp					=	$row["lsmtp"];
				$usuario_smtp			=	$row["lusuario_smtp"];
				$senha_smtp				=	$row["lsenha_smtp"];
								
				$usuario			=	get_usuario($row["lcodusuario"],"unome");		
				$data_alteracao		=	formata_data_tela($row["ldata_alteracao"]);
				$data_cadastro		=	formata_data_tela($row["ldata_cadastro"]);
				
			}
			mysql_free_result($result);
		}	
		
	}
	
	
	
	if($id==0){
		$ativa = -1;
		$exibe_imagem = -1;
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {							   		
		$("#razaosocial").focus();	
		$("#cep").mask("99999-999");
		$("#telefone").mask("99 9999-9999");
		
	});

</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form method="post" name="frm_loja" id="frm_loja" action="loja.php" onsubmit="return valida_loja();">
    <input name="action" id="action" type="hidden" value="gravar" />  
    <input name="id" id="id" type="hidden" value="<?php echo $id?>" />
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Loja &raquo;  <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='loja.php';"><?php echo ($id==0) ? "Novo Registro" : $nome;  ?></span> </span>
       	  </div>
            
            <div id="conteudo-interno">
				<table width="98%" border="0" cellspacing="0" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="130">ID</td>
                <td>#<?php echo $id;?></td>
                <td width="200" rowspan="49" align="center" valign="top">
				<?php echo $msg;?>
                    <div id="dica" style="margin:10px;">
                    &nbsp;
                    </div>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Razão Social</td>
                <td><input name="razaosocial" type="text" class="formulario" id="razaosocial" value="<?php echo $razaosocial;?>" size="75" maxlength="100" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr> 
              <tr>
                <td>Nome Fantasia</td>
                <td><input name="nome" type="text" class="formulario" id="nome" value="<?php echo $nome;?>" size="75" maxlength="100" /></td>
              </tr>  
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>                                                        
              <tr>
                <td>E-mail</td>
                <td><input name="email" type="text" class="formulario" id="email" value="<?php echo $email;?>" size="75" maxlength="100" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Site</td>
                <td><input name="site" type="text" class="formulario" id="site" value="<?php echo $site;?>" size="75" maxlength="100" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>                                          
              <tr>
                <td>Endereço</td>
                <td><input name="endereco" type="text" class="formulario" id="endereco" value="<?php echo $endereco;?>" size="75" maxlength="100" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Número</td>
                <td><input name="numero" type="text" class="formulario" id="numero" value="<?php echo $numero;?>" size="75" maxlength="100" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Complemento</td>
                <td><input name="complemento" type="text" class="formulario" id="complemento" value="<?php echo $complemento;?>" size="75" maxlength="100" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Bairro</td>
                <td><input name="bairro" type="text" class="formulario" id="bairro" value="<?php echo $bairro;?>" size="75" maxlength="100" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Cidade</td>
                <td><input name="cidade" type="text" class="formulario" id="cidade" value="<?php echo $cidade;?>" size="75" maxlength="100" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Estado</td>
                <td><input name="estado" type="text" class="formulario" id="estado" value="<?php echo $estado;?>" size="5" maxlength="2" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Cep</td>
                <td><input name="cep" type="text" class="formulario" id="cep" value="<?php echo $cep;?>" size="15" maxlength="9" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Telefone</td>
                <td><input name="telefone" type="text" class="formulario" id="telefone" value="<?php echo $telefone;?>" size="75" maxlength="100" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Atendimento</td>
                <td><textarea name="atendimento" cols="75" rows="3" class="formulario" id="atendimento"><?php echo $atendimento;?></textarea></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="2">&nbsp;</td>
                </tr>
              <tr>
                <td>Descrição</td>
                <td><textarea name="descricao" cols="75" rows="3" class="formulario" id="descricao"><?php echo $descricao;?></textarea></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Palavras Chave</td>
                <td><input name="palavra_chave" type="text" class="formulario" id="palavra_chave" value="<?php echo $palavra_chave;?>" size="100" maxlength="100" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>SMTP</td>
                <td><input name="smtp" type="text" class="formulario" id="smtp" value="<?php echo $smtp;?>" size="75" maxlength="100" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Usuário SMTP</td>
                <td><input name="usuario_smtp" type="text" class="formulario" id="usuario_smtp" value="<?php echo $usuario_smtp;?>" size="75" maxlength="100" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Senha Usuário SMTP</td>
                <td><input name="senha_smtp" type="password" class="formulario" id="senha_smtp" value="<?php echo $senha_smtp;?>" size="75" maxlength="100" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              
              <tr>
                <td>Usuário</td>
                <td><?php echo $usuario;?></td>
                </tr>
              <tr>
                <td>Data Alteração</td>
                <td><?php echo $data_alteracao;?></td>
                </tr>
              <tr>
                <td>Data Cadastro</td>
                <td><?php echo $data_cadastro;?></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" id="btn-cmd-gravar" name="btn-cmd-gravar" value="Gravar" class="btn-gravar" /></td>
                </tr>
              </table>
          </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>