<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");
	
	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$uri = str_replace("/videokestore/","",$_SERVER['REQUEST_URI']);	

	$pagina = 1;
	$start = 0;
	$limit = 20;

	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$('#string').focus();
	});
  

</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    

    	<div id="conteudo">
			<div id="titulo-consulta">
            	<span class="label-inicio">Condição de Pagamento &raquo; <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='condicao_pagamento_consulta.php';">Consulta</span></span> 
                <a href="condicao_pagamento.php">Novo Registro</a>
            </div>
            
            <div id="conteudo-interno">
            <?php 
				if (!$_GET){
			?>
             <form name="frm_consulta" id="frm_consulta" method="get" action="?" >
             <table width="99%" border="0" cellspacing="2" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="50%">&nbsp;</td>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>Condição de Pagamento</td>
                <td>Status</td>
               </tr>
              <tr>
                <td><input name="string" type="text" class="formulario" id="string" size="75" maxlength="200" /></td>
                <td><select name="status" size="1" class="formulario" id="status">
                  <option value="2">Selecione</option>
                  <option value="-1">Ativa</option>
                  <option value="0">Inativa</option>
                </select></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Forma de Pagamento</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>
                <select name="forma_pagamento" size="1" class="formulario" id="forma_pagamento">
                  <option value="0">Selecione</option>
                  <?php
                  	$ssql = "select formapagamentoid, fforma_pagamento from tblforma_pagamento order by fforma_pagamento";
					$result = mysql_query($ssql);
					if($result){
						while($row=mysql_fetch_assoc($result)){
							echo "<option value='".$row["formapagamentoid"]."'>".$row["fforma_pagamento"]."</option>";
						}
						mysql_free_result($result);
					}
				  
				  ?>
                </select>
                </td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><input type="submit" id="btn-cmd-busca" name="btn-cmd-busca" value="Consultar" class="btn-gravar" /></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
             </table>
             
             </form>
			<?php
				}else{
			?>	
                
                
                <table width="99%" border="0" cellspacing="0" cellpadding="3" style="margin:10px;">
                  <tr>
                    <td width="50" class="titulo_table">#ID</td>
                    <td class="titulo_table">Forma de Pagamento</td>
                    <td align="left" class="titulo_table">Condição</td>
                    <td width="75" class="titulo_table">No. Parcelas</td>
                    <td align="center" class="titulo_table">Status</td>
                    <td width="50" align="center" class="titulo_table">Editar</td>
                    <td width="50" align="center" class="titulo_table">Excluir</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
                  
                  <?php
				  
				  	$string = addslashes($_REQUEST["string"]);
					$status = $_REQUEST["status"];
					$forma_pagamento = $_REQUEST["forma_pagamento"];
				  
				  	if(!is_numeric($status)){
						$status = 2;	
					}
				  	
					if(!is_numeric($forma_pagamento)){
						$forma_pagamento = 0;	
					}


                  	$ssql = "select tblcondicao_pagamento.condicaoid, tblcondicao_pagamento.ccondicao, tblcondicao_pagamento.cativa, tblcondicao_pagamento.cnumero_parcelas,
							tblforma_pagamento.fforma_pagamento
							from tblcondicao_pagamento
							left join tblforma_pagamento on tblcondicao_pagamento.ccodforma_pagamento=tblforma_pagamento.formapagamentoid
							where tblcondicao_pagamento.ccondicao like '%{$string}%' ";
					if($status!=2){
						$ssql .= " and tblcondicao.cativa='{$status}' ";	
					}
					if($forma_pagamento!=0){
						$ssql .= " and tblcondicao.ccodforma_pagamento='{$forma_pagamento}' ";	
					}
					
					
					$ssql .= " or tblcondicao_pagamento.cdescricao like '%{$string}%' ";
					if($status!=2){
						$ssql .= " and tblcondicao.cativa='{$status}' ";	
					}
					if($forma_pagamento!=0){
						$ssql .= " and tblcondicao.ccodforma_pagamento='{$forma_pagamento}' ";	
					}
					
					
					$ssql .= " order by tblforma_pagamento.fforma_pagamento, tblcondicao_pagamento.cnumero_parcelas, tblcondicao_pagamento.ccondicao ";
		
					$result = mysql_query($ssql);
					if($result){
						$total_registros = mysql_num_rows($result);	
					}					
								
					$ssql .= " limit $start, $limit";
							
							//echo $ssql;
							
				  	$result = mysql_query($ssql);
				  	if($result){
					
						if(mysql_num_rows($result)==0){
							echo '
								  <tr>
									<td colspan="7">Nenhum registro localizado.</td>
								  </tr>';							
						}
					
						while($row=mysql_fetch_assoc($result)){
						
						$status = ($row["cativa"]==-1) ? "Ativa" : "Desativada";
						
							echo '
								  <tr class="tr_lista">
									<td class="linha_table">'.$row["condicaoid"].'</td>
									<td class="linha_table">'.$row["fforma_pagamento"].'</td>
									<td class="linha_table">'.$row["ccondicao"].'</td>
									<td align="center" class="linha_table">'.$row["cnumero_parcelas"].'</td>
									<td align="center" class="linha_table">'.$status.'</td>
									<td align="center" class="linha_table"><a href="condicao_pagamento.php?id='.$row["condicaoid"].'"><img src="images/ico_editar.gif" border="0" /></a></td>
									<td align="center" class="linha_table"><a href="javascript:void(0)" onclick="javascript:condicao_pagamento_excluir('.$row["condicaoid"].');"><img src="images/ico_excluir.gif" border="0" /></a></td>
								  </tr>							
							';
						}
						mysql_free_result($result);
					
					}
				  ?>
                  

                  
				  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
				  <tr>
				    <td colspan="7">
				      <div class="paginacao"><span class="paginacao-text">Página:</span> 
				        <?php
                        echo paginacao($pagina, $limit, $total_registros);
                        ?>                            
				        </div>			        </td>
			      </tr>

                </table>
                
                
                <?php
                
				}
				?>
                
          </div>
            
       </div>

 
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>