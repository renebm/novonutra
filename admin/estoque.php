<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");
	
	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}

	$id = $_REQUEST["id"];	
	if(!is_numeric($id)){
		$id = 0;	
	}


	if($_POST && $_REQUEST["action"]=="gravar"){
		$tipo			= addslashes($_REQUEST["tipo"]);
		$quantidade 	= addslashes($_REQUEST["quantidade"]);
		$tamanhos	 	= $_REQUEST["tamanho"];
		$propriedades 	= $_REQUEST["propriedade"];
		$note			= $_REQUEST["note"];
		
		$quantidade		= formata_valor_db($quantidade);
		
		$count_p = (count($propriedades)==0) ? 1 : count($propriedades);
		$count_t = (count($tamanhos)==0) ? 1 : count($tamanhos);
				
		for($i=1;$i<=$count_t;$i++){
			$tamanho = ($tamanhos[$i-1]=="") ? 0 : $tamanhos[$i-1];
			
			
			for($n=1;$n<=$count_p;$n++){
				$propriedade = ($propriedades[$n-1]=="") ? 0 : $propriedades[$n-1];
			
				$ssql = "insert into tblestoque_ajuste (acodproduto, acodpropriedade, acodtamanho, atipo, aquantidade, anote, acodusuario, adata_cadastro) 
						values('{$id}','{$propriedade}','{$tamanho}', '{$tipo}', '{$quantidade}','{$note}','{$usuario}','{$data_hoje}') ";	
				mysql_query($ssql);
				//echo $ssql;
				//echo "<br />";
				//echo "<br />";				
			}
			
			//exit();
			$estoque = 0;
			
			//pega o estoque atual
			$ssql = "select estoqueid, eestoque from tblestoque where ecodproduto='{$id}' and ecodpropriedade='{$propriedade}' and ecodtamanho='{$tamanho}'";
			$result = mysql_query($ssql);
			if($result){
				$num_rows = mysql_num_rows($result);
				while($row=mysql_fetch_assoc($result)){
					$estoqueid	= $row["estoqueid"];
					$estoque 	= $row["eestoque"];	
				}	
				mysql_free_result($result);
			}
			
			//echo $ssql;
			//echo "<br />";			
			
			if($tipo==1){
				$estoque = $estoque + $quantidade;	
			}

			if($tipo==2){
				$estoque = $estoque - $quantidade;	
			}
			
			if($estoqueid==0){
				$ssql = "insert into tblestoque (ecodproduto, ecodtamanho, ecodpropriedade, eestoque, ecodusuario, edata_alteracao, edata_cadastro)
						values('{$id}','{$tamanho}','{$propriedade}','{$estoque}','{$usuario}','{$data_hoje}','{$data_hoje}')";
				mysql_query($ssql);
			}else{
				$ssql = "update tblestoque set eestoque='{$estoque}', ecodusuario='{$usuario}', edata_alteracao='{$data_hoje}' 
						where estoqueid='{$estoqueid}' and ecodtamanho='{$tamanho}' and ecodpropriedade='{$propriedade}' ";
				mysql_query($ssql);				
			}
			
			//echo $ssql;
			//echo "<br />";	

		}

		
		header("location: estoque.php?id=$id");
		exit();
		

		
	}






	if($id>0){
	
		$ssql = "select produtoid, pcodigo, pproduto, psubtitulo, pcodusuario, pdata_alteracao, pdata_cadastro from tblproduto where produtoid='{$id}'";
		$result = mysql_query($ssql);
		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				
				$codigo				=	$row["pcodigo"];
				$produto			=	$row["pproduto"];
				$subtitulo			=	$row["psubtitulo"];

				$usuario			=	get_usuario($row["pcodusuario"],"unome");		
				$data_alteracao		=	formata_data_tela($row["pdata_alteracao"]);
				$data_cadastro		=	formata_data_tela($row["pdata_cadastro"]);
				
				$subtitulo	= $subtitulo!=$produto ? " - ".$subtitulo : "";
				
			}
			mysql_free_result($result);
		}	
		
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {		
		//call
		produto_load_estoque();
	});
    
</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form method="post" name="frm_estoque" id="frm_estoque" action="estoque.php" onsubmit="return valida_estoque();">
    <input name="action" id="action" type="hidden" value="gravar" />  
    <input name="id" id="id" type="hidden" value="<?php echo $id?>" />
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Estoque &raquo;  <span><?php echo $produto  ?></span> </span> <a href="estoque_ajuste_consulta.php?id=<?php echo $id;?>">Consultar Histórico</a>
   	    </div>
            
            <div id="conteudo-interno">
				<table width="98%" border="0" cellspacing="0" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="130">ID</td>
                <td>#<?php echo $id;?></td>

              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>SKU</td>
                <td><?php echo $codigo;?></td>
                </tr>
              <tr>
                <td>Produto</td>
                <td><?php echo $produto . "&nbsp;" . $subtitulo;?></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Tipo de lançamento</td>
                <td>
                	<label><input type="radio" name="tipo" id="tipo" value="1" class="jtipo" />Entrada</label>&nbsp;&nbsp;&nbsp;
                    <label><input type="radio" name="tipo" id="tipo" value="2" class="jtipo" />Saída</label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Quantidade</td>
                <td><input name="quantidade" type="text" class="formulario" id="quantidade" size="12" maxlength="200" /> 
                  0,00</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td valign="top">Tamanho</td>
                <td>
                
					<?php
                        $ssql = "SELECT tblproduto_caracteristica.caracteristicaid, tblproduto_caracteristica.ccodpropriedade, tblproduto_propriedade.ppropriedade, 
								tblproduto_propriedade.pcodpropriedade, pai.ppropriedade AS propriedade_pai
								FROM tblproduto_caracteristica
								LEFT JOIN tblproduto_propriedade ON tblproduto_caracteristica.ccodpropriedade = tblproduto_propriedade.propriedadeid
								LEFT JOIN tblproduto_propriedade AS pai ON tblproduto_propriedade.pcodpropriedade = pai.propriedadeid
								WHERE tblproduto_caracteristica.ccodproduto ='{$id}' and pai.propriedadeid=1
								ORDER BY propriedade_pai, tblproduto_propriedade.ppropriedade";
						
						//echo $ssql;
						
                        $result = mysql_query($ssql);
						if(mysql_num_rows($result)==0){
							echo "N/C";	
						}
                        if($result){
                                while($row=mysql_fetch_assoc($result)){
                                    echo '<label><input type="checkbox" name="tamanho[]" id="tamanho[]" value="'.$row["ccodpropriedade"].'" '.$checked.' class="tamanho" />'.$row["propriedade_pai"]." : ".$row["ppropriedade"].'</label>';	
									echo '<br />';
                                }
                            echo "</ul>";
                            mysql_free_result($result);
                        }
                    
                    ?>                                
                </td>
              </tr>             
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td valign="top">Propriedade</td>
                <td>
                
					<?php
                        $ssql = "SELECT tblproduto_caracteristica.caracteristicaid, tblproduto_caracteristica.ccodpropriedade, tblproduto_propriedade.ppropriedade, 
								tblproduto_propriedade.pcodpropriedade, pai.ppropriedade AS propriedade_pai
								FROM tblproduto_caracteristica
								LEFT JOIN tblproduto_propriedade ON tblproduto_caracteristica.ccodpropriedade = tblproduto_propriedade.propriedadeid
								LEFT JOIN tblproduto_propriedade AS pai ON tblproduto_propriedade.pcodpropriedade = pai.propriedadeid
								WHERE tblproduto_caracteristica.ccodproduto ='{$id}' and pai.propriedadeid>1
								ORDER BY propriedade_pai, tblproduto_propriedade.ppropriedade";
						
						//echo $ssql;
						
                        $result = mysql_query($ssql);
						if(mysql_num_rows($result)==0){
							echo "N/C";	
						}						
                        if($result){
                                while($row=mysql_fetch_assoc($result)){
                                    echo '<label><input type="checkbox" name="propriedade[]" id="propriedade[]" value="'.$row["ccodpropriedade"].'" '.$checked.' class="propriedade" />'.$row["propriedade_pai"]." : ".$row["ppropriedade"].'</label>';	
									echo '<br />';
                                }
                            echo "</ul>";
                            mysql_free_result($result);
                        }
                    
                    ?>                                
                </td>
              </tr>             
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>

              <tr>
                <td>Observações</td>
                <td><textarea name="note" cols="75" rows="3" id="note"></textarea></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>                                         
              <tr>
                <td>Usuário</td>
                <td><?php echo $usuario;?></td>
                </tr>
              <tr>
                <td>Data Alteração</td>
                <td><?php echo $data_alteracao;?></td>
                </tr>
              <tr>
                <td>Data Cadastro</td>
                <td><?php echo $data_cadastro;?></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" id="btn-cmd-gravar" name="btn-cmd-gravar" value="Gravar" class="btn-gravar" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>                            
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="2">
                    <div id="td-estoque">
                        
                    </div>
                </td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              </table>
          </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>