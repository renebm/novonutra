<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}	
	
	/*-------------------------------------------------------------
	grava dados
	--------------------------------------------------------------*/	
	if($_POST && $_REQUEST["action"]=="gravar"){	
		$status = $_REQUEST["status"];
		left($id = $_REQUEST["id"],15);
		
		$codigo_rastreamento = addslashes($_REQUEST["codigo_rastreamento"]);
		
		if($status>0){
			$ssql = "update tblpedido set pcodstatus = '{$status}' where pedidoid='{$id}'";
			mysql_query($ssql);
		}

		if($codigo_rastreamento!=""){
			$ssql = "update tblpedido set pcodigo_rastreamento='{$codigo_rastreamento}' where pedidoid='{$id}'";
			mysql_query($ssql);
		}


		////////////////////////////////////////////////////////////////////////	
		//atualiza o estoque quando cancelado voltando o produto para o estoque
		if( $status == 7 || $status == 8 ){
				$ssql = "select tblpedido_item.itemid, tblpedido_item.pcodproduto, tblpedido_item.pcodtamanho, tblpedido_item.pcodpropriedade, tblpedido_item.pquantidade, 
						tblpedido_item.pbaixa_estoque, tblproduto.pcontrola_estoque
						from tblpedido_item
						inner join tblproduto ON tblpedido_item.pcodproduto = tblproduto.produtoid
						where tblpedido_item.pcodpedido = '{$id}' and tblpedido_item.pquantidade > 0 and tblpedido_item.pbaixa_estoque=-1 ";			
				$result = mysql_query($ssql);
				
				if($result){
					while($row=mysql_fetch_assoc($result)){
						$baixa_estoque = 1;					// nao controla estoque
						if($row["pcontrola_estoque"]==-1){
							$baixa_estoque = -1;				
						
							$produtoid = $row["pcodproduto"];
							$propriedade = $row["pcodpropriedade"];
							$tamanho = $row["pcodtamanho"];
							$tipo = 1;
							$quantidade = $row["pquantidade"];
							$note = "Pedido #".$id . " - Cancelado";
							
							//lanca o historico na tblajuste_estoque
							$ssql = "insert into tblestoque_ajuste (acodproduto, acodpropriedade, acodtamanho, atipo, aquantidade, anote, acodpedido, acodusuario, adata_cadastro) 
										values('{$produtoid}','{$propriedade}','{$tamanho}', '{$tipo}', '{$quantidade}','{$note}','{$id}','{$usuario}','{$data_hoje}') ";	
							mysql_query($ssql);						
							
							
							//atualiza o estoque
							$ssql = "update tblestoque set eestoque=eestoque+'{$quantidade}', ecodusuario='{$usuario}', edata_alteracao='{$data_hoje}' 
											where ecodproduto='{$produtoid}' and ecodtamanho='{$tamanho}' and ecodpropriedade='{$propriedade}' ";
							mysql_query($ssql);									
							//echo $ssql;
							
							//atualiza linha do pedido_item
							$ssql = "update tblpedido_item set pbaixa_estoque='{$baixa_estoque}' where itemid='".$row["itemid"]."'";
							mysql_query($ssql);
							
						}
					}
					mysql_free_result($result);
				}
				
		}
		
function gera_email_lista_presente($listaid,$id){
	$dados = mysql_fetch_array(mysql_query("SELECT CONCAT(ad.crazao_social,' ',ad.cnome) AS ad_nome, ad.cemail, CONCAT(cl.crazao_social,' ',cl.cnome) AS cli_nome, pr.pproduto, it.pquantidade
	FROM tbllista_presente AS li
	LEFT JOIN tblcadastro AS ad ON ad.cadastroid = li.lcodcadastro
	LEFT JOIN tblpedido AS pe ON pe.pcodlista_presente = li.listaid AND pe.pedidoid = $id
	LEFT JOIN tblcadastro AS cl ON cl.cadastroid = pe.pcodcadastro
	LEFT JOIN tblpedido_item AS it ON it.pcodpedido = pe.pedidoid and it.pquantidade > 0
	LEFT JOIN tblproduto AS pr ON it.pcodproduto = pr.produtoid
	WHERE li.listaid = $listaid"), MYSQL_ASSOC);
	$body = '<table width="600" border="0" cellspacing="0" cellpadding="0" style="font-family:Verdana, Geneva, sans-serif; font-size:11px; color:#000;">
			  <tr>
				<td><a href="'.$GLOBALS["site_site"].'"><img src="'.$GLOBALS["site_site"].'/images/logo.png" alt="'.$GLOBALS["site_nome"].'" border="0" /></a></td>
				<td width="450" align="left"><strong>Aquisição de Presente</strong></td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2"><p>Parabéns, '.$dados["ad_nome"].'!</p><br /></td>
			  </tr>
			  <tr>
				<td colspan="2">
					<p>
						O(a) convidado(a) <b>'.$dados["cli_nome"].'</b> lhe presenteou com '.$dados["pquantidade"].' ítem(s) do <b>'.$dados["pproduto"].'</b>, 
						que será entregue no endereço solicitado ou creditado em sua conta da Pop Decor, conforme a sua escolha de lista de presentes.
					</p>
					<br />
				</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
			  </tr>
			  </table>
			  <table width="600" border="0" cellspacing="0" cellpadding="0" style="font-family:Verdana, Geneva, sans-serif; font-size:11px; color:#000;">
			  <tr>
				<td colspan="2">&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2"><p>Caso precise alterar qualquer informação do seu pedido entre em contato com nossa Central de Relacionamento.</p><br />
				<p>Atenciosamente,</p><br />
				<p>Equipe "'.$GLOBALS["site_nome"].'"<br>
				  '.$GLOBALS["site_site"].'<br>
				  Central de relacionamento - '.$GLOBALS["site_email"].'
				</p></td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
			  </tr>
			</table>';
	$ret[0] = $body;
	$ret[1] = $dados;
	return $ret;
}
		
		////////////////////////////////////////////////////////////////////////	
		//envia email de lista de presente
		$listaid = intval(get_pedido($id,"pcodlista_presente"));
		if( $listaid > 0 ){
			$notificacao = intval(get_lista_presente($listaid, "lnotificacao"));
			if( $notificacao == -1 ){
				if( intval(get_configuracao("lista_presente_notificacao_status")) == intval($status) ){
					$dados 		= gera_email_lista_presente($listaid,$id);
					$body		= $dados[0];
					$dados		= $dados[1];
					$assunto	= "Aquisição de Presente";
					//carrega dados de envio do email de notificação

					if(envia_email($site_nome, $site_email, $dados["ad_nome"], $dados["cemail"], $assunto, $body)){
						$texto = "Notificação de lista de presente enviado automaticamente para $to_email.";		   						
					}
					else
					{
						$texto = "Erro ao enviar notificação de lista de presente para $to_email.";		   						
					}
					
					
					$ssql = "insert into tblpedido_historico (hcodpedido, hcodigo, hcodstatus, htexto, hip, hemail, hcodusuario, hdata_alteracao, hdata_cadastro) ";
					$ssql .= " values('{$id}','{$id}','{$status}','{$texto}','{$ip}','-1','{$usuario}','{$data_hoje}','{$data_hoje}')";
					mysql_query($ssql);			
					
					
					
					
				}

			}
			
		}

					
					
		////////////////////////////////////////////////////////////////////////	
		//atualiza historico
		$texto = "Status de pedido alterado.";
		$ip = $_SERVER['REMOTE_ADDR'];
		$email = 0;
		
		$ssql = "insert into tblpedido_historico (hcodpedido, hcodigo, hcodstatus, htexto, hip, hemail, hcodusuario, hdata_alteracao, hdata_cadastro) ";
		$ssql .= " values('{$id}','{$id}','{$status}','{$texto}','{$ip}','{$email}','{$usuario}','{$data_hoje}','{$data_hoje}')";
		mysql_query($ssql);
	
		//echo $ssql;
	
		//verifica se envia email para o cliente 
		$ssql = "select statusid, sstatus, sdescricao, semail, semail_texto, ssms, ssms_texto, scodusuario
				 from tblpedido_status where statusid='{$status}'";
		$result = mysql_query($ssql);
		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				
				$pedido_status		=	$row["sstatus"];
				$enviar_email		=	$row["semail"];
				$texto_email		=	$row["semail_texto"];
				$sms				=	$row["ssms"];
				$sms_texto			=	$row["ssms_texto"];				
			}
			mysql_free_result($result);
		}	
		
		
		if($enviar_email==-1){
			
			$ssql = "select tblcadastro.crazao_social, tblcadastro.cnome, tblcadastro.cemail 
					from tblcadastro
					inner join tblpedido on tblcadastro.cadastroid = tblpedido.pcodcadastro and tblpedido.pedidoid='{$id}'
					";
			$result = mysql_query($ssql);
			if($result){
				while($row=mysql_fetch_assoc($result)){
					$cliente_nome	=	$row["crazao_social"] . " " . $row["cnome"];
					$cliente_email	=	$row["cemail"];
				}
				mysql_free_result($result);
			}			
			
			/*-------------------------------------------------------------------------------
			//pega o codigo de rastreamente se tiver 
			--------------------------------------------------------------------------------*/
			$codigo_rastreamento = "";
			$link_rastreamento = "";
			
			$ssql = "select tblpedido.pcodigo_rastreamento, tblfrete_tipo.flink_rastreamento
					from tblpedido
					left join tblfrete_tipo on tblpedido.pcodfrete=tblfrete_tipo.freteid
					where tblpedido.pedidoid='{$id}'";
			$result = mysql_query($ssql);
			if($result){
				while($row=mysql_fetch_assoc($result)){
					$codigo_rastreamento = $row["pcodigo_rastreamento"];
					$link_rastreamento = $row["flink_rastreamento"];
				}
				mysql_free_result($result);
			}
			
			if($codigo_rastreamento=="" || $codigo_rastreamento == null){
				$link_rastreamento = $site_site . ". <br><br>Acesse o menu minha conta e acompanhe seu pedido.";	
			}
			
						
			/*-------------------------------------------------------------------------------			
			//prepara o email
			--------------------------------------------------------------------------------*/
			$texto_email = str_replace("#nome#",$cliente_nome,$texto_email);
			$texto_email = str_replace("#pedido#",$id,$texto_email);			
			$texto_email = str_replace("#link_rastreamento#",$link_rastreamento,$texto_email);
			$texto_email = str_replace("#codigo_rastreamento#",$codigo_rastreamento,$texto_email);
			
			
			//echo $texto_email;
			//exit();			
			
			$assunto = "Andamento do seu pedido $id na $site_nome - $pedido_status";
			
			if(envia_email($site_nome, $site_email, $cliente_nome, $cliente_email, $assunto, $texto_email)){
				$texto = "E-mail enviado automaticamente para $cliente_email com alerta do novo status.";		   
				
				$ssql = "insert into tblpedido_historico (hcodpedido, hcodigo, hcodstatus, htexto, hip, hemail, hcodusuario, hdata_alteracao, hdata_cadastro) ";
				$ssql .= " values('{$id}','{$id}','{$status}','{$texto}','{$ip}','-1','{$usuario}','{$data_hoje}','{$data_hoje}')";
				mysql_query($ssql);			
			}
			else
			{
				$texto = "Erro ao enviar e-mail automaticamente para $cliente_email com alerta do novo status.";		   
				
				$ssql = "insert into tblpedido_historico (hcodpedido, hcodigo, hcodstatus, htexto, hip, hemail, hcodusuario, hdata_alteracao, hdata_cadastro) ";
				$ssql .= " values('{$id}','{$id}','{$status}','{$texto}','{$ip}','0','{$usuario}','{$data_hoje}','{$data_hoje}')";
				mysql_query($ssql);					
			}
			
		}	

	
	}
	
	
	
	
	
	/*-------------------------------------------------------------
	pega número do pedido para detalhar
	--------------------------------------------------------------*/
	$id = $_REQUEST["id"];
	
	$ssql = "select bnosso_numero, bnosso_numero_dac, tblpedido.pedidoid, tblpedido.pcodorcamento, tblpedido.pcodnota_fiscal, tblpedido.pcodigo, tblpedido.pcodcadastro, tblpedido.ptitulo, tblpedido.pnome, 
			tblpedido.pendereco, tblpedido.pnumero, tblpedido.pcomplemento, tblpedido.pbairro, tblpedido.pcidade, tblpedido.pestado, tblpedido.pcep, 
			tblpedido.psubtotal, tblpedido.pvalor_desconto, tblpedido.pvalor_desconto_cupom, tblpedido.pvalor_frete, tblpedido.pvalor_presente, tblpedido.pvalor_total, 
			tblpedido.pcodforma_pagamento, tblpedido.pcodcondicao_pagamento, tblpedido.pcodfrete, tblpedido.pcodcupom_desconto, 
			tblpedido.pcodigo_rastreamento, tblpedido.pcodstatus, tblpedido.pdata_cadastro, 
			tblpedido.pcartao_numero, tblpedido.pcartao_retorno, tblpedido.ptexto_presente, tblpedido.preferencia, tblpedido.deposito,
	
			tblcadastro_endereco.etitulo, tblcadastro_endereco.enome, tblcadastro_endereco.eendereco, tblcadastro_endereco.enumero, tblcadastro_endereco.ecomplemento,
			tblcadastro_endereco.ebairro, tblcadastro_endereco.ecidade, tblcadastro_endereco.eestado, tblcadastro_endereco.ecep, tblcadastro_endereco.ereferencia,
	
			tblforma_pagamento.fdescricao, tblcondicao_pagamento.ccondicao,
			tblcadastro.cadastroid, tblcadastro.crazao_social, tblcadastro.cnome, tblcadastro.ccodtipo, tblcadastro.ccpf_cnpj, tblcadastro.crg_ie, tblcadastro.cdata_nascimento, tblcadastro.cemail,
			tblcadastro.ctelefone, tblcadastro.ccelular, tblcadastro_tipo.ttipo, 
			tblpedido_status.statusid, tblpedido_status.sdescricao, tb_cadastro_entrega.ereferencia as referencia_entrega,
			tblcupom_desconto.ccupom,
			tbllista_presente.ltitulo, tbllista_presente.lcredito 
	
			from tblpedido 
	
			left join tblcadastro_endereco on tblpedido.pcodendereco_fatura=tblcadastro_endereco.enderecoid 
			left join tblcadastro_endereco as tb_cadastro_entrega on tblpedido.pcep=tb_cadastro_entrega.ecep 

			left join tblcadastro on tblpedido.pcodcadastro=tblcadastro.cadastroid 
			left join tblcadastro_tipo on tblcadastro.ccodtipo=tblcadastro_tipo.tipoid
			left  join tblforma_pagamento on tblpedido.pcodforma_pagamento=tblforma_pagamento.formapagamentoid 
			left join tblcondicao_pagamento on tblpedido.pcodcondicao_pagamento=tblcondicao_pagamento.condicaoid 
			left join tblpedido_status on tblpedido.pcodstatus=tblpedido_status.statusid 
			left join tblcupom_desconto on tblpedido.pcodcupom_desconto=tblcupom_desconto.cupomid 
			left join tbllista_presente on tblpedido.pcodlista_presente=tbllista_presente.listaid 
			left join tblboleto on tblpedido.pedidoid = tblboleto.bcodpedido
			
			where tblpedido.pedidoid='{$id}' 
			limit 0,1";
	
	//echo $ssql;
	
	$result = mysql_query($ssql);
	if($result){
		
		while($row=mysql_fetch_assoc($result)){
			$orcamento			= $row["pcodorcamento"];
			$nota_fiscal		= intval($row["pcodnota_fiscal"]);		
			
			$codigo_boleto			= $row["bnosso_numero"].$row["bnosso_numero_dac"];
			$codigo_pedido	 		= 15500+$row["pcodigo"];
			//$codigo_pedido	 		= substr($codigo_pedido, strlen($codigo_pedido)-6,6);
			$codigo_cliente			= $row["pcodcadastro"];
			$_SESSION['cod']        = $row["pcodcadastro"];
			$razao_social			= $row["crazao_social"] . "&nbsp;" . $row["cnome"];
			$tipo_cadastro			= $row["ttipo"];
			$cnpj_cpf				= $row["ccpf_cnpj"];
			$ie_rg					= $row["crg_ie"];
			$data_nascimento 		= formata_data_tela($row["cdata_nascimento"]);
			$telefone				= $row["ctelefone"];
			$celular				= $row["ccelular"];
			$data_pedido     		= formata_data_tela($row["pdata_cadastro"]);
			$identificacao	 		= $row["ptitulo"];
			$nome 			 		= $row["pnome"];
			$email 			 		= $row["cemail"];
			$endereco		 		= $row["pendereco"]. ", " . $row["pnumero"]. " - " . $row["pcomplemento"]. " - " . $row["pbairro"];
			$cidade 		 		= $row["pcidade"];
			$estado			 		= $row["pestado"];
			$cep 			 		= $row["pcep"];
			$cep			 		= substr($cep, 0,5). "-" . substr($cep, 5,3);
			$referencia_entrega		= $row["referencia_entrega"];
			$codigo_rastreamento	= $row["pcodigo_rastreamento"];
			$codigo_forma_pagamento = $row["pcodforma_pagamento"];
			$deposito 				= $row["deposito"];
			
			if($row["etitulo"]!=""){
				$endereco_fatura	=  $row["etitulo"]." - ".$row["eendereco"].",&nbsp;".$row["enumero"]."&nbsp;".$row["ecomplemento"]." - ".$row["ebairro"]." - ".$row["ecidade"]." - ".$row["eestado"]." - ".$row["ecep"];
			}
			
			$subtotal		 	= formata_valor_tela($row["psubtotal"]);
			$valor_desconto	 	= formata_valor_tela($row["pvalor_desconto"]);
			$valor_presente		= formata_valor_tela($row["pvalor_presente"]);
			$valor_frete	 	= formata_valor_tela($row["pvalor_frete"]);
			$valor_total	 	= formata_valor_tela($row["pvalor_total"]);
			$forma_pagamento 	= $row["fdescricao"];
			$cod_condicao_pgto 	= $row["pcodcondicao_pagamento"];
			$condicao_pagamento = $row["ccondicao"];
			
			$status				= $row["statusid"];
			$status_pedido 		= $row["sdescricao"];
			
			$referencia			= $row["preferencia"];
			
			$tipo_frete 		= $row["pcodfrete"];
			$cupom_desconto		= $row["ccupom"];
			$valor_cupom_desconto= number_format($row["pvalor_desconto_cupom"],2,",",".");
			
			$texto_presente		= str_replace("\r","<br />",$row["ptexto_presente"]."");
			
			
			$lista_presente_titulo = $row["ltitulo"];
			if($lista_presente_titulo!=""){
				$lista_presente_tipo = ($row["lcredito"]==-1) ? "Crédito" : "Envio de Mercadoria";
			}
			
			
			$cartao_numero		= "**** ".right("".$row["pcartao_numero"],4);
			$cartao_retorno		= utf8_decode($row["pcartao_retorno"]);	
			
		}
		mysql_free_result($result);
	}
	
	
	// Nova busca para pegar tipo do frete - Nome coluna igual a de outra tabela
	$ssql = "select tblfrete_tipo.fdescricao from tblfrete_tipo where freteid=" . $tipo_frete;
	$result = mysql_query($ssql);
	if ($result){
		while($row=mysql_fetch_assoc($result)){
			$tipo_frete = $row["fdescricao"];
		}
		mysql_free_result($result);
	} 
	
	
	
	/*--------------------------------------------------------------------------
	//NF-e
	---------------------------------------------------------------------------*/
	$arquivo_xml 	= 	"nfe/xml/" . right(str_repeat("0",9).$id,9) . ".xml";
	
	
	$xml 		= simplexml_load_file($arquivo_xml); 	
	$chave_nfe 	= $xml->protNFe->infProt->chNFe ; 
	
	$arquivo_danfe = "nfe/pdf/".$chave_nfe.".pdf";
	
	
	// ===========================================
	
	

	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />
<meta name="author" content="Betasoft" />
<meta name="copyright" content="Betasoft" />

<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-colorbox.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-colorbox.js"></script>


<script language="javascript" type="text/javascript">
	$(document).ready(function() {	

    });	
	
	//click no combo do pedido
	$('#status').live('change', function(){
		var $this = $( this );  
		var id = $this.attr("value");  
		if(confirm("Deseja realmente alterar o status do pedido?")){
			$("#frm_pedido").submit();
		}
	});
		
</script>

</head>

<body>

<div id="modal-info">
    <div id="modal-close"><a href="javascript:void(0);" onclick="$('#modal-info').fadeOut();"><strong>[ FECHAR AVISO ]</strong></a></div>
	<div id="modal-conteudo">
    ##
    </div>
</div>

<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">
    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
	?>
        
    </div>
    
    <div id="content" style="position: absolute; margin-left: 220px;">
    
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Gerenciamento de Pedidos » Detalhes do Pedido</span>
                <span style="float:right; margin:5px 5px 0 0;">
                <a href="pedido_consulta.php" id="link-voltar">[ voltar ]</a>
                <a href="javascript:void(0);" id="link-voltar" onclick="javascript:pedido_nfe(<?php echo $id;?>)">[ NF-e ]</a>                
                <a href="javascript:void(0);" id="link-voltar" onclick="javascript:pedido_print(<?php echo $id;?>)">[ imprimir ]</a>
                <a href="javascript:void(0);" id="link-voltar" onclick="javascript:pedido_clear_sale(<?php echo $id;?>)">[ clear sale ]</a>
                </span>
            </div>
            <div id="conteudo-interno">
            	<form name="frm_pedido" id="frm_pedido" action="pedido.php" method="post" >
                <input type="hidden" name="action" id="action" value="gravar" >
                <input type="hidden" name="id" id="id" value="<?php echo $id;?>" >
            	<table width="98%" border="0" style="margin: 10px; float:left;">
                <tr>
                	<td height="25">Detalhes do Pedido n&deg;: <?php echo $codigo_pedido; ?></td>
                	<td width="200">Código de Rastreamento</td>
               	  <td width="200">Alterar Status do Pedido</td>
               	</tr>				
                	<tr>
                	  <td height="25">Data do Pedido: </span> <span class="txt-detalhe-pedido"><?php echo $data_pedido; ?></span></td>
                	  <td><input name="codigo_rastreamento" type="text" class="formulario" id="codigo_rastreamento" value="<?php echo $codigo_rastreamento;?>" <?php if( $status == 7 || $status == 8 ){ echo 'readonly="readonly"'; }?> /></td>
                	  <td><select name="status" size="1" class="formulario" id="status" <?php if( $status == 7 || $status == 8 ){ echo 'disabled="disabled"'; }?> >
                	    <option value="0">Selecione</option>
                	    <?php
						$ssql = "select statusid, sstatus from tblpedido_status order by statusid";
						$result = mysql_query($ssql);
						if($result){
							while($row=mysql_fetch_assoc($result)){
								echo '<option value="'.$row["statusid"].'"';
								
								if($row["statusid"]==$status){
									echo ' selected';	
								}
								
								echo '>';
								echo $row["sstatus"];
								echo '</option>';
							}
							mysql_free_result($result);
						}
					  ?>
              	    </select>
                    </td>
                    </tr>
                    <tr>
                      <td height="25">NF-e: <span id="notafiscalid"><?php echo $nota_fiscal;?></span></td>
                      <td colspan="2">Danfe: <a href="<?php echo $arquivo_danfe;?>" id="danfe_url" target="_blank"><span id="danfe_chave"><?php echo $chave_nfe;?></span></a></td>
                   </tr>
                  </table>
                </form>
            </div>
        </div>
    	
        <div class="box-botoes-home">
        	<table border="0" style="margin: 5px 0 5px 10px; text-align:center;">
                <tr>
                    <td colspan="2" align="center"><strong>Dados do cliente</strong></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                	<td width="12%" align="left">&nbsp;</td>
                   	<td width="88%" align="left">&nbsp;</td>
              	</tr>                 
            <tr>
                	<td width="12%" align="left"><strong>Referência</strong></td>
                   	<td width="88%" align="left"><?php echo $referencia; ?></td>
           	  </tr>
			  <?php if($lista_presente_titulo != ""){ ?>
                <tr style="color:red;font-size:12pt;">
                	<td width="12%" align="left"><strong>Lista de Presente</strong></td>
                   	<td width="88%" align="left"><?php echo $lista_presente_titulo . "&nbsp;&nbsp;" . $lista_presente_tipo;  ?> </td>
              	</tr> 
			  <?php } ?>
                <tr>
                	<td width="12%" align="left">&nbsp;</td>
                   	<td width="88%" align="left">&nbsp;</td>
              	</tr>                                
                <tr>
                	<td width="12%" align="left"><strong>Pessoa:</strong></td>
                   	<td width="88%" align="left"><?php echo $tipo_cadastro; ?></td>
              	</tr>
                <tr>
                	<td height="23" align="left"><strong>Razão Social:</strong></td>
                    <td align="left"><?php echo $razao_social; ?></td>
              	</tr>
                <tr>
                    <td align="left"><strong>CPF / CNPJ:</strong></td>
                    <td align="left"><?php echo $cnpj_cpf; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>IE / RG:</strong></td>
                    <td align="left"><?php echo $ie_rg; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Data Nascimento:</strong></td>
                    <td align="left"><?php echo $data_nascimento; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Telefone:</strong></td>
                    <td align="left"><?php echo $telefone; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Celular:</strong></td>
                    <td align="left"><?php echo $celular; ?></td>
                </tr>
                <tr>
                  <td align="left"><strong>E-mail:</strong></td>
                  <td align="left"><?php echo $email; ?></td>
                </tr>
                <tr>
                  <td align="left">&nbsp;</td>
                  <td align="left">&nbsp;</td>
                </tr>
                <tr>
                  <td align="left"><strong>Endereço Fatura</strong></td>
                  <td align="left"><?php echo $endereco_fatura;?></td>
                </tr>
                <tr>
                  <td align="left">&nbsp;</td>
                  <td align="left">&nbsp;</td>
                </tr>
				<tr>
                  <td align="left"><strong>Depósito</strong></td>
                  <td align="left">
				  <?php 
					if($codigo_forma_pagamento <> 9){ 
						echo "****";
					}else if($codigo_forma_pagamento == 9 && strlen($deposito) > 0){ ?>
						<a href="<?php echo "http://".$_SERVER["HTTP_HOST"]."/".$deposito;?>" target="_blank"><?php echo "http://".$_SERVER["HTTP_HOST"]."/".$deposito;?></a>
					<?php 
					}
					?>
				  </td>
                </tr>
                <tr>
                  <td align="left"><strong>No. Cartão</strong></td>
                  <td align="left"><?php echo $cartao_numero;?></td>
                </tr>
                <tr>
                  <td align="left"><strong>Retorno Cartão</strong></td>
                  <td align="left">
                  
				  	<?php 
					//echo $cartao_retorno;
					$xml = simplexml_load_string($cartao_retorno);
					if(strlen($cartao_retorno) >= 1){
						foreach($xml as $key => $value)
							{
								//echo '<span style="width:150px;">';	
								echo strtoupper($key) . " : " . str_replace("+","&nbsp;",$value) . "  |   ";
								//echo '</span>';
							}
					}
					//echo $cartao_retorno;
					
					?>
                  </td>
                </tr>
                <tr>
                  <td align="left">&nbsp;</td>
                  <td align="left">&nbsp;</td>
                </tr>
                <tr>
                  <td align="left"><strong>Observações</strong></td>
                  <td align="left"><?php echo $note;?></td>
                </tr>                
            </table>
		</div>
        
        
        <div class="box-botoes-home">
        	<table width="98%" border="0" style="margin: 5px 0 5px 10px; text-align:center">
                <tr>
                    <td colspan="2" align="center"><strong>Dados da entrega</strong></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                	<td width="12%" align="left"><strong>Identificação:</strong></td>
                   	<td width="88%" align="left"><?php echo $identificacao; ?></td>
              	</tr>
                <tr>
                	<td align="left"><strong>Destinatário:</strong></td>
                    <td align="left"><?php echo $nome; ?></td>
              	</tr>
                <tr>
                    <td align="left"><strong>Endereço:</strong></td>
                    <td align="left"><?php echo $endereco; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Cidade:</strong></td>
                    <td align="left"><?php echo $cidade; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Estado:</strong></td>
                    <td align="left"><?php echo $estado; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>CEP:</strong></td>
                    <td align="left"><?php echo $cep; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Ponto de Referência:</strong></td>
                    <td align="left"><?php echo $referencia_entrega; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;  </td>
                </tr>
                <tr>
                    <td align="left"><strong>Status:</strong></td>
                    <td align="left"><?php echo $status_pedido; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;  </td>
                </tr>
                <tr>
                    <td align="left"><strong>Frete:</strong></td>
                    <td align="left"><?php echo $tipo_frete; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Tipo Frete:</strong></td>
                    <td align="left">Normal</td>
                </tr>
            </table>
      </div>  
      
      <?php
      	if(trim($texto_presente)!=""){
			echo '<div class="box-botoes-home" style="color:#333;">';
			echo '<strong>Mensagem para presente:</strong><br /><br />';
			echo $texto_presente;
			echo '</div>';
		}
	  ?>
      

            
      <div class="box-botoes-home">
        	<table width="98%" border="0" style="margin: 5px 0 5px 10px; text-align:center">
                <tr><td><strong>Itens do Pedido</strong></td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                    	<table width="100%" border="0" cellpadding="2" cellspacing="2">
                        	<tr bgcolor="#eeeeee">
                            	<strong>
                                <td align="left">Descrição do produto</td>
                                <td align="center">Quantidade</td>
                                <td align="center">Serviços</td>
                                <td align="center">Preço Unitário</td>
                                <td align="center">Valor Total</td>
                                </strong>
                            </tr>
                            
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            
                            <?php
								
								$ssql = "SELECT tblproduto.produtoid, tblproduto.pcodigo, tblproduto.pproduto, tblproduto.psubtitulo, tblpedido_item.pvalor_unitario, tblproduto.pdisponivel, tblproduto.ppeso, 
								tblpedido_item.pquantidade, tblpedido_item.pcodproduto, tblpedido_item.ppresente, pai.ppropriedade AS propriedade_pai, tblpedido_item.pcodtamanho, tblproduto_propriedade.ppropriedade AS ptamanho, 
								tblpedido_item.pcodpropriedade, proper.ppropriedade AS ppropriedade
								FROM tblproduto
								
								INNER JOIN tblpedido_item ON tblproduto.produtoid = tblpedido_item.pcodproduto
								LEFT JOIN tblproduto_propriedade ON tblpedido_item.pcodtamanho = tblproduto_propriedade.propriedadeid
								LEFT JOIN tblproduto_propriedade AS proper ON tblpedido_item.pcodpropriedade = proper.propriedadeid
								LEFT JOIN tblproduto_propriedade AS pai ON tblproduto_propriedade.pcodpropriedade = pai.propriedadeid
								where tblpedido_item.pcodpedido=$id and tblpedido_item.pquantidade > 0
								";
								
								//echo $ssql;
				
								$result = mysql_query($ssql);
								if($result){
									$contador = 1;
									while($row=mysql_fetch_assoc($result)){
										$produto			= $row["pcodigo"] . " - " . $row["pproduto"] . "&nbsp;&nbsp;" . $row["ppropriedade_pai"] . "&nbsp;&nbsp;" . $row["ptamanho"]; 
										$subtitulo			= $row["psubtitulo"];
										
										if( get_configuracao("admin_pedido_item_subtitulo") == -1 ){
											$produto .= "&nbsp;" . $subtitulo;
										}
										
										$produto_qtde 	 	= $row["pquantidade"];
										$valor_unitario 	= $row["pvalor_unitario"];
										$produto_desconto 	= formata_valor_tela($row["pdesconto"]);
										$valor_final 		= $produto_qtde * $valor_unitario;
										$valor_final 		= formata_valor_tela($valor_final);
										
										$peso				= $peso + ($row["pquantidade"]*$row["ppeso"]);
										
										if($row["ppresente"]){
											$gift = "style='color:red; font-size:11pt;'";
										}else{
											$gift = "";
										}
										
										if ((round($contador/2)) <> ($contador/2)){
											echo '<tr bgcolor="#eeeeee" '.$gift.' >';
										}else{
											echo '<tr bgcolor="#ffffff" '.$gift.' >';
										}
										
										$servico = ($row["ppresente"]==-1) ? "*presente" : "";
										
										echo '
													<td align="left">'. $produto .'</td>
													<td align="center">'. $produto_qtde .'</td>
													<td align="center">'.$servico.'</td>
													<td align="right">'. formata_valor_tela($valor_unitario) .'</td>
													<td align="right">'. $valor_final .'</td>
												</tr>';
												$contador ++;
									}
								}mysql_free_result($result);
							?>
                            <tr>
                                <td align="left">&nbsp;</td>
                                <td align="center">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left">Peso total dos produtos: <?php echo number_format($peso,2,",",".");?>KG</td>
                                <td align="center">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                            </tr>                            
                            <tr>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right"><strong>Subtotal:</strong></td>
                                <td align="right">R$ <?php echo $subtotal; ?></td>
                            </tr>
                            <tr>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right"><strong>Desconto:</strong></td>
                                <td align="right">R$ <?php echo $valor_desconto; ?></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right"><strong>Cupom de Desconto: <?php if($cupom_desconto!=""){echo "".$cupom_desconto."";}?></strong></td>
                                <td align="right">R$ <?php echo $valor_cupom_desconto; ?></td>
                            </tr>                           
                            <tr>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right"><strong>Frete:</strong></td>
                                <td align="right">R$ <?php echo $valor_frete; ?></td>
                            </tr>
                            <tr>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right"><strong>Total:</strong></td>
                                <td align="right">R$ <?php echo $valor_total; ?></td>
                            </tr>
                            <tr>
                              <td align="right">&nbsp;</td>
                              <td align="right">&nbsp;</td>
                              <td align="right">&nbsp;</td>
                              <td align="right">&nbsp;</td>
                              <td align="right">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
      </div>
      
      <div class="box-botoes-home">
        	<table width="98%" border="0" style="margin: 5px 0 5px 10px; text-align:center">
                <tr><td><strong>Dados do Pagamento</strong></td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                    	<table width="100%" border="0" cellpadding="3" cellspacing="0">
                            <tr>
                            	<td width="120" align="left"><strong>Pagamento:</strong></td>
                            	<td align="left"><strong><?php echo $forma_pagamento; ?></strong></td>
                            </tr>
                            <tr>
                            	<td align="left"><strong>Valor:</strong></td>
                                <td align="left">R$ <?php echo $valor_total; ?></td>
                            </tr>
                            <?php 
								$ssql = "select * from tblcondicao_pagamento where condicaoid=" . $cod_condicao_pgto;
								
								
									$result = mysql_query($ssql);
									if($result){
									while($row=mysql_fetch_assoc($result)){
										$qtde_parcelas = $row["cnumero_parcelas"];
									}
								}mysql_free_result($result);
								
									
							?>
                            <tr>
                            	<td align="left"><strong>Parcelas:</strong></td>
                                <td align="left"><?php echo $qtde_parcelas; ?></td>
                            </tr>
                            <tr>
                            	<td align="left"><strong>Status:</strong></td>
                                <td align="left"><?php echo $status_pedido; ?></td>
                            </tr>
							<tr>
							   	<td align="left"><strong>Codigo Boleto:</strong></td>
                                <td align="left"><?php echo $codigo_boleto; ?></td>
                            </tr>
                            <tr>
							   	<td align="left"><strong>Arquivo de remessa:</strong></td>
                                <td align="left"><a href="http://nutracorpore.com.br/boletos/<?php echo $id;?>.txt" download>Baixar arquivo</a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
      </div>
      <div class="box-botoes-home">

			<!-- HISTORICO DO PEDIDO -->

        	<table width="98%" border="0" style="margin: 5px 0 5px 10px; text-align:center">
                <tr><td><strong>Histórico do Pedido</strong></td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                    	<table width="100%" border="0" cellpadding="0" cellspacing="2">
                        	<tr bgcolor="#eeeeee">
                            	<strong>
                                <td width="150" align="center">Data</td>
                                <td width="250" align="center">Status</td>
                                <td align="center"><strong>Texto</strong></td>
                                <td width="150" align="center"><strong>Usuário</strong></td>
                                </strong>
                            </tr>
                            
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            
                            <?php
								$ssql = " select tblpedido_historico.historicoid, tblpedido_historico.hcodpedido, tblpedido_historico.hcodstatus, tblpedido_historico.htexto, ";
								$ssql .= " tblpedido_historico.hcodusuario,";
								$ssql .= " tblpedido_historico.hdata_alteracao, tblusuario.usuarioid, tblusuario.unome, tblpedido_status.sstatus ";
								$ssql .= " FROM tblpedido_historico, tblusuario, tblpedido_status";
								$ssql .= " where tblpedido_historico.hcodpedido=" . $id;
								$ssql .= " and tblpedido_historico.hcodstatus=tblpedido_status.statusid";								
								$ssql .= " and tblusuario.usuarioid=tblpedido_historico.hcodusuario";
								$ssql .= " order by hdata_alteracao";
																
								//echo $ssql;
				
								$result = mysql_query($ssql);
								if($result){
									$contador = 1;
									while($row=mysql_fetch_assoc($result)){
										$data_status 	= $row["hdata_alteracao"];
										$status_pedido 	= $row["sstatus"];
										$usuario		= $row["unome"];
										$texto			= $row["htexto"];
										
										if ((round($contador/2)) <> ($contador/2)){
											echo '<tr bgcolor="#eeeeee">';
										}else{
											echo '<tr bgcolor="#ffffff">';
										}
										
										echo '
													<td align="left">'. $data_status .'</td>
													<td align="center">'. $status_pedido .'</td>
													<td>'. $texto .'</td>
													<td>'. $usuario .'</td>
												</tr>';
												$contador ++;
									} 
								}mysql_free_result($result);
							?>
                            <tr>
                                <td align="left">&nbsp;</td>
                                <td align="center">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                	<td>&nbsp;</td>
                </tr>
                <tr>
                	<td>&nbsp;</td>
                </tr>
            </table>
			<!-- HISTORICO DO CLIENTE -->
				<p style="text-align:center;color:black"><strong>Historico do cliente</strong></p>
				<table width="99%" border="0" cellspacing="0" cellpadding="3" style="margin:10px;">
                  <tr>
                    <td width="50" class="titulo_table">#ID</td>
                    <td width="150" class="titulo_table">Data</td>
                    <td class="">Valor</td>
                    <td class="titulo_table">Forma de Pagto</td>
					<td class="titulo_table">Detalhes do pedido</td>
                  </tr>
                  <?php 
					
					$userid = $_SESSION['cod'];					
					
					$ssql = "select tblpedido.pedidoid, tblpedido.pcodigo, tblpedido.pvalor_total, tblpedido.pdata_cadastro, tblforma_pagamento.fforma_pagamento from tblpedido left join tblforma_pagamento on tblpedido.pcodforma_pagamento=tblforma_pagamento.formapagamentoid";
							
					$ssql .= "  where tblpedido.pcodcadastro='{$userid}' ORDER BY tblpedido.pedidoid DESC";						
											
					$result = mysql_query($ssql);
					if($result){
						
						if(mysql_num_rows($result)==0){
							echo "Nenhum pedido encontrado desde " . $data1 . " escolha outro período ou refaça a busca. <br>";	
						}
							$o = 0;
						while($row=mysql_fetch_assoc($result)){
							$o++;
							$codigo_pedido	= 15500+$row["pcodigo"];
							$codigo_pedido_link = "0000".$row["pcodigo"];
							$codigo_pedido_link	= substr($codigo_pedido_link, strlen($codigo_pedido_link)-6,6);
							$data 			= formata_data_tela($row["pdata_cadastro"]);
							$valor			= number_format($row["pvalor_total"],2,",",".");
							$forma_pagamento= $row["fforma_pagamento"]; ?>

							<tr>
								<td><?= $codigo_pedido ?></td>
								<td><?= $data ?></td>
								<td>R$ <?= $valor ?></td>
								<td><?= $forma_pagamento ?></td>
								<td><a href="pedido.php?id=<?= $codigo_pedido_link ?>">Ver Detalhes</a></td>
							</tr>	
			<?php
						}
					}
			?>
    			<b style="color:black">Esse cliente já fez <?= $o ?> <?php if($o == 1){echo "compra";}else{echo "compras";}?> em seu site.</b>
            </table>

    
    <div id="footer">
    </div>
</div>
</body>
</html>