<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}

	$id = $_REQUEST["id"];	
	if(!is_numeric($id)){
		$id = 0;	
	}


	/*-----------------------------------------------------------------------
	ajax de excluisão
	------------------------------------------------------------------------*/
	if($_POST && $_REQUEST["action"]=="excluir"){
		
		$ssql = "select orcamentoid from tblorcamento where ocodcondicao_pagamento = '{$id}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)>0){
				echo "A condição de pagamento não pode ser excluida pois existem orçamentos/pedidos associados a mesma.";
				exit();
			}	
		}
		
		$ssql = "select pedidoid from tblpedido where pcodcondicao_pagamento = '{$id}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)>0){
				echo "A condição de pagamento não pode ser excluida pois existem pedidos associados a mesma.";
				exit();
			}	
		}


		$ssql = "select descontoid from tbldesconto where dcodcondicao_pagamento = '{$id}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)>0){
				echo "A condicao de pagamento não pode ser excluida pois existem descontos associados a mesma.";
				exit();
			}	
		}


		$ssql = "delete from tblcondicao_pagamento where condicaoid='{$id}'";
		mysql_query($ssql);
		echo "ok";
		exit();
		
	}




	if($_POST && $_REQUEST["action"]=="gravar"){
		$id					= 	addslashes($_REQUEST["id"]);
		$forma_pagamento	=	addslashes($_REQUEST["forma_pagamento"]);
		$condicao			=	addslashes($_REQUEST["condicao"]);
		$descricao			=	addslashes($_REQUEST["descricao"]);
		$ativa				=	addslashes($_REQUEST["ativa"]);
		$numero_parcelas	=	addslashes($_REQUEST["numero_parcelas"]);

		if(!is_numeric($forma_pagamento)){
			$forma_pagamento = 0;	
		}

		if(!is_numeric($ativa)){
			$ativa = 0;	
		}
			
		
		if($id==0){
			
			//verifica se nao existe ja uma descricao com o mesmo nome;
			$ssql = "select condicaoid from tblcondicao_pagamento where ccodforma_pagamento='{$forma_pagamento}' and ccondicao='{$condicao}'";
			$result = mysql_query($ssql);
				if($result){
					$num_rows = mysql_num_rows($result);
					if($num_rows>0){
						$msg = "Já existe um registro com os mesmos dados digitados";
						$erro = 1;
					}	
				}
				
			//--------------------------------------------------------	
			
			if($num_rows==0){
				$ssql = "insert into tblcondicao_pagamento (ccodforma_pagamento, ccondicao, cdescricao, cnumero_parcelas, cativa, 
														 ccodusuario, cdata_alteracao, cdata_cadastro) 
							values('{$forma_pagamento}','{$condicao}','{$descricao}','{$numero_parcelas}','{$ativa}',  
								   '{$usuario}', '{$data_hoje}','{$data_hoje}')";
				$result = mysql_query($ssql);
							
				if(!$result){
					$msg = "Erro ao gravar registro";
					$erro = 1;
				}
				else
				{
					$id = mysql_insert_id();
					$msg = "Registro salvo com sucesso";
				}
			}
			
		}
		else
		{
				//edita/atualiza o registro	
				$ssql = "update tblcondicao_pagamento set ccodforma_pagamento='{$forma_pagamento}', ccondicao='{$condicao}', cdescricao='{$descricao}', cativa='{$ativa}', 
				ccodusuario='{$usuario}', cdata_alteracao='{$data_hoje}'
							 where condicaoid = $id";
				$result = mysql_query($ssql);
				if(!$result){
					$msg = "Erro ao atualizar registro";
					$erro = 1;
				}
				else
				{
					$msg = "Registro atualizado com sucesso";
				}			
			
		}
		
		
	}






	if($id>0){
	
		$ssql = "select condicaoid, ccodforma_pagamento, ccondicao, cdescricao, cnumero_parcelas, cativa, ccodusuario, cdata_alteracao, cdata_cadastro
				from tblcondicao_pagamento where condicaoid='{$id}'";
		$result = mysql_query($ssql);
		//echo $ssql;
		if($result){
			while($row=mysql_fetch_assoc($result)){
				
				$forma_pagamento		=	$row["ccodforma_pagamento"];
				$condicao				=	$row["ccondicao"];
				$descricao				=	$row["cdescricao"];
				$ativa					=	$row["cativa"];
				$numero_parcelas		=	$row["cnumero_parcelas"];
								
				$usuario			=	get_usuario($row["ccodusuario"],"unome");		
				$data_alteracao		=	formata_data_tela($row["cdata_alteracao"]);
				$data_cadastro		=	formata_data_tela($row["cdata_cadastro"]);
				
			}
			mysql_free_result($result);
		}	
		
	}
	
	
	
	if($id==0){
		$ativa = -1;
		$numero_parcelas = 0;
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {							   		
		$("#forma_pagamento").focus();	
	});
    
</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form method="post" name="frm_condicao_pagamento" id="frm_condicao_pagamento" action="condicao_pagamento.php" onsubmit="return valida_condicao_pagamento();">
    <input name="action" id="action" type="hidden" value="gravar" />  
    <input name="id" id="id" type="hidden" value="<?php echo $id?>" />
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Condição de Pagamento &raquo;  <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='condicao_pagamento.php';"><?php echo ($id==0) ? "Novo Registro" : $condicao;  ?></span> </span> <a href="condicao_pagamento_consulta.php">Consulta</a>
   	    </div>
            
            <div id="conteudo-interno">
				<table width="98%" border="0" cellspacing="0" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="130">ID</td>
                <td>#<?php echo $id;?></td>
                <td width="200" rowspan="19" align="center" valign="top">
				<?php echo $msg;?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Forma de Pagamento</td>
                <td>
                <select name="forma_pagamento" size="1" class="formulario" id="forma_pagamento">
                  <option value="0">Selecione</option>
                  <?php
                  	$ssql = "select formapagamentoid, fforma_pagamento from tblforma_pagamento order by fforma_pagamento";
					$result = mysql_query($ssql);
					if($result){
						while($row=mysql_fetch_assoc($result)){
							echo "<option value='".$row["formapagamentoid"]."' ";
							if($forma_pagamento==$row["formapagamentoid"]){
								echo " selected";	
							}	
							echo ">";
							echo $row["fforma_pagamento"];
							echo "</option>";
						}
						mysql_free_result($result);
					}
				  ?>
                </select>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>              
              <tr>
                <td>Condição de Pagto</td>
                <td><input name="condicao" type="text" class="formulario" id="condicao" value="<?php echo $condicao;?>" size="75" maxlength="100" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Descrição</td>
                <td><input name="descricao" type="text" class="formulario" id="descricao" value="<?php echo $descricao;?>" size="75" maxlength="100" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Número de Parcelas</td>
                <td><input name="numero_parcelas" type="text" class="formulario" id="numero_parcelas" value="<?php echo $numero_parcelas;?>" size="10" maxlength="3" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Ativa</td>
                <td><label>
                  <input type="radio" name="ativa" id="ativa" value="-1" <?php echo ($ativa == -1) ? "checked" : "" ;?>  />
                  Sim</label>
                  <label>
                    <input type="radio" name="ativa" id="ativa" value="0" <?php echo ($ativa == 0) ? "checked" : "" ;?>  />
                Não</label></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Usuário</td>
                <td><?php echo $usuario;?></td>
                </tr>
              <tr>
                <td>Data Alteração</td>
                <td><?php echo $data_alteracao;?></td>
                </tr>
              <tr>
                <td>Data Cadastro</td>
                <td><?php echo $data_cadastro;?></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" id="btn-cmd-gravar" name="btn-cmd-gravar" value="Gravar" class="btn-gravar" /></td>
                </tr>
              </table>
          </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>