<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");
	
	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	if($_POST){
		if($_POST["action"]=="dologin"){
				$login 	= addslashes($_POST["login"]);
				$senha  = addslashes($_POST["senha"]);
				$senha	= base64_encode($senha);
				
				$lembrar_login 	= $_POST["lembrar-login"];
				
				if($lembrar_login==-1){
					setcookie("login",$login,$expires);
					$login = $login;
				}else{
					setcookie("login","",time()-3600);
					$login = "";	
				}				
				
				
				//----------------------------------
				$ssql = "select usuarioid, unome, ulogin, usenha from tblusuario where ulogin='{$login}' and usenha='{$senha}' limit 0 ,1";
				$result = mysql_query($ssql);
				if($result){
					
					$num_rows = mysql_num_rows($result);
					
					if($num_rows==0){
						$msg = "Login e senha incorreto.";
					}
					
					while($row=mysql_fetch_assoc($result)){
						$_SESSION["loginid"] = $row["usuarioid"];
						$_SESSION["login"] = $row["ulogin"];
						header("location: home.php");
						exit();
					}
					mysql_fetch_assoc($result);
				}
				
				
				
				
			}	
	}
	
	
	if(isset($_COOKIE["login"])){
		$login = $_COOKIE["login"];	
		$lembrar_login = -1;
	}
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		$("#login").focus();
		
		<?php
			if($msg!=""){
				echo 'alert("'.$msg.'");';	
			}
		?>
		
    });	
</script>

</head>
<body>
<div id="header">
    <span class="label-inicio"></span>
</div>
<div id="global-container">
	<span id="titulo-login">Painel de Administração</span>
	<div id="box-login">
        <form name="login" method="post" action="index.php" onsubmit="return valida_login();">
        <input type="hidden" name="action" id="action" value="dologin" />
            <span class="label-login">Unidade</span>
            <input name="login" type="text" class="campos-login" id="login" value="<?php echo $login;?>" />
            <span class="label-login">Senha</span>
            <input type="password" class="campos-login" name="senha" id="senha" />
          <input type="checkbox" value="-1" id="lembrar-login" name="lembrar-login" <?php echo $lembrar_login==-1 ? " checked" : ""; ?> /><span id="lembrar-dados">Lembrar</span>
          <input type="submit" id="btn-login" name="btn-login" value="Login" />
        </form>
	</div>
</div>
</body>
</html>