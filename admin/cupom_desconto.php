<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}

	$id = $_REQUEST["id"];	
	if(!is_numeric($id)){
		$id = 0;	
	}
	

	/*-----------------------------------------------------------------------
	ajax de excluisão
	------------------------------------------------------------------------*/
	if($_POST && $_REQUEST["action"]=="excluir"){
		
		$ssql = "select orcamentoid from tblorcamento where ocodcupom = '{$id}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)>0){
				echo "O cupom não pode ser excluido pois existem orçamentos/pedidos associados ao cupom.";
				exit();
			}	
		}
		
		
		$ssql = "select pedidoid from tblpedido where pcodcupom = '{$id}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)>0){
				echo "O cupom não pode ser excluido pois existem pedidos associados ao cupom.";
				exit();
			}	
		}
		
		$ssql = "delete from tblcupom_desconto where cupomid='{$id}'";
		mysql_query($ssql);
		echo "ok";
		exit();
		
	}


	function validaDados(){
		
		
	}


	if($_POST && $_REQUEST["action"]=="gravar"){
		$id					= 	addslashes($_REQUEST["id"]);
		
		$cupom				=	addslashes($_REQUEST["cupom"]);
		
		$descricao          = 	addslashes($_REQUEST["descricao"]);
		
		$tipo_cupom         = addslashes($_REQUEST["tipo_cupom"]);
		
		if($tipo_cupom == 1){
			$quantidade_geral         = addslashes($_REQUEST["quantidade_geral"]);
			$quantidade_geral_cliente = addslashes($_REQUEST["quantidade_geral_cliente"]);
		}else{
			$email				=  $_POST["email"];
			$quantidade         =  $_POST["quantidade"];
		}
		$desconto_por 		=  addslashes($_REQUEST["desconto_por"]);
		$categoria			=  $_REQUEST["categoria"];
		$produto            =  $_REQUEST["id-produto"];
		
		$data_validade		=	addslashes($_REQUEST["data_validade"]);
		$data_validade		=	formata_data_db($data_validade);
		
		//$ccodtipo			=	intval($_REQUEST["tipo"]);
		
		$desconto			=	addslashes($_REQUEST["desconto"]);
		$desconto			=	formata_valor_db($desconto);
		
		$status				=	intval($_REQUEST["status"]);
		
		$ccodtipo 			= 	intval($_REQUEST["ccodtipo"]);
		
		$filtro				=	intval($_REQUEST["filtro"]);

		$produtoDesconto	=	intval($_REQUEST["produtoDesconto"]);
		
		//$tipo 				= 1;
		/*
		if($qtde>1){
			$tipo = 2;	
		}
		*/

		if($id==0){
			//verifica se nao tem ja o desconto cadastrado			
			$ssql = "select cupomid from tblcupom_desconto where ccupom='{$cupom}' and ccodtipo='{$ccodtipo}' and cdesconto='{$desconto}' and ccodproduto='{$produto}'";
			$result = mysql_query($ssql);
			if($result){
				$num_rows = mysql_num_rows($result);
				if($num_rows>0){
					$msg = "Já existe um registro com os mesmos dados digitados";
					$erro = 1;
				}
			}
			
			if($num_rows==0){
						
					$ssql  = " INSERT INTO ";
					$ssql .= " tblcupom_desconto(ccupom, cdata_validade, cdesconto, desconto_por, tipo_cupom, ccodtipo, cstatus, descricao, quantidade_geral, quantidade_geral_cliente)";
					$ssql .= " VALUES('{$cupom}', '{$data_validade}', '{$desconto}', '{$desconto_por}', '{$tipo_cupom}', '{$ccodtipo}', '{$status}', '{$descricao}', '{$quantidade_geral}', '{$quantidade_geral_cliente}')";
					mysql_query($ssql);
					
					$ultimo = mysql_insert_id();
					if($tipo_cupom == 2){
						$cnt = count($email);
						for($i=0;$i<$cnt;$i++){
							$email_gravar = $email[$i];
							$quantidade_gravar = $quantidade[$i];
							if(strlen($email_gravar) > 0){
								mysql_query("INSERT INTO tblcupomdesconto_usuario(idcupom, email, quantidade) VALUES({$ultimo},'{$email_gravar}','{$quantidade_gravar}')");
							}
						}
					}

					if($ccodtipo == 3){
						mysql_query("INSERT INTO produtogratuito (idCupom, idProduto) values ('".$ultimo."', '".$produtoDesconto."')");
					}
					
					if($desconto_por == 2){
						foreach($categoria as $c){
							mysql_query("INSERT INTO tblcupomdesconto_categoria(idcupom, idcategoria) VALUES('".$ultimo."', '".$c."')");
						}
					}elseif($desconto_por == 3){
						foreach($produto as $p){
							if(strlen($p) > 0){
								mysql_query("INSERT INTO tblcupomdesconto_produto(idcupom, idproduto) VALUES('".$ultimo."', '".$p."')");
							}
						}
					}
					
					
					if(!$result){
						$msg = "Erro ao gravar registro";
						$erro = 1;
					}
					else
					{
						$qr = mysql_query("SELECT crazao_social FROM tblcadastro WHERE cemail = '".$email."'");
						while($ln = mysql_fetch_assoc($qr)){
							$nome_email = $ln["crazao_social"];
						}
					
						$subject = "Cupom de Desconto ".utf8_encode($site_nome);
						$body = '<table width="800" border="0" cellspacing="0" cellpadding="0" style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#444;">
									  <tr>
										<td><a href="'.$site_site.'"><img src="'.$site_site.'/images/logo.png" alt="$site_nome" border="0" /></a></td>
									  </tr>
									  <tr>
										<td colspan="2">&nbsp;</td>
									  </tr>
									  <tr>
										<td colspan="2"><p>Olá '.$nome_email.', segue abaixo o nome do seu cupom de desconto.</p>
										<p>Cupom: '.$cupom.'</p>
										<p>&nbsp;</p>
										<p>&nbsp;</p>
										<p>Atenciosamente,</p>
										<p>&nbsp;</p>
										<p>Equipe '.$site_nome.'<br>
										'.$site_site.'
										</p>
										<p>&nbsp;</p></td>
									  </tr>
									  <tr>
										<td colspan="2">&nbsp;</td>
									  </tr>
								</table>';
						
						$from_name	= $site_nome;
						$from_email	= "";
						$to_name	= $nome_email;
						$to_email	= $email;
						
						envia_email($from_name, $from_email, $to_name, $to_email, $subject, $body);
						$id = mysql_insert_id();
						$msg = "Registro salvo com sucesso";
						
						
					}
					
					$id					= 	"";
		
					$cupom				=	"";
					
					$descricao          = 	"";
					
					$tipo_cupom         =   "";
					
					
					$quantidade_geral         = "";
					$quantidade_geral_cliente = "";
					
					$email				=  "";
					$quantidade         =  "";
					                       "";
					$desconto_por 		=  "";
					$categoria			=  "";
					$produto            =  "";
					                       "";
					$data_validade		=  "";
					$data_validade		=  "";
					                         
					$desconto			=  "";
					$desconto			=  "";
					                         
					$status				=  "";
					                         
					$ccodtipo 			=  "";
					                         
					$filtro				=  "";
				
			}
			
			
		}
		else
		{
		
			$ssql = "update tblcupom_desconto set ctitulo='{$titulo}', cemail='{$email}', ccodtipo='{$ccodtipo}', ccodfiltro='{$filtro}', cdesconto='{$desconto}', cstatus='{$status}', 
					cdata_validade='{$data_validade}', ccodcategoria='{$categoria}', cvalor='{$valor}', ccodproduto='{$produto}', cminimo = '0',					
					ccodusuario='{$usuario}', cdata_alteracao='{$data_hoje}', desconto_por='{$desconto_por}', tipo_cupom='{$tipo_cupom}', quantidade_geral='{$quantidade_geral}', quantidade_geral_cliente='{$quantidade_geral_cliente}'
					where cupomid='{$id}'";
			$result = mysql_query($ssql);
			
			if($tipo_cupom == 2){
				mysql_query("DELETE FROM tblcupomdesconto_usuario WHERE idcupom = ".$id);
				$cnt = count($email);
				for($i=0;$i<$cnt;$i++){
					$email_gravar = $email[$i];
					$quantidade_gravar = $quantidade[$i];
					
					mysql_query("INSERT INTO tblcupomdesconto_usuario(idcupom, email, quantidade) VALUES({$id},'{$email_gravar}','{$quantidade_gravar}')");
				}
			}
			
			if($desconto_por == 2){
				mysql_query("DELETE FROM tblcupomdesconto_categoria WHERE idcupom = ".$id);
				foreach($categoria as $c){
					mysql_query("INSERT INTO tblcupomdesconto_categoria(idcupom, idcategoria) VALUES('".$id."', '".$c."')");
				}
			}elseif($desconto_por == 3){
				mysql_query("DELETE FROM tblcupomdesconto_produto WHERE idcupom = ".$id);
				foreach($produto as $p){
					mysql_query("INSERT INTO tblcupomdesconto_produto(idcupom, idproduto) VALUES('".$id."', '".$p."')");
				}
			}
			
			if($result){
				$msg = "Registro atualizado com sucesso.";	
			}else{
				$msg = "Erro ao atualizar registro.";	
			}		

		}

	
	}
	
	
	//echo $ssql;



	if($id>0){
	
		$ssql = "select tblcupom_desconto.quantidade_geral, tblcupom_desconto.quantidade_geral_cliente, tblcupom_desconto.desconto_por, tblcupom_desconto.tipo_cupom, tblcupom_desconto.descricao, tblcupom_desconto.cupomid, tblcupom_desconto.ctitulo, tblcupom_desconto.ccupom, tblcupom_desconto.cemail, tblcupom_desconto.ccodtipo, 
				tblcupom_desconto.ccodfiltro, tblcupom_desconto.cdesconto, tblcupom_desconto.cstatus, tblcupom_desconto.cdata_validade, 
				tblcupom_desconto.ccodcategoria, tblcupom_desconto.ccodproduto, tblcupom_desconto.cminimo, tblcupom_desconto.cvalor, 
				tblcupom_desconto.ccodusuario, tblcupom_desconto.cdata_alteracao, tblcupom_desconto.cdata_cadastro,
				tblproduto.pcodigo, tblproduto.pproduto
				from tblcupom_desconto 
				left join tblproduto on tblcupom_desconto.ccodproduto = tblproduto.produtoid
				where tblcupom_desconto.cupomid='{$id}'
				";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				
				$cupom					=	$row["ccupom"];
				
				
				$quantidade_geral       =   $row["quantidade_geral"];
				$quantidade_geral_cliente =   $row["quantidade_geral_cliente"];
				
				$email					=	$row["cemail"];
				
				$ccodtipo					=	$row["ccodtipo"];
				$filtro					=	$row["ccodfiltro"];
				$desconto				=	number_format($row["cdesconto"],2,",",".");
				$status					=	$row["cstatus"];
				$data_validade			=	formata_data_tela($row["cdata_validade"]);
				$categoria				=	$row["ccodcategoria"];
				$produto				=	$row["ccodproduto"];
				
				$tipo_cupom				= 	$row["tipo_cupom"];
				$desconto_por 			=   $row["desconto_por"];
				
				$descricao				=  	$row["descricao"];
				
				$valor					=	number_format($row["cvalor"],2,",",".");
				
				$codigo					=	$row["pcodigo"];
				$produto_descricao		=	$row["pproduto"];
								
				$minimo					=	number_format($row["cminimo"],2,",",".");
				
				$usuario			=	get_usuario($row["ccodusuario"],"unome");		
				$data_alteracao		=	formata_data_tela($row["cdata_alteracao"]);
				$data_cadastro		=	formata_data_tela($row["cdata_cadastro"]);
				
			}
			mysql_free_result($result);
		}	
		
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - desconto Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - desconto Virtual" />
<meta name="description" content="Painel de administração da desconto virtual" />
<meta name="keywords" content="desconto virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {							   		
		$("#titulo").focus();
		$("#data_validade").mask("99/99/9999 99:99:99");
		//box geral
		$("#geral-check").click(function(){
			$(".box-body-quantidade-geral").slideDown("slow");
			$(".box-body-pessoa").slideUp("slow");
		});
		
		//box individual
		$("#individual-check").click(function(){
			$(".box-body-pessoa").slideDown("slow");
			$(".box-body-quantidade-geral").slideUp("slow");
		});	

		$("#tipo").on('change', function(){

			if($(this).val() == 3){
				$("#produto-desconto").slideDown("slow");
				$("#valor-desconto").slideUp("slow");
			}else{
				$("#produto-desconto").slideUp("slow");
				$("#valor-desconto").slideDown("slow");
			}
		});	
		
		//box categoria
		$("#categoria-check").click(function(){
			$(".box-body-categoria").slideDown("slow");
			$(".box-body-produto").slideUp("slow");
		});		

		//box produto
		$("#produto-check").click(function(){
			$(".box-body-produto").slideDown("slow");
			$(".box-body-categoria").slideUp("slow");
		});	
		$("#adicionar-produto").click(function(){
			$(this).parents("#produto-individual").prepend('<div class="produto-content"><p><label>ID do Produto:</label><input type="text" name="id-produto" value="" class="id-produto-busca" /></div><div class="nome-produto-content"><label class="nome-produto-label">Nome do Produto:</label><input type="text" name="nome_produto" value="" class="nome-produto-busca" /><div class="autocomplete" id="autocomplete-produto-box"><ul class="autocomplete-list"></ul></div></div>');
		});
		$("#geral-check-desconto-por").click(function(){
			$(".box-body-produto").slideUp("slow");
			$(".box-body-categoria").slideUp("slow");
		});
		// $(".tipo_cupom").click(function(){
		// 	$(".box-tipo-cupom").stop().slideUp();
		// 	if($(this).val() == "1"){
		// 		$("#cupom_geral").stop().slideToggle();
		// 	}else if($(this).val() == "2"){
		// 		$("#cupom_individual").stop().slideDown();
		// 	}
		// });

		$("#adicionar-email").click(function(){
			$(this).parents("#pessoa-individual").prepend('<div id="email-content"><p><label>Email:</label><input type="text" name="email[]" value="" class="email-cupom" id="email-bar" autocomplete=\"off\" /></p><div class="autocomplete" id="autocomplete-geral-box"><ul class="autocomplete-list"><li class="autocomplete-item">Lorem Ipsum</li><li class="autocomplete-item">Lorem Ipsum</li><li class="autocomplete-item">Lorem Ipsum</li><li class="autocomplete-item">Lorem Ipsum</li><li class="autocomplete-item">Lorem Ipsum</li><li class="autocomplete-item">Lorem Ipsum</li><li class="autocomplete-item">Lorem Ipsum</li><li class="autocomplete-item">Lorem Ipsum</li><li class="autocomplete-item">Lorem Ipsum</li><li class="autocomplete-item">Lorem Ipsum</li><li class="autocomplete-item">Lorem Ipsum</li><li class="autocomplete-item">Lorem Ipsum</li><li class="autocomplete-item">Lorem Ipsum</li><li class="autocomplete-item">Lorem Ipsum</li><li class="autocomplete-item">Lorem Ipsum</li></ul></div></div><div id="quantidade-pessoas-content"><label class="quantidade-pessoas-label">Quantidade:</label><input type="text" id="quantidade-bar" name="quantidade[]" value="" /></p></div>');
		});
		// $(".desconto_por").click(function(){
		// 	$(".box-desconto-cupom").stop().slideUp();
		// 	if($(this).val() == "2"){
		// 		$("#desconto_categoria").stop().slideToggle();
		// 	}else if($(this).val() == "3"){
		// 		$("").stop().slideToggle();
		// 	}
		// });
		$(".email-cupom").live("keyup", function(){
			var email = $(this);
			$.ajax({
				url: "ajax_busca_email.php",
				type: "post",
				data: {email: $(email).val()},
				success: function(retorno){
					$(email).parents("p").siblings(".autocomplete").find(".autocomplete-list").html(retorno);
					$(email).parents("p").siblings(".autocomplete").slideDown();
				}
			});
		});

		$(window).click(function(){
			$(".autocomplete").slideUp();
		});
		
		$(".autocomplete-item").live("click", function(){
			$(this).parents(".autocomplete").siblings("p").find(".email-cupom").val($(this).html());
			$(".autocomplete").slideUp();
		});
		
		$(".autocomplete-item-produto").live("click", function(){
			$(this).parents(".autocomplete").siblings(".nome-produto-busca").val($(this).html());
			$(this).parents(".nome-produto-content").prev(".produto-content").find(".id-produto-busca").val($(this).attr("id"));
		});
		
		
		
		$(".nome-produto-busca").blur(function(){
			$(this).siblings(".autocomplete").slideUp();
		});
		
		$(".id-produto-busca").live("blur", function(){
			var idproduto = $(this);
			$.ajax({
				url: "ajax_busca_produto.php",
				type: "post",
				data: {action: "buscaid", idproduto: $(idproduto).val()},
				success: function(retorno){
					$(idproduto).parents(".produto-content").next(".nome-produto-content").find(".nome-produto-busca").val(retorno);
				}
			});
		});
		
		$(".nome-produto-busca").live("keyup", function(){
			var produto = $(this);
			$.ajax({
				url: "ajax_busca_produto.php",
				type: "post",
				data: {action: "buscanome", produto: $(produto).val()},
				success: function(retorno){
					$(produto).siblings(".autocomplete").find(".autocomplete-list").html(retorno);
					$(produto).siblings(".autocomplete").slideDown();
					$(produto).parents("#nome-produto-content").siblings("#produto-content");
				}
			});
		});
		
	});
	function validaCupom(){
		
		var verifica_email = false;
		var verifica_categoria = false;
		var verifica_produto = false;
		
		var tipo_cupom = "";
		var desconto_por = "";
		var cupom = $("#cupom").val();
		var validade = $("#data_validade").val();
		var tipo_desconto = $("#tipo").val();
		var desconto = $("#desconto").val();
		var status = $("#status").val();
		
		if(cupom.length == 0){
			alert("Preencha o campo Cupom.");
			$("#cupom").focus();
			return false;
		}
		
		$(".tipo_cupom").each(function(){
			if($(this).is(":checked")){
				tipo_cupom = $(this).val();
			}
		});
		
		$(".desconto_por").each(function(){
			if($(this).is(":checked")){
				desconto_por = $(this).val();
			}
		});
		
		if(tipo_cupom.length > 0){
			if(tipo_cupom == 2){
				$(".email-cupom").each(function(){
					if($(this).val().length > 0){
						verifica_email = true;
					}
				});
				if(!verifica_email){
					alert("Informe pelo menos um e-mail");
					return false;
				}
			}else if(tipo_cupom == 1){
				if($("#quantidade-bar").val().length == 0){
					alert("Informe a quantidade");
					return false;
				}
			}
		}else{
			alert("Escolha o tipo do Cupom");
			return false;
		}
		
		if(desconto_por.length > 0){
			if(desconto_por == 2){
				$(".categoria").each(function(){
					if($(this).is(":checked")){
						verifica_categoria = true;
					}
				});
				if(!verifica_categoria){
					alert("Informe pelo menos uma categoria.");
					return false;
				}
			}else if(desconto_por == 3){
				$(".id-produto-busca").each(function(){
					if($(this).val().length > 0){
						verifica_produto = true;
					}
				});
				if(!verifica_produto){
					alert("Informe pelo menos um produto");
					return false;
				}
			}
		}else{
			alert("Escolha como o desconto será aplicado.");
			return false;
		}
		
		if(tipo_desconto.length == 0){
			alert("Preencha o campo tipo de desconto.");
			$("#tipo").focus();
			return false;
		}
		
		if(desconto.length == 0){
			alert("Preencha o valor de desconto.");
			$("#desconto").focus();
			return false;
		}
		
		if(status.length == 0){
			alert("Preencha o campo Status.");
			$("#status").focus();
			return false;
		}
		
		return true;
	}
</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>
<style>
		*{color:#000;}
		.formulario{margin-top: 15px;}
		#conteudo-interno{padding: 10px;}
		
		/*alinhamento labels*/
		#tipo-cupom-align{margin-top: 3px;}
		#desconto-por-align{margin-top: 4px;}
		#expira-align, #tipo-desconto-align{margin-top: 17px;}
		#valor-align, #status-align{margin-top: 16px;}
		/**/	
		.linha_form{
			width: 825px;
			margin-top: 10px;
		}
		.label_form{
			width: 200px;
			float: left;
			position: relative;
			margin-left: 0px;
		}
		.campo_form{
			position: relative;
			margin-left: 200px;
		}
		.box-tipo-cupom, .box-desconto-cupom{
			width: auto;
			margin-left: 200px;
			display: none;
		}
		textarea{
			resize: none;
			width: 610px;
		}
		#cupom{
			border: 1px solid #CCC;
			width: 613px;
			margin-top: 0px;
		}
		.autocomplete{
			float: left;
			border: 1px solid lightgray;
			background: #FFF;
			position: absolute;
			z-index: 9999;
			margin-top: 0;
			
		}
		.autocomplete-list{
			position: relative;
			z-index: 9999;
			list-style: none;
			overflow-x: hidden;
			overflow-y: auto;
			margin-top: 0;
			margin-left: -39px;
		}
		.autocomplete-list:nth-child(2){
			position: relative;
			z-index: 9999;
			list-style: none;
			margin-top: 0;
			margin-left: -39px;
			width: 170px;
		}
		.autocomplete-item{
			position: relative;
			border-bottom: 1px solid #D4D4D4;
			padding: 5px;
			cursor: pointer;
			z-index: 9999;
		}
		.autocomplete-item-produto{
			position: relative;
			border-bottom: 1px solid #D4D4D4;
			padding: 5px;
			cursor: pointer;
			z-index: 9999;
		}
		.autocomplete-item:hover{background: #e3f7ff;}
		.autocomplete-item:nth-last-child(1){border-bottom: none;}
/*box geral*/
		.box-body-quantidade-geral{
			width: 73%;
			min-width: 73%;
			float: left;
			display: none;
			/*border: 1px solid red;	*/
		}
		#quantidade-geral{
			border: 1px solid #CCC;
			margin-left: 200px;
			padding: 0px 0px 11px 10px;
		}		
		.quantidade-cliente-label{margin-left: 40px;}
		#quantidade-cliente-bar{
			width: 138px;
			margin-left: 10px;
			position: relative;
			z-index: 5555;
		}

		#autocomplete-geral-box{
			margin: -14px 0px 0px 43px;
			display: none;
		}
/*box individual*/
		.box-body-pessoa{
			width: 73%;
			min-width: 73%;
			float: left;
			display: none;
			/*border: 1px solid red;	*/
		}
		#pessoa-individual{
			border: 1px solid #CCC;
			margin-left: 200px;
			padding: 0px 0px 11px 10px;
		}
		#email-content{
			width: 220px;
			height: 50px;
			float: left;
			/*border: 1px solid red;*/
		}
		#email-bar{
			margin-left: 10px;
			position: relative;
			z-index: 5555;
		}
		#quantidade-pessoas-content{
			width: 280px;
			height: 50px;
			float: left;
			position: relative;
			z-index: 1111;
			margin-left: 85px;
			padding-top: 10px;
			/*border: 1px solid blue;*/
		}
		#quantidade-bar{
			width: 190px;
			margin-left: 10px;
			position: relative;
			z-index: 5555;
		}
		#add-email-content{
			float: left;
			position: relative;
			z-index: 1111;
			margin-top: 5px;
			/*border: 1px solid blue;*/
		}
		
		#autocomplete-produto-box{
			margin: -3px 3px 0px 104px;
			display: none;
			width: 171px;
		}

		.autocomplete-item-produto:hover{background: #e3f7ff;}
		.autocomplete-item-produto:nth-last-child(1){border-bottom: none;}
/*Categorias e subcategorias*/
		#box-container{
			min-width: 98%;
			width: 98%;
			height: auto;
			margin-top: 10px;
			float: left;
		}
		.box-body-categoria{
			height: auto;
			border: 1px solid #CCC;
			float: left;
			margin-left: 7px;
			display: none;
			padding-right: 10px;
		}
		.box-body-categoria:nth-child(1){margin-left: -1px;}
		ul{
			list-style: none;
			float: left;
			width: 99%;
			min-width: 99%;
			margin-left: -30px;
		}
		.line-subcategoria,{
			width: 100%;
			display: inline-block;
			float: left;
			margin-left: 6px !important;
		}

/*Produto*/
		.box-body-produto{
			width: 100%;
			display: none;
		}
		.box-tipo-produto{
			width: 37%;
			min-width: 37%;
			float: left;
			/*border: 1px solid red;*/
		}
		#produto-individual{
			border: 1px solid #CCC;
			margin-left: 200px;
			padding: 0px 0px 11px 10px;
		}
		#adicionar-produto{
			position: relative;
			z-index: 1111;
		}
		/*apenas para alinhamento das labels*/
		.id-produto-busca,.nome-produto-busca{
			margin-left: 7px;
			position: relative;
			z-index: 1111;
		}

		.produto-content{
			width: 260px;
			height: 50px;
			float: left;
			/*border: 1px solid red;*/
		}
		.nome-produto-content{
			width: 280px;
			height: 50px;
			float: left;
			margin-left: 300px;
			padding-top: 10px;
			margin-top: -53px;
			/*border: 1px solid blue;*/
		}
		#add-produto-content{
			width: 150px;
			height: 15px;
			float: left;
			/*border: 1px solid red;*/
		}

	</style>
<div id="geral">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content"> 
		<form method="post" name="frm_desconto" id="frm_desconto" action="" onsubmit="return validaCupom();">
			<input name="action" id="action" type="hidden" value="gravar" />  
			<input name="id" id="id" type="hidden" value="<?php echo $id?>" />
			<div id="conteudo">
				<div id="titulo-conteudo">
					<span class="label-inicio">Cupom de Desconto &raquo;  <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='cupom_desconto.php';"><?php echo ($id==0) ? "Novo Registro" : $titulo;  ?></span> </span> <a href="cupom_desconto_consulta.php">Consulta</a>
				</div>
				<div id="conteudo-interno">
					<?php echo $msg;?>
					<div class="linha_form">
						<div class="label_form"><label for="Código do Cupom">Código do Cupom</label></div>
						<div class="campo_form"><input name="cupom" type="text" class="formulario" id="cupom" value="<?php echo $cupom;?>" size="75" maxlength="100" /></div>
					</div>
					
					<div class="linha_form">
						<div class="label_form"><label for="Descrição">Descrição</label></div>
						<div class="campo_form"><textarea name="descricao" rows="5" cols="76"><?php echo $descricao;?></textarea></div>
					</div>
					
					<div class="linha_form" >
						<div class="label_form"><label for="Tipo de Cupom">Tipo de Cupom</label></div>
						<div class="campo_form">
							<input name="tipo_cupom" type="radio" class="tipo_cupom" id="geral-check" value="1" <?php echo ($tipo_cupom == 1) ? "checked=\"checked\"" : "";?>/>Geral
							<input name="tipo_cupom" type="radio" class="tipo_cupom" id="individual-check" value="2" <?php echo ($tipo_cupom == 2) ? "checked=\"checked\"" : "";?>/>Individual
						</div>
					</div>
					
					
					<div class="linha_form">
						<div class="linha_box">
							<div id="quantidade-geral" class="box-body-quantidade-geral" style="<?php echo ($tipo_cupom == 1) ? "display: block;" : "";?>">
								<p><label>Quantidade:</label><input type="text" name="quantidade_geral" value="<?php echo $quantidade_geral; ?>" id="quantidade-bar" /><label class="quantidade-cliente-label">Quantidade por cliente:</label><input type="text" id="quantidade-cliente-bar" name="quantidade_geral_cliente" value="<?php echo $quantidade_geral_cliente;?>" /></p>
							</div>
							<div id="pessoa-individual" class="box-body-pessoa" style="<?php echo ($tipo_cupom == 2) ? "display: block;" : "";?>">
								<?php
									if($tipo_cupom == 2){
										$qr_user = mysql_query("SELECT * FROM tblcupomdesconto_usuario WHERE idcupom = ".$id);
										while($ln = mysql_fetch_assoc($qr_user)){
								?>
											<div id="email-content"> 	
												<p><label>Email:</label><input type="text" name="email[]" value="<?php echo $ln["email"];?>" class="email-cupom" id="email-bar" autocomplete="off" /></p>
												<div class="autocomplete" id="autocomplete-geral-box">
													<ul class="autocomplete-list">
														
													</ul>
												</div>
											</div>
											<div id="quantidade-pessoas-content">
												<label class="quantidade-pessoas-label">Quantidade:</label><input type="text" id="quantidade-bar" name="quantidade[]" value="<?php echo $ln["quantidade"];?>" /></p>
											</div>
								<?php 
										}
								?>
											<div id="add-email-content">
												<a id="adicionar-email" style="cursor: pointer;">[+] Adicionar mais pessoas</a>
											</div>
								<?php
									}else{
								?>
										<div id="email-content"> 	
											<p><label>Email:</label><input type="text" name="email[]" value="" class="email-cupom" id="email-bar" autocomplete="off" /></p>
											<div class="autocomplete" id="autocomplete-geral-box">
												<ul class="autocomplete-list">
													
												</ul>
											</div>
										</div>
										<div id="quantidade-pessoas-content">
											<label class="quantidade-pessoas-label">Quantidade:</label><input type="text" id="quantidade-bar" name="quantidade[]" value="" /></p>
										</div>
										<div id="add-email-content">
											<a id="adicionar-email" style="cursor: pointer;">[+] Adicionar mais pessoas</a>
										</div>
								<?php
									}
								?>
							</div>
<!-- 							<div id="cupom_individual" class="box-tipo-cupom">	
									<p><label>Email:</label><input type="text" name="email[]" value="" class="email_cupom" />
									<label>Quantidade:</label><input type="text" name="quantidade[]" value="" /></p>
									<a id="adicionar_email" style="cursor: pointer;">[+] Adicionar mais pessoas</a> <br />
							</div> -->
						</div>
					</div>
					
					<div id="box-expira-to-end">
						<div class="linha_form">
							<div class="label_form" id="expira-align"><label for="Expira em">Expira em:</label></div>
							<div class="campo_form"><input name="data_validade" type="text" class="formulario" id="data_validade" value="<?php echo $data_validade;?>" size="18" maxlength="20" /> dd/mm/aaaa hh:mm:ss</div>
						</div>
					</div>
					
					<div class="linha_form">
						<div class="label_form"><label for="Desconto por">Desconto Em:</label></div>
						<div class="campo_form">
							<input name="desconto_por" type="radio" class="desconto_por" id="geral-check-desconto-por" value="1" <?php echo ($desconto_por == 1) ? "checked=\"checked\"" : "";?>/>Todos os Produtos
							<input name="desconto_por" type="radio" class="desconto_por" id="categoria-check" value="2" <?php echo ($desconto_por == 2) ? "checked=\"checked\"" : "";?>/>Categoria
							<input name="desconto_por" type="radio" class="desconto_por" id="produto-check" value="3" <?php echo ($desconto_por == 3) ? "checked=\"checked\"" : "";?>/>Produto
						</div>
					</div>
						<!-- Desconto por Categoria -->
						<div id="box-container">
						<?php
							if($id > 0){
								$qr_categoria = mysql_query("SELECT * FROM tblcupomdesconto_categoria WHERE idcupom = ".$id);
								$array_categoria = array();
								while($ln=mysql_fetch_assoc($qr_categoria)){
									$array_categoria[] = $ln["idcategoria"];
								}
							}
							
							$qr = mysql_query("SELECT * FROM tblcategoria WHERE ccodcategoria = 0");
							while($ln = mysql_fetch_assoc($qr)){
						?>
								
									<div class="box-body-categoria" style="<?php echo ($desconto_por == 2) ? "display: block;" : "";?>">
										<ul>
											<li class="line-categoria"><input type="checkbox" class="categoria" name="categoria[]" value="<?php echo $ln["categoriaid"];?>" <?php echo (in_array($ln["categoriaid"], $array_categoria)) ? "checked=\"checked\"" : ""; ?>><?php echo $ln["ccategoria"];?><br></li>
											<?php
												$qr2 = mysql_query("SELECT * FROM tblcategoria WHERE ccodcategoria = ".$ln["categoriaid"]);
												if(mysql_num_rows($qr2) > 0){
													echo "<ul>";
													while($ln2 = mysql_fetch_assoc($qr2)){
											?>
														<li class="line-subcategoria"><input type="checkbox" class="categoria" name="categoria[]" value="<?php echo $ln2["categoriaid"]?>" <?php echo (in_array($ln2["categoriaid"], $array_categoria)) ? "checked=\"checked\"" : ""; ?>><?php echo $ln2["ccategoria"]?><br></li>
											<?php
														 $qr3 = mysql_query("SELECT * FROM tblcategoria WHERE ccodcategoria = ".$ln2["categoriaid"]);
														 if(mysql_num_rows($qr3) > 0){
															echo "<ul>";
															while($ln3 = mysql_fetch_assoc($qr3)){
											?>
																<li class="line-subcategoria"><input type="checkbox" class="categoria" name="categoria[]" value="<?php echo $ln3["categoriaid"]?>" <?php echo (in_array($ln3["categoriaid"], $array_categoria)) ? "checked=\"checked\"" : ""; ?>><?php echo $ln3["ccategoria"]?><br></li>
											<?php
															}
															echo "</ul>";
														 }
													}
													echo "</ul>";
												}
											?>
										</ul>				
									</div>								
								
						<?php
							}
						?>
						</div>
						<!-- Fim Desconto por Categoria -->
						
						<!-- Desconto por Produto -->
						<div id="box-container">
							<div class="box-body-produto" style="<?php echo ($desconto_por == 3) ? "display:block" : "";?>">
								<div id="produto-individual" class="box-tipo-produto">
									<?php
										if($desconto_por == 3){
											$qr_produto = mysql_query("SELECT * FROM tblcupomdesconto_produto INNER JOIN tblproduto ON tblcupomdesconto_produto.idproduto = tblproduto.produtoid WHERE idcupom = ".$id);
											while($ln = mysql_fetch_assoc($qr_produto)){
									?>
												<div class="produto-content"> 
													<p><label>ID do Produto:</label><input type="text" name="id-produto[]" value="<?php echo $ln["idproduto"];?>" class="id-produto-busca" />
												</div>
												<div class="nome-produto-content">	
													<label class="nome-produto-label">Nome do Produto:</label><input type="text" name="nome_produto[]" value="<?php echo $ln["pproduto"];?>" class="nome-produto-busca" autocomplete="off" />
													<div class="autocomplete" id="autocomplete-produto-box">
														<ul class="autocomplete-list">
				
														</ul>
													</div>
												</div>
									<?php
											}
									?>
												<div id="add-produto-content">	
													<a id="adicionar-produto" style="cursor: pointer;">[+] Adicionar mais produtos</a>
												</div>
									<?php
										}else{
									?>
										<div class="produto-content"> 
											<p><label>ID do Produto:</label><input type="text" name="id-produto[]" value="" class="id-produto-busca" />
										</div>
										<div class="nome-produto-content">	
											<label class="nome-produto-label">Nome do Produto:</label><input type="text" name="nome_produto[]" value="" class="nome-produto-busca" />
											<div class="autocomplete" id="autocomplete-produto-box">
												<ul class="autocomplete-list">
		
												</ul>
											</div>
										</div>
										<div id="add-produto-content">	
											<a id="adicionar-produto" style="cursor: pointer;">[+] Adicionar mais produtos</a>
										</div>
									<?php
										}
									?>
								</div>
						</div>
						<!-- Fim Desconto por produto -->			
					<div class="linha_form">
						<div class="linha_box">

							<div id="desconto_categoria" class="box-desconto-cupom" style="width: 1000px;">
								<?php
									/*
									$qr = mysql_query("SELECT * FROM tblcategoria WHERE ccodcategoria = 0");
									while($ln=mysql_fetch_assoc($qr)){
										echo '<ul style="list-style: none; float: left;">';
										echo '<li><input type="checkbox" name="categoria" value="{$ln["categoriaid"]}" />'.$ln["ccategoria"]."</li>";
										echo '</ul>';
									}
									*/
								?>
							</div>
							<div id="cupom_individual" class="box-tipo-cupom">
								
							</div>
						</div>
					</div>
					<div class="linha_form">
						<div class="label_form" id="tipo-desconto-align"><label for="Tipo de Desconto">Tipo de Desconto:</label></div>
						<div class="campo_form">
							<select name="ccodtipo" size="1" id="tipo" class="formulario">
							  <option value="0">Selecione</option>
							  <option value="1" <?php if($ccodtipo==1)echo "selected";?>>Percentual</option>
							  <option value="2" <?php if($ccodtipo==2)echo "selected";?>>Valor Real</option>
							  <option value="3" <?php if($ccodtipo==3)echo "selected";?>>Produto</option>
							</select>
						</div>
					</div>
					<div class="linha_form" id="valor-desconto">
						<div class="label_form" id="valor-align"><label for="Valor de Desconto">Valor de Desconto:</label></div>
						<div class="campo_form">
							<input name="desconto" type="text" class="formulario" id="desconto" value="<?php echo $desconto;?>" size="20" maxlength="100" />
						</div>
					</div>

					<div class="linha_form" id="produto-desconto" style="display:none;">
						<div class="label_form" id="valor-align"><label for="Valor de Desconto">Produto gratuíto:</label></div>
						<div class="campo_form">
							<select name="produtoDesconto" id="">
								<option>Selecione</option>
								<?php 
									$sql = mysql_query("SELECT produtoid, pproduto FROM tblproduto");

									while($prod = mysql_fetch_assoc($sql)){
								?>
										<option value="<?php echo $prod["produtoid"]; ?>"><?php echo $prod["pproduto"]; ?></option>
								<?php 
									}
								?>
							</select>
						</div>
					</div>

					<div class="linha_form">
						<div class="label_form" id="status-align"><label for="status">Status:</label></div>
						<div class="campo_form">
							<select name="status" size="1" id="status" class="formulario">
							  <option value="0" <?php if($status==0)echo "selected";?>>Selecione</option>
							  <option value="-1" <?php if($status==-1)echo "selected";?>>Ativo</option>
							  <option value="1" <?php if($status==1)echo "selected";?>>Utilizado</option>
							</select>
						</div>
					</div>
					<div class="linha_form">
						<input type="submit" name="gravar" value="Gravar" class="btn-gravar" />
					</div>
				</div>
			</div>
		</form>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>