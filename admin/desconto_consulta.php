<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$uri = str_replace("/videokestore/","",$_SERVER['REQUEST_URI']);	

	$pagina = 1;
	$start = 0;
	$limit = 20;

	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$('#string').focus();
	});
  

</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    

    	<div id="conteudo">
			<div id="titulo-consulta">
            	<span class="label-inicio">Desconto &raquo; <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='desconto_consulta.php';">Consulta</span></span> <a href="desconto.php">Novo Registro</a>
            </div>
            
            <div id="conteudo-interno">
            <?php 
				if (!$_GET){
			?>
             <form name="frm_consulta" id="frm_consulta" method="get" action="?" >
             <table width="99%" border="0" cellspacing="2" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="50%">&nbsp;</td>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>Título</td>
                <td>Tipo:</td>
               </tr>
              <tr>
                <td><input name="string" type="text" class="formulario" id="string" size="75" maxlength="200" /></td>
                <td><select name="codtipo" size="1" id="codtipo" class="formulario">
                  <option value="0">Selecione</option>
                  <option value="1">Valor do Pedido</option>
                  <option value="2">Frete Grátis</option>
                  <option value="3">Desconto por Produto</option>
                  <option value="4">Desconto na 1o. Compra</option>
                </select></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><input type="submit" id="btn-cmd-busca" name="btn-cmd-busca" value="Consultar" class="btn-gravar" /></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
             </table>
             
             </form>
			<?php
				}else{
			?>	
                
                
                <table width="99%" border="0" cellspacing="0" cellpadding="3" style="margin:10px;">
                  <tr>
                    <td width="50" class="titulo_table">#ID</td>
                    <td class="titulo_table">T&iacute;tulo</td>
                    <td align="center" class="titulo_table">Pagamento</td>
                    <td align="center" class="titulo_table">R$ Valor</td>
                    <td align="center" class="titulo_table">Desconto</td>
                    <td width="75" align="center" class="titulo_table">Marca</td>
                    <td width="75" align="center" class="titulo_table">Frete</td>
                    <td width="75" align="center" class="titulo_table">Status</td>
                    <td width="75" align="center" class="titulo_table">Editar</td>
                    <td width="75" align="center" class="titulo_table">Excluir</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
                  
                  <?php
				  
				  	$string = addslashes($_REQUEST["string"]);
					$tipo = addslashes($_REQUEST["codtipo"]);

				  
                  	$ssql = "select tbldesconto.descontoid, tbldesconto.dtitulo, tbldesconto.dcodtipo, tbldesconto.dtipo, tbldesconto.dvalor, tbldesconto.ddesconto, tbldesconto.dativo, 
							tblforma_pagamento.fforma_pagamento, tblcondicao_pagamento.ccondicao, tblmarca.mmarca, tblfrete_tipo.fdescricao
							from tbldesconto
							left join tblforma_pagamento on tbldesconto.dcodforma_pagamento=tblforma_pagamento.formapagamentoid
							left join tblcondicao_pagamento on tbldesconto.dcodcondicao_pagamento=tblcondicao_pagamento.condicaoid
							left join tblmarca on tbldesconto.dcodmarca=tblmarca.marcaid
							left join tblfrete_tipo on tbldesconto.dcodfrete=tblfrete_tipo.freteid
							where tbldesconto.dtitulo like '%{$string}%' 
							";	
					
					if($codtipo != 0 ){
							$ssql .= " and tbldesconto.dcodtipo=$tipo ";	
					}
					
					$ssql .= " order by tbldesconto.dtitulo";
	
		
					$result = mysql_query($ssql);
					if($result){
						$total_registros = mysql_num_rows($result);	
					}					
							
							
					$ssql .= " limit $start, $limit";
							
							//echo $ssql;
							
				  	$result = mysql_query($ssql);
				  	if($result){
					
						if(mysql_num_rows($result)==0){
							echo '
								  <tr>
									<td colspan="5">Nenhum registro localizado.</td>
								  </tr>';							
						}
					
					
						while($row=mysql_fetch_assoc($result)){
							$status = ($row["dativo"]==-1) ? "Ativo" : "Inativo";
							$desconto = number_format($row["ddesconto"],2,",",".");
							
							if($row["dtipo"]==1){
								$desconto .= "%";
							}
							else
							{
								$desconto = "R$ ". $desconto;
							}
							
							if($row["dcodtipo"]==2){
								$desconto = "frete grátis";
							}
							
							echo '
								  <tr class="tr_lista">
									<td>'.$row["descontoid"].'</td>
									<td>'.$row["dtitulo"].'</td>
									<td align="center">'.$row["forma_pagamento"].'</td>
									<td align="center">R$ '.number_format($row["dvalor"],2,",",".").'</td>
									<td align="center">'.$desconto.'</td>
									<td align="center">'.$row["mmarca"].'</td>
									<td align="center">'.$row["fdescricao"].'</td>
									<td align="center">'.$status.'</td>
									<td align="center"><a href="desconto.php?id='.$row["descontoid"].'"><img src="images/ico_editar.gif" border="0" /></a></td>
									<td align="center"><a href="javascript:void(0)" onclick="javascript:desconto_excluir('.$row["descontoid"].');"><img src="images/ico_excluir.gif" border="0" /></a></td>
								  </tr>							
							';
						}
						mysql_free_result($result);
					
					}
				  ?>
                  

                  
				  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
				  <tr>
				    <td colspan="10">
				      <div class="paginacao"><span class="paginacao-text">Página:</span> 
				        <?php
                        echo paginacao($pagina, $limit, $total_registros);
                        ?>                            
				        </div>			        </td>
			      </tr>

                </table>
                
                
                <?php
                
				}
				?>
                
          </div>
            
       </div>

 
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>