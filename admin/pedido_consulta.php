<?php
	session_start();
	$_SESSION["acesso_admin"] = -1;
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");
	
	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$uri = str_replace("/stylemarket/","",$_SERVER['REQUEST_URI']);	

	$pagina = 1;
	$start = 0;
	$limit = 20;

	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}

	$data1 = "01".date("/m/Y");				
	
	
	
	/*--------------------------------------------------------------------------
	exportar
	---------------------------------------------------------------------------*/
	if($_REQUEST["action"]=="exportar"){
		
			header("Content-type: application/vnd.ms-excel");
			header("Content-type: application/force-download");
			header("Content-Disposition: attachment; filename=report_pedido.csv");
			header("Pragma: no-cache");		
		
			echo utf8_decode("#ID;Pedido;Nome;Forma Pagto;Subtotal;Total;Status;Data Pedido;\r\n");
				$ssql = $_SESSION["ssql"];
				$result = mysql_query($ssql);
				if($result){
						while($row=mysql_fetch_assoc($result)){		
						
							$subtotal	+= $row["psubtotal"];
							$total		+= $row["pvalor_total"];
						
							echo $row["pedidoid"].";";
							echo $row["pcodigo"].";";
							echo utf8_decode($row["pnome"]).";";
							echo utf8_decode($row["fforma_pagamento"]).";";
							echo number_format($row["psubtotal"],2,",",".").";";
							echo number_format($row["pvalor_total"],2,",",".").";";
							echo utf8_decode($row["sstatus"]).";";
							echo formata_data_tela($row["pdata_cadastro"]).";";
							echo "\r\n";
							
						}
						mysql_free_result($result);				
				}
				
				echo utf8_decode(";\r\n");
				echo utf8_decode(";;;;".number_format($subtotal,2,",",".").";".number_format($total,2,",",".").";;;");
				
				
				die();
		
	}	
	
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$("#data1").mask("99/99/9999");
		$("#data2").mask("99/99/9999");
		$('#pedido').focus();
	});
	
	function seta_print(valor){
		$("#print").attr("value",valor);	
	}
	
	
	//click na linha do pedido
	$('.tr_lista').live('click', function(){
		var $this = $( this );  
		var id = $this.attr("rel");  
		if($("#print").attr("value")=="0"){	
			window.location.href = "pedido.php?id="+id;		
		}
		else
		{
			$("#print").attr("value","0");		
		}
	}); 	
	
  
</script>

</head>

<body>


<div id="global-container">

    
    <div id="content">    

    	<div id="conteudo">
			<div id="titulo-consulta"><input type="hidden" name="print" id="print" value="0" />
            	<span class="label-inicio">Pedido &raquo; <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='pedido_consulta.php';">Consulta</span></span>
            </div>
            
            <div id="conteudo-interno">
            <?php 
				if (!$_GET){
			?>
             <form name="frm_consulta" id="frm_consulta" method="get" action="?" >
             <table width="99%" border="0" cellspacing="2" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="50%">&nbsp;</td>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>No. do Pedido &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nome do Cliente</td>
                <td>Status</td>
               </tr>
              <tr>
                <td><input name="pedidoid" type="text" class="formulario" id="pedidoid" size="12" maxlength="200" />&nbsp;
                <input name="string" type="text" class="formulario" id="string" size="60" maxlength="200" /></td>
                <td>
                <select name="status" size="1" class="formulario" id="status">
                <option value="0">Selecione</option>
                  <?php
                  	$ssql = "select statusid, sstatus from tblpedido_status order by statusid";
					$result = mysql_query($ssql);
					if($result){
						while($row=mysql_fetch_assoc($result)){
							echo '<option value="'.$row["statusid"].'"';
							
							if($row["statusid"]==$status){
								echo ' selected';	
							}
							
							echo '>';
							echo $row["sstatus"];
							echo '</option>';
						}
						mysql_free_result($result);
					}
				  ?>
                </select>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Retorno do Cartão [ TID | PAN | ARP | NSU ]</td>
                <td>Número do Cartão</td>
              </tr>
              <tr>
                <td><input name="cartao_retorno" type="text" class="formulario" id="cartao_retorno" size="60" maxlength="50" /></td>
                <td><input name="cartao_numero" type="text" class="formulario" id="cartao_numero" size="20" maxlength="4" />
4 últimos dígitos</td>
              </tr>   
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>                                          
              <tr>
                <td>Data de Início</td>
                <td>Data de Término</td>
              </tr>
              <tr>
                <td><label>
                  <input name="data1" type="text" class="formulario" id="data1" value="<?php echo $data1;?>" size="15" maxlength="10" />
                </label></td>
                <td><input name="data2" type="text" class="formulario" id="data2" value="<?php echo $data2;?>" size="15" maxlength="10" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><input type="submit" id="btn-cmd-busca" name="btn-cmd-busca" value="Consultar" class="btn-gravar" /></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
             </table>
             
             </form>
			<?php
				}else{
			?>	
                
                
                <table width="99%" border="0" cellspacing="0" cellpadding="3" style="margin:10px;">
                  <tr>
                    <td width="50" class="titulo_table">#ID</td>
                    <td width="150" class="titulo_table">Pedido</td>
                    <td class="titulo_table">Nome</td>
                    <td align="center" class="titulo_table">Forma de Pagto</td>
                    <td width="100" align="center" class="titulo_table">Subtotal</td>
                    <td width="100" align="center" class="titulo_table">Total</td>
                    <td align="center" class="titulo_table">Status Pedido</td>
                    <td align="center" class="titulo_table">Data Pedido</td>
                    <td align="center" class="titulo_table">Imprimir</td>
					<td align="center" class="titulo_table">Boleto</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
                  
                  <?php
				  
				  	$string = addslashes($_REQUEST["string"]);
					$string = str_replace(" ","%",$string);
					$pedido = addslashes($_REQUEST["pedidoid"]);
							
					$status = addslashes($_REQUEST["status"]);
					
					$cartao_retorno = addslashes($_REQUEST["cartao_retorno"]);
					$cartao_numero = addslashes($_REQUEST["cartao_numero"]);
					
					
					$data1 = addslashes($_REQUEST["data1"]);
					
					$data2 = addslashes($_REQUEST["data2"]);
				  
                  	$ssql = "select bnosso_numero, bnosso_numero_dac, tblpedido.pedidoid, tblpedido.pcodorcamento, tblpedido.pcodigo, tblpedido.ptitulo, concat(tblcadastro.crazao_social,' ',tblcadastro.cnome) as pnome, tblpedido.pcodstatus, tblpedido.pcodforma_pagamento, 
							tblpedido.psubtotal, tblpedido.pvalor_total, tblpedido.pdata_cadastro, tblpedido_status.sstatus, tblforma_pagamento.fforma_pagamento
							from tblpedido
							left join tblpedido_status on tblpedido.pcodstatus = tblpedido_status.statusid
							left join tblforma_pagamento on tblpedido.pcodforma_pagamento = tblforma_pagamento.formapagamentoid
							LEFT JOIN tblcadastro on tblcadastro.cadastroid = tblpedido.pcodcadastro
							left join tblboleto on tblpedido.pedidoid = tblboleto.bcodpedido
							where tblpedido.pfinalizado=-1 and ((tblpedido.pnome like '%{$string}%' || tblcadastro.crazao_social like '%{$string}%' || tblcadastro.cnome like '%{$string}%') or tblpedido.pnome is null )
							";
					
					if($cartao_retorno!=""){
							$ssql .= " and tblpedido.pcartao_retorno like '%{$cartao_retorno}%' ";						
					}

					if($cartao_retorno!=""){
							$ssql .= " and tblpedido.pcartao_numero like '%{$cartao_numero}%' ";						
					}
					
					if($pedido!=""){
							$ssql .= " and tblpedido.pedidoid ='{$pedido}' ";	
					}
										
					if($data1!=""){
							$data1 = formata_data_db($data1) . " 00:00:00";
							$ssql .= " and tblpedido.pdata_cadastro >= '{$data1}'";	
					}
					if($data2!=""){
							$data2 = formata_data_db($data2) . " 23:59:59";
							$ssql .= " and tblpedido.pdata_cadastro <= '{$data2}' ";	
					}
					
					if($status > 0 ){
							$ssql .= " and tblpedido.pcodstatus=$status ";	
					}

					$ssql .= " order by tblpedido.pdata_cadastro desc, tblpedido.ptitulo, tblpedido.pnome ";
		
					$result = mysql_query($ssql);
					if($result){
						$total_registros = mysql_num_rows($result);	
					}					
					
					
					$_SESSION["ssql"] = $ssql;
							
					$ssql .= " limit $start, $limit";
							
					//echo $ssql;
							
				  	$result = mysql_query($ssql);
				  	if($result){
					
						if(mysql_num_rows($result)==0){
							echo '
								  <tr>
									<td colspan="9">Nenhum registro localizado.</td>
								  </tr>';							
						}
					
					
						while($row=mysql_fetch_assoc($result)){
							
							$disponivel = ($row["pdisponivel"]==-1) ? "Sim" : "Não";
														
							$vitrine = ($row["pvitrine"]=="-1") ? "Sim" : "Não";
							$numpedido = 15500 + $row["pcodigo"];
							echo '
								  <tr class="tr_lista" rel="'.$row["pedidoid"].'">
									<td>'.$row["pedidoid"].'</td>
									<td>'.$numpedido.'</td>
									<td>'.$row["pnome"].'</td>
									<td>'.$row["fforma_pagamento"].'</td>
									<td align="right">R$ '.number_format($row["psubtotal"],2,",",".").'&nbsp;&nbsp;</td>
									<td align="right">R$ '.number_format($row["pvalor_total"],2,",",".").'&nbsp;&nbsp;</td>
									<td align="center">'.$row["sstatus"].'</td>
									<td align="center">'. formata_data_tela($row["pdata_cadastro"]).'</td>
									<td align="center"><img src="images/ico_pedido.gif" width="16" height="16" onclick="javascript:seta_print(1); pedido_print('.$row["pedidoid"].'); return;" /></td>
									<td align="center">'.$row["bnosso_numero"].$row["bnosso_numero_dac"].'</td>
								  </tr>							
							';
						}
						mysql_free_result($result);
					
					}
				  ?>
                  

                  
				  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
				  <tr>
				    <td colspan="9">
				      <div class="paginacao"><span class="paginacao-text">Página:</span> 
				        <?php
                        echo paginacao($pagina, $limit, $total_registros);
                        ?>                            
				        </div>			        </td>
			      </tr>
				  <tr>
				    <td colspan="9"><a href="?action=exportar">[ Exportar Excel ]</a></td>
			      </tr>

                </table>
                
                
                <?php
                
				}
				?>
                
          </div>
            
       </div>

 
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>