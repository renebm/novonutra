<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$uri = str_replace("/videokestore/","",$_SERVER['REQUEST_URI']);	

	$pagina = 1;
	$start = 0;
	$limit = 20;

	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}


	/*-----------------------------------------------------------------------
	ajax de exclusão
	------------------------------------------------------------------------*/
	if($_POST && $_REQUEST["action"]=="excluir"){
		$id = addslashes($_REQUEST["id"]);
		$ssql = "delete from tblproduto_aviseme where avisemeid='{$id}'";
		mysql_query($ssql);
		echo "ok";
		exit();
	}






	/*-----------------------------------------------------------------------
	ajax de envio
	------------------------------------------------------------------------*/
	if($_REQUEST["action"]=="enviar"){
		$id = addslashes($_REQUEST["id"]);
		$body = get_configuracao("email_produto_aviseme");
		
		if($body==""){
			echo "Clique em `Personalizar e-mail` antes de iniciar o envio.";
			exit();
		}
		
		$ssql = "SELECT tblproduto_aviseme.avisemeid, tblproduto_aviseme.anome, tblproduto_aviseme.aemail, tblproduto_aviseme.adata_cadastro, 
							tblproduto.pcodigo, tblproduto.pproduto, tblproduto.pdescricao, tblproduto.pvalor_unitario, tblproduto.plink_seo, 
							tblestoque.eestoque, t.ppropriedade AS tamanho, p.ppropriedade AS propriedade, tblproduto_midia.marquivo
							FROM tblproduto_aviseme
							LEFT JOIN tblestoque ON tblproduto_aviseme.acodproduto = tblestoque.ecodproduto
							AND tblestoque.ecodpropriedade = tblproduto_aviseme.acodpropriedade
							AND tblestoque.ecodtamanho = tblproduto_aviseme.acodtamanho
							LEFT JOIN tblproduto ON tblproduto_aviseme.acodproduto = tblproduto.produtoid
							LEFT JOIN tblproduto_propriedade AS t ON tblproduto_aviseme.acodtamanho = t.propriedadeid
							LEFT JOIN tblproduto_propriedade AS p ON tblproduto_aviseme.acodpropriedade = p.propriedadeid
							LEFT JOIN tblproduto_midia ON tblproduto_aviseme.acodproduto = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal=-1
							where tblproduto_aviseme.avisemeid='{$id}'";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$nome		=	$row["anome"];
				$email		=	$row["aemail"];
				
				$codigo		=	$row["pcodigo"];
				$produto	=	$row["pproduto"];
				$descricao	=	$row["pdescricao"];
				$imagem		=	$row["marquivo"];
				$link		=	$row["plink_seo"];
				$valor_unitario	=	$row["pvalor_unitario"];
				$valor_unitario = number_format($valor_unitario,2,",",".");
				
			}
			mysql_free_result($result);
		}
		
				
		$body = str_replace("#nome#",$nome,$body);		
		$body = str_replace("#produto-codigo#",$codigo,$body);
		$body = str_replace("#produto#",$produto,$body);
		$body = str_replace("#produto-descricao#",$descricao,$body);
		$body = str_replace("#produto-imagem#",$imagem,$body);
		$body = str_replace("#produto-link#",$link,$body);
		$body = str_replace("#valor-unitario#",$valor_unitario,$body);
		
		$subject = utf8_encode("O produto ".$produto." já está a venda na loja ");
		$subject .= $site_nome;
		
		if(envia_email($site_nome, $site_email, $nome, $email, $subject, $body)){
			mysql_query("update tblproduto_aviseme set astatus=-1, adata_alteracao='{$data_hoje}' where avisemeid='{$id}'");
			echo "ok";
			exit();
		}
		else
		{
			echo "Erro ao enviar email para $email." ;	
			exit();
		}
		
		exit();
	}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$('#string').focus();
	});
  

</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    

    	<div id="conteudo">
			<div id="titulo-consulta">
            	<span class="label-inicio">Produto Avise-me &raquo; <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='produto_aviseme_consulta.php';">Consulta</span></span> <a href="produto_aviseme_email.php">Personalizar e-mail</a>
            </div>
            
            <div id="conteudo-interno">
            <?php 
				if (!$_GET){
			?>
             <form name="frm_consulta" id="frm_consulta" method="get" action="?" >
             <table width="99%" border="0" cellspacing="2" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="50%">&nbsp;</td>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>Produto</td>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td><input name="string" type="text" class="formulario" id="string" size="75" maxlength="200" /></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><input type="submit" id="btn-cmd-busca" name="btn-cmd-busca" value="Consultar" class="btn-gravar" /></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
             </table>
             
             </form>
			<?php
				}else{
			?>	
                
                <table width="99%" border="0" cellspacing="0" cellpadding="3" style="margin:10px;">
                  <tr>
                    <td width="50" class="titulo_table"><input name="check_all" type="checkbox" id="check_all" value="-1" onclick="javascript:produto_aviseme_checkbox();" /></td>
                    <td class="titulo_table">Produto</td>
                    <td align="center" class="titulo_table">Nome</td>
                    <td align="center" class="titulo_table">E-mail</td>
                    <td align="center" class="titulo_table">Celular</td>
                    <td align="center" class="titulo_table">Estoque</td>
                    <td width="75" align="center" class="titulo_table">Excluir</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
                  
                  <?php
				  
				  	$string = addslashes($_REQUEST["string"]);					
				  
                  	$ssql = "SELECT tblproduto_aviseme.avisemeid, tblproduto_aviseme.anome, tblproduto_aviseme.aemail,tblproduto_aviseme.acelular, tblproduto_aviseme.adata_cadastro, 
							tblproduto.pcodigo, tblproduto.pproduto, tblproduto.pdescricao, tblproduto.pdisponivel, tblproduto.pcontrola_estoque, 
							tblestoque.eestoque, t.ppropriedade AS tamanho, p.ppropriedade AS propriedade
							FROM tblproduto_aviseme
							LEFT JOIN tblestoque ON tblproduto_aviseme.acodproduto = tblestoque.ecodproduto
							AND tblestoque.ecodpropriedade = tblproduto_aviseme.acodpropriedade
							AND tblestoque.ecodtamanho = tblproduto_aviseme.acodtamanho
							LEFT JOIN tblproduto ON tblproduto_aviseme.acodproduto = tblproduto.produtoid
							LEFT JOIN tblproduto_propriedade AS t ON tblproduto_aviseme.acodtamanho = t.propriedadeid
							LEFT JOIN tblproduto_propriedade AS p ON tblproduto_aviseme.acodpropriedade = p.propriedadeid
							where tblproduto_aviseme.astatus=0 and tblproduto.pcodigo like '%{$string}%' 
							or tblproduto_aviseme.astatus=0 and tblproduto.pproduto like '%{$string}%' 
							or tblproduto_aviseme.astatus=0 and tblproduto.pdescricao like '%{$string}%'";	
				
					$ssql .= " order by pproduto";
	
		
					$result = mysql_query($ssql);
					if($result){
						$total_registros = mysql_num_rows($result);	
					}					
							
							
					$ssql .= " limit $start, $limit";
							
					//echo $ssql;
							
				  	$result = mysql_query($ssql);
				  	if($result){
					
						if(mysql_num_rows($result)==0){
							echo '
								  <tr>
									<td colspan="5">Nenhum registro localizado.</td>
								  </tr>';							
						}
						$count = $start;
						while($row=mysql_fetch_assoc($result)){
							
							$estoque = $row["eestoque"];
							if(!is_numeric($estoque)){
								$estoque = 0;	
							}
							
							$disabled = "";
							if($estoque<=0 && $row["pcontrola_estoque"]==-1 || $row["pdisponivel"]==0){
								$disabled = ' disabled="disabled" ';	
							}							
								

							$count++;
							echo '
								  <tr class="tr_lista">
									<td><input type="checkbox" name="avise[]" id="avise[]" value="'.$row["avisemeid"].'" '.$disabled.' class="aviseme" /></td>
									<td>'.$row["pcodigo"].' - '.$row["pproduto"].'&nbsp;&nbsp;' . $row["tamanho"] . '&nbsp;&nbsp;' . $row["ppropriedade"] . '</td>
									<td align="center">'.$row["anome"].'</td>
									<td align="center">'.$row["aemail"].'</td>
									<td align="center">'.formata_telefone_tela($row["acelular"]).'</td>
									<td align="center">'.$estoque.'</td>
									<td align="center"><a href="javascript:void(0)" onclick=javascript:produto_aviseme_excluir('.$row["avisemeid"].');><img src="images/ico_excluir.gif" border="0" /></a></td>
								  </tr>							
							';
						}
						mysql_free_result($result);
					
					}
				  ?>
                  
				  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
				  <tr>
				    <td colspan="7">
				      <div class="paginacao"><span class="paginacao-text">Página:</span> 
				        <?php
                        echo paginacao($pagina, $limit, $total_registros);
                        ?>                            
				        </div>			        </td>
			      </tr>
				  <tr>
				    <td colspan="7">&nbsp;</td>
			      </tr>
				  <tr>
				    <td colspan="7" align="left">
                    	<input type="button" id="btn-cmd-busca2" name="btn-cmd-busca2" value="Enviar" class="btn-gravar" onclick="javascript:produto_aviseme_enviar();" />
                        <div id="spinner" style="margin:4px 0 0 100px; width:auto; height:20px;"></div>
                    </td>
			      </tr>

                </table>
                
                
                <?php
                
				}
				?>
                
          </div>
            
       </div>

 
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>