var _spinner = '<img src="images/spinner.gif" border="0" />';

function valida_loja(){
	var razaosocial = document.getElementById("razaosocial");
	var nome = document.getElementById("nome");
	var email = documento.get_only_numbers("email");
	var site = document.getElementById("site");
	
	if(trim(razaosocial.value)==""){
		alert("A razão social deve ser informada");
		razaosocial.focus();
		return false;
	}

	if(trim(nome.value)==""){
		alert("O nome fantasia deve ser informado");
		nome.focus();
		return false;
	}


	if(!ValidaEmail(email.value)){
		alert("O e-mail deve ser informado");
		email.focus();
		return false;
	}

	if(trim(site.value)==""){
		alert("O site deve ser informado");
		site.focus();
		return false;
	}	
}



/*------------------------------------------------------------------------*/
function valida_usuario(){
	var nome = document.getElementById("nome");
	var email = document.getElementById("email");
	var login = document.getElementById("login");
	var senha = document.getElementById("senha");
	var senha1 = document.getElementById("senha1");
	
	
	if(trim(nome.value)==""){
		alert("O nome deve ser informado");
		nome.focus();
		return false;
	}

	if(!ValidaEmail(email.value)){
		alert("O e-mail deve ser informado");
		email.focus();
		return false;
	}
	
	if(trim(login.value)==""){
		alert("O login deve ser informado");
		login.focus();
		return false;
	}
	
	
	if(trim(senha.value)==""){
		alert("A senha deve ser informada");
		senha.focus();
		return false;
	}
	
	if(senha.value.length<4){
		alert("A senha deve ter no mínimo 4 caracteres");
		senha.focus();
		return false;
	}
	
	
	if(senha.value!=senha1.value){
		alert("A senha deve ser informada e confirmada");
		senha.focus();
		return false;
	}
	
	
	
	
}


/*--------------------------------------------------------------------------*/
function valida_cedente(){
	var razaosocial = document.getElementById("razaosocial");
	var cnpj = document.getElementById("cnpj");
	var banco = document.getElementById("banco");
	var numero_banco = document.getElementById("numero_banco");
	var numero_banco_dac = document.getElementById("numero_banco_dac");
	var agencia = document.getElementById("agencia");
	var conta_corrente = document.getElementById("conta_corrente");
	var conta_corrente_dac = document.getElementById("conta_corrente_dac");
	var nosso_numero = document.getElementById("nosso_numero");
	var numero_documento = document.getElementById("numero_documento");
	var carteira = document.getElementById("carteira");
	var convenio = document.getElementById("convenio");
	var taxa = document.getElementById("taxa");
	var dias = document.getElementById("dias");
	
	if(trim(razaosocial.value)==""){
		alert("A razão social deve ser informada");
		razaosocial.focus();
		return false;
	}

	if(trim(cnpj.value)==""){
		alert("O cnpj deve ser informado");
		cnpj.focus();
		return false;
	}
	
	if(trim(banco.value)==""){
		alert("O nome do banco deve ser informado");
		banco.focus();
		return false;
	}
	
	if(trim(numero_banco.value)=="" || !isNumber(numero_banco.value)){
		alert("O número do banco deve ser informado");
		numero_banco.focus();
		return false;
	}

	if(trim(numero_banco_dac.value)=="" || !isNumber(numero_banco_dac.value)){
		alert("O número do dac do banco deve ser informado");
		numero_banco_dac.focus();
		return false;
	}

	if(trim(agencia.value)=="" || !isNumber(agencia.value)){
		alert("O número da agência deve ser informado");
		agencia.focus();
		return false;
	}

	if(trim(conta_corrente.value)=="" || !isNumber(conta_corrente.value)){
		alert("O número da conta corrente deve ser informada");
		conta_corrente.focus();
		return false;
	}

	if(trim(conta_corrente_dac.value)=="" || !isNumber(conta_corrente_dac.value)){
		alert("O número do dac da conta corrente deve ser informado");
		conta_corrente_dac.focus();
		return false;
	}


	if(trim(nosso_numero.value)=="" || !isNumber(nosso_numero.value)){
		alert("O nosso número deve ser informado");
		nosso_numero.focus();
		return false;
	}

	if(trim(numero_documento.value)=="" || !isNumber(numero_documento.value)){
		alert("O número do documento deve ser informado");
		numero_documento.focus();
		return false;
	}

	if(trim(carteira.value)=="" || !isNumber(carteira.value)){
		alert("O número da carteira deve ser informado");
		carteira.focus();
		return false;
	}
	
	if(trim(convenio.value)=="" || !isNumber(convenio.value)){
		alert("O número de convênio deve ser informado");
		convenio.focus();
		return false;
	}
	
	if(!isNumberVA(taxa.value,2)){
		alert("A taxa de emissão de boleto deve ser informada");
		taxa.focus();
		return false;
	}

	if(trim(dias.value)=="" || !isNumber(dias.value)){
		alert("O número de dias do vencimento deve ser informado");
		dias.focus();
		return false;
	}
	
}




/*--------------------------------------------------------------------------*/
function valida_config(){
	
	var vitrine_qtde_item = document.getElementById("vitrine_qtde_item");
	var qtde_dias_lancamento = document.getElementById("qtde_dias_lancamento");
	var vitrine_ordem = document.getElementById("vitrine_ordem");
	var qtde_item_relacionado = document.getElementById("qtde_item_relacionado");
	var cielo_filiacao = document.getElementById("cielo_filiacao");
	var cielo_chave = document.getElementById("cielo_chave");
	var redecard_filiacao = document.getElementById("redecard_filiacao");
	var pagseguro_email = document.getElementById("pagseguro_email");
	var valor_minimo_parcela = document.getElementById("valor_minimo_parcela");
	var qtde_categoria_header = document.getElementById("qtde_categoria_header");
	
	
	if(!isNumber(vitrine_qtde_item.value)){
		alert("A quantidade de itens na vitrine deve ser informada");
		vitrine_qtde_item.focus();
		return false;
	}
	
	
	if(qtde_dias_lancamento.value==0){
		alert("A quantidade de dias para calculo de lançamento deve ser informada");
		qtde_dias_lancamento.focus();
		return false;
	}
	
	
	if(vitrine_ordem.value==0){
		alert("A ordem de exibição da vitrine deve ser informada");
		vitrine_ordem.focus();
		return false;
	}
	
	if(!isNumber(qtde_item_relacionado.value)){
		alert("A quantidade de itensrelacionados deve ser informada");
		qtde_item_relacionado.focus();
		return false;
	}
		
	if(trim(pagseguro_email.value)!="" && !ValidaEmail(pagseguro_email.value)){
		alert("O email informado é inválido");
		pagseguro_email.focus();
		return false;
	}
	
	
	if(!isNumberVA(valor_minimo_parcela.value,2)){
		alert("O valor mínimo de cada parcela deve ser informado");
		valor_minimo_parcela.focus();
		return false;
	}
	
	
	if(!isNumber(qtde_categoria_header.value)){
		alert("A quantidade de categoria relacionadas no cabeçalho deve ser informada");
		qtde_categoria_header.focus();
		return false;
	}	
	
	
	
	
}







/*---------------------------------------------------------------------------
regra de descontos
-----------------------------------------------------------------------------*/
function desconto_habilita_campos(tipo){

	
	//alert($("#categoria").attr("disabled"));
	
	if(tipo==0){
		$.each($('.formulario'), function(key, value) { 
			var $this = $( this ); 
			if($this.attr("type") == "select-one" || $this.attr("type")=="text"){
				if($this.attr("name")!="codtipo" && $this.attr("name")!="titulo" ){
					$this.attr("disabled",true);
				}
			}
		});
	}
	else
	{
		$.each($('.formulario'), function(key, value) { 
			var $this = $( this ); 
			if($this.attr("type") == "select-one" || $this.attr("type")=="text"){
				$this.attr("disabled",false);
			}
		});		
	}


	if(tipo==1){	// valor pedido
		$("#categoria").attr("disabled",true);
		$("#categoria").attr("value","0");
		
		$("#marca").attr("disabled",true);
		$("#marca").attr("value","0");
		
		$("#codigo").attr("disabled",true);
		$("#codigo").attr("value","");
		$("#produto").attr("value","0");
		
		$("#utm_source").attr("disabled",true);
		$("#utm_soucer").attr("value","");
		
		$("#cep_inicial").attr("disabled",true);
		$("#cep_inicial").attr("value","");
		
		$("#cep_final").attr("disabled",true);
		$("#cep_final").attr("value","");
		
		$("#frete").attr("disabled",true);
		$("#frete").attr("value","0");
	}
	
	
	
	if(tipo==2){	//frete gratis

		$("#tipo").attr("disabled",true);
		$("#tipo").attr("value","0");

		$("#desconto").attr("disabled",true);
		$("#desconto").attr("value","0,00");

		$("#forma_pagamento").attr("disabled",true);
		$("#forma_pagamento").attr("value","0");

		$("#condicao_pagamento").attr("disabled",true);
		$("#condicao_pagamento").attr("value","0");

		$("#categoria").attr("disabled",true);
		$("#categoria").attr("value","0");
		
		$("#marca").attr("disabled",true);
		$("#marca").attr("value","0");
		
		$("#codigo").attr("disabled",true);
		$("#codigo").attr("value","");
		$("#produto").attr("value","0");
		
		$("#utm_source").attr("disabled",true);
		$("#utm_soucer").attr("value","");
	}	


	if(tipo==3){

		$("#forma_pagamento").attr("disabled",true);
		$("#forma_pagamento").attr("value","0");

		$("#condicao_pagamento").attr("disabled",true);
		$("#condicao_pagamento").attr("value","0");	
			
	}
	

	if(tipo==4){
		$("#forma_pagamento").attr("disabled",true);
		$("#forma_pagamento").attr("value","0");

		$("#condicao_pagamento").attr("disabled",true);
		$("#condicao_pagamento").attr("value","0");

		$("#categoria").attr("disabled",true);
		$("#categoria").attr("value","0");
		
		$("#marca").attr("disabled",true);
		$("#marca").attr("value","0");
		
		$("#codigo").attr("disabled",true);
		$("#codigo").attr("value","");
		$("#produto").attr("value","0");

		$("#quantidade").attr("disabled",true);
		$("#quantidade").attr("value","0,00");

		$("#utm_source").attr("disabled",true);
		$("#utm_soucer").attr("value","");
		
		$("#cep_inicial").attr("disabled",true);
		$("#cep_inicial").attr("value","00000000");
		
		$("#cep_final").attr("disabled",true);
		$("#cep_final").attr("value","99999999");
		
		$("#frete").attr("disabled",true);
		$("#frete").attr("value","0");
	}	


	if(tipo==5){
		
		$("#valor").attr("disabled",true);
		$("#valor").attr("value","0,00");

		$("#forma_pagamento").attr("disabled",true);
		$("#forma_pagamento").attr("value","0");

		$("#condicao_pagamento").attr("disabled",true);
		$("#condicao_pagamento").attr("value","0");	
		
		$("#quantidade").attr("disabled",true);
		$("#quantidade").attr("value","0,00");		
		
		$("#frete").attr("disabled",true);
		$("#frete").attr("value","0");
		
		$("#cep_inicial").attr("disabled",true);
		$("#cep_inicial").attr("value","00000-000");
		
		$("#cep_final").attr("disabled",true);
		$("#cep_final").attr("value","00000-000");	
		
	}




}


function valida_desconto(){
	var codtipo = document.getElementById("codtipo");
	var titulo = document.getElementById("titulo");
	var tipo = document.getElementById("tipo");
	var valor = document.getElementById("valor");
	var desconto = document.getElementById("desconto");
	var quantidade = document.getElementById("quantidade");

	if(codtipo.value==0){
		alert("O tipo de desconto deve ser informado");
		codtipo.focus();
		return false;
	}
	
	if(titulo.value==""){
		alert("O titulo deve ser informado");
		titulo.focus();
		return false;
	}
	
	if(tipo.value==0 && codtipo.value!=2){
		alert("O tipo de desconto deve ser informado");
		tipo.focus();
		return false;
	}
	
	if(!isNumberVA(valor.value,2)){
		alert("O valor do pedido deve ser informado");
		valor.focus();
		return false;
	}


	if(!isNumberVA(desconto.value,2)){
		if(codtipo.value==2){
			desconto.value="0,00";	
		}
		else
		{
			alert("O desconto deve ser informado");
			desconto.focus();
			return false;
		}
	}

	if(!isNumberVA(quantidade.value,2)){
		if(codtipo.value==4){
			quantidade.value="0,00";	
		}
		else
		{
			alert("A quantidade de produtos deve ser informada");
			quantidade.focus();
			return false;
		}
	}
	
}



function desconto_excluir(id){
	if(confirm("Deseja realmente excluir o desconto selecionado ?")){

		var action = "excluir";
		$.ajax({
			type: "POST",
			url: "desconto.php",
			dataType: "html",
			timeout: 5000,
			data: {action: action, id: id},
			success: function(data)
			{
				if(data=="ok"){
					window.location.reload();
				}else{
					alert(data);
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				//alert(textStatus);
			}
		});		
	}	
}



/*---------------------------------------------------------------------*/

function valida_cupom_desconto(){
	var id = document.getElementById("id");
	var titulo = document.getElementById("titulo");
	var cupom = document.getElementById("cupom");
	var email = document.getElementById("email");
	var data_validade = document.getElementById("data_validade");
	var tipo = document.getElementById("tipo");
	var produto = document.getElementById("produto");
	var categoria = document.getElementById("categoria");
	var desconto = document.getElementById("desconto");
	var status = document.getElementById("status");
	var valor = document.getElementById("valor");
	var qtde = document.getElementById("qtde");
	
	
	if(trim(titulo.value)==""){
		alert("O título deve ser informado");
		titulo.focus();
		return false;
	}


	if(trim(cupom.value)==""){
		alert("O cupom deve ser informado");
		cupom.focus();
		return false;
	}

	if(cupom.value.length<6){
		alert("O cupome deve ser informado");
		cupom.focus();
		return false;
	}

	if(cupom.value.indexOf(" ")> -1){
		alert("O cupom não pode ter espaços");
		cupom.focus();
		return false;
	}


	if(!ValidaEmail(email.value)){
		alert("O e-mail deve ser informado");
		email.focus();
		return false;
	}
	
	if(!isDate(data_validade.value)){
		alert("A data de validade deve ser informado");
		data_validade.focus();
		return false;
	}
	

	if( produto.value>0 && categoria.value > 0 ){
		alert("Informe o produto ou categoria, não marque as duas opções");
		codigo.focus();
		return false;
	}
	
	
	
	if(!isNumberVA(valor.value,2)){
		alert("O valor deve ser informado");
		valor.focus();
		return false;
	}
		

	if(trim(tipo.value)=="0"){
		alert("O tipo de desconto deve ser informado");
		tipo.focus();
		return false;
	}
	
	if(!isNumberVA(desconto.value,2)){
		alert("O desconto deve ser informado");
		desconto.focus();
		return false;
	}
	
	if(trim(status.value)==""){
		if(confirm("Deseja gravar o desconto sem informar o status?")){
		}else{
			status.focus();
			return false;
		}
	}
	
	
	if( isNumber(qtde.value) == false ){
		alert("A quantidade deve ser informada");
		qtde.focus();
		return false;
	}
	
	if( qtde.value > 1 && id.value > 0 ){
		alert("A quantidade só poderá ser maior que 1 se for um novo registro");
		qtde.value = "1";
		return false;
	}
	

	if( (qtde.value) >= 10000 ){
		alert("A quantidade de cupons excede o limite permitido")
		qtde.focus();
		return false;
	}	


	if( (qtde.value) > 1 ){
		if(!confirm("Deseja mesmo gerar " + qtde.value + " cupoms ?")){
			return false;
		}
	}	
	
	
	
}


function cupom_desconto_excluir(id){
	if(confirm("Deseja realmente excluir o cupom de desconto selecionado ?")){
		var action = "excluir";
		$.ajax({
			type: "POST",
			url: "cupom_desconto.php",
			dataType: "html",
			timeout: 5000,
			data: {action: action, id: id},
			success: function(data)
			{
				if(data=="ok"){
					window.location.reload();
				}else{
					alert(data);
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				//alert(textStatus);
			}
		});		
	}	
}





/*----------------------------------------------------------------------------*/
function produto_aviseme_checkbox(){
	var status = document.getElementById("check_all");	

	$.each($('.aviseme'), function(key, value) { 
		var $this = $( this ); 
		if(!$this.attr("disabled")){
			$this.attr("checked",status.checked);
		}
	});	
		
}


function produto_aviseme_enviar(){
	var avise = "";
	
	$.each($('.aviseme'), function(key, value) { 
		var $this = $( this ); 
		if($this.attr("checked")){
			avise = avise + $this.attr("value")+",";
		}
	});	
	
	if(trim(avise)!=""){
		if(confirm("Deseja realmente enviar e-mail de notificação ?")){
		 	var id = avise.split(",");
			var total = id.length-1;		
				for(i=0;i<total;i++){
					
					$("#spinner").html("<img src='images/spinner.gif' /> Processando, aguarde...");					
					
					var action = "enviar";
					$.ajax({
						type: "POST",
						url: "produto_aviseme_consulta.php",
						dataType: "html",
						timeout: 5000,
						data: {action: action, id: id[i]},
						success: function(data)
						{
							if(data=="ok"){
								//
							}else{
								alert(data);
								setTimeout("window.location.reload()",1000);
							}
						},
						error: function (jqXHR, textStatus, errorThrown)
						{
							alert(textStatus);
						}
					});						
				}

			
				//recarrega a pagina
				setTimeout("window.location.reload()",1500);
			
		}
	}
	
}




function produto_aviseme_excluir(id){
	if(confirm("Deseja realmente excluir o registro selecionado ?")){
		var action = "excluir";
		$.ajax({
			type: "POST",
			url: "produto_aviseme_consulta.php",
			dataType: "html",
			timeout: 5000,
			data: {action: action, id: id},
			success: function(data)
			{
				if(data=="ok"){
					window.location.reload();
				}else{
					alert(data);
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				//alert(textStatus);
			}
		});		
	}		
}


/*--------------------------------------------------------------------*/
function busca_status(tag, id){
	if(confirm("Deseja realmente alterar o status da expressão selecionada ?")){
		var action = "status";
		$.ajax({
			type: "POST",
			url: "busca_consulta.php",
			dataType: "html",
			timeout: 5000,
			data: {action: action, tag: tag},
			success: function(data)
			{
				$("#status_"+id).html(String(data));
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				//alert(textStatus);
			}
		});		
	}	
}

function busca_excluir(tag){
	if(confirm("Deseja realmente excluir a expressão selecionada ?")){
		var action = "excluir";
		$.ajax({
			type: "POST",
			url: "busca_consulta.php",
			dataType: "html",
			timeout: 5000,
			data: {action: action, tag: tag},
			success: function(data)
			{
				if(data=="ok"){
					window.location.reload();
				}else{
					alert(data);
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				//alert(textStatus);
			}
		});		
	}	
}










/*--------------------------------------------------------------------*/
function get_produtoid_by_sku(codigo){
	
	var action = "get_produtoid_by_sku";

	$.ajax({
        type: "POST",
        url: "ajax_produto.php",
        dataType: "html",
		timeout: 10000,
        data: {action: action, codigo: codigo},
        success: function(data)
        {
			var ret = data.split("||");
			if(ret[0]=="ok"){
				$("#produto").attr("value",ret[1]);
				$("#produto_descricao").attr("value",ret[2]);
			}
			else
			{
				$("#produto").attr("value","0");
				$("#codigo").attr("value","");
				$("#produto_descricao").attr("value","");
			}
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	//alert(textStatus);
        }
    });	
}







/*--------------------------------------------------------------------------*/
function procura_produto_referencia(){
	var referencia = document.getElementById("referencia");

	window.location.href = "produto.php?referencia="+referencia.value;

}



/*--------------------------------------------------------------------------*/

function valida_login(){
	var login = document.getElementById("login");
	var senha = document.getElementById("senha");
	
	if(trim(login.value)==""){
		alert("O login deve ser informado");
		login.focus();
		return false;
	}


	if(trim(senha.value)==""){
		alert("A senha deve ser informada");
		senha.focus();
		return false;
	}

}


/*---------------------------------------------------------------------------------------*/
function valida_propriedade(){
	var propriedade = document.getElementById("propriedade");
	
	if(trim(propriedade.value)==""){
		alert("A propriedade deve ser informada");
		propriedade.focus();
		return false;
	}
	
	$("#msg").html("<img src='images/spinner.gif' /> Processando, aguarde...");
	
}



/*---------------------------------------------------------------------------------------*/
function produto_load_propriedades(){
	
	var action = "load_propriedades";
	
	$.ajax({
        type: "POST",
        url: "ajax_produto_propriedade.php",
        dataType: "html",
		timeout: 5000,
        data: {action: action},
        success: function(data)
        {
			$("#td-propriedade").html(data);	
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	//alert(textStatus);
        }
    });	
}


/*---------------------------------------------------------------------------------------*/
function produto_propriedade_excluir(id){
	
	if(id<=2){
		alert("Esse propriedade não pode ser excluida");
		$("#msg").html("");
		return;
	}
	
	var action = "del_propriedade";
	
	$.ajax({
        type: "POST",
        url: "ajax_produto_propriedade.php",
        dataType: "html",
		timeout: 5000,
        data: {action: action, id: id},
        success: function(data)
        {
			$("#msg").html(data);
			produto_load_propriedades();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
			produto_load_propriedades();
        	//alert(textStatus);
        }
    });	
}




/*---------------------------------------------------------------------------------------*/
function produto_load_estoque(){

	var id = document.getElementById("id");
	var action = "load_estoque";	
	
	$.ajax({
        type: "POST",
        url: "ajax_estoque.php",
        dataType: "html",
		timeout: 5000,
        data: {action: action, id: id.value},
        success: function(data)
        {
			$("#td-estoque").html(data);	
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	alert(textStatus + errorThrown);
        }
    });	
	
}




/*---------------------------------------------------------------------------------------*/
function valida_produto(){

	var action = "gravar";
	var tab = document.getElementById("tab").value;
	var id = document.getElementById("id").value;

	
	//$("#btn-cmd-gravar-"+tab).attr("disabled","1");
	
	if(tab==1){	// Principal

			var referencia = document.getElementById("referencia");
			var codigo = document.getElementById("codigo");
			var codigo_barras = document.getElementById("codigo_barras");
			var ncm = document.getElementById("ncm");
		
			var produto = document.getElementById("produto");
			var subtitulo = document.getElementById("subtitulo");
			
			var link_seo = document.getElementById("link_seo");
		
			var marca = document.getElementById("marca");
			var codigo_fabricante = document.getElementById("codigo_fabricante");
		
			var valor_comparativo = document.getElementById("valor_comparativo");
			var valor_unitario = document.getElementById("valor_unitario");
			
			var outlet = document.getElementById("outlet");
			outlet = outlet.checked==false ? "0" : "-1";
			if(trim(referencia.value)==""){
				referencia.value = codigo.value;
			}
			
			var recomendados = document.getElementById("recomendados");
			recomendados = recomendados.checked==false ? "0" : "-1";
			if(trim(referencia.value)==""){
				referencia.value = codigo.value;
			}			
			if(trim(codigo.value)=="" || codigo.value.indexOf(" ")>-1){
				alert("O código do produto deve ser informado");
				codigo.focus();
				$("#btn-cmd-gravar-"+tab).attr("disabled",0);
				return false;
			}
			
		
			if(trim(produto.value)==""){
				alert("O nome do produto deve ser informado");
				produto.focus();
				$("#btn-cmd-gravar-"+tab).attr("disabled",0);
				return false;
			}
		
			if(trim(codigo_barras.value)==""){
				codigo_barras.value = codigo.value;
			}
				
		
			if(trim(link_seo.value)==""){
				alert("A url amigável do produto do produto deve ser informada");
				link_seo.focus();
				$("#btn-cmd-gravar-"+tab).attr("disabled",0);
				return false;
			}
		
			
			if(marca.value=="0" || marca.value==""){
				alert("A marca deve ser informada");
				marca.focus();
				$("#btn-cmd-gravar-"+tab).attr("disabled",0);		
				return false;
			}
			
			
			if(!isNumberVA(valor_comparativo.value,2)){
				alert("O valor comparativo deve ser informado");
				valor_comparativo.focus();
				$("#btn-cmd-gravar-"+tab).attr("disabled",0);		
				return false;
			}	
			
			
			if(!isNumberVA(valor_unitario.value,2)){
				alert("O valor unitário deve ser informado");
				valor_unitario.focus();
				$("#btn-cmd-gravar-"+tab).attr("disabled",0);		
				return false;
			}	
			
			
			if(valor_unitario.value>=valor_comparativo.value && valor_comparativo.value>0){
				alert("O valor comparativo não pode ser igual ou menor que o valor unitário");
				valor_comparativo.focus();
				$("#btn-cmd-gravar-"+tab).attr("disabled",0);		
				return false;
			}
					
			$("#dica1").html("<img src='images/spinner.gif' /> Processando, aguarde...");		

			//document.getElementById("frm_produto").action = "ajax_produto.php";
			//document.getElementById("frm_produto").submit();

			$.ajax({
				type: "POST",
				url: "ajax_produto.php",
				dataType: "html",
				timeout: 5000,
				data: {action: action, tab: tab, id: id,  referencia:referencia.value, codigo: codigo.value, codigo_barras: codigo_barras.value, produto:produto.value, 
						subtitulo:subtitulo.value, link_seo: link_seo.value, marca:marca.value, codigo_fabricante: codigo_fabricante.value,
						valor_comparativo: valor_comparativo.value, valor_unitario: valor_unitario.value, ncm:ncm.value, outlet: outlet, recomendados: recomendados},
				success: function(data)
				{	
					ret = data.split("||");
					if(ret[0]=="ok"){
						$("#tabs").tabs({disabled: false});
						$("#id").attr("value",ret[1]);
						$("#produtoid").html(ret[1]);
						$("#dica1").html("Registro salvo com sucesso<br/>");
					}else{
						$("#dica1").html("Erro ao gravar <br /><br />Erro: " + ret[2]);	
					}
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					
					$("#dica1").html("Erro ao gravar");
				}
			});	
			

		//setTimeout('$("#btn-cmd-gravar-"+tab).attr("disabled",0)',5000);
		//return false;

	}//principal 
	
	

	
	
	
	
	if(tab==2){	//Propriedades
	
		var peso = document.getElementById("peso");
		var largura = document.getElementById("largura");
		var altura = document.getElementById("altura");
		var comprimento = document.getElementById("comprimento");
		
		var disponivel = document.getElementById("disponivel");
		var controla_estoque = document.getElementById("controla_estoque");
		var frete_gratis = document.getElementById("frete_gratis");
		var top20	= document.getElementById("top20");
		
		var minimo = document.getElementById("minimo");
		var maximo = document.getElementById("maximo");
		
		var vitrine = document.getElementById("vitrine");
		var data_inicio = document.getElementById("data_inicio");
		var data_termino = document.getElementById("data_termino");
		
		var propriedade = document.getElementById("disponivel");

		//$("#btn-cmd-gravar-"+tab).attr("disabled","1");

		if(!isNumberVA(peso.value,2)){
			alert("O peso deve ser informado");	
			peso.focus();
			$("#btn-cmd-gravar-"+tab).attr("disabled",0);	
			return false;			
		}
		
		if(!isNumber(largura.value) || eval(largura.value)<11){
			alert("A largura deve ser informada");	
			largura.focus();
			$("#btn-cmd-gravar-"+tab).attr("disabled",0);
			return false;			
		}	
		
		if(!isNumber(altura.value) || eval(altura.value)<2){
			alert("A altura deve ser informada");	
			altura.focus();
			$("#btn-cmd-gravar-"+tab).attr("disabled",0);
			return false;			
		}			
		
		if(!isNumber(comprimento.value) || eval(comprimento.value)<16){
			alert("O ocmprimento deve ser informado");	
			comprimento.focus();
			$("#btn-cmd-gravar-"+tab).attr("disabled",0);
			return false;			
		}			
		
		
		disponivel = disponivel.checked==false ? "0" : "-1";
		
		controla_estoque = controla_estoque.checked==false ? "0" : "-1";
		
		frete_gratis = frete_gratis.checked==false ? "0" : "-1"

		
		if(!isNumber(minimo.value)){
			alert("O mínimo deve ser informado");	
			minimo.focus();
			$("#btn-cmd-gravar-"+tab).attr("disabled",0);	
			return false;			
		}			
		
		if(!isNumber(maximo.value)){
			alert("O máximo deve ser informado");	
			maximo.focus();
			$("#btn-cmd-gravar-"+tab).attr("disabled",0);
			return false;			
		}	
		
		
		vitrine = vitrine.checked==false ? "0" : "-1";
		
		
		if(!isDate(data_inicio.value)){
			alert("A data de início deve ser informada");	
			data_inicio.focus();
			$("#btn-cmd-gravar-"+tab).attr("disabled",0);		
			return false;			
		}			

	
		if(!isDate(data_termino.value)){
			alert("A data de termino deve ser informada");	
			data_termino.focus();
			$("#btn-cmd-gravar-"+tab).attr("disabled",0);
			return false;			
		}			

			$("#dica"+tab).html("<img src='images/spinner.gif' /> Processando, aguarde...");
			

			$.ajax({
				type: "POST",
				url: "ajax_produto.php",
				dataType: "html",
				timeout: 5000,
				data: {action: action, tab: tab, id: id, peso: peso.value, altura: altura.value, largura: largura.value, comprimento: comprimento.value, 
						disponivel: disponivel, controla_estoque: controla_estoque, frete_gratis: frete_gratis, minimo: minimo.value, maximo: maximo.value, 
						vitrine: vitrine, data_inicio: data_inicio.value, data_termino: data_termino.value, propriedade: propriedade.value, top20: top20.value},
				success: function(data)
				{				
					ret = data.split("||");
					if(ret[0]=="ok"){
						$("#id").attr("value",ret[1]);
						$("#produtoid").html(ret[1]);
						$("#dica"+tab).html("Registro salvo com sucesso<br/>");
					}else{
						$("#dica"+tab).html("Erro ao gravar <br /><br />Erro: " + ret[2]);	
					}
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					
					$("#dica"+tab).html("Erro ao gravar");
				}
			});				
		
		//setTimeout('$("#btn-cmd-gravar-"+tab).attr("disabled",0)',5000);
		//return false;		
	
	}
	
	

	if(tab==3){	//Categorias

		var categoria="";		
		
		$.each($('.categoria'), function(key, value) { 
			var $this = $( this ); 
			if($this.attr("checked")){
				categoria = categoria + $this.attr("value")+",";
			}
		});
		
				
			$("#dica"+tab).html("<img src='images/spinner.gif' /> Processando, aguarde...");
			

			$.ajax({
				type: "POST",
				url: "ajax_produto.php",
				dataType: "html",
				timeout: 5000,
				data: {action: action, tab: tab, id: id, categoria:categoria},
				success: function(data)
				{				
					ret = data.split("||");
					if(ret[0]=="ok"){
						$("#id").attr("value",ret[1]);
						$("#produtoid").html(ret[1]);
						$("#dica"+tab).html("Registro salvo com sucesso<br/>");
					}else{
						$("#dica"+tab).html("Erro ao gravar <br /><br />Erro: " + ret[2]);	
					}
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					$("#dica"+tab).html("Erro ao gravar");
				}
			});		

	}
	


	if(tab==4){	//Detalhes

		var descricao = document.getElementById("descricao");
		var palavra_chave = document.getElementById("palavra_chave");
		//var descricao_detalhada = document.getElementBy("descricao_detalhada");
		
		if(trim(descricao.value)==""){
			alert("A descrição do produto deve ser informada");
			descricao.focus();
			$("#btn-cmd-gravar-"+tab).attr("disabled",0);
			return false;					
		}

		if(trim(palavra_chave.value)==""){
			alert("As palavras chaves do produto devem ser informadas");
			palavra_chave.focus();
			$("#btn-cmd-gravar-"+tab).attr("disabled",0);
			return false;					
		}

		var descricao_detalhada = String(CKEDITOR.instances.editor1.getData());

		//alert(descricao_detalhada);

		//if(trim(descricao_detalhada.value)==""){
			//alert("A descrição detalhada do produto deve ser informada");
			//descricao_detalhada.focus();
			//$("#btn-cmd-gravar-"+tab).attr("disabled",0);
			//return false;					
		//}


			$("#dica"+tab).html("<img src='images/spinner.gif' /> Processando, aguarde...");
			
			//document.getElementById("frm_produto").submit();

			$.ajax({
				type: "POST",
				url: "ajax_produto.php",
				dataType: "html",
				timeout: 5000,
				data: {action: action, tab: tab, id: id, descricao:descricao.value, palavra_chave:palavra_chave.value, descricao_detalhada:descricao_detalhada},
				success: function(data)
				{				
					ret = data.split("||");
					if(ret[0]=="ok"){
						$("#id").attr("value",ret[1]);
						$("#produtoid").html(ret[1]);
						$("#dica"+tab).html("Registro salvo com sucesso<br/>");
					}else{
						$("#dica"+tab).html("Erro ao gravar <br /><br />Erro: " + ret[2]);	
					}
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					$("#dica"+tab).html("Erro ao gravar");
				}
			});	


	}
	
	
	
	if(tab==5){	//Opções e Tamanhos
		var propriedade="";
		
		$.each($('.propriedade'), function(key, value) { 
			var $this = $( this ); 
			if($this.attr("checked")){
				propriedade = propriedade + $this.attr("value")+",";
			}
		});
	
		$("#dica"+tab).html("<img src='images/spinner.gif' /> Processando, aguarde...");

		$.ajax({
			type: "POST",
			url: "ajax_produto.php",
			dataType: "html",
			timeout: 5000,
			data: {action: action, tab: tab, id: id, propriedade:propriedade},
			success: function(data)
			{				
				ret = data.split("||");
				if(ret[0]=="ok"){
					$("#id").attr("value",ret[1]);
					$("#produtoid").html(ret[1]);
					$("#dica"+tab).html("Registro salvo com sucesso<br/>");
				}else{
					$("#dica"+tab).html("Erro ao gravar <br /><br />Erro: " + ret[2]);	
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				$("#dica"+tab).html("Erro ao gravar");
			}
		});		

	}	
	
	
	
	
	
	
	if(tab==6){	//Imagens
		var tipo = document.getElementById("tipo");
		var principal = document.getElementById("principal");
		var arquivo = document.getElementById("arquivo");
		var titulo = document.getElementById("titulo");
	
		if(trim(tipo.value)==0 || trim(tipo.value)==""){
			alert("O tipo de ve ser infromado");
			tipo.focus();
			$("#btn-cmd-gravar-"+tab).attr("disabled",0);
			return false;	
		}
		
		if(trim(tipo.value)==3 || trim(tipo.value)=="3"){
			$("#principal").attr("checked", false);
			if( trim(titulo.value)=="" ){
				alert("O titulo deve ser informado");
				titulo.focus();
				return false;
			}
		}
		
		principal = principal.checked==false ? "0" : "-1";
		
		if(trim(arquivo.value)==""){
			alert("O arquivo de ve ser infromado");
			arquivo.focus();
			$("#btn-cmd-gravar-"+tab).attr("disabled",0);
			return false;	
		}
	
	
		$("#dica"+tab).html("<img src='images/spinner.gif' /> Enviando arquivo, aguarde...");		
		
		//$('#frm_produto').append('<input name="action" id="action" type="hidden" value="gravar" />');
		document.getElementById("frm_produto").action = 'ajax_produto.php';
		document.getElementById("frm_produto").target = 'upload_target';
		document.getElementById("frm_produto").submit();		
	}
	
	
	
	if(tab==7){	//Condicao de pagamento
		var condicao="";
		
		$.each($('.condicao'), function(key, value) { 
			var $this = $( this ); 
			if($this.attr("checked")){
				condicao = condicao + $this.attr("value")+",";
			}
		});
	
			$("#dica"+tab).html("<img src='images/spinner.gif' /> Processando, aguarde...");
			

			$.ajax({
				type: "POST",
				url: "ajax_produto.php",
				dataType: "html",
				timeout: 5000,
				data: {action: action, tab: tab, id: id, condicao:condicao},
				success: function(data)
				{				
					ret = data.split("||");
					if(ret[0]=="ok"){
						$("#id").attr("value",ret[1]);
						$("#produtoid").html(ret[1]);
						$("#dica"+tab).html("Registro salvo com sucesso<br/>");
					}else{
						$("#dica"+tab).html("Erro ao gravar <br /><br />Erro: " + ret[2]);	
					}
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					$("#dica"+tab).html("Erro ao gravar");
				}
			});		

	}
	
	
	


	if(tab==8){	//frete condicao
		var frete="";
		
		$.each($('.frete'), function(key, value) { 
			var $this = $( this ); 
			if($this.attr("checked")){
				frete = frete + $this.attr("value")+",";
			}
		});
		
	
			$("#dica"+tab).html("<img src='images/spinner.gif' /> Processando, aguarde...");
			

			$.ajax({
				type: "POST",
				url: "ajax_produto.php",
				dataType: "html",
				timeout: 5000,
				data: {action: action, tab: tab, id: id, frete:frete},
				success: function(data)
				{				
					ret = data.split("||");
					if(ret[0]=="ok"){
						$("#id").attr("value",ret[1]);
						$("#produtoid").html(ret[1]);
						$("#dica"+tab).html("Registro salvo com sucesso<br/>");
					}else{
						$("#dica"+tab).html("Erro ao gravar <br /><br />Erro: " + ret[2]);	
					}
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					$("#dica"+tab).html("Erro ao gravar");
				}
			});		

	}
	
	
	setTimeout('$("#btn-cmd-gravar-"'+tab+').attr("disabled",0)',5000);
	return false;
}




function produto_excluir(id){

	if(!confirm("Deseja realmente excluir o produto ?\n Essa ação não poderá ser desfeita!")){
		return;
	}
	

	var action = "excluir";
	$.ajax({
        type: "POST",
        url: "produto_consulta.php",
        dataType: "html",
		timeout: 10000,
        data: {action: action, id: id},
        success: function(data)
        {
			if(data=="ok"){
				window.location.reload();			
			}
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	//alert(textStatus);
        }
    });
	
}



function produto_load_images(id){
	
	if(id==0){
		return;	
	}
	

	var action = "load_images";
	
	$.ajax({
        type: "POST",
        url: "ajax_produto.php",
        dataType: "html",
		timeout: 10000,
        data: {action: action, id: id},
        success: function(data)
        {
			$("#td-img-produto").html(data);	
			//$("#x").text(data);
			document.getElementById("midiaid").value = "0";
			//$("#arquivo").attr("value","");
			$("#btn-cmd-gravar-6").attr("value","Enviar");
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	//alert(textStatus);
        }
    });
	
	//carrega oa arquivos tb
	produto_load_arquivos(id);	
	
}

function produto_load_arquivos(id){
	
	if(id==0){
		return;	
	}

	var action = "load_arquivos";
	
	$.ajax({
        type: "POST",
        url: "ajax_produto.php",
        dataType: "html",
		timeout: 10000,
        data: {action: action, id: id},
        success: function(data)
        {
			$("#td-arquivo-produto").html(data);	
			//$("#x").text(data);
			document.getElementById("midiaid").value = "0";
			//$("#arquivo").attr("value","");
			$("#btn-cmd-gravar-6").attr("value","Enviar");
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	//alert(textStatus);
        }
    });	
}




function produto_del_images(id, midiaid){
	
	if(id==0){
		return;	
	}

	$("#dica6").html("<img src='images/spinner.gif' /> Excluindo arquivo, aguarde...");		
	
	var action = "del_image";
	
	$.ajax({
        type: "POST",
        url: "ajax_produto.php",
        dataType: "html",
		timeout: 5000,
        data: {action: action, id: id, midiaid: midiaid},
        success: function(data)
        {
			//call
			//produto_load_images(id);
			$("#midiaid").attr("value","0");
			$("#img_produto_"+midiaid).hide();
			$("#dica6").html("Arquivo excluido com sucesso");		
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	//alert(textStatus);
        }
    });	
}


function produto_filtro_ordem(categoria){
	document.getElementById("categoria").value = categoria;
	document.getElementById("action").value = "filtro";
	$("#frm_ordem").submit();
}


function valida_produto_ordem(){
	var produto="";
	
	$.each($('.produto'), function(key, value) { 
		var $this = $( this ); 
		produto = produto + $this.attr("value")+",";
	});	
	
	$("#categoria").attr("value",$("#categoriaid").attr("value"));
	$("#produto").attr("value",produto);

}





function valida_estoque(){
	var tipo = 0;
	var id = document.getElementById("id");
	var quantidade = document.getElementById("quantidade");
	
	
	if(id.value==0){
		alert("Erro ao carregar informações do produto");
		return false;
	}
	
	
	$.each($('.jtipo'), function(key, value) { 
		var $this = $( this ); 
		if($this.attr("checked")){
			tipo = $this.attr("value");
		}
	});	
	
	if(tipo==0){
		alert("Selecione o tipo de lançamento: entrada ou saída.");
		return false;
	}
	
	if(!isNumberVA(quantidade.value, 2)){
		alert("A quantidade deve ser informada");
		quantidade.focus();
		return false;
	}
	
	
	if(!confirm("Deseja realmente efetuar este lançamento?")){
		return false;	
	}
	
	
	
}





/*--------------------------------------------------------------------------------
categoria
--------------------------------------------------------------------------------*/

function valida_categoria(){

	var categoria = document.getElementById("categoria");
	var link_seo = document.getElementById("link_seo");
	
	var data_inicio = document.getElementById("data_inicio");
	var data_termino = document.getElementById("data_termino");

	if(trim(categoria.value)==""){
		alert("A categoria deve ser informada");
		categoria.focus();
		return false;
	}

	if(trim(link_seo.value)==""){
		alert("A url virtual (link-seo) deve ser informada");
		link_seo.focus();
		return false;
	}
	
	if(trim(data_inicio.value)==""){
		data_inicio.value = "01/01/2012 00:00:00";
	}

	if(trim(data_termino.value)==""){
		data_termino.value = "31/12/2012 23:59:59";
	}	

}



function valida_categoria_ordem(){
	var categoria="";
	
	$.each($('.categoria'), function(key, value) { 
		var $this = $( this ); 
		categoria = categoria + $this.attr("value")+",";
	});	
	
	$("#categoria").attr("value",categoria);

}


function categoria_excluir(id){
	if(confirm("Deseja realmente excluir a categoria selecionada ?")){

		var action = "excluir";
		$.ajax({
			type: "POST",
			url: "categoria.php",
			dataType: "html",
			timeout: 5000,
			data: {action: action, id: id},
			success: function(data)
			{
				if(data=="ok"){
					window.location.reload();
				}else{
					alert(data);
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				//alert(textStatus);
			}
		});		
	}	
}

function categoria_artigo_excluir(id){
	if(confirm("Deseja realmente excluir a categoria selecionada ?")){

		var action = "excluir";
		$.ajax({
			type: "POST",
			url: "categoria_artigo.php",
			dataType: "html",
			timeout: 5000,
			data: {action: action, id: id},
			success: function(data)
			{
				if(data=="ok"){
					window.location.reload();
				}else{
					alert(data);
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				//alert(textStatus);
			}
		});		
	}	
}

function artigo_excluir(id){
	if(confirm("Deseja realmente excluir o artigo selecionado ?")){

		var action = "excluir";
		$.ajax({
			type: "POST",
			url: "artigo.php",
			dataType: "html",
			timeout: 5000,
			data: {action: action, id: id},
			success: function(data)
			{
				if(data=="ok"){
					window.location.reload();
				}else{
					alert(data);
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				//alert(textStatus);
			}
		});		
	}	
}

/*--------------------------------------------------------------------------------
marca
---------------------------------------------------------------------------------*/
function valida_marca(){
	var id = document.getElementById("id");
	var marca = document.getElementById("marca");
	var descricao = document.getElementById("descricao");
	var link_seo = document.getElementById("link_seo");
	var texto = String(CKEDITOR.instances.editor1.getData());
	var imagem = document.getElementById("arquivo");
	
	if(trim(marca.value)==""){
		alert("A marca deve ser informada");
		marca.focus();
		return false;
	}

	if(trim(descricao.value)==""){
		alert("A descrição deve ser informada");
		descricao.focus();
		return false;
	}
	
	if(trim(link_seo.value)==""){
		alert("A url amigável deve ser informada");
		link_seo.focus();
		return false;
	}
	
	
	if(id.value==0 && trim(arquivo.value)==""){
		alert("A imagem deve ser informada");
		arquivo.focus();
	}
	
	arquivo.value = arquivo.value.toLowerCase();
	
	if(trim(arquivo.value)!=""){
		if(right(arquivo.value,4)!=".jpg" && right(arquivo.value,4)!=".gif" && right(arquivo.value,4)!=".png"){
			alert("O formato do arquivo é inválido");
			arquivo.focus();
			return false;
		}
	}
	
	
}

function marca_excluir(id){
	if(confirm("Deseja realmente excluir a marca selecionada ?")){

		var action = "excluir";
		$.ajax({
			type: "POST",
			url: "marca.php",
			dataType: "html",
			timeout: 5000,
			data: {action: action, id: id},
			success: function(data)
			{
				if(data=="ok"){
					window.location.reload();
				}else{
					alert(data);
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				//alert(textStatus);
			}
		});		
	}	
}


function valida_marca_ordem(){
	var marca="";
	
	$.each($('.marca'), function(key, value) { 
		var $this = $( this ); 
		marca = marca + $this.attr("value")+",";
	});	
	
	$("#marca").attr("value",marca);

}






/*--------------------------------------------------------------------------------
pedido status
---------------------------------------------------------------------------------*/
function valida_pedido_status(){
	var id = document.getElementById("id");

	if(id.value==0 || id.value>8){
		alert("Selecione um status da lista para atualizar");
		return false;
	}
}


function pedido_print(id){
	window.open("pedido_print.php?id="+id,"_pedido_print","");	
}


/*-----------------------------------------------------------------------------
NF-e
------------------------------------------------------------------------------*/
function pedido_nfe(id){
	if(!confirm("Deseja realmente gerar nota fiscal para o pedido "+id+"?")){
		return;
	}
	pedido_nfe_gera_txt(id);
}


function pedido_nfe_gera_txt(id){
		
		show_processando("Aguarde, gerando arquivo txt...",1);
		
		var action = "gera_txt";
		$.ajax({ type: "POST", url: "ajax_nfe.php", dataType: "html", timeout: 30000, data: {action: action, id: id}, success: function(data){
				var ret = data.split("||");	//[0] = resposta // [1] retorno
				if(ret[0]=="ok"){
					//chama a funcao que gera o xml	
					pedido_nfe_gera_xml(id);				
				}else{
					//alert(ret[1]);
					show_processando(ret[1],2);
					return;
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert(textStatus + " " + errorThrow);
			}
		});		

}


function pedido_nfe_gera_xml(id){
	
		show_processando("Aguarde, gerando xml...",1);
		
		var action = "gera_xml";
		$.ajax({ type: "POST", url: "ajax_nfe.php", dataType: "html", timeout: 30000, data: {action: action, id: id}, success: function(data){
				var ret = data.split("||");	//[0] = resposta // [1] retorno
				if(ret[0]=="ok"){
					//chama a funcao que assina o xml
					pedido_nfe_gera_assina(id);				
				}else{
					//alert(ret[1]);
					show_processando(ret[1],2);
					return;
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert(textStatus + " " + errorThrow);
			}
		});			
		
}


function pedido_nfe_gera_assina(id){
	
		show_processando("Aguarde, assinando xml...",1);
		
		var action = "assina_xml";
		$.ajax({ type: "POST", url: "ajax_nfe.php", dataType: "html", timeout: 30000, data: {action: action, id: id}, success: function(data){
				var ret = data.split("||");	//[0] = resposta // [1] retorno
				if(ret[0]=="ok"){
					//chama a funcao que valida o xml
					pedido_nfe_valida_xml(id);				
				}else{
					//alert(ret[1]);
					show_processando(ret[1],2);
					return;
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert(textStatus + " " + errorThrow);
			}
		});		

}				


function pedido_nfe_valida_xml(id){
	
		show_processando("Aguarde, validando xml...",1);
		
		var action = "valida_xml";
		$.ajax({ type: "POST", url: "ajax_nfe.php", dataType: "html", timeout: 30000, data: {action: action, id: id}, success: function(data){
				var ret = data.split("||");	//[0] = resposta // [1] retorno
				if(ret[0]=="ok"){
					//chama a funcao que envia o xml
					pedido_nfe_envia(id);				
				}else{
					//alert(ret[1]);
					show_processando(ret[1],2);
					return;
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert(textStatus + " " + errorThrow);
			}
		});			
}



function pedido_nfe_envia(id){
	
		show_processando("Aguarde, enviando xml...",1);
		
		var action = "envia_xml";
		$.ajax({ type: "POST", url: "ajax_nfe.php", dataType: "html", timeout: 30000, data: {action: action, id: id}, success: function(data){
				var ret = data.split("||");	//[0] = resposta // [1] retorno
				if(ret[0]=="ok"){
					//chama a funcao que recebe o recibo
					show_processando("Aguarde, solicitando recibo de entrega...",1);
					
					pedido_nfe_recibo(id,ret[1]);
					
				}else{
					//alert(ret[1]);
					show_processando(ret[1],2);
					return;
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert(textStatus + " " + errorThrow);
			}
		});	
		
}

function pedido_nfe_recibo(id, recibo){

		show_processando("Aguarde, abrindo recibo...",1);
		
		var action = "recibo";
		$.ajax({ type: "POST", url: "ajax_nfe.php", dataType: "html", timeout: 30000, data: {action: action, id: id, recibo: recibo}, success: function(data){
				var ret = data.split("||");	//[0] = resposta // [1] retorno
				if(ret[0]=="ok"){
					//chama a funcao que gera a danfe
					show_processando("Aguarde, adicionando protocolo do recibo...",1);
					
					pedido_nfe_danfe(id);
					
				}else{
					//alert(ret[1]);
					show_processando(ret[1],2);
					return;
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert(textStatus + " " + errorThrow);
			}
		});	

}				


function pedido_nfe_danfe(id){
	
		show_processando("Aguarde, gerando arquivo danfe...",1);
		
		var action = "danfe";
		$.ajax({ type: "POST", url: "ajax_nfe.php", dataType: "html", timeout: 30000, data: {action: action, id: id}, success: function(data){
				var ret = data.split("||");	//[0] = resposta // [1] retorno
				if(ret[0]=="ok"){
					
					show_processando(ret[1],2);

					//mostra os dados em tela
					$("#notafiscalid").html(ret[2]);
					
					$("#danfe_chave").html(ret[3]); 
					$("#danfe_url").attr("href",ret[4]);
					

				}else{
					//alert(ret[1]);
					
					show_processando(ret[1],2);
					return;
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert(textStatus + " " + errorThrow);
			}
		});	
		
}




function show_processando(texto,acao){
	if(acao==1){
		$("#modal-info").fadeIn();
		$("#modal-conteudo").html(_spinner + "&nbsp;"+texto);	
	}
	
	if(acao==0){
		$("#modal-info").fadeOut(3000);
		$("#modal-conteudo").html(texto);	
	}

	if(acao==2){
		//$("#modal-info").fadeOut(10000);
		$("#modal-conteudo").html(texto);	
	}

	
	
}

/*---------------------------------------------------------------------------------
Orçamento
----------------------------------------------------------------------------------*/
function orcamento_print(id){
	window.open("orcamento_print.php?id="+id,"_orcamento_print","");	
}







/*--------------------------------------------------------------------------------
forma de pagamento
---------------------------------------------------------------------------------*/
function valida_forma_pagamento(){
	var tipo = document.getElementById("tipo");
	var forma_pagamento = document.getElementById("forma_pagamento");
	var descricao = document.getElementById("descricao");
	var bandeira = document.getElementById("bandeira");
	
	if(tipo.value==0){
		alert("O tipo de pagamento deve ser informado");
		tipo.focus();
		return false;
	}
	
	if(trim(forma_pagamento.value)==""){
		alert("A forma de pagamento deve ser informada");
		forma_pagamento.focus();
		return false;
	}
	
	if(trim(descricao.value)==""){
		descricao.value = forma_pagamento.value;	
	}
	
	if(tipo.value==2 || tipo.value==3){
		if(trim(bandeira.value)==""){
			alert("A bandeira do cartão deve ser informada");
			bandeira.focus();
			return false;
		}	
	}
	
	
}


function forma_pagamento_excluir(id){
	if(confirm("Deseja realmente excluir a forma de pagamento selecionada ?")){

		var action = "excluir";
		$.ajax({
			type: "POST",
			url: "forma_pagamento.php",
			dataType: "html",
			timeout: 5000,
			data: {action: action, id: id},
			success: function(data)
			{
				if(data=="ok"){
					window.location.reload();
				}else{
					alert(data);
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert("Erro ao excluir a forma de pagamento");
			}
		});		
	}	
}










/*--------------------------------------------------------------------------------
condicao pagamento
--------------------------------------------------------------------------------*/
function valida_condicao_pagamento(){
	var forma_pagamento = document.getElementById("forma_pagamento");
	var condicao = document.getElementById("condicao");
	var descricao = document.getElementById("descricao");
	var numero_parcelas = document.getElementById("numero_parcelas");
	
	if(forma_pagamento.value==0){
		alert("A forma de pagamento deve ser informada");
		forma_pagamento.focus();
		return false;
	}
	
	if(trim(condicao.value)==""){
		alert("A condicao de pagamento deve ser informada");
		condicao.focus();
		return false;
	}
	
	if(trim(descricao.value)==""){
		descricao.value = condicao.value;	
	}
	
	
	if(!isNumber(numero_parcelas.value) || parseInt(numero_parcelas.value)==0){
		alert("O número de parcelas deve ser informado em formato numérico");
		numero_parcelas.focus();
		return false;
	}
	
	
}


function condicao_pagamento_excluir(id){
	if(confirm("Deseja realmente excluir a condição de pagamento selecionada ?")){

		var action = "excluir";
		$.ajax({
			type: "POST",
			url: "condicao_pagamento.php",
			dataType: "html",
			timeout: 5000,
			data: {action: action, id: id},
			success: function(data)
			{
				if(data=="ok"){
					window.location.reload();
				}else{
					alert(data);
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert("Erro ao excluir a condição de pagamento");
			}
		});		
	}	
}



/*--------------------------------------------------------------------------------
bannner
--------------------------------------------------------------------------------*/
function valida_banner(){
	var id = document.getElementById("id");
	var tipo = document.getElementById("tipo");
	var titulo = document.getElementById("titulo");
	var descricao = document.getElementById("descricao");
	
	var slink = document.getElementById("link");
	var target = document.getElementById("target");
	
	var data_inicio = document.getElementById("data_inicio");
	var data_termino = document.getElementById("data_termino");
	
	var arquivo = document.getElementById("arquivo");
	
	if(tipo.value==0){
		alert("O tipo deve ser informado");
		tipo.focus();
		return false;
	}
	
	if(trim(titulo.value)==""){
		alert("O título deve ser informado");
		titulo.focus();
		return false;
	}

	if(trim(descricao.value)==""){
		descricao.value = titulo.value;
	}
	
	if(trim(slink.value)==""){
		alert("O link deve ser informado");
		slink.focus();
		return false;
	}
	else
	{
		slink.value = slink.value.toLowerCase();
		
		if(left(slink.value,4)=="www."){
			slink.value = "http://"+slink.value;
		}		
		
	}
	
	if(target.value=="0"){
		alert("Selecione o destino do link");
		target.focus();
		return false;
	}


	if(trim(data_inicio.value)==""){
		alert("A data de início deve ser informado");
		data_inicio.focus();
		return false;
	}

	if(trim(data_termino.value)==""){
		alert("A data de término deve ser informada");
		data_termino.focus();
		return false;
	}


	if(id.value==0 && trim(arquivo.value)==""){
		alert("A imagem deve ser informada");
		arquivo.focus();
	}
	
	arquivo.value = arquivo.value.toLowerCase();
	
	if(trim(arquivo.value)!=""){
		if(right(arquivo.value,4)!=".jpg" && right(arquivo.value,4)!=".gif" && right(arquivo.value,4)!=".png"){
			alert("O formato do arquivo é inválido");
			arquivo.focus();
			return false;
		}
	}
	
	
}


function banner_excluir(id){
	if(confirm("Deseja realmente excluir o banner selecionado ?")){
		var action = "excluir";
		$.ajax({
			type: "POST",
			url: "banner.php",
			dataType: "html",
			timeout: 5000,
			data: {action: action, id: id},
			success: function(data)
			{
				if(data=="ok"){
					window.location.reload();
				}else{
					alert(data);
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				//alert(textStatus);
			}
		});		
	}	
}




function valida_conteudo(){
	
}


function habilita_botao(id){
	$(id).attr("disabled",0);	
}



function gera_link_seo(texto, destino){
	var action = "url_seo";
	
	$.ajax({
        type: "POST",
        url: "ajax_produto.php",
        dataType: "html",
		timeout: 5000,
        data: {action: action, string: texto},
        success: function(data)
        {
			data = data.toLowerCase();
			$("#"+destino).attr("value",data);		
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	//alert(textStatus);
        }
    });	
}


/*---------------------------------------------------------------------------------------*/

function busca_auto_completa(){
	var str = document.getElementById("busca");
	if(str.value.length<2){
		return;	
	}
	$.ajax({
        type: "GET",
        url: "ajax_busca.php",
        dataType: "html",
		timeout: 5000,
        data: {string: str.value},
        success: function(data)
        {
			str = data.split(",")
			busca_auto_completa_show(str);		
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        	//alert(textStatus);
        }
    });		
}

function busca_auto_completa_show(tags){
	$("input#busca").autocomplete({									
		source: tags,
		minLength: 0,
		autoFocus: true,
		delay: 0
	});	   
}








function seta_field_valor(campo, valor){
	
	if(trim(campo.value)==""){
		campo.value = valor;
	}	
}

function clear_field(campo, valor){
	if(campo.value==valor){
		campo.value = "";
	}
}


function goto(url){
	window.open(url, "_parent");
}


function go_to(url,target,parameter){
	window.open(url, target, paremeter);
}


function paginacao(parametro, valor){
	filtro_valor(parametro, valor);
}



function exportar(url_export){
	var url = "?";
	if(variaveis!=""){
		for(i=0;i<variaveis.length;i++){
			nvar=variaveis[i].split("=")
			url += "&" + nvar[0]+"="+unescape(nvar[1]);
		}
	}
	window.open(url_export+url,"");
	
}



/*-----------------------------------------------------------------
GATEWAY
------------------------------------------------------------------*/
function gateway_cielo(){
	
	$(".btn-gravar").attr("disabled",true);
	
	var pedido 	= document.getElementById("pedido");
	var tid		= document.getElementById("tid");
	var acao 	= document.getElementById("acao");
	var valor	= document.getElementById("valor");
	
	
	if(trim(pedido.value)==""){
		alert("O pedido deve ser informado");
		pedido.focus();
		$(".btn-gravar").attr("disabled",false);
		return false;
	}
	
	if(trim(tid.value)==""){
		alert("O tid deve ser informado");
		tid.focus();
		$(".btn-gravar").attr("disabled",false);
		return false;
	}
	
	
	if(trim(acao.value)=="0"){
		alert("Selecione a ação");
		acao.focus();
		$(".btn-gravar").attr("disabled",false);
		return false;
	}


	if(!isNumberVA(valor.value,2)){
		alert("O valor deve ser informado");
		valor.focus();
		$(".btn-gravar").attr("disabled",false);
		return false;
	}

	
	
	
	if(acao.value=="1"){
		action = "cancelar-pagamento";	
	}
	
	if(acao.value=="9"){
		action = "consultar-transacao";	
	}	
	
	$("#status").html('<img src="images/spinner.gif" border="0" /> Processando, aguarde...');	

	$.ajax({
		type: "POST",
		url: "ajax_gateway.php",
		dataType: "html",
		timeout: 20000,
		data: {action: action, id: pedido.value, tid: tid.value, valor: valor.value },
		success: function(data)
		{
			var ret = data.split("||");
			$("#status").html(ret[1]);
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			$("#status").html("&nbsp;");
			alert(textStatus + " " + errorThrown);
		}
	});	
	
	$(".btn-gravar").attr("disabled",false);
	return false;
}



/*-------------------------------------------------------------------
CLEAR SALE
--------------------------------------------------------------------*/
function pedido_clear_sale(id){
	if(!confirm("Deseja realmente submeter o pedido a clear sale?")){
		return;	
	}	
	
	$.colorbox({href:"ajax_clear_sale.php?id="+id});
}


/*-----------------------------------------------------------------*
  Descricao   : Query String
*-----------------------------------------------------------------*/

qs=new Array()
variaveis=location.search.replace(/\x3F/,"").replace(/\x2B/g," ").split("&");
if(variaveis!=""){
	for(i=0;i<variaveis.length;i++){
		nvar=variaveis[i].split("=");
		qs[nvar[0]]=unescape(nvar[1]);
	}
}

function query_string(variavel){
	return qs[variavel];
}



/*-----------------------------------------------------------------*
  Descricao   : Tira brancos à esquerda e à direita
 *-----------------------------------------------------------------*/
function trim(s)
{
    return s.replace (/^\s+/,'').replace (/\s+$/,'');
}




/*-----------------------------------------------------------------------------------------*
 Descricao...: pega X caratres a esquerda
 Paramentros.: data: 
 Retorno.....: string
 *-----------------------------------------------------------------------------------------*/
function left(texto, posicao){
	
	return texto.substr(0,posicao);

}




/*-----------------------------------------------------------------------------------------*
 Descricao...: pega X caratres a direita
 Retorno.....: string
 *-----------------------------------------------------------------------------------------*/
function right(texto, posicao){
	
	return texto.substr(texto.length-posicao , posicao);

}




/*-----------------------------------------------------------------------------------------*
 Descricao...: pega X caratres da posicao informada
 Retorno.....: string
 *-----------------------------------------------------------------------------------------*/
function mid(texto, inicio, tamanho){
	
	return texto.substr(inicio , tamanho);

}


function get_only_numbers(texto){
	var numero = "";

	valor = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

	for (n = 0; n < texto.length; n++)
	{

		for (a = 0; a < 10; a++)
		{
			if (texto.substr(n, 1) == valor[a])
			{
				numero = numero + texto.substr(n, 1);
				break;
			}
		}
	}

	return numero;
}




/*-----------------------------------------------------------------*
  IsNumber
  Descricao   : Verifica se e' um numero
  Parametros  : Numero que contém dígitos de 0 a 9
    Atualizacoes: [00] Versao Inicial    Data: 22/03/2006     Autor: Fábio Calixto
 *-----------------------------------------------------------------*/
function isNumber (Numero)
{
   var i;
   if (Numero == "")
      return false;
   for (i=0; i < Numero.length; i++)
   {
   		if(Numero.charAt(i) != ","){   
		  if (Numero.charAt(i) < "0" || Numero.charAt(i) > "9")
			 return false;
		}   
   }
   return true;
}


/*-----------------------------------------------------------------*
 | isNumber      Retorna True se o String dada for um número      |
 |                com casas decimais dadas.                        |
 *-----------------------------------------------------------------*/
function isNumberVA(sNumero, iDecimais) {

  sNumero = sNumero.replace(".","");	//remove o ponto do milhar

  var bRet
  var i
  bRet = true
  if (iDecimais > 0)
    {
    if ((sNumero.length < iDecimais + 2) || (sNumero.indexOf(",", 0) == -1))
      bRet = false
    }
  if (bRet)
    {
    i = 0
    while(i < sNumero.length && bRet)
      {
      if (iDecimais > 0)
        {
        if (i == sNumero.length - (iDecimais + 1))
          {
          if (sNumero.charAt(i) != ",")
            bRet = false
          }
        else
          {
          if (sNumero.charAt(i) < "0" || sNumero.charAt(i) > "9")
            bRet = false
          }
        }
      else
        {
        if (sNumero.charAt(i) < "0" || sNumero.charAt(i) > "9")
          bRet = false
        }
      i++
      }
    }
  return bRet
}



/*-----------------------------------------------------------------------------------------*
 Descricao...: Verifica se a data é valida
 Retorno.....: true(ok) ou false(nok)
 *-----------------------------------------------------------------------------------------*/
function isDate(data){

	var dia = data.substr(0,2);
	var mes = data.substr(3,2);
	var ano = data.substr(6,4);


	if (data.substr(2,1)!="/" || data.substr(5,1)!="/"){
		return false;
	}

	if (data.length<10){
		return false;
	}

	if (isNaN(dia) || isNaN(mes) || isNaN(ano)){
		return false;
	}

	if(dia < 0 || dia > 31 || mes < 0 || mes > 12 || ano < 1800 || ano > 2100){
		return false;	
	}

	if(mes == "04" && dia > 30 || mes=="06" && dia > 30 || mes=="09" && dia > 30 || mes=="11" && dia > 30){
		return false;	
	}

	if(mes == "02" && dia > 28){
		return false;	
	}

	return true;

}

/*-----------------------------------------------------------------------------------------*
 Descricao...: Verifica se o E-mail é válido
 Paramentros.: email: String de E-mail
 Retorno.....: true(ok) ou false(nok)
 *-----------------------------------------------------------------------------------------*/
function ValidaEmail(email)
{
   var aux;
   //checando se a string não é vazia
   if((email.replace (/^\s+/,'').replace (/\s+$/,'') == ""))
   {
      return false;
   }
   //checando se existe pelo menos uma arroba e pelo menos algum ponto
   if((email.indexOf("@") == -1)||(email.indexOf(".") == -1))
   {
      return false;
   }
   //checando se a string tem pelo menos 5 caracteres
   if(email.length<5)
   {
      return false;
   }
   //checando se existe brancos
   if(email.indexOf(" ") != -1)
   {
      return false;
   }
   //checando se depois de . não tem outra @, ou um ponto ou espaço
   if((email.substr(email.lastIndexOf(".")+1,1) == "")||(email.substr(email.indexOf(".")+1,1) == "@")||(email.substr(email.indexOf(".")+1,1) == "."))
   {
      return false;
   }
   //checando se depois de @ não tem outra @, ou um ponto ou espaço
   if((email.substr(email.lastIndexOf("@")+1,1) == "")||(email.substr(email.indexOf("@")+1,1) == "@")||(email.substr(email.indexOf("@")+1,1) == "."))
   {
      return false;
   }
   //procurando por mais de uma @
   aux = email.substr(email.indexOf("@")+1);
   if(aux.indexOf("@") != -1)
   {
      return false;
   }
   //checando se o primeiro caracter é @
   if(email.substr(0, 1) == "@")
   {
      return false;
   }
   return true;
}



/*-----------------------------------------------------------------------------------------*
 Nome........: mOver
 Descricao...: seta luz de foco na TR da tabela
 Paramentros.: campo , cor: 
 Atualizacoes: [00] Versao Inicial    Data: 26/10/2007     Autor: Fábio Calixto
 *-----------------------------------------------------------------------------------------*/
function mOvr(src,clrOver) { 
	if (!src.contains(event.fromElement)) {
		src.bgColor = clrOver;
	}
} 

/*-----------------------------------------------------------------------------------------*
 Nome........: mOut
 Descricao...: seta luz de foco na TR da tabela
 Paramentros.: campo , cor: 
 Atualizacoes: [00] Versao Inicial    Data: 26/10/2007     Autor: Fábio Calixto
 *-----------------------------------------------------------------------------------------*/
function mOut(src,clrIn) {
	if (!src.contains(event.toElement)) {
	  	src.bgColor = clrIn;
	}
} 


setTimeout("verifica_resolucao()",300);

function verifica_resolucao(){
	
		var browser = navigator.userAgent.toLowerCase();
		
		if(browser.indexOf("iphone")>0 || browser.indexOf("ipad")>0){
			$("#dica1").hide();
			$("#td_dica1").html("");
			$("#td_dica1").attr("width","1px");
			return;	
		}
	
		var largura = $(document).width();	
		largura = largura - 254;
		console.log(largura);
		document.getElementById('content').style.width= largura+'px';
}

function filtro_valor(parametro1, valor1,parametro2, valor2){

	try{
		var url = decodeURIComponent(document.URL);
	}
	catch(err){
	  var url = document.URL;
	}
	
	var pos = url.indexOf("?");
	
	if(pos==-1){
		url = url.replace(/\/$/, "");
		url += "/";	
	}else{
		url = url.substr(0,pos);
	}
	
	url += "?"+parametro1+"="+valor1;
	if(typeof(parametro2) != 'undefined'){
		url += "&"+parametro2+"="+valor2;
	}

	if(variaveis!=""){
		for(i=0;i<variaveis.length;i++){
			nvar=variaveis[i].split("=")
			if( nvar[0] != parametro1 && nvar[0] != parametro2 ){
				url += "&" + nvar[0]+"="+nvar[1];
			}
		}
	}
	
	window.location.href = url;
	
}