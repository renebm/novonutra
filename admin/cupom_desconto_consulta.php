<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$uri = str_replace("/evoluamoda/","",$_SERVER['REQUEST_URI']);	

	$pagina = 1;
	$start = 0;
	$limit = 20;


	/*--------------------------------------------------------------------------
	exportar
	----------------------------------------------------------------------------*/
	if( $_REQUEST['action']=='exportar' ){
	
		header("Content-type: application/vnd.ms-excel");
		header("Content-type: application/force-download");
		header("Content-Disposition: attachment; filename=report_cupom_desconto.csv");
		header("Pragma: no-cache");		
		
		echo utf8_decode("#ID;Titulo;E-mail;Código;Desconto;Validade;Status;\r\n");
		
		$ssql = $_SESSION['ssql'];
		$result = mysql_query($ssql);
		if($result){
			
			
			while($row=mysql_fetch_assoc($result)){
				
				$status = ($row["cstatus"]==-1) ? "Ativo" : "Inativo";
				
				$desconto = number_format($row["cdesconto"],2,",",".");
				
				if($row["ccodtipo"]==1){
					$desconto .= "%";
				}
				else
				{
					$desconto = "R$ ". $desconto;
				}
			
				echo $row["cupomid"].";";
				echo $row["ctitulo"].";";
				echo $row["cemail"].";";
				echo utf8_decode(" " . $row["ccupom"]).";";
				echo $desconto.";";
				echo formata_data_tela($row["cdata_validade"]).";";
				echo $status.";";			
				echo "\r\n";			
						
			}
			mysql_free_result($result);			
		}
		die();
	}


	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$('#string').focus();
	});
  

</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="geral">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    

    	<div id="conteudo">
			<div id="titulo-consulta">
            	<span class="label-inicio">Cupom de Desconto &raquo; <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='cupom_desconto_consulta.php';">Consulta</span></span> <a href="cupom_desconto.php">Novo Registro</a>
            </div>
            
            <div id="conteudo-interno">
            <?php 
				if (!$_GET){
			?>
             <form name="frm_consulta" id="frm_consulta" method="get" action="?" >
             <table width="99%" border="0" cellspacing="2" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="50%">&nbsp;</td>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>Código do Cupom</td>
                <td>Status:</td>
               </tr>
              <tr>
                <td><input name="string" type="text" class="formulario" id="string" size="75" maxlength="200" /></td>
                <td><select name="status" size="1" id="status" class="formulario">
                  <option value="0" <?php if($tipo==0)echo "selected";?>>Selecione</option>
                  <option value="-1" <?php if($tipo==-1)echo "selected";?>>Ativo</option>
                  <option value="1" <?php if($tipo==1)echo "selected";?>>Utilizado</option>
                </select></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>E-mail</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><input name="email" type="text" class="formulario" id="email" size="75" maxlength="200" /></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><input type="submit" id="btn-cmd-busca" name="btn-cmd-busca" value="Consultar" class="btn-gravar" /></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
             </table>
             
             </form>
			<?php
				}else{
			?>	
                
                
                <table width="99%" border="0" cellspacing="0" cellpadding="3" style="margin:10px;">
                  <tr>
                    <td width="50" class="titulo_table">#ID</td>
                    <td class="titulo_table">T&iacute;tulo</td>
                    <!--<td align="center" class="titulo_table">E-mail</td>-->
                    <td width="120" align="center" class="titulo_table">Código</td>
                    <td width="100" align="center" class="titulo_table">Desconto</td>
                    <td align="center" class="titulo_table">Validade</td>
                    <td width="75" align="center" class="titulo_table">Status</td>
                    <td width="75" align="center" class="titulo_table">Editar</td>
                    <td width="75" align="center" class="titulo_table">Excluir</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <!--<td>&nbsp;</td>-->
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
                  
                  <?php
				  
				  	$string = addslashes($_REQUEST["string"]);
					$status = addslashes($_REQUEST["status"]);
					$email = addslashes($_REQUEST["email"]);

                  	$ssql = "select cupomid, ctitulo, ccupom, cemail, ccodtipo, cdesconto, cstatus, cdata_validade, ccodcategoria, ccodproduto, cminimo 
							from tblcupom_desconto
							where ccupom like '%{$string}%' 
							";	
					
					if($status != 0 ){
						$ssql .= " and ccodstatus=$status ";	
					}
					
					$ssql .= " or cemail like '{$email}' ";
					
					if($status != 0 ){
						$ssql .= " and ccodstatus=$status ";	
					}
										
					
					$ssql .= " order by cdata_validade, ctitulo";
	
					$_SESSION["ssql"] = $ssql;
	
					$result = mysql_query($ssql);
					if($result){
						$total_registros = mysql_num_rows($result);	
					}					
							
							
					$ssql .= " limit $start, $limit";
							
							//echo $ssql;
							
				  	$result = mysql_query($ssql);
				  	if($result){
					
						if(mysql_num_rows($result)==0){
							echo '
								  <tr>
									<td colspan="5">Nenhum registro localizado.</td>
								  </tr>';							
						}
					
					
						while($row=mysql_fetch_assoc($result)){
							$status = ($row["cstatus"]==-1) ? "Ativo" : "Inativo";
							
							$desconto = number_format($row["cdesconto"],2,",",".");
							
							if($row["ccodtipo"]==1){
								$desconto .= "%";
							}
							else
							{
								$desconto = "R$ ". $desconto;
							}

							
							echo '
								  <tr class="tr_lista">
									<td>'.$row["cupomid"].'</td>
									<td>'.$row["ccupom"].'</td>
									
									<td align="center">'.$row["ccupom"].'</td>
									<td align="center">'.$desconto.'</td>
									<td align="center">'.formata_data_tela($row["cdata_validade"]).'</td>
									<td align="center">'.$status.'</td>
									<td align="center"><a href="cupom_desconto.php?id='.$row["cupomid"].'"><img src="images/ico_editar.gif" border="0" /></a></td>
									<td align="center"><a href="javascript:void(0)" onclick="javascript:cupom_desconto_excluir('.$row["cupomid"].');"><img src="images/ico_excluir.gif" border="0" /></a></td>
								  </tr>							
							';
						}
						mysql_free_result($result);
					
					}
				  ?>
                  

                  
				  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
				  <tr>
				    <td colspan="9">
				      <div class="paginacao"><span class="paginacao-text">Página:</span> 
				        <?php
                        echo paginacao($pagina, $limit, $total_registros);
                        ?>                            
				        </div>			        </td>
			      </tr>
				  <tr>
				    <td colspan="9"><a href="?action=exportar">[ Exportar Excel ]</a></td>
			      </tr>

                </table>
                
                
                <?php
                
				}
				?>
                
          </div>
            
       </div>

 
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>