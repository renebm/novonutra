<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}

	$id = $_REQUEST["id"];	
	if(!is_numeric($id)){
		$id = 0;	
	}


	/*-----------------------------------------------------------------------
	ajax de excluisão
	------------------------------------------------------------------------*/
	if($_POST && $_REQUEST["action"]=="excluir"){
		
		$ssql = "select condicaoid from tblcondicao_pagamento where ccodforma_pagamento = '{$id}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)>0){
				echo "A forma de pagamento não pode ser excluida pois existe uma condição de pagamento associada a mesma.";
				exit();
			}	
		}


		$ssql = "select orcamentoid from tblorcamento where ocodforma_pagamento = '{$id}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)>0){
				echo "A forma de pagamento não pode ser excluida pois existem orçamentos/pedidos associados a mesma.";
				exit();
			}	
		}
		
		$ssql = "select pedidoid from tblpedido where pcodforma_pagamento = '{$id}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)>0){
				echo "A forma de pagamento não pode ser excluida pois existem pedidos associados a mesma.";
				exit();
			}	
		}


		$ssql = "select descontoid from tbldesconto where dcodforma_pagamento = '{$id}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)>0){
				echo "A forma de pagamento não pode ser excluida pois existem descontos associados a mesma.";
				exit();
			}	
		}


		
		$ssql = "delete from tblforma_pagamento where formapagamentoid='{$id}'";
		mysql_query($ssql);
		echo "ok";
		exit();
		
	}




	if($_POST && $_REQUEST["action"]=="gravar"){
		$id					= 	addslashes($_REQUEST["id"]);
		$tipo				=	addslashes($_REQUEST["tipo"]);
		$forma_pagamento	=	addslashes($_REQUEST["forma_pagamento"]);
		$descricao			=	addslashes($_REQUEST["descricao"]);
		$instrucao_pagamento=	addslashes($_REQUEST["instrucao_pagamento"]);
		$ativa				=	addslashes($_REQUEST["ativa"]);
		$exibe_imagem		=	addslashes($_REQUEST["exibe_imagem"]);
		$bandeira			=	addslashes($_REQUEST["bandeira"]);
		$imagem				=	addslashes($_REQUEST["imagem"]);

		if(!is_numeric($tipo)){
			$ativa = 0;	
		}

		if(!is_numeric($exibe_imagem)){
			$exibe_imagem = 0;	
		}

		if(!is_numeric($ativa)){
			$ativa = 0;	
		}
			
		
		if($id==0){
			
			//verifica se nao existe ja uma descricao com o mesmo nome;
			$ssql = "select formapagamentoid from tblforma_pagamento where fcodtipo='{$tipo}' and fforma_pagamento='{$forma_pagamento}' and fbandeira='{$bandeira}'";
			$result = mysql_query($ssql);
				if($result){
					$num_rows = mysql_num_rows($result);
					if($num_rows>0){
						$msg = "Já existe um registro com os mesmos dados digitados";
						$erro = 1;
					}	
				}
				
			//--------------------------------------------------------	
			
			if($num_rows==0){
				$ssql = "insert into tblforma_pagamento (fcodtipo, fforma_pagamento, fdescricao, finstrucao_pagamento, fbandeira, fativa, fexibe_imagem, fimagem, fordem,
														 fcodusuario, fdata_alteracao, fdata_cadastro) 
							values('{$tipo}','{$forma_pagamento}','{$descricao}','{$instrucao_pagamento}','{$bandeira}','{$ativa}', '{$exibe_imagem}', '{$imagem}', '0', 
								   '{$usuario}', '{$data_hoje}','{$data_hoje}')";
				$result = mysql_query($ssql);
				if(!$result){
					$msg = "Erro ao gravar registro";
					$erro = 1;
				}
				else
				{
					$id = mysql_insert_id();
					$msg = "Registro salvo com sucesso";
				}
			}
			
		}
		else
		{
				//edita/atualiza o registro	
				$ssql = "update tblforma_pagamento set fcodtipo='{$tipo}', fforma_pagamento='{$forma_pagamento}', fdescricao='{$descricao}', finstrucao_pagamento='{$instrucao_pagamento}',
							fbandeira='{$bandeira}', fativa='{$ativa}', fexibe_imagem='{$exibe_imagem}', fimagem='{$imagem}', fcodusuario='{$usuario}', fdata_alteracao='{$data_hoje}'
							 where formapagamentoid = $id";
				$result = mysql_query($ssql);
				if(!$result){
					$msg = "Erro ao atualizar registro";
					$erro = 1;
				}
				else
				{
					$msg = "Registro atualizado com sucesso";
				}			
			
		}
		
		
	}






	if($id>0){
	
		$ssql = "select fcodtipo, formapagamentoid, fforma_pagamento, fdescricao, finstrucao_pagamento, fbandeira, fativa, fexibe_imagem, fimagem, 
				fcodusuario, fdata_alteracao, fdata_cadastro
				from tblforma_pagamento where formapagamentoid='{$id}'";
		$result = mysql_query($ssql);
		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				
				$tipo					=	$row["fcodtipo"];
				$forma_pagamento		=	$row["fforma_pagamento"];
				$descricao				=	$row["fdescricao"];
				$instrucao_pagamento	=	$row["finstrucao_pagamento"];
				$ativa					=	$row["fativa"];
				$exibe_imagem			=	$row["fexibe_imagem"];
				$imagem					=	$row["fimagem"];
				$bandeira				=	$row["fbandeira"];
								
				$usuario			=	get_usuario($row["fcodusuario"],"unome");		
				$data_alteracao		=	formata_data_tela($row["fdata_alteracao"]);
				$data_cadastro		=	formata_data_tela($row["fdata_cadastro"]);
				
			}
			mysql_free_result($result);
		}	
		
	}
	
	
	
	if($id==0){
		$ativa = -1;
		$exibe_imagem = -1;
		$imagem = "images/ico-boleto.gif";
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {							   		
		$("#tipo").focus();	
	});
    
</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form method="post" name="frm_forma_pagamento" id="frm_forma_pagamento" action="forma_pagamento.php" onsubmit="return valida_forma_pagamento();">
    <input name="action" id="action" type="hidden" value="gravar" />  
    <input name="id" id="id" type="hidden" value="<?php echo $id?>" />
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Forma de Pagamento &raquo;  <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='forma_pagamento.php';"><?php echo ($id==0) ? "Novo Registro" : $forma_pagamento;  ?></span> </span> <a href="forma_pagamento_consulta.php">Consulta</a>
       	  </div>
            
            <div id="conteudo-interno">
				<table width="98%" border="0" cellspacing="0" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="130">ID</td>
                <td>#<?php echo $id;?></td>
                <td width="200" rowspan="19" align="center" valign="top">
				<?php echo $msg;?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Tipo de Pagamento</td>
                <td><select name="tipo" size="1" id="tipo">
                  <option value="0">Selecione</option>
                  <option value="1" <?php if($tipo==1)echo "selected";?>>Boleto bancário</option>
                  <option value="2" <?php if($tipo==2)echo "selected";?>>Cielo - WebService</option>
                  <option value="3" <?php if($tipo==3)echo "selected";?>>Redecard - WebService</option>
                  <option value="4" <?php if($tipo==4)echo "selected";?>>POS - Cartão Crédito</option>
                  <option value="5" <?php if($tipo==5)echo "selected";?>>PagSeguro</option>
                  <option value="6" <?php if($tipo==6)echo "selected";?>>PayPal</option>
                  <option value="7" <?php if($tipo==7)echo "selected";?>>F2B</option>
                </select></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr> 
              <tr>
                <td>Bandeira</td>
                <td><select name="bandeira" size="1" id="bandeira">
                  <option value=""></option>
                  <option value="visa" <?php if($bandeira=="visa")echo "selected";?>>Visa</option>
                  <option value="mastercard" <?php if($bandeira=="mastercard")echo "selected";?>>Mastercard</option>
                  <option value="diners" <?php if($bandeira=="diners")echo "selected";?>>Diners</option>
                  <option value="discover" <?php if($bandeira=="discover")echo "selected";?>>Discover</option>
                  <option value="elo" <?php if($bandeira=="elo")echo "selected";?>>Elo</option>
                  <option value="amex" <?php if($tipo=="amex")echo "selected";?>>Amex</option>
                </select></td>
              </tr>  
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>                                                        
              <tr>
                <td>Forma de Pagamento</td>
                <td><input name="forma_pagamento" type="text" class="formulario" id="forma_pagamento" value="<?php echo $forma_pagamento;?>" size="75" maxlength="100" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Descrição</td>
                <td><input name="descricao" type="text" class="formulario" id="descricao" value="<?php echo $descricao;?>" size="75" maxlength="100" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Instruções de Pagto</td>
                <td><textarea name="instrucao_pagamento" cols="75" rows="4" class="formulario" id="instrucao_pagamento"><?php echo $instrucao_pagamento;?></textarea></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>                                          
              <tr>
                <td>Exibir Imagem</td>
                <td><label>
                  <input type="radio" name="exibe_imagem" id="exibe_imagem" value="-1" <?php echo ($exibe_imagem == -1) ? "checked" : "" ;?>  />
                  Sim</label>
                  <label>
                    <input type="radio" name="exibe_imagem" id="exibe_imagem" value="0" <?php echo ($exibe_imagem == 0) ? "checked" : "" ;?>  />
                Não</label></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>imagem</td>
                <td>
                <div style="height:30px; float:left; margin:7px 0 0 0;">
               	  <input type="radio" name="imagem" id="imagem" value="images/ico-boleto.gif" <?php echo ($imagem == "images/ico-boleto.gif") ? "checked" : "" ;?>  />
                </div>
      <div style="float:left; margin:0 10px 0 0;">
                	<img src="../images/ico-boleto.gif" width="30" height="30" />
                </div>

      <div style="height:30px; float:left; margin:7px 0 0 0;">
                	<input type="radio" name="imagem" id="imagem" value="images/ico-visa.gif" <?php echo ($imagem == "images/ico-visa.gif") ? "checked" : "" ;?>  />
                </div>
      <div style="float:left; margin:0 10px 0 0;">
                	<img src="../images/ico-visa.gif" width="30" height="30" />
                </div>


                    <div style="height:30px; float:left; margin:7px 0 0 0;">
                    <input type="radio" name="imagem" id="imagem" value="images/ico-master.gif" <?php echo ($imagem == "images/ico-master.gif") ? "checked" : "" ;?>  />
                    </div>
                    <div style="float:left; margin:0 10px 0 0;">
                    <img src="../images/ico-master.gif" width="30" height="30" />
                    </div>
                    
                    <div style="height:30px; float:left; margin:7px 0 0 0;">
                    <input type="radio" name="imagem" id="imagem" value="images/ico-diners.gif" <?php echo ($imagem == "images/ico-diners.gif") ? "checked" : "" ;?>  />
                    </div>
                    <div style="float:left; margin:0 10px 0 0;">
                    <img src="../images/ico-diners.gif" width="30" height="30" />
                    </div>
                    
                    
                    <div style="height:30px; float:left; margin:7px 0 0 0;">
                    <input type="radio" name="imagem" id="imagem" value="images/ico-amex.gif" <?php echo ($imagem == "images/ico-amex.gif") ? "checked" : "" ;?>  />
                    </div>
                    <div style="float:left; margin:0 10px 0 0;">
                    <img src="../images/ico-amex.gif" width="30" height="30" />
                    </div>
                    
                    
                    <div style="height:30px; float:left; margin:7px 0 0 0;">
                    <input type="radio" name="imagem" id="imagem" value="images/ico-elo.gif" <?php echo ($imagem == "images/ico-elo.gif") ? "checked" : "" ;?>  />
                    </div>
                    <div style="float:left; margin:0 10px 0 0;">
                    <img src="../images/ico-elo.gif" width="30" height="30" />
                    </div>    
                
                    
                    <div style="height:30px; float:left; margin:7px 0 0 0;">
                    <input type="radio" name="imagem" id="imagem" value="images/ico-descover.gif" <?php echo ($imagem == "images/ico-descover.gif") ? "checked" : "" ;?>  />
                    </div>
                    <div style="float:left; margin:0 10px 0 0;">
                    <img src="../images/ico-descover.gif" width="50" height="30" />
                    </div>                                
                    
                    
                    <div style="height:30px; float:left; margin:7px 0 0 0;">
                    <input type="radio" name="imagem" id="imagem" value="images/ico-pagseguro.gif" <?php echo ($imagem == "images/ico-pagseguro.gif") ? "checked" : "" ;?>  />
                    </div>
                    <div style="float:left; margin:0 10px 0 0;">
                    <img src="../images/ico-pagseguro.gif" width="50" height="30" />
                    </div>                
                    
                    
                    <div style="height:30px; float:left; margin:7px 0 0 0;">
                    <input type="radio" name="imagem" id="imagem" value="images/ico-paypal.gif" <?php echo ($imagem == "images/ico-paypal.gif") ? "checked" : "" ;?>  />
                    </div>
                    <div style="float:left; margin:0 10px 0 0;">
                    <img src="../images/ico-paypal.gif" width="67" height="30" />
                    </div>        
                    
                    <div style="height:30px; float:left; margin:7px 0 0 0;">
                    <input type="radio" name="imagem" id="imagem" value="images/ico-f2b.gif" <?php echo ($imagem == "images/ico-f2b.gif") ? "checked" : "" ;?>  />
                    </div>
                    <div style="float:left; margin:0 10px 0 0;">
                    <img src="../images/ico-f2b.gif" width="38" height="30" />
                    </div>                   
                    
                </td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Ativa</td>
                <td><label>
                  <input type="radio" name="ativa" id="ativa" value="-1" <?php echo ($ativa == -1) ? "checked" : "" ;?>  />
                  Sim</label>
                  <label>
                    <input type="radio" name="ativa" id="ativa" value="0" <?php echo ($ativa == 0) ? "checked" : "" ;?>  />
                Não</label></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Usuário</td>
                <td><?php echo $usuario;?></td>
                </tr>
              <tr>
                <td>Data Alteração</td>
                <td><?php echo $data_alteracao;?></td>
                </tr>
              <tr>
                <td>Data Cadastro</td>
                <td><?php echo $data_cadastro;?></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" id="btn-cmd-gravar" name="btn-cmd-gravar" value="Gravar" class="btn-gravar" /></td>
                </tr>
              </table>
          </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>