<?php

	include("../include/inc_conexao.php");	
	include("inc_sessao.php");
	
?>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />
<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />

<link href="css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script>
	$(document).ready(function(){
		
		$("input[type=checkbox]").click(function(){
		
		$.ajax({  
            type: 'POST',  
            url: 'ajax_merchant.php',  
            data: {id:$(this).val(),
				   check:$(this).attr("checked")
				  },
             success: function(retorno){
				if(!retorno){
					alert('erro ao inserir o produto');
				}
			 } 
        });  
		
		});
	});

</script>

</head>

	<body>

		<div id="header">
			<span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
		</div>
	<div id="global-container">

		<div id="menu-left">
			<?php include("inc_menu.php");?>
		</div>
    
			<div id="content">
    
				<div id="conteudo">
					<div id="titulo-conteudo">
						<span class="label-inicio">Campanha - Google Merchant Center</span>
					</div>
			
					<div id="conteudo-interno">
			
						<table width="98%" border="0" style="margin: 10px; float:left;">
							<tr>
								<td height="25" colspan="3" style="background-color:pink;">Selecione os produtos que serão enviados ao Google Merchant Center</td>
							</tr>				
								<?php
								$pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1;
								
								$ssql = "SELECT * from tblproduto";
								$result = mysql_query($ssql);
								$total = mysql_num_rows($result);
								$registros = 50;
								$paginas = ceil($total / $registros);
								$inicio = ($registros * $pagina) - $registros;
								
								$ssql = "SELECT * from tblproduto limit $inicio,$registros";
								$result = mysql_query($ssql);
								
								
								if($result){ 
								?>
									<form id="merchant" method="post" action="merchant.php">
								<?php
									
									while($row=mysql_fetch_assoc($result)){
										$preco = number_format($row['pvalor_unitario'],2,",",".");
										$id = $row['produtoid'];
										if($row['pfeed']==1){$checked = 'checked';}
										else{$checked = 'unchecked';}
										
											echo '  <tr class="tr_lista">
													
													<td height="25"> '.$row["pproduto"].' </td> 
													<td>'.$preco.'</td> 
													<td><input type="checkbox" name="feed[]" value="'.$id.'" '.$checked.'></td>
												
													</tr>';
										}
									
								?>
									
								
						</table>
										
										</form>
								
								<?php
									
								for($i = 1; $i < $paginas + 1; $i++) {
									echo "<ul style='list-style-type: none;'><li style='float:left;'><a href='merchant.php?pagina=$i'>".$i." &nbsp;</a></li></ul> ";
								}					
									mysql_free_result($result);
								}
									
								?>
										  
            </div>
        </div>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>