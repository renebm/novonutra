<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");
	
	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}

	$id = $_REQUEST["id"];	
	if(!is_numeric($id)){
		$id = 0;	
	}


	if($_POST && $_REQUEST["action"]=="gravar"){
		$id					= 	addslashes($_REQUEST["id"]);
		$codpropriedade		=	addslashes($_REQUEST["codpropriedade"]);
		$propriedade		=	addslashes($_REQUEST["propriedade"]);
		$tipo				=	($codpropriedade==1) ? "1" : "0";			
		
		if(!is_numeric($tipo)){
			$tipo = 0;	
		}

		if(!is_numeric($codpropriedade)){
			$codpropriedade = 0;	
		}
				
		
		if($id==0){
			
			//verifica se nao existe ja uma propriedade com o mesmo nome;
			$ssql = "select propriedadeid from tblproduto_propriedade where ppropriedade='{$propriedade}' and pcodpropriedade='{$codpropriedade}'";
			$result = mysql_query($ssql);
				if($result){
					$num_rows = mysql_num_rows($result);
					if($num_rows>0){
						$msg = "Já existe um registro com os mesmos dados digitados";
						$erro = 1;
					}	
				}
				
			//--------------------------------------------------------	
			
			if($num_rows==0){
				$ssql = "insert into tblproduto_propriedade (pcodpropriedade, ppropriedade, pcodtipo, pcodusuario, pdata_alteracao, pdata_cadastro) 
							values('{$codpropriedade}','{$propriedade}','{$tipo}','{$usuario}','{$data_hoje}','{$data_hoje}')";
				$result = mysql_query($ssql);
				if(!$result){
					$msg = "Erro ao gravar registro";
					$erro = 1;
				}
				else
				{
					$id = mysql_insert_id();
					$msg = "Registro salvo com sucesso";
				}
				
			}
			
		}
		else
		{
				//edita/atualiza o registro	
				$ssql = "update tblproduto_propriedade set pcodpropriedade='{$codpropriedade}', ppropriedade='{$propriedade}', pcodtipo='{$tipo}', 
						pcodusuario='{$usuario}', pdata_alteracao='{$data_hoje}'
						where propriedadeid = $id";
				$result = mysql_query($ssql);
				if(!$result){
					$msg = "Erro ao atualizar registro";
					$erro = 1;
				}
				else
				{
					$msg = "Registro atualizado com sucesso";
				}			
			
		}
		
		
	}






	if($id>0){
	
		$ssql = "select propriedadeid, pcodpropriedade, ppropriedade, pcodusuario, pdata_alteracao, pdata_cadastro
				from tblproduto_propriedade where propriedadeid='{$id}'";
				//echo $ssql;
		$result = mysql_query($ssql);
		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				
				$codpropriedade		=	$row["pcodpropriedade"];
				$propriedade		=	$row["ppropriedade"];

				$usuario			=	get_usuario($row["pcodusuario"],"unome");		
				$data_alteracao		=	formata_data_tela($row["pdata_alteracao"]);
				$data_cadastro		=	formata_data_tela($row["pdata_cadastro"]);
				
			}
			mysql_free_result($result);
		}	
		
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
							   
		$("#data_inicio").mask("99/99/9999 99:99:99");					   	
		$("#data_termino").mask("99/99/9999 99:99:99");	   	
		
		$("#codpropriedade").focus();
		
		//call
		produto_load_propriedades();
		
		
	
		//exclui 
		$('.propriedade_del').live('click', function(){
			var $this = $( this );  
			var id = $this.attr("rel");  
			
			if(confirm("Deseja realmente excluir a propriedade selecionada?")){
				$("#msg").html("<img src='images/spinner.gif' /> Excluindo registro, aguarde...");
				produto_propriedade_excluir(id)
			}			
			
		}); 		
		
	});
    
</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form method="post" name="frm_propriedade" id="frm_propriedade" action="propriedade.php" onsubmit="return valida_propriedade();">
    <input name="action" id="action" type="hidden" value="gravar" />  
    <input name="id" id="id" type="hidden" value="<?php echo $id?>" />
    	<div id="conteudo">
        	<div id="titulo-consulta">
            	<span class="label-inicio">Propriedade &raquo;  <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='propriedade.php';"><?php echo ($id==0) ? "Novo Registro" : $propriedade;  ?></span> </span> <a href="propriedade.php">Novo Registro</a>
       	  </div>
            
            <div id="conteudo-interno">
				<table width="98%" border="0" cellspacing="0" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="130">ID</td>
                <td>#<?php echo $id;?></td>
                <td width="200" rowspan="23" align="center" valign="top"  style="border:#CCC dotted 1px; text-align:justify; padding:0 10px 10px 10px;">
				<p><strong>Atenção:</strong></p>
				<div id="msg">
				<?php echo $msg;?>
                </div>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Propriedade</td>
                <td>
                <select name="codpropriedade" class="formulario" id="codpropriedade" style="position: relative;z-index: -1;">
                <!-- <option value="0">Selecione</option> -->
                	<?php
                    	$ssql = "select propriedadeid, ppropriedade from tblproduto_propriedade where pcodpropriedade = 0 order by ppropriedade";
						$result = mysql_query($ssql);
						if($result){
							while($row=mysql_fetch_assoc($result)){
								echo '<option value="'.$row["propriedadeid"].'"';
								if($codpropriedade==$row["propriedadeid"]){
									echo " selected";	
								}
								echo '>';
								echo $row["ppropriedade"];
								echo '</option>';
							}
							mysql_free_result($result);
						}
					?>
                </select>
                </td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Propriedade</td>
                <td><input name="propriedade" type="text" class="formulario" id="propriedade" value="<?php echo $propriedade;?>" size="75" maxlength="100" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Usuário</td>
                <td><?php echo $usuario;?></td>
                </tr>
              <tr>
                <td>Data Alteração</td>
                <td><?php echo $data_alteracao;?></td>
                </tr>
              <tr>
                <td>Data Cadastro</td>
                <td><?php echo $data_cadastro;?></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" id="btn-cmd-gravar" name="btn-cmd-gravar" value="Gravar" class="btn-gravar" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>                            
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="2">
                    <div id="td-propriedade">
                        
                    </div>
                </td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              </table>
          </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>