<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}	
	
	/*-------------------------------------------------------------
	grava dados - converte o orcamento em pedido
	--------------------------------------------------------------*/	
	if( $_POST && $_REQUEST["action"]=="gravar" && intval($_REQUEST["converter"]) == -1 ){	
	
			$orcamento = intval($_REQUEST["id"]);
			
			//verifica se já nao existe o pedido
			$ssql = "select pedidoid, pcodorcamento from tblpedido where pcodorcamento = '{$orcamento}'";
			$result = mysql_query($ssql);
			if($result){
				while($row=mysql_fetch_assoc($result)){
					$pedidoid = $row["pedidoid"];	
				}
				mysql_free_result($result);
			}		
		
		
				if($pedidoid==0){
					/*---------------------------------------------------------------------------
					// grava os dados do orcamento para pedido e finaliza o orcamento e pedido
					---------------------------------------------------------------------------*/
					$ssql = "insert into tblpedido (pcodorcamento, pcodigo, pcodcadastro, ptitulo, pnome, pendereco, pnumero, pcomplemento, pbairro, pcidade, pestado, pcep, psubtotal, 
													pvalor_desconto, pvalor_frete, pvalor_presente, pvalor_presente_cartao, pvalor_desconto_cupom, pvalor_desconto_troca, pvalor_total, 
													pcodforma_pagamento, pcodcondicao_pagamento, pcodfrete, pcodcupom_desconto, pcodendereco_fatura, pcodstatus, pfinalizado, preferencia, 
													pcartao_titular, pcartao_numero, pcartao_validade, pcartao_codseguranca, pcartao_retorno, pip)
											select orcamentoid, ocodigo, ocodcadastro, otitulo, onome, oendereco, onumero, ocomplemento, obairro, ocidade, oestado, ocep, osubtotal, 
													ovalor_desconto, ovalor_frete, ovalor_presente, ovalor_presente_cartao, ovalor_desconto_cupom, ovalor_desconto_troca, ovalor_total, 
													ocodforma_pagamento, ocodcondicao_pagamento, ocodfrete, ocodcupom_desconto, ocodendereco_fatura,  '1' as ocodstatus, '-1' as ofinalizado, oreferencia, 
													ocartao_titular, ocartao_numero, ocartao_validade, ocartao_codseguranca, ocartao_retorno, oip from tblorcamento 
													where orcamentoid='{$orcamento}'";
													
					$result = mysql_query($ssql);
					$pedidoid = mysql_insert_id();
					
					
					/*--------------------------------------------------------------------------
					// grava os itens 
					---------------------------------------------------------------------------*/
					$ssql = "insert into tblpedido_item (pcodpedido, pcodigo, pcodproduto, pcodpropriedade, pcodtamanho, pquantidade, pvalor_unitario, ppeso, pdesconto, 
														 ppresente, pbaixa_estoque)
							select $pedidoid , $pedidoid, ocodproduto,  ocodpropriedade, ocodtamanho, oquantidade, ovalor_unitario, opeso, odesconto, 
														 opresente, obaixa_estoque	from tblorcamento_item where ocodorcamento = '{$orcamento}'";
					mysql_query($ssql);
					
					
					
					//atualiza o orcamento
					$ssql = "update tblorcamento set ofinalizado = -1, ocodstatus=1, odata_alteracao='{$data_hoje}' where orcamentoid='{$orcamento}'";			
					mysql_query($ssql);
					
					
					//atualiza o status do cupom de desconto  | 1 = Utilizado
					$ssql = "update tblcupom_desconto set cstatus = 1 where cupomid in (select ocodcupom_desconto from tblorcamento where orcamentoid='{$orcamento}')";
					mysql_query($ssql);
					
					
					//atualiza o pedido
					$ssql = "update tblpedido set pip = '{$ip}', pcodigo='{$pedidoid}', pdata_alteracao='{$data_hoje}', pdata_cadastro='{$data_hoje}' where pedidoid='{$pedidoid}'";			
					mysql_query($ssql);					
					
					
					
					//atualiza o estoque
					$ssql = "select tblpedido_item.itemid, tblpedido_item.pcodproduto, tblpedido_item.pcodtamanho, tblpedido_item.pcodpropriedade, tblpedido_item.pquantidade, 
							tblpedido_item.pbaixa_estoque, tblproduto.pcontrola_estoque
							from tblpedido_item
							inner join tblproduto ON tblpedido_item.pcodproduto = tblproduto.produtoid
							where tblpedido_item.pcodpedido = '{$pedidoid}' and tblpedido_item.pquantidade >0 and tblpedido_item.pbaixa_estoque=0 ";			
					$result = mysql_query($ssql);
				
					if($result){
						while($row=mysql_fetch_assoc($result)){
							$baixa_estoque = 1;					// nao controla estoque
							if($row["pcontrola_estoque"]==-1){
								$baixa_estoque = -1;			// controla estoque
							
								$produtoid = $row["pcodproduto"];
								$propriedade = $row["pcodpropriedade"];
								$tamanho = $row["pcodtamanho"];
								$tipo = 2;
								$quantidade = $row["pquantidade"];
								$note = "Pedido #".$pedidoid;
								$usuario = "0";
								
								//lanca o historico na tblajuste_estoque
								$ssql = "insert into tblestoque_ajuste (acodproduto, acodpropriedade, acodtamanho, atipo, aquantidade, anote, acodpedido, acodusuario, adata_cadastro) 
											values('{$produtoid}','{$propriedade}','{$tamanho}', '{$tipo}', '{$quantidade}','{$note}','{$pedid}','{$usuario}','{$data_hoje}') ";	
								mysql_query($ssql);						
								
								
								//atualiza o estoque
								$ssql = "update tblestoque set eestoque=eestoque-'{$quantidade}', ecodusuario='{$usuario}', edata_alteracao='{$data_hoje}' 
												where ecodproduto='{$produtoid}' and ecodtamanho='{$tamanho}' and ecodpropriedade='{$propriedade}' ";
								mysql_query($ssql);									
								//echo $ssql;
								
								//atualiza linha do pedido_item
								$ssql = "update tblpedido_item set pbaixa_estoque='{$baixa_estoque}' where itemid='".$row["itemid"]."'";
								mysql_query($ssql);
								
							}
						}
						mysql_free_result($result);
					}					
					
					
					echo "ok||$pedidoid";
					die();
					
				}
				else
				{
					echo "erro||Já existe um pedido vinculado a este orçamento";
					die();
				}
		
		
		
	}
	
	
	
	
	
	/*-------------------------------------------------------------
	pega número do orcamento para detalhar
	--------------------------------------------------------------*/
	$id = $_REQUEST["id"];
	
	$ssql = "select tblorcamento.orcamentoid, tblorcamento.ocodigo, tblorcamento.ocodcadastro, tblorcamento.otitulo, tblorcamento.onome, tblorcamento.oendereco, tblorcamento.onumero, tblorcamento.ocomplemento,";
	$ssql .= " tblorcamento.obairro, tblorcamento.ocidade, tblorcamento.oestado, tblorcamento.ocep, tblorcamento.osubtotal, tblorcamento.ovalor_desconto, tblorcamento.ovalor_frete, tblorcamento.ovalor_presente,";
	$ssql .= " tblorcamento.ovalor_total, tblorcamento.ocodforma_pagamento, tblorcamento.ocodcondicao_pagamento, tblorcamento.ocodfrete, tblorcamento.ocodcupom_desconto, tblorcamento.odata_cadastro,";
	$ssql .= " tblorcamento.ocartao_numero, tblorcamento.ocartao_retorno, ";	
	
	$ssql .= " tblcadastro_endereco.etitulo, tblcadastro_endereco.enome, tblcadastro_endereco.eendereco, tblcadastro_endereco.enumero, tblcadastro_endereco.ecomplemento, 
				tblcadastro_endereco.ebairro, tblcadastro_endereco.ecidade, tblcadastro_endereco.eestado, tblcadastro_endereco.ecep, tblcadastro_endereco.ereferencia,";
	
	$ssql .= " tblforma_pagamento.fdescricao, tblcondicao_pagamento.ccondicao,";
	
	$ssql .= " tblcadastro.cadastroid, tblcadastro.crazao_social, tblcadastro.cnome, tblcadastro.ccodtipo, tblcadastro.ccpf_cnpj, tblcadastro.crg_ie, tblcadastro.cdata_nascimento, tblcadastro.cemail,";
	$ssql .= " tblcadastro.ctelefone, tblcadastro.ccelular, tblcadastro_tipo.ttipo, ";
	$ssql .= " tb_cadastro_entrega.ereferencia as referencia_entrega";
	
	$ssql .= " from tblorcamento ";
	
	$ssql .= " left join tblcadastro_endereco on tblorcamento.ocodendereco_fatura=tblcadastro_endereco.enderecoid ";
	$ssql .= " left join tblcadastro_endereco as tb_cadastro_entrega on tblorcamento.ocep=tb_cadastro_entrega.ecep ";

	$ssql .= " left join tblcadastro on tblorcamento.ocodcadastro=tblcadastro.cadastroid ";
	$ssql .= " left join tblcadastro_tipo on tblcadastro.ccodtipo=tblcadastro_tipo.tipoid ";
	$ssql .= " left  join tblforma_pagamento on tblorcamento.ocodforma_pagamento=tblforma_pagamento.formapagamentoid ";
	$ssql .= " left join tblcondicao_pagamento on tblorcamento.ocodcondicao_pagamento=tblcondicao_pagamento.condicaoid ";
	$ssql .= " where tblorcamento.orcamentoid=". $id;
	$ssql .= " limit 0,1";
	
	//echo $ssql;
	
	$result = mysql_query($ssql);
	if($result){
		
		while($row=mysql_fetch_assoc($result)){
			
			$codigo_orcamento	 	= "000000".$row["ocodigo"];
			$codigo_orcamento	 	= substr($codigo_orcamento, strlen($codigo_orcamento)-6,6);
			$codigo_cliente		= $row["ocodcadastro"];
			$razao_social		= $row["crazao_social"] . "&nbsp;" . $row["cnome"];
			$tipo_cadastro		= $row["ttipo"];
			$cnpj_cpf			= $row["ccpf_cnpj"];
			$ie_rg				= $row["crg_ie"];
			$data_nascimento 	= formata_data_tela($row["cdata_nascimento"]);
			$telefone			= $row["ctelefone"];
			$celular			= $row["ccelular"];
			$data_orcamento     	= formata_data_tela($row["odata_cadastro"]);
			$identificacao	 	= $row["otitulo"];
			$nome 			 	= $row["onome"];
			$email 			 	= $row["cemail"];
			$endereco		 	= $row["oendereco"]. ", " . $row["onumero"]. " - " . $row["ocomplemento"]. " - " . $row["obairro"];
			$cidade 		 	= $row["ocidade"];
			$estado			 	= $row["oestado"];
			$cep 			 	= $row["ocep"];
			$cep			 	= substr($cep, 0,5). "-" . substr($cep, 5,3);
			$referencia 	 	= $row["referencia_entrega"];
			$codigo_rastreamento= $row["ocodigo_rastreamento"];
			
			if($row["etitulo"]!=""){
				$endereco_fatura	=  $row["etitulo"]." - ".$row["eendereco"].",&nbsp;".$row["enumero"]."&nbsp;".$row["ecomplemento"]." - ".$row["ebairro"]." - ".$row["ecidade"]." - ".$row["eestado"]." - ".$row["ecep"];
			}
			
			$subtotal		 	= formata_valor_tela($row["osubtotal"]);
			$valor_desconto	 	= formata_valor_tela($row["ovalor_desconto"]);
			$valor_presente		= formata_valor_tela($row["ovalor_presente"]);
			$valor_frete	 	= formata_valor_tela($row["ovalor_frete"]);
			$valor_total	 	= formata_valor_tela($row["ovalor_total"]);
			$forma_pagamento 	= $row["fdescricao"];
			$cod_condicao_pgto 	= $row["ocodcondicao_pagamento"];
			$condicao_pagamento = $row["ccondicao"];
			$status_orcamento 		= $row["sdescricao"];
			$tipo_frete 		= $row["ocodfrete"];
			$cupom_desconto		= $row["ccupom"];
			
			$cartao_numero		= "**** ".right("".$row["ocartao_numero"],4);
			$cartao_retorno		= utf8_decode($row["ocartao_retorno"]);	
			
		}
	}
	mysql_free_result($result);
	
	// Nova busca para pegar tipo do frete - Nome coluna igual a de outra tabela
	
	$ssql = "select tblfrete_tipo.fdescricao from tblfrete_tipo where freteid=" . $tipo_frete;
	$result = mysql_query($ssql);
	if ($result){
		while($row=mysql_fetch_assoc($result)){
			
			$tipo_frete = $row["fdescricao"];
		}
	} mysql_free_result($result);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		window.print();
    });	
</script>
</head>

<body>

<div id="global-container">
    
    <div id="content">
    
    	<div id="conteudo">
    	  <div id="conteudo-interno">
           	  <form name="frm_orcamento" id="frm_orcamento" action="orcamento.php" method="post" >
                <input type="hidden" name="action" id="action" value="gravar" >
                <input type="hidden" name="id" id="id" value="<?php echo $id;?>" >
            	<table width="98%" border="0" style="margin: 10px; float:left;">
                <tr>
                	<td height="25">Orçamento n&deg;: <?php echo $codigo_orcamento; ?></td>
               	  </tr>				
                	<tr>
                	  <td height="25">Data do Orçamento: </span> <span class="txt-detalhe-orcamento"><?php echo $data_orcamento; ?></td>
               	  </table>
                </form>
            </div>
        </div>
    	
        <div class="box-botoes-home">
        	<table width="98%" border="0" style="margin: 5px 0 5px 10px; text-align:center">
                <tr>
                    <td colspan="2" align="center"><strong>Dados do cliente</strong></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                	<td width="12%" align="left"><strong>Pessoa:</strong></td>
                   	<td width="88%" align="left"><?php echo $tipo_cadastro; ?></td>
              	</tr>
                <tr>
                	<td height="23" align="left"><strong>Razão Social:</strong></td>
                    <td align="left"><?php echo $razao_social; ?></td>
              	</tr>
                <tr>
                    <td align="left"><strong>CPF / CNPJ:</strong></td>
                    <td align="left"><?php echo $cnpj_cpf; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>IE / RG:</strong></td>
                    <td align="left"><?php echo $ie_rg; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Data Nascimento:</strong></td>
                    <td align="left"><?php echo $data_nascimento; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Telefone:</strong></td>
                    <td align="left"><?php echo $telefone; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Celular:</strong></td>
                    <td align="left"><?php echo $celular; ?></td>
                </tr>
                <tr>
                  <td align="left"><strong>E-mail:</strong></td>
                  <td align="left"><?php echo $email; ?></td>
                </tr>
                <tr>
                  <td align="left">&nbsp;</td>
                  <td align="left">&nbsp;</td>
                </tr>
                <tr>
                  <td align="left"><strong>Endereço Fatura</strong></td>
                  <td align="left"><?php echo $endereco_fatura;?></td>
                </tr>
                <tr>
                  <td align="left">&nbsp;</td>
                  <td align="left">&nbsp;</td>
                </tr>
                <tr>
                  <td align="left"><strong>No. Cartão</strong></td>
                  <td align="left"><?php echo $cartao_numero;?></td>
                </tr>
                <tr>
                  <td align="left"><strong>Retorno Cartão</strong></td>
                  <td align="left">
                  
				  	<?php 
					//echo $cartao_retorno;
					$xml = simplexml_load_string($cartao_retorno);
					if(strlen($cartao_retorno) >= 1){
						foreach($xml as $key => $value)
							{
								//echo '<span style="width:150px;">';	
								echo strtoupper($key) . " : " . str_replace("+","&nbsp;",$value) . "  |   ";
								//echo '</span>';
							}
					}
					//echo $cartao_retorno;
					
					?>
                  </td>
                </tr>                
            </table>
  		</div>
        
        <div class="box-botoes-home">
        	<table width="98%" border="0" style="margin: 5px 0 5px 10px; text-align:center">
                <tr>
                    <td colspan="2" align="center"><strong>Dados da entrega</strong></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                	<td width="12%" align="left"><strong>Identificação:</strong></td>
                   	<td width="88%" align="left"><?php echo $identificacao; ?></td>
              	</tr>
                <tr>
                	<td align="left"><strong>Destinatário:</strong></td>
                    <td align="left"><?php echo $nome; ?></td>
              	</tr>
                <tr>
                    <td align="left"><strong>Endereço:</strong></td>
                    <td align="left"><?php echo $endereco; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Cidade:</strong></td>
                    <td align="left"><?php echo $cidade; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Estado:</strong></td>
                    <td align="left"><?php echo $estado; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>CEP:</strong></td>
                    <td align="left"><?php echo $cep; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Ponto de Referência:</strong></td>
                    <td align="left"><?php echo $referencia; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;  </td>
                </tr>
                <tr>
                    <td align="left"><strong>Status:</strong></td>
                    <td align="left"><?php echo $status_orcamento; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;  </td>
                </tr>
                <tr>
                    <td align="left"><strong>Frete:</strong></td>
                    <td align="left"><?php echo $tipo_frete; ?></td>
                </tr>
                <tr>
                    <td align="left"><strong>Tipo Frete:</strong></td>
                    <td align="left">Normal</td>
                </tr>
            </table>
      </div>
      <div class="box-botoes-home">
        	<table width="98%" border="0" style="margin: 5px 0 5px 10px; text-align:center">
                <tr><td><strong>Itens do orcamento</strong></td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                    	<table width="100%" border="0" cellpadding="2" cellspacing="2">
                        	<tr bgcolor="#eeeeee">
                            	<strong>
                                <td align="left">Descrição do produto</td>
                                <td align="center">Quantidade</td>
                                <td align="center">Serviços</td>
                                <td align="center">Preço Unitário</td>
                                <td align="center">Valor Total</td>
                                </strong>
                            </tr>
                            
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            
                            <?php
								
								$ssql = "SELECT tblproduto.produtoid, tblproduto.pcodigo, tblproduto.pproduto, tblproduto.pvalor_unitario, tblproduto.pdisponivel, 
								tblorcamento_item.oquantidade, tblorcamento_item.ovalor_unitario, tblorcamento_item.ocodproduto, pai.ppropriedade AS propriedade_pai, tblorcamento_item.ocodtamanho, tblproduto_propriedade.ppropriedade AS ptamanho, 
								tblorcamento_item.ocodpropriedade, proper.ppropriedade AS ppropriedade
								FROM tblproduto
								
								INNER JOIN tblorcamento_item ON tblproduto.produtoid = tblorcamento_item.ocodproduto
								LEFT JOIN tblproduto_propriedade ON tblorcamento_item.ocodtamanho = tblproduto_propriedade.propriedadeid
								LEFT JOIN tblproduto_propriedade AS proper ON tblorcamento_item.ocodpropriedade = proper.propriedadeid
								LEFT JOIN tblproduto_propriedade AS pai ON tblproduto_propriedade.pcodpropriedade = pai.propriedadeid
								where tblorcamento_item.ocodorcamento=$id and tblorcamento_item.oquantidade > 0
								";
								
								//echo $ssql;
				
								$result = mysql_query($ssql);
								if($result){
									$contador = 1;
									while($row=mysql_fetch_assoc($result)){
										$produto			= $row["pcodigo"] . " - " . $row["pproduto"] . "&nbsp;&nbsp;" . $row["ppropriedade_pai"] . "&nbsp;&nbsp;" . $row["ptamanho"] . " - " . $row["onome"]; 
										$produto_qtde 	 	= $row["oquantidade"];
										$valor_unitario 	= formata_valor_tela($row["ovalor_unitario"]);
										$produto_desconto 	= formata_valor_tela($row["odesconto"]);
										$valor_final 		= $produto_qtde * $valor_unitario;
										$valor_final 		= formata_valor_tela($valor_final);
										
										if ((round($contador/2)) <> ($contador/2)){
											echo '<tr bgcolor="#eeeeee">';
										}else{
											echo '<tr bgcolor="#ffffff">';
										}
										
										echo '
													<td align="left">'. $produto .'</td>
													<td align="center">'. $produto_qtde .'</td>
													<td align="right">0,00</td>
													<td align="right">'. $valor_unitario .'</td>
													<td align="right">'. $valor_final .'</td>
												</tr>';
												$contador ++;
									}
								}mysql_free_result($result);
							?>
                            <tr>
                                <td align="left">&nbsp;</td>
                                <td align="center">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right"><strong>Subtotal:</strong></td>
                                <td align="right">R$ <?php echo $subtotal; ?></td>
                            </tr>
                            <tr>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right"><?php if($cupom_desconto!=""){echo "CUPOM ".$cupom_desconto;}?><strong>Desconto:</strong></td>
                                <td align="right">R$ <?php echo $valor_desconto; ?></td>
                            </tr>
                            <!--//
                            <tr>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right"><strong>Frete:</strong></td>
                                <td align="right">R$ <?php echo $valor_frete; ?></td>
                            </tr>
                            //-->
                            <tr>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right"><strong>Total:</strong></td>
                                <td align="right">R$ <?php echo $valor_total; ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
      </div>
      
      <div class="box-botoes-home">
        	<table width="98%" border="0" style="margin: 5px 0 5px 10px; text-align:center">
                <tr><td><strong>Dados do Pagamento</strong></td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                    	<table width="100%" border="0" cellpadding="3" cellspacing="0">
                            <tr>
                            	<td width="120" align="left"><strong>Pagamento:</strong></td>
                            	<td align="left"><strong><?php echo $forma_pagamento; ?></strong></td>
                            </tr>
                            <tr>
                            	<td align="left"><strong>Valor:</strong></td>
                                <td align="left">R$ <?php echo $valor_total; ?></td>
                            </tr>
                            <?php 
								$ssql = "select * from tblcondicao_pagamento where condicaoid=" . $cod_condicao_pgto;
								
									$result = mysql_query($ssql);
									if($result){
									while($row=mysql_fetch_assoc($result)){
										$qtde_parcelas = $row["cnumero_parcelas"];
									}
								}mysql_free_result($result);
							?>
                            <tr>
                            	<td align="left"><strong>Parcelas:</strong></td>
                                <td align="left"><?php echo $qtde_parcelas; ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
      </div>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>