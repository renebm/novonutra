<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");
	
	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />

<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$("#data1").mask("99/99/9999");
		$("#data2").mask("99/99/9999");
		$('#pedido').focus();
		
		
		$("#acao").change(function(){
			var acao = $(this).attr("value");
			$("#valor").attr("disabled",false);
			if(acao == 9){
				$("#valor").attr("value","0,00");
				$("#valor").attr("disabled",true);
			}
		});
		
	});
	
	function seta_print(valor){
		$("#print").attr("value",valor);	
	}
	
	
	//click na linha do pedido
	$('.tr_lista').live('click', function(){
		var $this = $( this );  
		var id = $this.attr("rel");  
		if($("#print").attr("value")=="0"){	
			window.location.href = "pedido.php?id="+id;		
		}
		else
		{
			$("#print").attr("value","0");		
		}
	}); 	
	
</script>

</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    

    	<div id="conteudo">
			<div id="titulo-consulta">
            	<span class="label-inicio">Gateway de Pagamento<span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='pedido_consulta.php';"></span></span>
            </div>
            
            <div id="conteudo-interno">
             <form name="frm_consulta" id="frm_consulta" method="get" action="?" onsubmit="return gateway_cielo();">
             <table width="99%" border="0" cellspacing="2" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="50%">&nbsp;</td>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>No. Pedido&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;TID</td>
                <td>Ação</td>
               </tr>
              <tr>
                <td><input name="pedido" type="text" class="formulario" id="pedido" size="12" maxlength="200" />&nbsp;
                <input name="tid" type="text" class="formulario" id="tid" size="60" maxlength="200" /></td>
                <td>
                <select name="acao" id="acao" size="1" class="formulario">
                	<option value="0">Selecione</option>
                    <option value="9">Consultar Venda</option>
                    <option value="1">Cancelar Venda</option>
                </select>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Valor a Cancelar</td>
                <td rowspan="18" valign="top" id="status">&nbsp;</td>
              </tr>
              <tr>
                <td><input name="valor" type="text" class="formulario" id="valor" value="0,00" size="12" maxlength="20" style="text-align:center" />&nbsp;&nbsp;0,00</td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td valign="top"><input type="submit" id="btn-processar" name="btn-gravar" value="Processar" class="btn-gravar" /></td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
             </table>
             </form>
          </div>
            
       </div>

 
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>