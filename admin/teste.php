<?php
/*
<?xml version="1.0" encoding="ISO-8859-1"?><autorizacao><tid>10069930690A7F782006</tid><pan>Sjbrs0A5a0+05zzOn0uxnyVKCsFwZ46S6EdLpY7zoXM=</pan><codigo>6</codigo><lr>00</lr><mensagem>Transação autorizada</mensagem><arp>123456</arp><nsu>687992</nsu></autorizacao>
*/

$ws = "https://ecommerce.cielo.com.br/servicos/ecommwsec.do";

$xml_msg = '<?xml version="1.0" encoding="UTF-8"?>
<requisicao-consulta id="294" versao="1.2.1">
	<tid>10069930690A80822004</tid>
	<dados-ec>
		<numero>1006993069</numero>
		<chave>25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3</chave>
	</dados-ec>
</requisicao-consulta>';

$xml_string =  httprequest($ws, "mensagem=".$xml_msg);
	
	
print_r($xml_string);	
	


function httprequest($paEndereco, $paPost){

	$sessao_curl = curl_init();
	curl_setopt($sessao_curl, CURLOPT_URL, $paEndereco);
	
	curl_setopt($sessao_curl, CURLOPT_FAILONERROR, true);

	//  CURLOPT_SSL_VERIFYPEER
	//  verifica a validade do certificado
	curl_setopt($sessao_curl, CURLOPT_SSL_VERIFYPEER, true);
	//  CURLOPPT_SSL_VERIFYHOST
	//  verifica se a identidade do servidor bate com aquela informada no certificado
	curl_setopt($sessao_curl, CURLOPT_SSL_VERIFYHOST, 2);

	//  CURLOPT_SSL_CAINFO
	//  informa a localização do certificado para verificação com o peer
	curl_setopt($sessao_curl, CURLOPT_CAINFO, "../include/VeriSignClass3PublicPrimaryCertificationAuthority-G5.crt");
	curl_setopt($sessao_curl, CURLOPT_SSLVERSION, 3);

	//  CURLOPT_CONNECTTIMEOUT
	//  o tempo em segundos de espera para obter uma conexão
	curl_setopt($sessao_curl, CURLOPT_CONNECTTIMEOUT, 10);

	//  CURLOPT_TIMEOUT
	//  o tempo máximo em segundos de espera para a execução da requisição (curl_exec)
	curl_setopt($sessao_curl, CURLOPT_TIMEOUT, 40);

	//  CURLOPT_RETURNTRANSFER
	//  TRUE para curl_exec retornar uma string de resultado em caso de sucesso, ao
	//  invés de imprimir o resultado na tela. Retorna FALSE se há problemas na requisição
	curl_setopt($sessao_curl, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($sessao_curl, CURLOPT_POST, true);
	curl_setopt($sessao_curl, CURLOPT_POSTFIELDS, $paPost );

	$resultado = curl_exec($sessao_curl);

	$response = curl_getinfo($sessao_curl, CURLINFO_HTTP_CODE);

	curl_close($sessao_curl);

	if ($resultado)
	{
		return $resultado;
	}
	else
	{
		return curl_error($sessao_curl);
	}

}


?>


