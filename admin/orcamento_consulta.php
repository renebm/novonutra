<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");
	
	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$uri = str_replace("/divertecultural/","",$_SERVER['REQUEST_URI']);	

	$pagina = 1;
	$start = 0;
	$limit = 20;

	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}

	$data1 = "01".date("/m/Y");												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$("#data1").mask("99/99/9999");
		$("#data2").mask("99/99/9999");
		$('#pedido').focus();
	});
	
	function seta_print(valor){
		$("#print").attr("value",valor);	
	}
	
	//click na linha do orcamento
	$('.tr_lista').live('click', function(){
		var $this = $( this );  
		var id = $this.attr("rel");  
		if($("#print").attr("value")=="0"){	
			window.location.href = "orcamento.php?id="+id;		
		}
		else
		{
			$("#print").attr("value","0");		
		}
	}); 	
	
  
</script>

</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    

    	<div id="conteudo">
			<div id="titulo-consulta"><input type="hidden" name="print" id="print" value="0" />
            	<span class="label-inicio">Orçamento &raquo; <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='orcamento_consulta.php';">Consulta</span></span>
            </div>
            
            <div id="conteudo-interno">
            <?php 
				if (!$_GET){
			?>
             <form name="frm_consulta" id="frm_consulta" method="get" action="?" >
             <table width="99%" border="0" cellspacing="2" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="50%">&nbsp;</td>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td> Nome do Cliente</td>
                <td>Filtrar por:</td>
               </tr>
              <tr>
                <td><input name="string" type="text" class="formulario" id="string" size="60" maxlength="200" /></td>
                <td>
                <select name="filtro" size="1" class="formulario" id="filtro">
                  <option value="0">Todos</option>
                  <option value="1">Incompleto</option>
                  <option value="2">Completo</option>
                </select>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Retorno do Cartão [ TID | PAN | ARP | NSU ]</td>
                <td>Número do Cartão</td>
              </tr>
              <tr>
                <td><input name="cartao_retorno" type="text" class="formulario" id="cartao_retorno" size="60" maxlength="50" /></td>
                <td><input name="cartao_numero" type="text" class="formulario" id="cartao_numero" size="20" maxlength="4" />
4 últimos dígitos</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>                                          
              <tr>
                <td>Data de Início</td>
                <td>Data de Término</td>
              </tr>
              <tr>
                <td><label>
                  <input name="data1" type="text" class="formulario" id="data1" value="<?php echo $data1;?>" size="15" maxlength="10" />
                </label></td>
                <td><input name="data2" type="text" class="formulario" id="data2" value="<?php echo $data2;?>" size="15" maxlength="10" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><input type="submit" id="btn-cmd-busca" name="btn-cmd-busca" value="Consultar" class="btn-gravar" /></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
             </table>
             
             </form>
			<?php
				}else{
			?>	
                
                
                <table width="99%" border="0" cellspacing="0" cellpadding="3" style="margin:10px;">
                  <tr>
                    <td width="50" class="titulo_table">#ID</td>
                    <td width="75" class="titulo_table">Orçamento</td>
                    <td class="titulo_table">Nome</td>
                    <td class="titulo_table">Forma de Pagto</td>
                    <td width="100" align="center" class="titulo_table">Subtotal</td>
                    <td width="100" align="center" class="titulo_table">Total</td>
                    <td width="140" align="center" class="titulo_table">Data Orçamento</td>
                    <td width="80" align="center" class="titulo_table">Imprimir</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
                  
                  <?php
				  
				  	$string = addslashes($_REQUEST["string"]);
					$filtro = intval($_REQUEST["filtro"]);
					
					$cartao_retorno = addslashes($_REQUEST["cartao_retorno"]);
					$cartao_numero = addslashes($_REQUEST["cartao_numero"]);										
					
					$data1 = addslashes($_REQUEST["data1"]);
					$data2 = addslashes($_REQUEST["data2"]);
				  
                  	$ssql = "select tblorcamento.orcamentoid, tblorcamento.ocodigo, tblorcamento.otitulo, tblorcamento.onome, tblorcamento.ocodstatus, tblorcamento.ocodforma_pagamento, 
							tblorcamento.osubtotal, tblorcamento.ovalor_total, tblorcamento.odata_cadastro, tblforma_pagamento.fforma_pagamento
							from tblorcamento
							left join tblforma_pagamento on tblorcamento.ocodforma_pagamento = tblforma_pagamento.formapagamentoid
							where tblorcamento.ofinalizado=0 and tblorcamento.onome like '%{$string}%'						
							";
					
					if($cartao_retorno!=""){
							$ssql .= " and tblorcamento.ocartao_retorno like '%{$cartao_retorno}%' ";						
					}

					if($cartao_retorno!=""){
							$ssql .= " and tblorcamento.ocartao_numero like '%{$cartao_numero}%' ";						
					}					
					
					
					if($filtro==1){
						$ssql .= " and tblorcamento.ocodforma_pagamento = 0";		
					}

					if($filtro==2){
						$ssql .= " and tblorcamento.ocodforma_pagamento > 0";			
					}


					if($data1!=""){
							$data1 = formata_data_db($data1) . " 00:00:00";
							$ssql .= " and tblorcamento.odata_cadastro >= '{$data1}'";	
					}
					if($data2!=""){
							$data2 = formata_data_db($data2) . " 23:59:59";
							$ssql .= " and tblorcamento.odata_cadastro <= '{$data2}' ";	
					}
					
					$ssql .= " order by tblorcamento.odata_cadastro desc, tblorcamento.otitulo, tblorcamento.onome ";
		
					$result = mysql_query($ssql);
					if($result){
						$total_registros = mysql_num_rows($result);	
					}					
							
							
					$ssql .= " limit $start, $limit";
							
					//echo $ssql;
							
				  	$result = mysql_query($ssql);
				  	if($result){
					
						if(mysql_num_rows($result)==0){
							echo '
								  <tr>
									<td colspan="9">Nenhum registro localizado.</td>
								  </tr>';							
						}
					
					
						while($row=mysql_fetch_assoc($result)){
							
							echo '
								  <tr class="tr_lista" rel="'.$row["orcamentoid"].'">
									<td>'.$row["orcamentoid"].'</td>
									<td>'.right("000000".$row["ocodigo"],6).'</td>
									<td>'.$row["onome"].'</td>
									<td>'.$row["fforma_pagamento"].'</td>
									<td align="right">R$ '.number_format($row["osubtotal"],2,",",".").'&nbsp;&nbsp;</td>
									<td align="right">R$ '.number_format($row["ovalor_total"],2,",",".").'&nbsp;&nbsp;</td>
									<td align="center">'. formata_data_tela($row["odata_cadastro"]).'</td>
									<td align="center"><img src="images/ico_pedido.gif" width="16" height="16" onclick="javascript:seta_print(1); orcamento_print('.$row["orcamentoid"].'); return;" /></td>
								  </tr>							
							';
						}
						mysql_free_result($result);
					
					}
				  ?>
                  

                  
				  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
				  <tr>
				    <td colspan="8">
				      <div class="paginacao"><span class="paginacao-text">Página:</span> 
				        <?php
                        echo paginacao($pagina, $limit, $total_registros);
                        ?>                            
				        </div>			        </td>
			      </tr>

                </table>
                
                
                <?php
                
				}
				?>
                
          </div>
            
       </div>

 
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>