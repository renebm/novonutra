<?php

include("../include/inc_conexao.php");
include("../include/inc_image_resize.php");
include("inc_sessao.php");

header('Content-Type: text/html; charset=utf-8'); 


$usuario	=	$_SESSION["usuarioid"];

if(!is_numeric($usuario)){
	$usuario = 0;	
}


/*
echo "erro||0|";
foreach($_REQUEST as $key => $value) {echo "$key => $value <br>";}  
exit();
*/

if($_REQUEST["action"]=="gravar" && $_REQUEST["tab"]==1){		// principal


	$id					= addslashes($_REQUEST["id"]);
	$referencia 		= addslashes($_REQUEST["referencia"]);
	$codigo 			= addslashes($_REQUEST["codigo"]);
	$codigo_barras 		= addslashes($_REQUEST["codigo_barras"]);
	$ncm		 		= addslashes($_REQUEST["ncm"]);
	
	$produto			= addslashes($_REQUEST["produto"]);
	$subtitulo			= addslashes($_REQUEST["subtitulo"]);
	
	$link_seo		 	= addslashes($_REQUEST["link_seo"]);

	$marca				= $_REQUEST["marca"];
	$codigo_fabricante	= addslashes($_REQUEST["codigo_fabricante"]);

	$valor_comparativo	= addslashes($_REQUEST["valor_comparativo"]);
	$valor_unitario		= addslashes($_REQUEST["valor_unitario"]);
	
	$outlet				= addslashes($_REQUEST["outlet"]);
	$recomendados		= addslashes($_REQUEST["recomendados"]);


	$valor_comparativo	= formata_valor_db($valor_comparativo);
	$valor_unitario		= formata_valor_db($valor_unitario);
	
	
	if(!is_numeric($marca)){
		$marca = 0;	
	}
	
	
	if($id<=0 || $id ==""){		
		//verifica se ja nao existe o produto
		$id = 0;
		$ssql = "select produtoid, preferencia, pcodigo, pcodigo_barras, pproduto, psubtitulo, plink_seo, pcodmarca, pcodigo_fabricante, pvalor_comparativo, pvalor_unitario 
				from tblproduto where pcodigo='{$codigo}'";
		$result = mysql_query($ssql);
		
		//valida o result
		if($result){
			$num_rows = mysql_num_rows($result);
			
			if($num_rows>0){
				echo "erro||0||Já existe um produto com o código #$codigo.<br /><br />Lembre-se que o código é o SKU (código único do produto) e não pode ser repetido.";	
				exit();
			}
			
		}
		else
		{
			echo "erro||00||result error #1";
			exit();
		}
		
		
		if($id==0){
			$ssql = "insert into tblproduto (preferencia, pcodigo, pcodigo_barras, pncm, pproduto, psubtitulo, plink_seo, pcodmarca, pcodigo_fabricante, pvalor_comparativo, pvalor_unitario, 
											 pdata_alteracao, pdata_cadastro, outlet, recomendados) 
											values('{$referencia}','{$codigo}','{$codigo_barras}','{$ncm}','{$produto}','{$subtitulo}','{$link_seo}','{$marca}','{$codigo_fabricante}',
												   '{$valor_comparativo}','{$valor_unitario}','{$data_hoje}','{$data_hoje}','{$outlet}','{$recomendados}')";	
			$result = mysql_query($ssql);
			$id = mysql_insert_id();
			if(!is_numeric($id)){
				echo "erro||0||result error #2";
				exit();
			}
			
			/*---------------------------------------
			//atualiza o link_seo
			---------------------------------------*/
			$link_seo = gera_url_seo($produto);
			if($subtitulo!=""){
				$link_seo .= "-" . gera_url_seo($subtitulo);
			}
			
			$link_seo .= "---".$id;
			
			
			$link_seo = strtolower($link_seo);
			mysql_query("update tblproduto set plink_seo='{$link_seo}' where produtoid='{$id}'");
			
			echo "ok||$id||Registro salvo com sucesso";
			exit();
			
		}

	}
	

		
	if($id>0){
		
		$ssql = "update tblproduto set preferencia='{$referencia}', pcodigo='{$codigo}', pcodigo_barras='{$codigo_barras}', pncm='{$ncm}', pproduto='{$produto}', 
					psubtitulo='{$subtitulo}', plink_seo='{$link_seo}', pcodmarca='{$marca}', pcodigo_fabricante='{$codigo_fabricante}', 
					pvalor_comparativo='{$valor_comparativo}', pvalor_unitario='{$valor_unitario}', 
										 pdata_alteracao='{$data_hoje}', outlet='{$outlet}', recomendados='{$recomendados}' where produtoid='{$id}'";
		
		$result = mysql_query($ssql);
		if(!$result){
			echo "erro||0||result error # 2-1";
			exit();
		}
					
		
		/*---------------------------------------
		//atualiza o link_seo
		---------------------------------------*/
		$link_seo = gera_url_seo($produto);
		if($subtitulo!=""){
			$link_seo .= "-" . gera_url_seo($subtitulo);
		}
		$link_seo .= "---".$id;
		
		$link_seo = strtolower($link_seo);
		mysql_query("update tblproduto set plink_seo='{$link_seo}' where produtoid='{$id}'");
		
		echo "ok||$id||Registro atualizado com sucesso";
		exit();
		
	}

}





if($_POST["action"]=="gravar" && $_POST["tab"]==2){		//guia propriedades

		$id					=	addslashes($_REQUEST["id"]);
		$peso				=	addslashes($_REQUEST["peso"]);
		$largura			=	addslashes($_REQUEST["largura"]);
		$altura				=	addslashes($_REQUEST["altura"]);
		$comprimento		=	addslashes($_REQUEST["comprimento"]);
		$disponivel			=	addslashes($_REQUEST["disponivel"]);
		$controla_estoque	=	addslashes($_REQUEST["controla_estoque"]);
		$frete_gratis		=	addslashes($_REQUEST["frete_gratis"]);
		$top20				=	addslashes($_REQUEST["top20"]);
		$minimo				=	addslashes($_REQUEST["minimo"]);
		$maximo				=	addslashes($_REQUEST["maximo"]);
		$vitrine			=	addslashes($_REQUEST["vitrine"]);
		$data_termino		=	addslashes($_REQUEST["data_termino"]);
		$data_inicio		=	addslashes($_REQUEST["data_inicio"]);
		
		
		$peso				=	formata_valor_db($peso);
		$data_inicio		=	formata_data_db($data_inicio) . " 00:00:00";
		$data_termino		=	formata_data_db($data_termino) . " 23:59:59";
	
		
		if(!is_numeric($id) || $id<=0){
			echo "erro||0|| $id - id de produto inválido";
			exit();
		}
		
		
		$ssql = "update tblproduto set ppeso='{$peso}', paltura='{$altura}', plargura='{$largura}', pcomprimento='{$comprimento}', pdisponivel='{$disponivel}', pcontrola_estoque='{$controla_estoque}',
				pfrete_gratis='{$frete_gratis}', ptop20='{$top20}', pminimo='{$minimo}', pmaximo='{$maximo}', pvitrine='{$vitrine}', pdata_inicio='{$data_inicio}', pdata_termino='{$data_termino}', pdata_alteracao='{$data_hoje}' 
				where produtoid='{$id}'";
		$result = mysql_query($ssql);
			
		if(!$result){
			echo "erro||0||result error tab#2 ";
			exit();
		}else{
			echo "ok||$id||Registro salvo com sucesso  ".$ssql.time();
			exit();
		}	
	
	
}





if($_POST["action"]=="gravar" && $_POST["tab"]==3){		//guia categorias
		$id				=	addslashes($_REQUEST["id"]);
		$categorias		= 	explode(",",addslashes($_REQUEST["categoria"]));
		
		$ssql = "delete from tblproduto_categoria where pcodproduto='{$id}'";
		mysql_query($ssql);
		
		
		for($i=0;$i<count($categorias)-1;$i++){
			$categoria = $categorias[$i];
			$ssql = "insert into tblproduto_categoria (pcodproduto, pcodcategoria, pcodusuario, pdata_alteracao, pdata_cadastro)
					values('{$id}','{$categoria}','{$usuario}','{$data_hoje}','{$data_hoje}')"	;
			$result = mysql_query($ssql);
			
			if(!$result){
				echo "erro||$id||Erro ao atuaizar os produtos nas categorias.";
				exit();
				break;
			}
		}
		
		echo "ok||$id||Registro salvo com sucesso";
		exit();		
				
}







if($_POST["action"]=="gravar" && $_POST["tab"]==4){		// guia detalhes

	$id						= addslashes($_REQUEST["id"]);
	$descricao	 			= addslashes($_REQUEST["descricao"]);
	$palavra_chave 			= addslashes($_REQUEST["palavra_chave"]);
	$descricao_detalhada 	= $_REQUEST["descricao_detalhada"];
	$ficha_tecnica		 	= $_REQUEST["ficha_tecnica"];
	
	//converte os caracteres html para utf8
	$descricao_detalhada 	= $descricao_detalhada;
	

		if(!is_numeric($id) || $id<=0){
			echo "erro||0|| $id - id de produto inválido";
			exit();
		}
		
		$ssql = "update tblproduto set pdescricao='{$descricao}', ppalavra_chave='{$palavra_chave}', pdescricao_detalhada='{$descricao_detalhada}',pdata_alteracao='{$data_hoje}',pficha_tecnica='{$ficha_tecnica}'
				 where produtoid='{$id}'";
		$result = mysql_query($ssql);
		if(!$result){
			echo "erro||0||result error tab#4 ".$ssql;
			exit();
		}else{
			echo "ok||$id||Registro salvo com sucesso";
			exit();
		}	


}





if($_POST["action"]=="gravar" && $_POST["tab"]==5 ){		//guia opcoes e tamanhos


		$id				=	addslashes($_REQUEST["id"]);
		$propriedades	= 	explode(",",addslashes($_REQUEST["propriedade"]));
		
		$ssql = "delete from tblproduto_caracteristica where ccodproduto='{$id}'";
		mysql_query($ssql);
		
		
		for($i=0;$i<count($propriedades)-1;$i++){
			$propriedade = $propriedades[$i];
			$ssql = "insert into tblproduto_caracteristica (ccodproduto, ccodpropriedade, ccodusuario, cdata_alteracao, cdata_cadastro)
					values('{$id}','{$propriedade}','{$usuario}','{$data_hoje}','{$data_hoje}')"	;
			$result = mysql_query($ssql);
			
			if(!$result){
				echo "erro||$id||Erro ao atualizar os produtos nas opções e tamanhos." . $ssql;
				exit();
				break;
			}
		}
		
		echo "ok||$id||Registro salvo com sucesso";
		exit();		

}



// && && $_REQUEST["action"]=="gravar"
if($_POST["tab"]==6 ){		//guia imagens

	$id					= intval($_REQUEST["id"]);
	$midiaid			= intval($_REQUEST["midiaid"]);
	$tipo		 		= intval($_REQUEST["tipo"]);
	$principal	 		= intval($_REQUEST["principal"]);
	$titulo				= addslashes($_REQUEST["titulo"]);
	$error				= 0;
	$imagem				= "";


	if( $tipo==1 ){//imagem	
	
	
		////////////////////////////////////////////////////////////////////////////
		//tamanho da imagem original grande -> big
		$img_dimensao		= get_configuracao("config_img_dimensao");	//tamamnho da imagem grande
		
		if($img_dimensao=="" && strpos($img_dimensao,"x")==""){
			echo ajax_iframe("Erro: <br /><br />As dimensões de imagens não foram configuradas no sistema.");
			exit();		
		}
		
		$img_dimensao = strtolower($img_dimensao);
		list($imagem_x, $imagem_y) = explode("x",$img_dimensao);
		
	
		if(!is_numeric($imagem_x) || !is_numeric($imagem_y)){
			echo ajax_iframe("Erro: <br /><br />As dimensões de imagens não foram configuradas no sistema.");
			exit();				
		}	
		
		
		////////////////////////////////////////////////////////////////////////////
		//tamanho da imagem média -> página de produto
		$img_dimensao_m		= strtolower( get_configuracao("config_img_dimensao_m") );	

		if( trim($img_dimensao_m) == "" && strpos($img_dimensao_m,"x")=="" ){
			echo ajax_iframe("Erro: <br /><br />As dimensões de imagens não foram configuradas no sistema.");
			exit();		
		}
		

		////////////////////////////////////////////////////////////////////////////
		//tamanho da imagem pequena -> tumb
		$img_dimensao_p		= strtolower( get_configuracao("config_img_dimensao_p") );	

		if( trim($img_dimensao_p)=="" && strpos($img_dimensao_p,"x")=="" ){
			echo ajax_iframe("Erro: <br /><br />As dimensões de imagens não foram configuradas no sistema.");
			exit();		
		}
		


		if(!is_numeric($principal)){
			$principal = 0;	
		}

	}


	if( $tipo==2 ){//video embeded	
	
	}
	
	
	if( $tipo==3 ){//arquivo pdf
		$principal = 0;
		
		
		if( !file_exists("../arquivo") ){
			$dir = mkdir("../arquivo",777);
			
			if($dir != 1){
				unlink($file_tmp_name);
				echo ajax_iframe("Erro: <br /><br />O diretório `arquivo` não pode ser criado.");
				exit();					
			}
			
		}
		
			
		
		
	}
	


	
	
		if($_FILES){						
			$file_name = $_FILES['arquivo']['name'];
			$file_type = $_FILES['arquivo']['type'];
			$file_size = $_FILES['arquivo']['size'];
			$file_tmp_name = $_FILES['arquivo']['tmp_name'];
			$error = $_FILES['arquivo']['error'];
						
			
			if($tipo == 1){	//imagem
				
				$file_size =  number_format($file_size/1024,2);
				
				if($file_size>350 && $tipo==1){
					
					if( file_exists($file_tmp_name) ){
						unlink($file_tmp_name);	
					}
					
					echo ajax_iframe("Erro: <br /><br />O tamanho do arquivo não pode ser maior que 350KB");
					exit();
				}
				
				$imagesize = getimagesize($file_tmp_name); // Pega os dados
				
				$x = $imagesize[0]; // 0 será a largura.
				$y = $imagesize[1]; // 1 será a altura.
	
				if($x<$imagem_x || $y<$imagem_y){
					if( file_exists($file_tmp_name) ){
						unlink($file_tmp_name);	
					}					
					echo ajax_iframe("Erro: <br /><br />A imagem deve ser maior que ".$imagem_x."x".$imagem_y." pixels");
					exit();
				}
				
			}
			
			
			
			if($tipo == 3){	//arquivo PDF
				if( right( strtolower($file_name),4 ) != ".pdf" ){
					if( file_exists($file_tmp_name) ){
						unlink($file_tmp_name);	
					}					
					echo ajax_iframe("Erro: <br /><br />O arquivo informado é inválido, envie um arquivo pdf válido.");
					exit();					
				}
			
			}

		}


		///////////////////////////////////////////////////////////
		//arquivos de imagem	
		if($error==0 && $tipo == 1){
						
			$img_hash = mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"));			
						
			$imagem = get_produto($id,"pproduto")."-".get_produto($id,"psubtitulo")."-big";
			$imagem = gera_url_seo($imagem) . right($file_name,4);
			$imagem = strtolower($imagem);
			
			$extensao = right($imagem,4);
			
			$count = 0;
			if(file_exists("../imagem/produto/".$imagem)){
				/*for($i=1;$i<100;$i++){
					$count = $i;
					$imagem = str_replace($extensao,"-".$i.$extensao,$imagem);
					if(existe_arquivo("../imagem/produto/".$imagem)!=true){
						break;
					} 		
				}*/
				
				$result = mysql_query("select count(midiaid) as total from tblproduto_midia where mcodproduto = $id");
				if($result){
					while($row=mysql_fetch_assoc($result)){
						$count = $row["total"];
						$count++;
					}
													
					$imagem = str_replace($extensao,"-".$count.$extensao,$imagem);
					
					if(file_exists("../imagem/produto/".$imagem)){
						$imagem = str_replace("-".$count.$extensao,"-".$count."-".$img_hash.$extensao,$imagem);			
					}
					
				}
				
			}
			
			
			if($count==0){
				$principal = -1;	
			}
			
			
			
			//move para pasta TMP
			$move = move_uploaded_file($file_tmp_name,"../imagem/tmp/".$file_name);
			
			
			if(!$move){
				echo ajax_iframe("Erro: <br /><br />Erro ao mover arquivo");
				exit();
			}
			
			
			//carrega imagem para o resize
			$image = new SimpleImage();
			$image->load("../imagem/tmp/".$file_name);			


			//redimenciona para -big			
			$image->resize($imagem_x,$imagem_y);
			$image->save("../imagem/produto/".$imagem);		
			
			
			//////////////////////////////////////////////////////////
			//redimenciona para -med			
			list($imagem_x, $imagem_y) = explode("x",$img_dimensao_m);
			
			$image->load("../imagem/tmp/".$file_name);
			$count==0 ? $imagem_m = str_replace("-big.","-med.",$imagem) : $imagem_m = str_replace("-big-","-med-",$imagem);
			$image->resize( $imagem_x , $imagem_y );
			$image->save("../imagem/produto/".$imagem_m);					



			
			//////////////////////////////////////////////////////////
			//redimenciona para -tumb			
			list($imagem_x, $imagem_y) = explode("x",$img_dimensao_p);
			
			$image->load("../imagem/tmp/".$file_name);
			$count==0 ? $imagem_p = str_replace("-big.","-tumb.",$imagem) : $imagem_p = str_replace("-big-","-tumb-",$imagem);
			$image->resize( $imagem_x , $imagem_y );
			$image->save("../imagem/produto/".$imagem_p);					
		
			
			///////////////////////////////////////////////////////
			//seta o valor na variavel pra gravar no banco de dados
			$arquivo = "imagem/produto/".$imagem_p;
						
			
			if($principal == -1){
				mysql_query("update tblproduto_midia set mprincipal=0 where mcodproduto='{$id}' and mprincipal <> 2");	
			}

			if($principal == 2){
				mysql_query("update tblproduto_midia set mprincipal=0 where mcodproduto='{$id}' and mprincipal <> -1");	
			}

			
		}
		
		
		
		///////////////////////////////////////////////////////////
		//arquivos pdf
		if($error==0 && $tipo == 3){
			
			$arquivo = gera_url_seo($file_name) . right($file_name,4);
			$arquivo = "arquivo/".strtolower($arquivo);
			
			
			if( file_exists($arquivo) ){
				$arquivo = left($arquivo,-4) . mktime() . right($arquivo,4);
			}
			
			//move para pasta TMP
			$move = move_uploaded_file($file_tmp_name,"../".$arquivo);

		}
		
		
		
		
		
		
		
		
		
		
		///////////////////////////////////////////////////////////
		//insere no banco de dados
			
		if($midiaid==0){
			$ssql = "insert into tblproduto_midia(mcodproduto, mcodtipo, mtitulo, mtexto, mprincipal, marquivo, mcodusuario, mdata_alteracao, mdata_cadastro)
					values('{$id}','{$tipo}','{$titulo}','{$texto}','{$principal}','{$arquivo}', '{$usuario}','{$data_hoje}','{$data_hoje}')";
			$result = mysql_query($ssql);		
			if(!$result){
				//echo ajax_iframe('alert()');
				echo ajax_iframe("Erro: <br /><br />Erro ao inserir arquivo no banco de dados.");
				exit();
			}
			
			$midiaid = mysql_insert_id();
			
			if($tipo==1){
				echo ajax_iframe('Arquivo salvo com sucesso<br /><br /><p align=center><img src=../'.$arquivo.'></p>');				
			}
			else
			{
				echo ajax_iframe('Arquivo salvo com sucesso<br /><br />');
			}

				echo ajax_iframe_imagemid($midiaid);
				echo ajax_append_imagem($midiaid);

			
		}
		else
		{
			$ssql = "update tblproduto_midia set mcodproduto='{$id}', mcodtipo='{$tipo}', mtitulo='{$titulo}', mtexto='{$titulo}', mprincipal='{$principal}', 
					marquivo='{$imagem}', mcodusuario='{$usuario}', mdata_alteracao='{$data_hoje}' 
					where midiaid='{$midiaid}'";
			
			$result = mysql_query($ssql);		
			if(!$result){
				echo ajax_iframe("Erro: <br /><br />Erro ao atualizar arquivo no banco de dados");
				exit();
			}
			if($tipo==1){
				echo ajax_iframe('Arquivo salvo com sucesso<br /><br /><p align=center><img src=../'.$arquivo.'></p>');
			}
			else
			{
				echo ajax_iframe('Arquivo salvo com sucesso<br /><br />');
			}			
			
			echo ajax_append_imagem($midiaid);
					
			
		}
		
		///////////////////////////////////////////////////////
		//apaga o arquivo temporário
		if( file_exists("../imagem/tmp/".$file_name) ){
			unlink("../imagem/tmp/".$file_name);	
		}			
		exit();

	
}



if($_POST["action"]=="gravar" && $_POST["tab"]==7){		//guia condicao pagto
		$id				=	addslashes($_REQUEST["id"]);
		$condicoes		= 	explode(",",addslashes($_REQUEST["condicao"]));
		
		$ssql = "delete from tblproduto_condicao_pagamento where pcodproduto='{$id}'";
		mysql_query($ssql);
		
		
		for($i=0;$i<count($condicoes)-1;$i++){
			$condicao = $condicoes[$i];
			$ssql = "insert into tblproduto_condicao_pagamento (pcodproduto, pcodcondicao, pcodusuario, pdata_alteracao, pdata_cadastro)
					values('{$id}','{$condicao}','{$usuario}','{$data_hoje}','{$data_hoje}')"	;
			$result = mysql_query($ssql);
			
			if(!$result){
				echo "erro||$id||Erro ao atualizar os produtos nas conidções de pagamento.";
				exit();
				break;
			}
		}
		
		echo "ok||$id||Registro salvo com sucesso";
		exit();					
}











if($_POST["action"]=="gravar" && $_POST["tab"]==8){		//guia de frete
		$id				=	addslashes($_REQUEST["id"]);
		$fretes			= 	explode(",",addslashes($_REQUEST["frete"]));
		
		$ssql = "delete from tblproduto_frete where pcodproduto='{$id}'";
		mysql_query($ssql);
		
		
		for($i=0;$i<count($fretes)-1;$i++){
			$frete = $fretes[$i];
			$ssql = "insert into tblproduto_frete (pcodproduto, pcodfrete, pcodusuario, pdata_alteracao, pdata_cadastro)
					values('{$id}','{$frete}','{$usuario}','{$data_hoje}','{$data_hoje}')"	;
			$result = mysql_query($ssql);
			
			if(!$result){
				echo "erro||$id||Erro ao atualizar os produtos nas conidções de frete e envio.";
				exit();
				break;
			}
		}
		
		echo "ok||$id||Registro salvo com sucesso";
		exit();		
				
}





if($_REQUEST["action"]=="load_images"){		//imagens

	$id = $_REQUEST["id"];

	$ssql = "select midiaid, mcodtipo, marquivo, mtitulo, mprincipal from tblproduto_midia where mcodproduto = $id and mcodtipo=1 order by mprincipal, midiaid";
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			echo 
				'<div id="img_produto_'.$row["midiaid"].'" rel="'.$row["midiaid"].'" class="imagem_produto" principal="'.$row["mprincipal"].'" tipo="1" title="'.$row["mtitulo"].'">
					<img src="../'.$row["marquivo"].'"  />												
					<div class="imagem_produto_excluir"><img src="images/ico_excluir.gif" class="img_excluir" rel="'.$row["midiaid"].'" /></div>
				 </div>';
				echo "\r\n";
		}
		mysql_free_result($result);
	}
												
}


if($_REQUEST["action"]=="load_arquivos"){		//arquivos pdf

	$id = $_REQUEST["id"];

	$ssql = "select midiaid, mcodtipo, marquivo, mtitulo, mprincipal from tblproduto_midia where mcodproduto = $id and mcodtipo=3 order by mprincipal, midiaid";
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			echo 
				'<div id="img_produto_'.$row["midiaid"].'" rel="'.$row["midiaid"].'" class="imagem_produto" principal="'.$row["mprincipal"].'" tipo="3" title="'.$row["mtitulo"].'">
					<img src="../images/ico-pdf.png"  /><br/>
					'.$row["mtitulo"].'
					<div class="imagem_produto_excluir"><img src="images/ico_excluir.gif" class="img_excluir" rel="'.$row["midiaid"].'" /></div>
				 </div>';
				echo "\r\n";
		}
		mysql_free_result($result);
	}
												
}



if($_REQUEST["action"]=="del_image"){//imagens

	$id = $_REQUEST["id"];
	$midiaid = $_REQUEST["midiaid"];

	$ssql = "delete from tblproduto_midia where mcodproduto = $id and midiaid=$midiaid";
	mysql_query($ssql);
												
}



if($_REQUEST["action"]=="get_produtoid_by_sku"){
	$codigo = $_REQUEST["codigo"];
	$ssql = "select produtoid, pproduto, pdescricao from tblproduto where pcodigo='{$codigo}'";
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			echo "ok||".$row["produtoid"]."||".$row["pproduto"]."||".$row["pdescricao"];
		}
		mysql_free_result($result);
	}
	
}



function ajax_iframe($conteudo){
	$ret = '<script type="text/javascript" src="js/jquery.js"></script>';
    $ret .= '<script language="javascript" type="text/javascript">$("#dica6", top.document).html("'.$conteudo.'");</script>';
	return $ret;
}

function ajax_iframe_imagemid($id){
	/*return '<script language="javascript" type="text/javascript">$("#midiaid", top.document).attr("value","'.$id.'");</script>';*/
}

function ajax_append_imagem($id){
	echo "<script>\r\n";	
	echo '$("#midiaid", top.document).change()';
	echo "</script>";
}


function existe_arquivo($arquivo){
	return file_exists($arquivo);	
}


if($_POST["action"]=="url_seo"){
	$string = $_POST["string"];
	
	$string = html_entity_decode($string);
	
	//$string = strtolower($string);

	echo gera_url_seo($string);
}


/*function gera_url_seo($input){
	$ret = $input;
	
	$ret = str_replace("à","a",$ret);
	$ret = str_replace("á","a",$ret);
	$ret = str_replace("ã","a",$ret);
	$ret = str_replace("â","a",$ret);
	$ret = str_replace("é","e",$ret);
	$ret = str_replace("ê","e",$ret);
	$ret = str_replace("í","i",$ret);
	$ret = str_replace("ó","o",$ret);
	$ret = str_replace("õ","o",$ret);
	$ret = str_replace("ú","u",$ret);
	$ret = str_replace("ü","u",$ret);
	$ret = str_replace("ç","c",$ret);
	$ret = str_replace("ñ","n",$ret);
	$ret = trim(ereg_replace(' +','-',preg_replace('/[^a-zA-Z0-9-\s]/','',$ret))); 
	$ret = str_replace("--","-",$ret);
	$ret = str_replace("----","---",$ret);
	
	return $ret;
	
}*/

?>