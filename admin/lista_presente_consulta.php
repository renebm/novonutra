<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$uri = str_replace("/evoluamoda/","",$_SERVER['REQUEST_URI']);	

	$pagina = 1;
	$start = 0;
	$limit = 20;

	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}


	/*--------------------------------------------------------------------------
	exportar
	---------------------------------------------------------------------------*/
	if($_REQUEST["action"]=="exportar"){
		
			header("Content-type: application/vnd.ms-excel");
			header("Content-type: application/force-download");
			header("Content-Disposition: attachment; filename=report_lista_presente.csv");
			header("Pragma: no-cache");		
		
			echo utf8_decode("#ID;Titulo;Nomes;Data;Link;Tipo de Lista;Crédito; Data Cadastro;\r\n");
				$ssql = $_SESSION["ssql"];
				$result = mysql_query($ssql);
				if($result){
						while($row=mysql_fetch_assoc($result)){			
						
							$valor_credito	=	"0,00";
							$tipo 			= ($row["lcredito"]==-1) ? "Crédito" : "Venda";
							
							if( $row["lcredito"]== -1 ){
								$valor_credito = number_format($row["total"],2,",",".");
							}
													
						
							echo $row["listaid"].";";
							echo utf8_decode($row["ltitulo"]." "). " " . ";";
							echo utf8_decode($row["lnome"]." "). " " . ";";
							echo formata_data_tela($row["ldata_evento"],10). " " . ";";
							echo $site_site . "/" . $row["llink_seo"].";";
							echo utf8_decode($tipo) .";";
							echo $valor_credito .";";
							echo formata_data_tela($row["ldata_cadastro"]) .";";
							echo "\r\n";
						}
						mysql_free_result($result);				
				}
				die();
		
	}
												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$('#string').focus();
		$('#data1').mask("99/99/9999");
		$('#data2').mask("99/99/9999");
	});
  

</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    

    	<div id="conteudo">
			<div id="titulo-consulta">
            	<span class="label-inicio">Cadastro &raquo; <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='cadastro_consulta.php';">Relatório</span></span> 
          </div>
            
            <div id="conteudo-interno">
            <?php 
				if (!$_GET){
			?>
             <form name="frm_consulta" id="frm_consulta" method="get" action="?" >
             <table width="99%" border="0" cellspacing="2" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="50%">&nbsp;</td>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>Titulo / Nomes:</td>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td><input name="string" type="text" class="formulario" id="string" size="75" maxlength="200" /></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Data Início</td>
                <td>Data Término</td>
              </tr>
              <tr>
                <td><input name="data1" type="text" class="formulario" id="data1" size="15" maxlength="10" />
dd/mm/aaaa</td>
                <td><input name="data2" type="text" class="formulario" id="data2" size="15" maxlength="10" />
dd/mm/aaaa</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><input type="submit" id="btn-cmd-busca" name="btn-cmd-busca" value="Gerar" class="btn-gravar" /></td>
                <td>&nbsp;</td>
              </tr>
             </table>
             
             </form>
			<?php
				}else{
			?>	
                
                
                <table width="99%" border="0" cellspacing="0" cellpadding="3" style="margin:10px;">
                  <tr>
                    <td width="50" class="titulo_table">#ID</td>
                    <td class="titulo_table">Título</td>
                    <td class="titulo_table">Nomes</td>
                    <td class="titulo_table">Data</td>
                    <td class="titulo_table">Link</td>
                    <td class="titulo_table">Tipo de Lista</td>
                    <td width="120" align="center" class="titulo_table">Crédito</td>
                    <td width="120" align="center" class="titulo_table">Data Cadastro</td>
                    <td width="120" align="center" class="titulo_table">Excluir</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
                  
                  <?php
				  
				  	$string = addslashes($_REQUEST["string"]);
					$data1 = addslashes($_REQUEST["data1"]);
					$data2 = addslashes($_REQUEST["data2"]);
				  
                  	$ssql = "select l.listaid, l.lcodcadastro, l.ltitulo, l.lnome, l.ldata_evento, l.llink_seo, l.lcredito, l.lnotificacao, l.ldata_cadastro,
							sum(p.pvalor_total) as total
							from tbllista_presente as l
							left join tblpedido as p on l.listaid = p.pcodlista_presente and p.pcodstatus >= 4 and p.pcodstatus <=6 
							
							where l.ltitulo like '%{$string}%' 
							";

					if($data1!=""){
							$data1 = formata_data_db($data1) . " 00:00:00";
							$ssql .= " and l.ldata_evento >= '{$data1}'";	
					}
					
					if($data2!=""){
							$data2 = formata_data_db($data2) . " 23:59:59";
							$ssql .= " and l.ldata_evento >= '{$data2}'";	
					}

							
					$ssql .= " or l.lnome like '%{$string}%' 
							";	
					
					if($data1!=""){
							$data1 = formata_data_db($data1) . " 00:00:00";
							$ssql .= " and l.ldata_evento >= '{$data1}'";	
					}

					if($data2!=""){
							$data2 = formata_data_db($data2) . " 23:59:59";
							$ssql .= " and l.ldata_evento <= '{$data2}'";	
					}


					$ssql .= " 	group by l.listaid
								order by l.ldata_evento , l.lnome  ";
	
					$_SESSION["ssql"] = $ssql;
		
					$result = mysql_query($ssql);
					if($result){
						$total_registros = mysql_num_rows($result);	
					}					
							
							
					$ssql .= " limit $start, $limit";
							
					//echo $ssql;
							
				  	$result = mysql_query($ssql);
				  	if($result){
					
						if(mysql_num_rows($result)==0){
							echo '
								  <tr>
									<td colspan="7">Nenhum registro localizado.</td>
								  </tr>';							
						}
					
						while($row=mysql_fetch_assoc($result)){
							
							$valor_credito	=	"0,00";
							$tipo 			= ($row["lcredito"]==-1) ? "Crédito" : "Venda";
							
							if( $row["lcredito"]== -1 ){
								$valor_credito = number_format($row["total"],2,",",".");
							}
							
												
							echo '
								  <tr class="tr_lista">
									<td>'.$row["listaid"].'</td>
									<td>'.$row["ltitulo"].'</td>
									<td>'.$row["lnome"].'</td>
									<td>'.formata_data_tela(left($row["ldata_evento"],10)).'</td>
									<td>'.$row["llink_seo"].'</td>
									<td>'.$tipo.'</td>
									<td align="center">'.$valor_credito.'</td>
									<td>'.formata_data_tela($row["ldata_cadastro"]).'</td>
									<td align="center"><a href="javascript:lista_presente_excluir('.$row["listaid"].')"><img src="images/ico_excluir.gif" /></a></td>
								  </tr>							
							';
						}
						mysql_free_result($result);
					
					}
				  ?>
                  

                  
				  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
				  <tr>
				    <td colspan="5"><a href="?action=exportar">[ Exportar Excel ]</a></td>
				    <td colspan="4"><div class="paginacao"><span class="paginacao-text">Página:</span>
				      <?php
                        echo paginacao($pagina, $limit, $total_registros);
                        ?>
			        </div></td>
			      </tr>

                </table>
                
                
                <?php
                
				}
				?>
                
          </div>
            
       </div>

 
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>