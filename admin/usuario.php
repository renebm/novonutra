<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	$erro		= 	0;
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}

	$id = $_REQUEST["id"];	
	if(!is_numeric($id)){
		$id = 0;	
	}
	
	
	if($_SESSION["admin"]!=-1){
		if($usuario!=$id){
			header("location: usuario_consulta.php");
			exit();
		}	
	} 	
	
	
	if($_POST && $_REQUEST["action"]=="gravar"){
		$id						= 	addslashes($_REQUEST["id"]);
		$nome					=	addslashes($_REQUEST["nome"]);
		$email					=	addslashes($_REQUEST["email"]);
		$login					=	addslashes($_REQUEST["login"]);
		$senha					=	addslashes($_REQUEST["senha"]);
		$senha					= 	base64_encode($senha);
		
		$ativo					=	$_REQUEST["ativo"];
		$adm					=	$_REQUEST["adm"];
		
		if(!is_numeric($ativo)){
			$ativo = 0;	
		}

		if(!is_numeric($adm)){
			$adm = 0;	
		}


		if($id==0){
			
			
			if($_SESSION["admin"]!=-1){
				$erro = 1;	
				$msg = "Acesso não permitido";
			}
			
			//verifica se já nao existe o login ou registro
			$ssql = "select ulogin from tblusuario where ulogin='{$login}'";
			$result = mysql_query($ssql);
			if($result){
				$num_rows = mysql_num_rows($result);	
			}
			
			if($num_rows>0){
				$msg = "Erro ao gravar registro. <br />Já existe um usuário <em>$login</em> cadastrado no sistema";	
			}
			
			//insere registro
			if($num_rows==0 && $erro==0){
				$ssql = "insert into tblusuario (ucodfranquia, unome, uemail, ulogin, usenha, ualterarsenha, uativo, uadm, ucodusuario, udata_alteracao, udata_cadastro)
						values('0','{$nome}','{$email}','{$login}','{$senha}','0','{$ativo}','{$adm}','{$usuario}','{$data_hoje}','{$data_hoje}');	
						";	
				$result = mysql_query($ssql);
				if(!$result){
					$msg = "Erro ao gravar registro";
					$erro = 1;
				}
				else
				{
					$id = mysql_insert_id();
					$msg = "Registro salvo com sucesso";
				}
			}
			
		}
		else
		{
				$ssql = "update tblusuario set unome='{$nome}', uemail='{$email}', ulogin='{$login}', usenha='{$senha}', uativo='{$ativo}', uadm='{$adm}', 
						ucodusuario='{$usuario}', udata_alteracao='{$data_hoje}' 
						where usuarioid=$id";
				$result = mysql_query($ssql);
				if(!$result){
					$msg = "Erro ao atualizar registro";
					$erro = 1;
				}
				else
				{
					$msg = "Registro atualizado com sucesso";
				}				
		}
		
		//echo $ssql;	
	}




	if($id>0){
	
		$ssql = "select ucodfranquia, unome, uemail, ulogin, usenha, ualterarsenha, uativo, uadm, ucodusuario, udata_alteracao, udata_cadastro
				from tblusuario where usuarioid=$id
				";
		$result = mysql_query($ssql);
		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				
				$nome					=	$row["unome"];
				$email					=	$row["uemail"];
				$login					=	$row["ulogin"];
				$senha					=	base64_decode($row["usenha"]);
				$ativo					=	$row["uativo"];
				$adm					=	$row["uadm"];
								
				$usuario			=	get_usuario($row["ucodusuario"],"unome");		
				$data_alteracao		=	formata_data_tela($row["udata_alteracao"]);
				$data_cadastro		=	formata_data_tela($row["udata_cadastro"]);
				
			}
			mysql_free_result($result);
		}	
		
	}
	
	
	
	if($id==0){
		$ativa = -1;
		$exibe_imagem = -1;
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {							   		
		$("#razaosocial").focus();	
		$("#cep").mask("99999-999");
		$("#telefone").mask("99 9999-9999");
		
	});

</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form method="post" name="frm_usuario" id="frm_usuario" action="usuario.php" onsubmit="return valida_usuario();">
    <input name="action" id="action" type="hidden" value="gravar" />  
    <input name="id" id="id" type="hidden" value="<?php echo $id?>" />
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Usuário &raquo;  <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='usuario.php';"><?php echo ($id==0) ? "Novo Registro" : $nome;  ?></span> </span> <a href="usuario_consulta.php">Consulta</a>
       	  </div>
            
            <div id="conteudo-interno">
				<table width="98%" border="0" cellspacing="0" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="130">ID</td>
                <td>#<?php echo $id;?></td>
                <td width="200" rowspan="22" align="center" valign="top"><?php echo $msg;?>
                
                </td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Nome</td>
                <td><input name="nome" type="text" class="formulario" id="nome" value="<?php echo $nome;?>" size="75" maxlength="100" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr> 
              <tr>
                <td>E-mail</td>
                <td><input name="email" type="text" class="formulario" id="email" value="<?php echo $email;?>" size="75" maxlength="100" /></td>
                </tr>  
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>                                                        
              <tr>
                <td>Login</td>
                <td><input name="login" type="text" class="formulario" id="login" value="<?php echo $login;?>" size="20" maxlength="20" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Senha</td>
                <td><input name="senha" type="password" class="formulario" id="senha" value="<?php echo $senha;?>" size="20" maxlength="20" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>                                          
              <tr>
                <td>Repetir a senha</td>
                <td><input name="senha1" type="password" class="formulario" id="senha1" value="<?php echo $senha;?>" size="20" maxlength="20" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Ativo</td>
                <td><label>
                  <input type="radio" name="ativo" id="ativo" value="-1" <?php echo ($ativo == -1) ? "checked" : "" ;?>  />
                  Sim</label>
                  <label>
                    <input type="radio" name="ativo" id="ativo" value="0" <?php echo ($ativo == 0) ? "checked" : "" ;?>  />
                Não</label></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Administrador</td>
                <td><label>
                  <input type="radio" name="adm" id="adm" value="-1" <?php echo ($adm == -1) ? "checked" : "" ;?>  <?php echo ($_SESSION["admin"] == 0) ? " disabled" : "" ;?> />
                  Sim</label>
                  <label>
                    <input type="radio" name="adm" id="adm" value="0" <?php echo ($adm == 0) ? "checked" : "" ;?>  <?php echo ($_SESSION["admin"] == 0) ? " disabled" : "" ;?> />
                Não</label></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              
              <tr>
                <td>Usuário</td>
                <td><?php echo $usuario;?></td>
                </tr>
              <tr>
                <td>Data Alteração</td>
                <td><?php echo $data_alteracao;?></td>
                </tr>
              <tr>
                <td>Data Cadastro</td>
                <td><?php echo $data_cadastro;?></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" id="btn-cmd-gravar" name="btn-cmd-gravar" value="Gravar" class="btn-gravar" /></td>
                </tr>
              </table>
          </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>