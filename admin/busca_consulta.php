<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$uri = str_replace("/videokestore/","",$_SERVER['REQUEST_URI']);	

	$pagina = 1;
	$start = 0;
	$limit = 20;

	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}




	/*-----------------------------------------------------------------------
	ajax de status
	------------------------------------------------------------------------*/
	if($_POST && $_REQUEST["action"]=="status"){
		$tag = addslashes(urldecode($_REQUEST["tag"]));
		
		$ssql = "select bativa from tblbusca where btag='{$tag}' order by bativa limit 0,1";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$status = $row["bativa"];
			}
			mysql_free_result($result);
		}
		
		if($status==0){
			$ssql = "update tblbusca set bativa='-1' where btag='{$tag}'";
			$status = "Ativa";
		}
		if($status==-1){
			$ssql = "update tblbusca set bativa='0' where btag='{$tag}'";	
			$status = "Inativa";
		}
		
		mysql_query($ssql);

		echo $status;
		exit();
	}


	/*-----------------------------------------------------------------------
	ajax de exclusão
	------------------------------------------------------------------------*/
	if($_POST && $_REQUEST["action"]=="excluir"){
		$tag = addslashes(urldecode($_REQUEST["tag"]));
		$ssql = "delete from tblbusca where btag='{$tag}'";
		mysql_query($ssql);
		echo "ok";
		exit();
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$('#string').focus();
	});
  

</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    

    	<div id="conteudo">
			<div id="titulo-consulta">
            	<span class="label-inicio">Busca &raquo; <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='busca_consulta.php';">Consulta</span></span>
            </div>
            
            <div id="conteudo-interno">
            <?php 
				if (!$_GET){
			?>
             <form name="frm_consulta" id="frm_consulta" method="get" action="?" >
             <table width="99%" border="0" cellspacing="2" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="50%">&nbsp;</td>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>Expressão de busca</td>
                <td>Status:</td>
               </tr>
              <tr>
                <td><input name="string" type="text" class="formulario" id="string" size="75" maxlength="200" /></td>
                <td><select name="status" size="1" id="status" class="formulario">
                  <option value="2">Selecione</option>
                  <option value="-1">Ativa</option>
                  <option value="0">Inativa</option>
                </select></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Ordenar por</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><select name="ordem" size="1" id="ordem" class="formulario">
                  <option value="0">Mais buscados</option>
                  <option value="1">Data</option>
                  <option value="2">Expressao</option>
                  <option value="3">Origem</option>
                </select></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><input type="submit" id="btn-cmd-busca" name="btn-cmd-busca" value="Consultar" class="btn-gravar" /></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
             </table>
             
             </form>
			<?php
				}else{
			?>	
                
                <table width="99%" border="0" cellspacing="0" cellpadding="3" style="margin:10px;">
                  <tr>
                    <td width="50" class="titulo_table">#ID</td>
                    <td class="titulo_table">Expressão</td>
                    <td align="center" class="titulo_table">Origem</td>
                    <td align="center" class="titulo_table">Referência</td>
                    <td align="center" class="titulo_table">Status</td>
                    <td width="75" align="center" class="titulo_table">Buscas</td>
                    <td width="75" align="center" class="titulo_table">Editar</td>
                    <td width="75" align="center" class="titulo_table">Excluir</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
                  
                  <?php
				  
				  	$string = addslashes($_REQUEST["string"]);
					$status = addslashes($_REQUEST["status"]);
					$ordem = addslashes($_REQUEST["ordem"]);
					
					if($ordem==0) $ordem = " total desc, btag ";
					if($ordem==1) $ordem = " bdata_cadastro ";
					if($ordem==2) $ordem = " btag ";
					if($ordem==3) $ordem = " breferencia, borigem ";
				  
                  	$ssql = "select count(buscaid) as total, btag, borigem, breferencia, bip, bativa
							from tblbusca 
							where btag like '%{$string}%' 
							";	
					
					if($status != 2 ){
							$ssql .= " and bativa=$status ";	
					}
					
					$ssql .= " group by btag ";
					$ssql .= " order by $ordem";
	
		
					$result = mysql_query($ssql);
					if($result){
						$total_registros = mysql_num_rows($result);	
					}					
							
							
					$ssql .= " limit $start, $limit";
							
					//echo $ssql;
							
				  	$result = mysql_query($ssql);
				  	if($result){
					
						if(mysql_num_rows($result)==0){
							echo '
								  <tr>
									<td colspan="5">Nenhum registro localizado.</td>
								  </tr>';							
						}
						$count = $start;
						while($row=mysql_fetch_assoc($result)){
							$status = ($row["bativa"]==-1) ? "Ativa" : "Inativa";
							$origem = ($row["borigem"]==1) ? "Site" : "Buscador";
							$referencia = $row["breferencia"]."&nbsp;";
							$count++;
							echo '
								  <tr class="tr_lista">
									<td>'.$count.'</td>
									<td>'.$row["btag"].'</td>
									<td align="center">'.$origem.'</td>
									<td align="center">'.$referencia.'</td>
									<td align="center" id="status_'.$count.'">'.$status.'</td>
									<td align="center">'.$row["total"].'</td>
									<td align="center"><a href="javascript:void(0)" onclick=javascript:busca_status("'.urlencode($row["btag"]).'","'.$count.'");><img src="images/ico_editar.gif" border="0" /></a></td>
									<td align="center"><a href="javascript:void(0)" onclick=javascript:busca_excluir("'.urlencode($row["btag"]).'");><img src="images/ico_excluir.gif" border="0" /></a></td>
								  </tr>							
							';
						}
						mysql_free_result($result);
					
					}
				  ?>
                  

                  
				  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
				  <tr>
				    <td colspan="8">
				      <div class="paginacao"><span class="paginacao-text">Página:</span> 
				        <?php
                        echo paginacao($pagina, $limit, $total_registros);
                        ?>                            
				        </div>			        </td>
			      </tr>

                </table>
                
                
                <?php
                
				}
				?>
                
          </div>
            
       </div>

 
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>