<?php
include("../include/inc_conexao.php");
include("inc_sessao.php");

	header('Content-Type: text/html; charset=utf-8'); 

	$usuario	=	intval($_SESSION["usuarioid"]);
	$action		=	addslashes($_REQUEST['action']);	


	$pedidoid	=	intval($_REQUEST["id"]);
	$nfeid		=	intval($_REQUEST["nfeid"]);
	
	$ambiente	= 	2;	// 1 - Producao | 2 = Homologacao
	//$ambiente	= 	1;	// 1 - Producao | 2 = Homologacao
	
	
/*----------------------------------------------------------------------------------------------------------
GERA ARQUIVO TXT
-----------------------------------------------------------------------------------------------------------*/
	
if( $action == "gera_txt" ){
		
	//
	//mysql_query("update tblpedido set pcodnota_fiscal = '0' where pedidoid='{$pedidoid}'");
	
	$nfe_serie 		= "1";
	$data_emissao	= date("Y-m-d");
	$data_saida		= date("Y-m-d");
	
	$impressao		= 1;
	$loja_crt		= 1; //	1 – Simples Nacional	//	2 – Simples Nacional – excesso de sublimite de receita bruta // 3 – Regime Normal. (v2.0).
	
	if($pedidoid==0){
		echo "erro||Pedido inválido";
		die();
	}



	/*--------------------------------------------------------------------------------------------------------------------------------------------------------
	verifica se o pedido já não tem nota fiscal
	---------------------------------------------------------------------------------------------------------------------------------------------------------*/
	if($nfeid==0){
		$ssql = "select pcodnota_fiscal from tblpedido where pedidoid = '{$pedidoid}' ";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$notafiscalid = $row["pcodnota_fiscal"];
			}	
			mysql_free_result($result);
		}
		
		if(intval($notafiscalid)>0){
			echo "erro||O pedido ".$pedidoid." já está vinculado a NF-e " .$notafiscalid;
			die();
		}
	}



	/*--------------------------------------------------------------------------------------------------------------------------------------------------------
	NO. DA NF-e
	---------------------------------------------------------------------------------------------------------------------------------------------------------*/
	$notafiscalid 	= 0; 
	
	$ssql = "select pcodnota_fiscal from tblpedido order by pcodnota_fiscal desc limit 0,1";
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$notafiscalid = $row["pcodnota_fiscal"];
		}	
		mysql_free_result($result);
	}
	
	
	if(intval($notafiscalid)==0){
		echo "erro||No. de nota fiscal não configurado.";
		die();
	}

	//número da nova nota fiscal ou da nfeid já emitida
	//$notafiscalid = ($nfeid==0) ? $notafiscalid++ : $nfeid; 
	if($nfeid==0){
		$notafiscalid++;
	}
	else
	{
		$nfeid; 
	}
	
	
	
	/*--------------------------------------------------------------------------------------------------------------------------------------------------------
	VINCULA A NF-E AO PEDIDO
	---------------------------------------------------------------------------------------------------------------------------------------------------------*/
	$ssql = "update tblpedido set pcodnota_fiscal = '{$notafiscalid}' where pedidoid='{$pedidoid}'";
	$result = mysql_query($ssql);
	if(!$result){
		echo "erro||".mysql_error($result);
		die();
	}
	
	
	/*--------------------------------------------------------------------------------------------------------------------------------------------------------
	SELECTS DOS DADOS DA LOJA
	---------------------------------------------------------------------------------------------------------------------------------------------------------*/

	//seleciona os dados do emitente - LOJA
	$ssql = "SELECT l.lrazaosocial, l.lnome, l.lemail, l.lsite, l.lendereco, l.lnumero, l.lcomplemento, l.lbairro, l.lcidade, l.lestado, l.lcep, l.ltelefone, 
			l.lcnpj, l.lie, l.lim, l.lcnae, l.lcrt
			FROM tblloja AS l
			";
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$loja_razao_social 	= $row["lrazaosocial"];	
			$loja_nome 			= $row["lnome"];
			$loja_email			= $row["lemail"];
			$loja_site			= $row["lsite"];
			$loja_endereco		= $row["lendereco"];
			$loja_numero		= $row["lnumero"];
			$loja_complemento	= $row["lcomplemento"];
			$loja_bairro		= $row["lbairro"];
			$loja_cidade		= $row["lcidade"];
			$loja_estado		= $row["lestado"];
			$loja_cep			= get_only_numbers($row["lcep"]);
			$loja_telefone		= get_only_numbers($row["ltelefone"]);
			
			$loja_cnpj			= get_only_numbers($row["lcnpj"]);
			$loja_ie			= get_only_numbers($row["lie"]);
			$loja_im			= get_only_numbers($row["lim"]);
			$loja_cnae			= get_only_numbers($row["lcnae"]);
			$loja_crt			= get_only_numbers($row["lcrt"]);
		}
		mysql_free_result($result);
	}

	//seleciona os dados da tblibge_municipio	
	$ssql = "SELECT m.mcodigo FROM tblibge_municipio AS m where mmunicipio = '{$loja_cidade}' ";
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$loja_ibge_cidade = $row["mcodigo"];
		}
		mysql_free_result($result);
	}
	
	
	//seleciona os dados da tblibge_estado	
	$ssql = "SELECT e.ecodigo FROM tblibge_estado AS e where esigla = '{$loja_estado}' ";
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$loja_ibge_estado = $row["ecodigo"];
		}
		mysql_free_result($result);
	}	



	if( trim($loja_cnpj) == "" || trim($loja_ie) == "" || trim($loja_im) == "" || trim($loja_cnae) == "" || trim($loja_crt) == "" ){
		
		//zera o codigo da NF-e no pedido
		nfe_reset_pedido($pedidoid);
		
		echo "erro||O CNPJ, IE, IM, CNAE, CRT da loja não foram configurados.";
		die();
	}
	
	
	
	/*-----------------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------------*/
	$base_url =  ($_SERVER['SERVER_NAME']=="servidor") ? "http://servidor/popdecor/admin/nfe" : "http://www.popdecor.com.br/admin/nfe";
	
	/*$GLOBALS['dados'] = array("ambiente"=>"1", "empresa"=>$loja_razao_social, "UF"=>$loja_estado, "cnpj"=>$loja_cnpj, "certName"=>"certificado-popdecor.pfx", 
			   "keyPass"=>"pop216327","passPhrase"=>"pop216327","arquivosDir"=>"arquivos/nfe","arquivoURLxml"=>"nfe_ws2.xml",
			   "baseurl"=>$base_url,"danfeLogo"=>"","danfeLogoPos"=>"L","danfeFormato"=>"L","danfeCanhoto"=>"1","danfeFonte"=>"Times",
			   "danfePrinter"=>"hpteste","schemes"=>"PL_006p","certsDir"=>"certs");
	*/
	
	
	//seleciona os dados do pedido
	$ssql = "SELECT p.pedidoid, p.pnome, p.pendereco, p.pnumero, p.pcomplemento, p.pbairro, p.pcidade, p.pestado, p.pcep, 
					p.psubtotal, p.pvalor_desconto, p.pvalor_frete, p.pvalor_total,	p.pvalor_desconto_cupom, p.pdata_cadastro,
					c.ccodtipo, c.ccpf_cnpj, c.crg_ie,
					cp.ccondicao, cp.cnumero_parcelas,
					f.fdescricao
					FROM tblpedido as p
					inner join tblcadastro as c on p.pcodcadastro = c.cadastroid
					inner join tblcondicao_pagamento as cp on p.pcodcondicao_pagamento = cp.condicaoid
					inner join tblfrete_tipo as f on p.pcodfrete = f.freteid
					where p.pedidoid = '{$pedidoid}'
			";
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			
			if($nfeid == 0 ){
				$data_emissao	=	left($row["pdata_cadastro"],10);
				$data_saida		=	left($row["pdata_cadastro"],10);
			}
			
			$cadastro_tipo	=	$row["ccodtipo"];
			$razao_social	=	$row["pnome"];
			$cpf_cnpj		=	get_only_numbers($row["ccpf_cnpj"]);
			$rg_ie			=	($row["ccodtipo"] == 1) ? "ISENTO" : $row["crg_ie"];
			
			if($rg_ie==""){
				$rg_ie = "ISENTO";	
			}
			
			$endereco		=	$row["pendereco"];
			$numero			=	$row["pnumero"];
			$complemento	=	$row["pcomplemento"];
			$bairro			=	$row["pbairro"];
			$cidade			=	trim($row["pcidade"]);
			$estado			=	trim($row["pestado"]);
			$cep			=	get_only_numbers($row["pcep"]);
			
			$condicao_pagamento = ($row["cnumero_parcelas"]==1) ? 1 : 2;		//0 = outros | 1 - A vista | 2 = a prazo
			
			$transportadora		= $row["fdescricao"];	
			
			//totais da NFe - empresa isenta de icms e ipi
			$base_calculo_icms		= "0.00";
			$valor_total_icms		= "0.00";
			$base_calculo_icms_st	= "0.00";
			$valor_total_icms_st	= "0.00";
			$subtotal				= number_format($row["psubtotal"] - ( $row["pvalor_desconto"]+$row["pvalor_desconto_cupom"] ) ,2,".","");
			//$valor_frete			= number_format($row["pvalor_frete"],2,".","");
			$valor_frete			= "0.00";
			$valor_seguro			= "0.00";
			$valor_desconto			= number_format($row["pvalor_desconto"]+$row["pvalor_desconto_cupom"],2,".","");
			$valor_total_II			= "0.00";
			$valor_ipi				= "0.00";
			$valor_pis				= "0.00";
			$valor_cofins			= "0.00";
			$valor_outras_despesas	= "0.00";
			$valor_total			= number_format($row["pvalor_total"]-$row["pvalor_frete"],2,".","");	
			
			$frete_tipo				= 1;//($valor_frete==0) ? "0" : "1";
			
		}
		mysql_free_result($result);
	}	
	
	//seleciona os dados da tblibge_municipio	
	$num_rows = 0;
	$ssql = "SELECT m.mcodigo FROM tblibge_municipio AS m where mmunicipio = '{$cidade}' and muf='{$estado}' limit 0,1";
	$result = mysql_query($ssql);
	if($result){
		$num_rows = mysql_num_rows($result);
		while($row=mysql_fetch_assoc($result)){
			$ibge_cidade = $row["mcodigo"];
		}
		mysql_free_result($result);
	}
	
	if( intval($num_rows) == 0 ){
		
		//zera o codigo da NF-e no pedido
		nfe_reset_pedido($pedidoid);
		
		echo "erro||A cidade $cidade não foi localizada na tabela do IBGE.";
		die();
	}
	
	
	//seleciona os dados da tblibge_estado	
	$num_rows = 0;
	$ssql = "SELECT e.ecodigo FROM tblibge_estado AS e where esigla = '{$estado}' ";
	$result = mysql_query($ssql);
	if($result){
		$num_rows = mysql_num_rows($result);
		while($row=mysql_fetch_assoc($result)){
			$ibge_estado = $row["ecodigo"];
		}
		mysql_free_result($result);
	}		

	if( intval($num_rows) == 0 ){
		
		//zera o codigo da NF-e no pedido
		nfe_reset_pedido($pedidoid);
		
		echo "erro||O estado $estado não foi localizado na tabela do IBGE.";
		die();
	}




	//seleciona os itens do pedido
	$produtos = array();
	$i=0;
	$ssql = "SELECT tblproduto.produtoid, tblproduto.pcodigo, tblproduto.pproduto, tblproduto.psubtitulo, tblproduto.ppeso, 
				tblproduto.pncm, tblproduto.picms, tblproduto.ppis, tblproduto.pcofins,
				tblpedido_item.pvalor_unitario, tblpedido_item.pquantidade, pai.ppropriedade AS propriedade_pai, tblproduto_propriedade.ppropriedade AS ptamanho, 
				proper.ppropriedade AS ppropriedade
				
				FROM tblproduto
				
				INNER JOIN tblpedido_item ON tblproduto.produtoid = tblpedido_item.pcodproduto
				LEFT JOIN tblproduto_propriedade ON tblpedido_item.pcodtamanho = tblproduto_propriedade.propriedadeid
				LEFT JOIN tblproduto_propriedade AS proper ON tblpedido_item.pcodpropriedade = proper.propriedadeid
				LEFT JOIN tblproduto_propriedade AS pai ON tblproduto_propriedade.pcodpropriedade = pai.propriedadeid
				where tblpedido_item.pcodpedido='{$pedidoid}' and tblpedido_item.pquantidade > 0
				";				
	$result = mysql_query($ssql);	
	if($result){
		
		$num_rows = mysql_num_rows($result);
		
		while($row=mysql_fetch_assoc($result)){
			
			$vdesconto						=	number_format(($valor_desconto/$num_rows)/$row["pquantidade"],2,".","");
			
			$produtos[$i]["codigo"]			=	trim($row["pcodigo"]);
			$produtos[$i]["ean"]			=	"";
			$produtos[$i]["produto"]		=	trim($row["pproduto"]);
			$produtos[$i]["ncm"]			=	($row["pncm"]=="") ? "70200000" : $row["pncm"];
			$produtos[$i]["cfop"]			=	($ibge_estado == $loja_ibge_estado ) ? "5102" : "6102";
			$produtos[$i]["unidade"]		=	"UN";
			$produtos[$i]["quantidade"]		=	number_format($row["pquantidade"],4,".","");
			$produtos[$i]["valor"]			=	number_format($row["pvalor_unitario"]-$vdesconto,4,".","");
			$produtos[$i]["desconto"]		= 	($row["pdesconto"]==0) ? "" : number_format($row["pdesconto"] ,2,".","");
			$produtos[$i]["valor_bruto"]	=	number_format( ( $row["pvalor_unitario"] * $row["pquantidade"] )-$vdesconto ,2,".","");
			
			//$produtos[$i]["subtotal"]		=	number_format( ($row["pvalor_unitario"] * $row["pquantidade"] )-$vdesconto,4,".","");
			

			$produtos[$i]["cfop"]			= 	($ibge_estado == $loja_ibge_estado ) ? "5102" : "6102";
			
			$produtos[$i]["ncm"]			= 	$row["pncm"];
			
			if( trim($row["pncm"]) == "" ){
				
				//zera o codigo da NF-e no pedido
				nfe_reset_pedido($pedidoid);				
				
				echo "erro||O produto " . $row["pcodigo"] . " não possui NCM.";
				die();
			}			
			
			$volumes						=	1;
			
			$cfop							=	($ibge_estado == $loja_ibge_estado ) ? "5102" : "6102";
			
			$i++;
		}
		mysql_free_result($result);
	}





	/*--------------------------------------------------------------------------------------------------------------------------------------------------------
	CRIAÇÃO DO ARQUIVO
	---------------------------------------------------------------------------------------------------------------------------------------------------------*/

	//cria o arquivo txt
	if(!file_exists("nfe")){
		mkdir("nfe",777);	
	}

	$arquivo = "nfe/txt/" . right(str_repeat("0",9).$pedidoid,9) . ".txt";
	
	if(file_exists($arquivo)){
		unlink($arquivo);	
	}		
	
	
	$chave_codigo		= rand(11111111,99999999);
	$chave_acesso		= nfe_gera_chave($ibge_estado, $loja_cnpj, $notafiscalid, $chave_codigo );
	$chave_acesso_dv	= right($chave_acesso,1);
	

	/*--------------------------------------------------------------------------------------------------------------------------------------------------------
	APPEND DE DADOS NO ARQUIVO
	---------------------------------------------------------------------------------------------------------------------------------------------------------*/

	//A - Atributos da NF-e
	$conteudo = "NOTAFISCAL|1"."\r\n";
	$conteudo .= "A|2.00|NFe$chave_acesso|"."\r\n";
	
	$x = nfe_gera_txt($arquivo,$conteudo);
	
	
	//B - Identificadores da NF-e	|	55 = substituicao modeloa 1 ou 1A de NF	
	$conteudo = "B|$loja_ibge_estado|$chave_codigo|VENDA|$condicao_pagamento|55|$nfe_serie|$notafiscalid|$data_emissao|$data_saida||1|$loja_ibge_cidade|$impressao|1|$chave_acesso_dv|$ambiente|1|3|2.0.6|" . "\r\n";


	
	$x = nfe_gera_txt($arquivo,$conteudo);
	
	
	
	//C - Emitente Loja
	$conteudo = "C|$loja_razao_social|$loja_nome|$loja_ie|$loja_iest|$loja_im|$loja_cnae|$loja_crt|"."\r\n";
	$conteudo .= "C02|$loja_cnpj|"."\r\n";
	$conteudo .= "C05|$loja_endereco|$loja_numero|$loja_complemento|$loja_bairro|$loja_ibge_cidade|$loja_cidade|$loja_estado|$loja_cep|1058|Brasil|$loja_telefone|"."\r\n";
	
	$x = nfe_gera_txt($arquivo,$conteudo);


	//E - Destinatário
	$conteudo = "E|$razao_social|$rg_ie|$suframa|"."\r\n";
	$conteudo .= ($cadastro_tipo==1) ? "E03" : "E02";
	$conteudo .= "|$cpf_cnpj|"."\r\n";
	
	$conteudo .= "E05|$endereco|$numero|$complemento|$bairro|$ibge_cidade|$cidade|$estado|$cep|1058|Brasil|$telefone|"."\r\n";
	
	$x = nfe_gera_txt($arquivo,$conteudo);


	//F - Local de retirada - Informar apenas quando for diferente do endereço do remetente.	//G - Local de entrega - Informar apenas quando for diferente do endereço do destinatário.
	
	
	//H - Detalhamento de produtos e serviços - máximo = 990	&& //I - Produto e serviço 	//I-18 Declaração de Importação		//I-25 - Adições
	$conteudo = "";
	
	for($i=0;$i<count($produtos);$i++){
		
		$count++;
		
		$conteudo .= "H|$count|".$produtos[$i]["codigo"]."|"."\r\n";
		$conteudo .= "I|".$produtos[$i]["codigo"]."|$ean|".$produtos[$i]["produto"]."|".$produtos[$i]["ncm"]."|$tipi|$genero|".$produtos[$i]["cfop"]."|";
		$conteudo .= $produtos[$i]["unidade"]."|".$produtos[$i]["quantidade"]."|".$produtos[$i]["valor"]."|".$produtos[$i]["valor_bruto"]."|$ean|".$produtos[$i]["unidade"]."|";
		$conteudo .= $produtos[$i]["quantidade"]."|".$produtos[$i]["valor"]."|||".$produtos[$i]["desconto"]."|".$produtos[$i]["outros"]."|1|"."|".$count."|";
		$conteudo .= "\r\n";
	
		//M - Grupo de Tributos incidentes no Produto ou Serviço
		$conteudo .= "M|"."\r\n";
		
		//N- ICMS // N10 - CST - 90 – Outros
		$conteudo .= "N|"."\r\n";
		$conteudo .= "N10d|0|102|"."\r\n";		//Tributação pelo ICMS 90 - Outros - N10d|Orig|CSOSN|
		
		
		//O - IPI - Informar apenas quando o item for sujeito ao IPI	[0 ou 1]
			
		//P - II - Informar apenas quando o item for sujeito ao II		[0 ou 1]
		
		//Q - PIS	//Q05 - PPIS - grupo de PIS Outras Operações
		$conteudo .= "Q|"."\r\n";
		$conteudo .= "Q05|99|0.00|0.00|0.00|0.0000|0.0000|"."\r\n";		//CST = 99  = 99 - Outras Operações.  // |CST|vPIS|vBC|pPIS|qBCProd|vAliqProd|	
		
		
		
		//R - PIS Substituição Tributária
		//$conteudo .= "R|"."\r\n";
		//$conteudo .= "R04|0.0000|0.0000|"."\r\n";
		
		
		//S - COFINS		//S05 - COFINS - grupo de COFINS Outras Operações
		$conteudo .= "S|"."\r\n";
		$conteudo .= "S05|99|0.00|0.00|0.00|0.0000|0.0000|"."\r\n";		//COFINS = 99  = 99 - Outras Operações.  // |CST|vCOFINS|vBC|pCOFINS|qBCProd|vAliqProd|	

		
		//T - COFINS Substituição Tributária
		
		//U - ISS - Informar os campos para cálculo do ISSQN nas NFe conjugadas, onde há a prestação de serviços sujeitos ao ISSQN e fornecimento de peças sujeitas ao ICMS
		
		
	}
	
	$x = nfe_gera_txt($arquivo,$conteudo);
	
	
	//J - Veículos Novos //K - Medicamento - múltiplas ocorrências (ilimitado)	//L - Armamento - múltiplas ocorrências (ilimitado) //L01-Combustível - Informar apenas para operações com combustíveis líquidos. 	//M - Grupo de Tributos incidentes no Produto ou Serviço
	

	$valor_desconto = "0.00";
	//W - Totais	//W-02 = Grupo de Valores Totais referentes ao ICMS
	$conteudo = "W|"."\r\n";
	$conteudo .= "W02|$base_calculo_icms|$valor_total_icms|$base_calculo_icms_st|$valor_total_icms_st|$subtotal|$valor_frete|$valor_seguro|$valor_desconto|";
	$conteudo .= "$valor_total_II|$valor_ipi|$valor_pis|$valor_cofins|$valor_outras_despesas|$valor_total|"."\r\n";
		
		
	$x = nfe_gera_txt($arquivo,$conteudo);
	
	//W17 - Grupo de Valores Totais referentes ao ISSQN
	//$conteudo = "W17|"."\r\n";
	//$conteudo .= "$valor_servicos|$base_calculo_iss|$valor_total_iss|$valor_pis_servico|$valor_confis_servico"."\r\n";
	
	//W23 - Grupo de Retenções de Tributos
	
	
	
	//X - Transporte	//X03 - Transportador	//X26 - Volumes
	$conteudo = "X|$frete_tipo|"."\r\n";
	$conteudo .= "X03|$transportadora|||||"."\r\n";	
	$conteudo .= "X04||"."\r\n";
	
	$conteudo .= "X26|$volumes|";
	
	$x = nfe_gera_txt($arquivo,$conteudo);



	echo "ok||Arquivo txt gerado com sucesso, gerando XML.";
	die();


}





/*----------------------------------------------------------------------------------------------------------
CONVERTE TXT EM XML
-----------------------------------------------------------------------------------------------------------*/

if( $action == "gera_xml" ){

	$arquivo_txt 	= "nfe/txt/" . right(str_repeat("0",9).$pedidoid,9) . ".txt";
	$arquivo_xml 	= "nfe/xml/" . right(str_repeat("0",9).$pedidoid,9) . ".xml";
	
	if($pedidoid == 0){
		echo "erro||Número do pedido inválido";
		die();
	}

	if( !is_file($arquivo_txt) ){
		echo "erro||Erro ao ler o arquivo " .$arquivo_txt."";
		die();
	}

	require_once('nfe/class/ConvertNFePHP.class.php');

	$nfe = new ConvertNFePHP();

	if ( is_file($arquivo_txt) ){
		$xml = $nfe->nfetxt2xml($arquivo_txt);
		if ($xml != ''){
			if (!file_put_contents($arquivo_xml,$xml)){
								
				echo "erro||Erro ao gravar o arquivo " . $arquivo_xml;
				die();
			} 
		}
	}
	
	
	echo "ok||Arquivo xml gerado com sucesso";
	die();

}




/*----------------------------------------------------------------------------------------------------------
ASSINA O XML
-----------------------------------------------------------------------------------------------------------*/
if( $action == "assina_xml" ){
	
	$arquivo_xml 	= 	"nfe/xml/" . right(str_repeat("0",9).$pedidoid,9) . ".xml";		
	$arquivo_xml_out= 	"nfe/xml/" . right(str_repeat("0",9).$pedidoid,9) . ".xml";
	
	if( !is_file($arquivo_xml) ){
		
		echo "erro||Erro ao ler o arquivo " .$arquivo_xml."";
		die();
	}


	require_once('nfe/class/ToolsNFePHP.class.php');
	$nfe = new ToolsNFePHP();
	
	
	$arq = file_get_contents($arquivo_xml);
	
	if ($xml = $nfe->signXML($arq, 'infNFe')){
		file_put_contents($arquivo_xml_out, $xml);
	} else {
		
		//zera o codigo da NF-e no pedido
		nfe_reset_pedido($pedidoid);
		
		
		echo "erro||".$nfe->errMsg;
	}
	
	echo "ok||Xml assinado com sucesso";
	die();

}


/*----------------------------------------------------------------------------------------------------------
VALIDA O XML
-----------------------------------------------------------------------------------------------------------*/
if( $action == "valida_xml" ){

	$arquivo_xml 	= 	"nfe/xml/" . right(str_repeat("0",9).$pedidoid,9) . ".xml";		
	
	if( !is_file($arquivo_xml) ){
		echo "erro||Erro ao ler o arquivo " .$arquivo_xml."";
		die();
	}


	require_once('nfe/class/ToolsNFePHP.class.php');
	
	$nfe = new ToolsNFePHP;
	$docxml = file_get_contents($arquivo_xml);
	
	$xsdFile = 'nfe/schemes/PL_006p/nfe_v2.00.xsd';
	$aErro = '';
	
	$c = $nfe->validXML($docxml,$xsdFile,$aErro);
	if (!$c){

		//zera o codigo da NF-e no pedido
		nfe_reset_pedido($pedidoid);


		echo 'erro||';
		foreach ($aErro as $er){
			
			$log = date("Y-m-d H:i:s") . " Erro ao validar XML - " . $er . "\r\n";
			gera_log($log);
			
			echo $er .'<br>';
		}
	} else {
		echo "ok||XML Validado com sucesso";
	}

	die();
}


/*----------------------------------------------------------------------------------------------------------
ENVIA O XML
-----------------------------------------------------------------------------------------------------------*/
if( $action == "envia_xml" ){

	$arquivo_xml 	= 	"nfe/xml/" . right(str_repeat("0",9).$pedidoid,9) . ".xml";		
	
	if( !is_file($arquivo_xml) ){
		echo "erro||Erro ao ler o arquivo " .$arquivo_xml."";
		die();
	}
			
	require_once('nfe/class/ToolsNFePHP.class.php');
	$nfe = new ToolsNFePHP;
	$modSOAP = '2'; //usando cURL
	
	
	//obter um numero de lote
	$lote = substr(str_replace(',','',number_format(microtime(true)*1000000,0)),0,15);
	
	// montar o array com a NFe
	//$recibo_string = trim(file_get_contents($arquivo_xml)); 
	$aNFe 			= array(0=>trim(file_get_contents($arquivo_xml)));
	
	//guarda os dados do recibo
	//nfe_recibo_txt($pedidoid, $recibo_string);
	
	//enviar o lote
	if ($aResp = $nfe->sendLot($aNFe, $lote, $modSOAP, $ambiente)){
		if ($aResp['bStat']){
			
			sleep(2);//espera 3 segundos
			
			echo "ok||" . $aResp['nRec'];
			die();
		} else {
			
			//zera o codigo da NF-e no pedido
			nfe_reset_pedido($pedidoid);			
			
			echo "erro||".$nfe->errMsg;
			die();
		}
	} else {		
		
		//zera o codigo da NF-e no pedido
		nfe_reset_pedido($pedidoid);
		
		echo "erro||".$nfe->errMsg . " " . $nfe->soapDebug;
		die();
	}

	die();

}



/*----------------------------------------------------------------------------------------------------------
CONSULTA RECIBO
-----------------------------------------------------------------------------------------------------------*/
if( $action == "recibo" ){

	$recibo			= 	addslashes($_REQUEST['recibo']);
	
	$arquivo_xml 		= 	"nfe/xml/" . right(str_repeat("0",9).$pedidoid,9) . ".xml";	
	$arquivo_xml_out 	= 	"nfe/xml/" . right(str_repeat("0",9).$pedidoid,9) . ".xml";		
	$arquivo_xml_prot	= 	"nfe/txt/prot-" . right(str_repeat("0",9).$pedidoid,9) . ".txt";
	

	if( trim($recibo) == "" ){
		echo "erro||O número do recibo deve ser informado.";
		die();
	}
	
	
	require_once('nfe/class/ToolsNFePHP.class.php');
	$nfe = new ToolsNFePHP;
	$modSOAP = '2'; //usando cURL
	
	
	$chave = '';
	$tpAmb = $ambiente; //1-producao  2- homologação
	
	
	if ($aResp = $nfe->getProtocol($recibo, $chave, $tpAmb, $modSOAP)){

		$status = $aResp['aProt'][0]['cStat']; //status : 100 OK
		$motivo = $aResp['aProt'][0]['xMotivo']; 
		
		$protocolo	= trim($aResp['xmlRetorno']);

		//guarda o protocolo
		nfe_protocolo_txt($pedidoid,$protocolo);

		if( $status == 100  ){
			
			if ($xml = $nfe->addProt($arquivo_xml, $arquivo_xml_prot)){
				$ret = file_put_contents($arquivo_xml_out, $xml);
				if($ret){
					echo "ok||Protocolo recebido com sucesso.";
					die();
				}
			}
			else
			{
				
				//zera o codigo da NF-e no pedido
				nfe_reset_pedido($pedidoid);
				
				echo "erro||Erro ao incluir protocolo";
				die();
			}
				
		}
		else
		{

			//zera o codigo da NF-e no pedido
			nfe_reset_pedido($pedidoid);

			echo "erro||Error : " . $status . " -> " . $motivo;	
			die();
		}
		
		//echo "<br/><br/>";
		//$aResp['xmlRetorno']
		//print_r($aResp);
		//print_r($aResp);
		die();
	} else {
		
		//zera o codigo da NF-e no pedido
		nfe_reset_pedido($pedidoid);
		
		
		//não houve retorno mostrar erro de comunicação
		echo "err||".$nfe->errMsg;
		die();
	}
		 
}



/*----------------------------------------------------------------------------------------------------------
GERA PDF 
-----------------------------------------------------------------------------------------------------------*/
if( $action == "danfe" ){

	$arquivo_xml 	= 	"nfe/xml/" . right(str_repeat("0",9).$pedidoid,9) . ".xml";
	$notafiscalid	=	get_pedido($pedidoid,"pcodnota_fiscal");
	
	//$xml = simplexml_load_file($arquivo_xml); 	
	//$protocolo 	= $xml->protNFe ; 
	//$chave		= $protocolo->infProt->chNFe;
	//print_r($protocolo->infProt);
	
	require_once('nfe/class/DanfeNFePHP.class.php');	
	
  	$docxml = file_get_contents($arquivo_xml);
    $danfe = new DanfeNFePHP($docxml, 'P', 'A4','','I','');
    $chave = $danfe->montaDANFE();
    
	$arquivo_pdf 	= 	"nfe/pdf/" . $chave . ".pdf";

	echo "ok||Nota Fiscal emitida com sucesso.<br><br><a href='".$arquivo_pdf."' target='_new'>Clique aqui</a> para imprimir/salvar a NF-e.||";
	echo $notafiscalid."||".$chave."||".$arquivo_pdf; 

	//salva o arquivo em pdf;
	$nfe = $danfe->printDANFE($arquivo_pdf,'F');

	sleep(2);
	
	die();
	

}



/*----------------------------------------------------------------------------------------------------------
ENVIA EMAIL COM PDF 
-----------------------------------------------------------------------------------------------------------*/
if( $action == "email" ){

	$arquivo_xml 	= 	"nfe/xml/" . right(str_repeat("0",9).$pedidoid,9) . ".xml";
	
	$xml = simplexml_load_file($arquivo_xml); 	
	$protocolo 	= $xml->protNFe ; 
	$chave		= $protocolo->infProt->chNFe;	
	
	$arquivo_pdf 	= 	"nfe/xml/" . $chave . ".pdf";
	

}





/*----------------------------------------------------------------------------------------------------------
funcoes gerais da nfe
-----------------------------------------------------------------------------------------------------------*/

function nfe_gera_txt($arquivo,$conteudo){
	$fp = fopen($arquivo, "a"); // a=coloca o ponteiro no final do arquivo 
	$escreve = fwrite($fp, $conteudo);
	fclose($fp);
	
	return $escreve;
}

function nfe_gera_chave($ibge_estado, $loja_cnpj, $notafiscalid, $chave_codigo ){

	$aamm = date("ym");
	$mod  = 55;
	$serie = "1";
	$tp_emissao = "1";
	$data = date("Y-m-j");
		
	
	//variaveis que monta a chave 
	
	$cn=''; 
	$dv=''; 
	$num = left("000000000".$notafiscalid, 9); 
	$cn = $chave_codigo; 
	$chave = "$ibge_estado$aamm$loja_cnpj$mod$serie$num$tp_emissao$cn"; 
	$dv = nfe_calcula_dv($chave); 
	$chave .= $dv; 
	
	return $chave;
}


function nfe_calcula_dv($chave43) { 
    $multiplicadores = array(2,3,4,5,6,7,8,9); 
    $i = 42; 
    while ($i >= 0) { 
        for ($m=0; $m<count($multiplicadores) && $i>=0; $m++) { 
            $soma_ponderada+= $chave43[$i] * $multiplicadores[$m]; 
            $i--; 
        } 
    } 
    $resto = $soma_ponderada % 11; 
    if ($resto == '0' || $resto == '1') { 
        return 0; 
    } else { 
        return (11 - $resto); 
   } 
} 


function nfe_protocolo_txt($id,$conteudo){
	
	$arquivo = "nfe/txt/prot-" . right("000000000".$id,9).".txt";
	
	if( file_exists($arquivo) ){
		unlink($arquivo);	
	}
	
	$fp = fopen($arquivo, "a"); // a=coloca o ponteiro no final do arquivo 
	$escreve = fwrite($fp, $conteudo);
	fclose($fp);
	
	return $escreve;
}


function nfe_recibo_txt($id,$conteudo){
	
	$arquivo = "nfe/txt/rec-" . right("000000000".$id,9).".txt";
	
	if( file_exists($arquivo) ){
		unlink($arquivo);	
	}
	
	$fp = fopen($arquivo, "a"); // a=coloca o ponteiro no final do arquivo 
	$escreve = fwrite($fp, $conteudo);
	fclose($fp);
	
	return $escreve;
}


function nfe_reset_pedido($pedidoid){
	$ssql_query = "update tblpedido set pcodnota_fiscal = '0' where pedidoid='{$pedidoid}'";
	mysql_query($ssql_query);
}

?>