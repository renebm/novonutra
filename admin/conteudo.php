<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}

	$id = $_REQUEST["id"];	
	if(!is_numeric($id)){
		$id = 0;	
	}


	if($_POST && $_REQUEST["action"]=="gravar"){
		$id					= 	addslashes($_REQUEST["id"]);
				
		$titulo				=	addslashes($_REQUEST["titulo"]);
		$descricao			=	addslashes($_REQUEST["descricao"]);
		$palavra_chave		=	addslashes($_REQUEST["palavra_chave"]);
		$texto				=	$_REQUEST["editor1"];
		
		
				$ssql = "update tblconteudo set ctitulo='{$titulo}', cdescricao='{$descricao}', cpalavra_chave='{$palavra_chave}', ctexto='{$texto}', 
							ccodusuario='{$usuario}', cdata_alteracao='{$data_hoje}' where conteudoid=$id";
				$result = mysql_query($ssql);
				if(!$result){
					$msg = "Erro ao gravar registro";
					$erro = 1;
				}
				else
				{
					$msg = "Registro atualizado com sucesso";
				}
		
	}






	if($id>0){
	
		$ssql = "select conteudoid, cpagina, ctitulo, cdescricao, cpalavra_chave, ctexto, ccodusuario, cdata_alteracao, cdata_cadastro
				from tblconteudo where conteudoid='{$id}'";
		$result = mysql_query($ssql);
		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				
				$pagina				=	$row["cpagina"];
				$titulo				=	$row["ctitulo"];
				$descricao			=	$row["cdescricao"];
				$palavra_chave		=	$row["cpalavra_chave"];
				$texto				=	$row["ctexto"];
				
				$usuario			=	get_usuario($row["ccodusuario"],"unome");		
				$data_alteracao		=	formata_data_tela($row["cdata_alteracao"]);
				$data_cadastro		=	formata_data_tela($row["cdata_cadastro"]);
				
			}
			mysql_free_result($result);
		}	
		
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
							   
		$("#data_inicio").mask("99/99/9999 99:99:99");					   	
		$("#data_termino").mask("99/99/9999 99:99:99");	   	
		
		$("#codconteudo").focus();
		
	});
    
</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form method="post" name="frm_conteudo" id="frm_conteudo" action="conteudo.php" onsubmit="return valida_conteudo();">
    <input name="action" id="action" type="hidden" value="gravar" />  
    <input name="id" id="id" type="hidden" value="<?php echo $id?>" />
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Conteúdo &raquo;  <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='conteudo.php';"><?php echo ($id==0) ? "Novo Registro" : $pagina;  ?></span> </span> <a href="conteudo_consulta.php">Consulta</a>
       	  </div>
            
            <div id="conteudo-interno">
				<table width="98%" border="0" cellspacing="0" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="130">ID</td>
                <td>#<?php echo $id;?></td>
                <td width="200" rowspan="19" align="center" valign="top"><?php echo $msg;?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Página</td>
                <td><?php echo $pagina;?></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Título</td>
                <td><input name="titulo" type="text" class="formulario" id="titulo" value="<?php echo $titulo;?>" size="75" maxlength="100"></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Descrição-Meta Tag</td>
                <td><input name="descricao" class="formulario" id="descricao" type="text" size="75" maxlength="250" value="<?php echo $descricao;?>" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Palavra Chave</td>
                <td><input name="palavra_chave" class="formulario" id="palavra_chave" type="text" size="75" maxlength="250" value="<?php echo $palavra_chave;?>" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Texto</td>
                <td><textarea name="editor1" class="ckeditor" id="editor1"><?php echo $texto;?></textarea></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Usuário</td>
                <td><?php echo $usuario;?></td>
                </tr>
              <tr>
                <td>Data Alteração</td>
                <td><?php echo $data_alteracao;?></td>
                </tr>
              <tr>
                <td>Data Cadastro</td>
                <td><?php echo $data_cadastro;?></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" id="btn-cmd-gravar" name="btn-cmd-gravar" value="Gravar" class="btn-gravar" /></td>
                </tr>
              </table>
          </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>