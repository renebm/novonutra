<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$uri = str_replace("/videokestore/","",$_SERVER['REQUEST_URI']);	

	$pagina = 1;
	$start = 0;
	$limit = 20;

	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}

		
		
	if($_POST && $_REQUEST["action"]=="gravar"){
		$categorias = addslashes($_REQUEST["categoria"]);
		$categoria = explode(",",$categorias);
		//echo $categorias;
		for($i=0;$i<count($categoria)-1;$i++){
			$ssql = "update tblcategoria set cordem = $i where categoriaid='{$categoria[$i]}'";
			mysql_query($ssql);
		}
		$msg = "Registros atualizados.<br /><br />Atualizado em " . date("d/m/Y H:i:s");		
	}
		

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		
	});
	
	$(function() {
		$( "#sortable" ).sortable({
			revert: true
		});
		$( "#draggable" ).draggable({
			connectToSortable: "#sortable",
			helper: "clone",
			revert: "invalid"
		});
		$( "ul, ol, li" ).disableSelection();
	});	  

</script>

	<style>
	.ui-state-default{background-color:#FFF;}
	.ui-state-sub{margin-left:30px}
	.sort ul { list-style-type: none; margin: 0; padding: 0; margin-bottom: 10px; }
	.sort li { margin: 5px; padding: 5px; width: 98%; }
	.sort ol { list-style-type: none; margin: 0; margin: 5px; padding: 5px; width: 98%; }
	</style>


</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    

    	<div id="conteudo">
			<div id="titulo-consulta">
            	<span class="label-inicio">Ordenar Categorias<span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='categoria_ordem.php';"></span></span>
        </div>
            
            <div id="conteudo-interno">
             <form name="frm_categoria" id="frm_categoria" method="post" action="categoria_ordem.php" onsubmit="return valida_categoria_ordem();" >
             <input type="hidden" name="action" id="action" value="gravar" >
             <input type="hidden" name="categoria" id="categoria" value="0" >
             <table width="99%" border="0" cellspacing="2" cellpadding="2" style="margin:10px;">
              <tr>
                <td>Selecione e arraste para cima ou para baixo para ordenar as categorias:</td>
                <td width="200" rowspan="6" align="left" valign="top">
				<?php echo $msg;?>                
                </td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td class="sort">
				<ul id="sortable">                
				<?php
					$countador = 0;
                	$ssql = "select categoriaid, ccategoria, cordem from tblcategoria where ccodcategoria=0 order by cordem, ccategoria";
					$result = mysql_query($ssql);
					if($result){
						while($row=mysql_fetch_assoc($result)){

							echo '<ol class="ui-state-default">';
							echo '<strong>'.$row["ccategoria"].'</strong>';
							echo '<input type="hidden" name="ordem[]" id="ordem[]" value="'.$row["categoriaid"].'" class="categoria">';
							echo '</ol>';	
								$ssql1 = "select categoriaid, ccategoria, cordem from tblcategoria where ccodcategoria=".$row["categoriaid"]." order by cordem, ccategoria";
								$result1 = mysql_query($ssql1);
								if($result1){
									while($row1=mysql_fetch_assoc($result1)){
										echo '<ol class="ui-state-default">';
										//echo '<li class="ui-state-sub">';
										echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
										echo $row["ccategoria"] ." - " . $row1["ccategoria"];
										echo '<input type="hidden" name="ordem[]" id="ordem[]" value="'.$row1["categoriaid"].'" class="categoria">';
										//echo '</li>';
										echo '</ol>';
									}
									mysql_free_result($result1);
								}
								
							//echo '</ol>';
		
						}
						mysql_free_result($result);
					}
				
				?>
                </ul>
                </td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>                            
              <tr>
                <td><input type="submit" id="btn-cmd-busca" name="btn-cmd-busca" value="Gravar" class="btn-gravar" /></td>
               </tr>
              <tr>
                <td>&nbsp;</td>
               </tr>
             </table>
             
             </form>
			
                
          </div>
            
       </div>

 
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>