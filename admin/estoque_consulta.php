<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");
	
	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$uri = str_replace("/videokestore/","",$_SERVER['REQUEST_URI']);	

	$pagina = 1;
	$start = 0;
	$limit = 20;

	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
		$('#string').focus();
	});
  

</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    

    	<div id="conteudo">
			<div id="titulo-consulta">
            	<span class="label-inicio">Estoque &raquo; <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='estoque_consulta.php';">Consulta</span></span> <?php if(isset($_GET["id"])){?><a href="javascript:estoque_novo(<?php echo $id;?>);">Novo Registro</a><?php }?>
            </div>
            
            <div id="conteudo-interno">
            <?php 
				if (!$_GET){
			?>
             <form name="frm_consulta" id="frm_consulta" method="get" action="?" >
             <table width="99%" border="0" cellspacing="2" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="50%">&nbsp;</td>
                <td>&nbsp;</td>
               </tr>
              <tr>
                <td>Produto:</td>
                <td>Categoria:</td>
               </tr>
              <tr>
                <td><input name="string" type="text" class="formulario" id="string" size="75" maxlength="200" /></td>
                <td><select name="categoria" id="categoria" class="formulario">
                  <option value="0" >Selecione</option>
                 	 <?php
                           $ssql = "select categoriaid, ccategoria from tblcategoria where ccodcategoria = 0 order by ccategoria";
						   $result = mysql_query($ssql);
						   if($result){
								while($row=mysql_fetch_assoc($result)){
									echo '<option value="'.$row["categoriaid"].'">'.$row["ccategoria"].'</option>';
									
									
									   $ssql1 = "select categoriaid, ccategoria from tblcategoria where ccodcategoria = ".$row["categoriaid"]." order by ccategoria";
									   $result1 = mysql_query($ssql1);
									   if($result1){
											while($row1=mysql_fetch_assoc($result1)){
												echo '<option value="'.$row1["categoriaid"].'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$row1["ccategoria"].'</option>';							
											}
											mysql_free_result($result1);
									   }
									
									
								}
								mysql_free_result($result);
						   }
					?>
                </select>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Status:</td>
                <td>Vitrine:</td>
              </tr>
              <tr>
                <td><select name="disponivel" size="1" class="formulario" id="disponivel">
                  <option value="2">Selecione</option>
                  <option value="-1">Disponível</option>
                  <option value="0">Indisponível</option>
                </select></td>
                <td><select name="vitrine" size="1" class="formulario" id="vitrine">
                  <option value="2">Selecione</option>
                  <option value="-1">Sim</option>
                  <option value="0">Não</option>
                </select></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><input type="submit" id="btn-cmd-busca" name="btn-cmd-busca" value="Consultar" class="btn-gravar" /></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
             </table>
             
             </form>
			<?php
				}else{
			?>	
                
                
                <table width="99%" border="0" cellspacing="0" cellpadding="3" style="margin:10px;">
                  <tr>
                    <td width="50" class="titulo_table">#ID</td>
                    <td width="150" class="titulo_table">Código SKU</td>                    
                    <td class="titulo_table">Produto</td>
                    <td class="titulo_table">Tamanho</td>
                    <td class="titulo_table">Propriedade</td>                    
                    <td width="100" align="center" class="titulo_table">Unitário</td>
                    <td width="100" align="center" class="titulo_table">Disponível</td>
                    <td width="50" align="center" class="titulo_table">Estoque</td>
                    <td width="50" align="center" class="titulo_table">Editar</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
                  
                  <?php
				  
				  	$id = addslashes($_REQUEST["id"]);
					$string = addslashes($_REQUEST["string"]);
					$categoria = addslashes($_REQUEST["categoria"]);
					$disponivel = addslashes($_REQUEST["disponivel"]);
					if(!is_numeric($disponivel)){
						$disponivel = 2;
					}
					$vitrine = addslashes($_REQUEST["vitrine"]);
					if(!is_numeric($vitrine)){
						$vitrine = 2;
					}


                  	/*$ssql = "select tblproduto.produtoid, tblproduto.preferencia, tblproduto.pproduto, tblproduto.pdisponivel, tblproduto.pcodigo, 
							tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo , tblproduto.pvitrine, tblproduto.pcontrola_estoque, 
							tblestoque.eestoque, tblestoque.ecodpropriedade, tblestoque.ecodtamanho, 
							tblproduto_propriedade.ppropriedade, tblproduto_propriedade.pcodpropriedade, pai.ppropriedade AS propriedade_pai
							from tblproduto
							inner join tblproduto_categoria on tblproduto.produtoid = tblproduto_categoria.pcodproduto  
							inner join tblestoque on tblproduto.produtoid=tblestoque.ecodproduto
							left join tblproduto_propriedade on tblestoque.ecodpropriedade=tblproduto_propriedade.propriedadeid
							LEFT JOIN tblproduto_propriedade AS pai ON tblproduto_propriedade.pcodpropriedade = pai.propriedadeid
							where pproduto like '%{$string}%' ";	
					*/
					
					$ssql = "SELECT tblproduto.produtoid, tblproduto.pcodigo, tblproduto.pproduto, tblproduto.pvalor_unitario, tblproduto.pdisponivel, 
							tblestoque.ecodproduto, pai.ppropriedade AS propriedade_pai, tblestoque.ecodtamanho, tblproduto_propriedade.ppropriedade AS ptamanho, 
							tblestoque.ecodpropriedade, proper.ppropriedade AS ppropriedade, tblestoque.eestoque, tblestoque.edata_alteracao
							FROM tblproduto
							inner join tblproduto_categoria on tblproduto.produtoid = tblproduto_categoria.pcodproduto  
							INNER JOIN tblestoque ON tblproduto.produtoid = tblestoque.ecodproduto
							LEFT JOIN tblproduto_propriedade ON tblestoque.ecodtamanho = tblproduto_propriedade.propriedadeid
							LEFT JOIN tblproduto_propriedade AS proper ON tblestoque.ecodpropriedade = proper.propriedadeid
							LEFT JOIN tblproduto_propriedade AS pai ON tblproduto_propriedade.pcodpropriedade = pai.propriedadeid
							where pproduto like '%{$string}%'
							";
					
					
					if($id > 0 ){
							$ssql .= " and tblproduto.produtoid=$id ";	
					}


					if($categoria > 0 ){
							$ssql .= " and tblproduto_categoria.pcodcategoria=$categoria ";	
					}


					if($disponivel != 2 ){
							$ssql .= " and tblproduto.pdisponivel=$disponivel ";	
					}

					if($vitrine != 2 ){
							$ssql .= " and tblproduto.pvitrine=$vitrine ";	
					}




					$ssql .= " or pcodigo like '%{$string}%' ";	

					if($id > 0 ){
							$ssql .= " and tblproduto.produtoid=$id ";	
					}


					if($categoria > 0 ){
							$ssql .= " and tblproduto_categoria.pcodcategoria=$categoria ";	
					}


					if($disponivel != 2 ){
							$ssql .= " and tblproduto.pdisponivel=$disponivel ";	
					}

					if($vitrine != 2 ){
							$ssql .= " and tblproduto.pvitrine=$vitrine ";	
					}



					
					$ssql .= "	group by tblproduto.produtoid, tblestoque.eestoque, tblestoque.ecodpropriedade, tblestoque.ecodtamanho
							order by tblproduto.pproduto ";
	
		
					$result = mysql_query($ssql);
					if($result){
						$total_registros = mysql_num_rows($result);	
					}					
							
							
					$ssql .= " limit $start, $limit";
							
					//echo $ssql;
							
				  	$result = mysql_query($ssql);
				  	if($result){
					
						if(mysql_num_rows($result)==0){
							echo '
								  <tr>
									<td colspan="9">Nenhum registro localizado.</td>
								  </tr>';							
						}
					
					
						while($row=mysql_fetch_assoc($result)){
							
							$disponivel = ($row["pdisponivel"]==-1) ? "Sim" : "Não";							
							$vitrine = ($row["pvitrine"]=="-1") ? "Sim" : "Não";
							$link_estoque = 'estoque.php?id='.$row["produtoid"];	
							$propriedade = $row["ppropriedade"];
							
							//if($row["propriedade_pai"]!=""){
							//	$propriedade = $row["propriedade_pai"] . " : " . $row["ppropriedade"];
							//}
							
							
							echo '
								  <tr class="tr_lista">
									<td>'.$row["produtoid"].'</td>
									<td>'.$row["pcodigo"].'</td>
									<td>'.$row["pproduto"].'</td>
									<td>'.$row["ptamanho"].'&nbsp;</td>								
									<td>'.$row["ppropriedade"].'&nbsp;</td>								
									<td align="right">R$ '.number_format($row["pvalor_unitario"],2,",",".").'&nbsp;&nbsp;</td>
									<td align="center">'.$disponivel.'</td>
									<td align="center">'.$row["eestoque"].'</td>
									<td align="center"><a href="'.$link_estoque.'"><img src="images/ico_editar.gif" border="0" /></a></td>
								  </tr>							
							';
						}
						mysql_free_result($result);
					
					}
				  ?>
                  

                  
				  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td colspan="7" align="right">
                        <div class="paginacao"><span class="paginacao-text">Página:</span> 
                        <?php
                        echo paginacao($pagina, $limit, $total_registros);
                        ?>                            
                        </div>                    
                    </td>
			      </tr>

                </table>
                
                
                <?php
                
				}
				?>
                
          </div>
            
       </div>

 
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>