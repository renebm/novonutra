<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}

	$id = $_REQUEST["id"];	
	if(!is_numeric($id)){
		$id = 0;	
	}


	/*-----------------------------------------------------------------------
	ajax de excluisão
	------------------------------------------------------------------------*/
	if($_POST && $_REQUEST["action"]=="excluir"){
		
		$ssql = "select categoriaid from tblcategoria where ccodcategoria = '{$id}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)>0){
				echo "A categoria não pode ser excluida pois existe uma categoria dependente associada a mesma.";
				exit();
			}	
		}
		
		
		$ssql = "select produtocategoriaid from tblproduto_categoria where pcodcategoria = '{$id}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)>0){
				echo "A categoria não pode ser excluida pois existem produtos associados a mesma.";
				exit();
			}	
		}
		
		
		$ssql = "delete from tblcategoria where categoriaid='{$id}'";
		mysql_query($ssql);
		echo "ok";
		exit();
		
	}




	if($_POST && $_REQUEST["action"]=="gravar"){
		$id					= 	addslashes($_REQUEST["id"]);
		$codcategoria		=	addslashes($_REQUEST["codcategoria"]);
		$categoria			=	addslashes($_REQUEST["categoria"]);
		$link_seo			=	addslashes($_REQUEST["link_seo"]);
		$ativa				=	$_REQUEST["ativa"];
		$ordem				=	addslashes($_REQUEST["ordem"]);
		$menu				=	addslashes($_REQUEST["menu"]);
		
		
		if(!is_numeric($ordem)){
			$ordem = 0;	
		}

		if(!is_numeric($menu)){
			$menu = 0;	
		}
		
		$data_inicio		=	addslashes($_REQUEST["data_inicio"]);
		$data_termino		=	addslashes($_REQUEST["data_termino"]);
		
		$data_inicio		=	formata_data_db($data_inicio);
		$data_termino		=	formata_data_db($data_termino);
		
		
		if($id==0){
			
			//verifica se nao existe ja uma categoria com o mesmo nome;
			$ssql = "select categoriaid from tblcategoria where ccategoria='{$categoria}' and ccodcategoria='{$codcategoria}'";
			$result = mysql_query($ssql);
				if($result){
					$num_rows = mysql_num_rows($result);
					if($num_rows>0){
						$msg = "Já existe um registro com os mesmos dados digitados";
						$erro = 1;
					}	
				}
				
			//--------------------------------------------------------	
			
			if($num_rows==0){
				$ssql = "insert into tblcategoria (ccodcategoria, ccategoria, clink_seo, cativa, cordem, cmenu, cdata_inicio, cdata_termino, ccodusuario, 
												   cdata_alteracao, cdata_cadastro) 
							values('{$codcategoria}','{$categoria}','{$link_seo}','{$ativa}', '{$ordem}', '{$menu}', '{$data_inicio}', '{$data_termino}', '{$usuario}',
								   '{$data_hoje}','{$data_hoje}')";
				$result = mysql_query($ssql);
				if(!$result){
					$msg = "Erro ao gravar registro";
					$erro = 1;
				}
				else
				{
					$id = mysql_insert_id();
					$msg = "Registro salvo com sucesso";
				}
				
			}
			
		}
		else
		{
				//edita/atualiza o registro	
				$ssql = "update tblcategoria set ccodcategoria='{$codcategoria}', ccategoria='{$categoria}', clink_seo='{$link_seo}', cativa='{$ativa}', cordem='{$ordem}', 
							cmenu='{$menu}', cdata_inicio='{$data_inicio}', cdata_termino='{$data_termino}', ccodusuario='{$usuario}', cdata_alteracao='{$data_hoje}'
							 where categoriaid = $id";
				$result = mysql_query($ssql);
				if(!$result){
					$msg = "Erro ao atualizar registro";
					$erro = 1;
				}
				else
				{
					$msg = "Registro atualizado com sucesso";
				}			
			
		}
		
		
	}






	if($id>0){
	
		$ssql = "select categoriaid, ccodcategoria, ccategoria, clink_seo, cativa, cordem, cmenu, cdata_inicio, cdata_termino, ccodusuario, cdata_alteracao, cdata_cadastro
				from tblcategoria where categoriaid='{$id}'";
		$result = mysql_query($ssql);
		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				
				$codcategoria		=	$row["ccodcategoria"];
				$categoria			=	$row["ccategoria"];
				$link_seo			=	$row["clink_seo"];
				$ativa				=	$row["cativa"];
				$ordem				=	$row["cordem"];
				$menu				=	$row["cmenu"];
				
				$data_inicio		=	formata_data_tela($row["cdata_inicio"]);
				$data_termino		=	formata_data_tela($row["cdata_termino"]);
				
				$usuario			=	get_usuario($row["ccodusuario"],"unome");		
				$data_alteracao		=	formata_data_tela($row["cdata_alteracao"]);
				$data_cadastro		=	formata_data_tela($row["cdata_cadastro"]);
				
			}
			mysql_free_result($result);
		}	
		
	}
	
	
	
	if($id==0){
		$data_inicio = "01/01/2012 00:00:00";
		$data_termino = "31/12/2050 23:59:59";
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
							   
		$("#data_inicio").mask("99/99/9999 99:99:99");					   	
		$("#data_termino").mask("99/99/9999 99:99:99");	   	
		
		$("#codcategoria").focus();
		
	});
    
</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form method="post" name="frm_categoria" id="frm_categoria" action="categoria.php" onsubmit="return valida_categoria();">
    <input name="action" id="action" type="hidden" value="gravar" />  
    <input name="id" id="id" type="hidden" value="<?php echo $id?>" />
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Categoria &raquo;  <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='categoria.php';"><?php echo ($id==0) ? "Novo Registro" : $categoria;  ?></span> </span> <a href="categoria_consulta.php">Consulta</a>
       	  </div>
            
            <div id="conteudo-interno">
				<table width="98%" border="0" cellspacing="0" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="130">ID</td>
                <td>#<?php echo $id;?></td>
                <td width="200" rowspan="19" align="center" valign="top"><?php echo $msg;?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Categoria Pai</td>
                <td>
                <select name="codcategoria" class="formulario" id="codcategoria">
                <option value="0">Selecione</option>
                	<?php
                    	$ssql = "select categoriaid, ccategoria from tblcategoria where ccodcategoria = 0 order by ccategoria";
						$result = mysql_query($ssql);
						if($result){
							while($row=mysql_fetch_assoc($result)){
								echo '<option value="'.$row["categoriaid"].'"';
								if($codcategoria==$row["categoriaid"]){
									echo " selected";	
								}
								echo '>';
								echo $row["ccategoria"];
								echo '</option>';
									
									
									$ssql1 = "select categoriaid, ccategoria from tblcategoria where ccodcategoria = ".$row["categoriaid"]." order by ccategoria";
									$result1 = mysql_query($ssql1);
									if($result1){
										while($row1=mysql_fetch_assoc($result1)){
											echo '<option value="'.$row1["categoriaid"].'"';
											if($codcategoria==$row1["categoriaid"]){
												echo " selected";	
											}
											echo '>';
											echo '&nbsp;&nbsp;&nbsp;'.$row1["ccategoria"];
											echo '</option>';										
										}
										mysql_fetch_assoc($result1);
									}
								
									
								
								
							}
							mysql_free_result($result);
						}
					?>
                </select>
                </td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Categoria</td>
                <td><input name="categoria" type="text" class="formulario" id="categoria" value="<?php echo $categoria;?>" size="75" maxlength="100" onblur="javascript: gera_link_seo(this.value,'link_seo');" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Exibir</td>
                <td>
                  <select name="menu" size="1" class="formulario" id="menu">
                    <option value="0" <?php echo $menu==0 ? 'selected="selected"' : ''; ?>>Topo &amp; Lateral</option>
                    <option value="1" <?php echo $menu==1 ? 'selected="selected"' : ''; ?>>Topo</option>
                    <option value="2" <?php echo $menu==2 ? 'selected="selected"' : ''; ?>>Lateral</option>
                  </select>
               </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>                                          
              <tr>
                <td>Link Seo</td>
                <td><input name="link_seo" class="formulario" id="link_seo" type="text" size="75" maxlength="250" value="<?php echo $link_seo;?>" style="color:#999;" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Ativa</td>
                <td><label>
                  <input type="radio" name="ativa" id="ativa" value="-1" <?php echo ($ativa == -1) ? "checked" : "" ;?>  />
                  Sim</label>
                  <label>
                    <input type="radio" name="ativa" id="ativa" value="0" <?php echo ($ativa == 0) ? "checked" : "" ;?>  />
                Não</label></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Data Início</td>
                <td><input name="data_inicio" class="formulario" id="data_inicio" type="text" size="20" maxlength="20" value="<?php echo $data_inicio;?>" />
                dd/mm/aaaa hh:mm:ss</td>
                </tr>
              <tr>
                <td>Data Término</td>
                <td><input name="data_termino" class="formulario" id="data_termino" type="text" size="20" maxlength="20" value="<?php echo $data_termino;?>" />
                dd/mm/aaaa hh:mm:ss</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Usuário</td>
                <td><?php echo $usuario;?></td>
                </tr>
              <tr>
                <td>Data Alteração</td>
                <td><?php echo $data_alteracao;?></td>
                </tr>
              <tr>
                <td>Data Cadastro</td>
                <td><?php echo $data_cadastro;?></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" id="btn-cmd-gravar" name="btn-cmd-gravar" value="Gravar" class="btn-gravar" /></td>
                </tr>
              </table>
          </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>