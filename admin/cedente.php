<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}

	$id = $_REQUEST["id"];	
	if(!is_numeric($id)){
		$id = 1;	
	}
	
	/*--------------------------------------------------------
	//verifica se existe algum cedente na tblcedente
	--------------------------------------------------------*/
	$ssql = "select cedenteid from tblcedente where cedenteid=1";
	$result = mysql_query($ssql);
	if($result){
		$num_rows = mysql_num_rows($result);
		if($num_rows==0){
			//insere dados
			$ssql = "INSERT INTO tblcedente (cedenteid, crazao_social, ccnpj, cbanco, cnumero_banco, cnumero_banco_dac, cagencia, cconta_corrente, cconta_corrente_dac, cnosso_numero, cnumero_documento, ccarteira, cconvenio, ctaxa, cdias, ccodusuario, cdata_alteracao, cdata_cadastro) VALUES
					(1, 'cedente Virtual', '05490968000167', 'Itaú', '341', '7', '8059', '10253', '6', '0000000', '000000', '175', '000', 0, 5, 0, '{$data_hoje}', '{$data_hoje}')
					";
			mysql_query($ssql);	
		}	
	}



	if($_POST && $_REQUEST["action"]=="gravar"){
		$id					= 	addslashes($_REQUEST["id"]);

		$razaosocial			=	addslashes($_REQUEST["razaosocial"]);
		$cnpj					=	addslashes($_REQUEST["cnpj"]);
		$banco					=	addslashes($_REQUEST["banco"]);
		$numero_banco			=	addslashes($_REQUEST["numero_banco"]);
		$numero_banco_dac		=	addslashes($_REQUEST["numero_banco_dac"]);
		$agencia				=	addslashes($_REQUEST["agencia"]);
		$conta_corrente			=	addslashes($_REQUEST["conta_corrente"]);
		$conta_corrente_dac		=	addslashes($_REQUEST["conta_corrente_dac"]);
		$nosso_numero			=	addslashes($_REQUEST["nosso_numero"]);
		$numero_documento		=	addslashes($_REQUEST["numero_documento"]);
		$carteira				=	addslashes($_REQUEST["carteira"]);
		$convenio				=	addslashes($_REQUEST["convenio"]);
		$taxa					=	addslashes($_REQUEST["taxa"]);
		$dias					=	addslashes($_REQUEST["dias"]);
		
		$cnpj = get_only_numbers($cnpj);
		$taxa = formata_valor_db($taxa);
		
		
		$ssql = "update tblcedente set crazao_social='{$razaosocial}', ccnpj='{$cnpj}', cbanco='{$banco}', cnumero_banco='{$numero_banco}', 
				cnumero_banco_dac='{$numero_banco_dac}', cagencia='{$agencia}', cconta_corrente='{$conta_corrente}', cconta_corrente_dac='{$conta_corrente_dac}', 
				cnosso_numero='{$nosso_numero}', cnumero_documento='{$numero_documento}', ccarteira='{$carteira}', cconvenio='{$convenio}', ctaxa='{$taxa}', 
				cdias='{$dias}', ccodusuario='{$usuario}', cdata_alteracao='{$data_hoje}'
				where cedenteid=1";
		$result = mysql_query($ssql);
		if($result){
			$msg = "Registro atualizado com sucesso.";	
		}else{
			$msg = "Erro ao atualizar registro.";	
		}		
	
	}




	if($id>0){
	
		$ssql = "select cedenteid, crazao_social, ccnpj, cbanco, cnumero_banco, cnumero_banco_dac, cagencia, cconta_corrente, cconta_corrente_dac, cnosso_numero, 
				cnumero_documento, ccarteira, cconvenio, ctaxa, cdias, ccodusuario, cdata_alteracao, cdata_cadastro
				from tblcedente where cedenteid=1
				";
		$result = mysql_query($ssql);
		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				
				$id						=	$row["cedenteid"];
				$razaosocial			=	$row["crazao_social"];
				$cnpj					=	$row["ccnpj"];
				$banco					=	$row["cbanco"];
				$numero_banco			=	$row["cnumero_banco"];
				$numero_banco_dac		=	$row["cnumero_banco_dac"];
				$agencia				=	$row["cagencia"];
				$conta_corrente			=	$row["cconta_corrente"];
				$conta_corrente_dac		=	$row["cconta_corrente_dac"];
				$nosso_numero			=	$row["cnosso_numero"];
				$numero_documento		=	$row["cnumero_documento"];
				$carteira				=	$row["ccarteira"];
				$convenio				=	$row["cconvenio"];
				$taxa					=	number_format($row["ctaxa"],2,",",".");
				$dias					=	$row["cdias"];
								
				$usuario			=	get_usuario($row["ccodusuario"],"unome");		
				$data_alteracao		=	formata_data_tela($row["cdata_alteracao"]);
				$data_cadastro		=	formata_data_tela($row["cdata_cadastro"]);
				
			}
			mysql_free_result($result);
		}	
		
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - cedente Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - cedente Virtual" />
<meta name="description" content="Painel de administração da cedente virtual" />
<meta name="keywords" content="cedente virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {							   		
		$("#razaosocial").focus();	
		$("#cnpj").mask("99.999.999/9999-99");
	});
</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form method="post" name="frm_cedente" id="frm_cedente" action="cedente.php" onsubmit="return valida_cedente();">
    <input name="action" id="action" type="hidden" value="gravar" />  
    <input name="id" id="id" type="hidden" value="<?php echo $id?>" />
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Cedente &raquo;  <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='cedente.php';"><?php echo ($id==0) ? "Novo Registro" : $razaosocial;  ?></span> </span>
       	  </div>
            
            <div id="conteudo-interno">
				<table width="98%" border="0" cellspacing="0" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="130">ID</td>
                <td>#<?php echo $id;?></td>
                <td width="200" rowspan="31" align="center" valign="top">
				<?php echo $msg;?>
                    <div id="dica" style="margin:10px;">
                    &nbsp;
                    </div>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Razão Social</td>
                <td><input name="razaosocial" type="text" class="formulario" id="razaosocial" value="<?php echo $razaosocial;?>" size="75" maxlength="100" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr> 
              <tr>
                <td>CNPJ</td>
                <td><input name="cnpj" type="text" class="formulario" id="cnpj" value="<?php echo $cnpj;?>" size="75" maxlength="100" /></td>
                </tr>  
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>                                                        
              <tr>
                <td>Banco</td>
                <td><input name="banco" type="text" class="formulario" id="banco" value="<?php echo $banco;?>" size="20" maxlength="30" /> 
                  ex: Itaú</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>No. Banco / Dac</td>
                <td><input name="numero_banco" type="text" class="formulario" id="numero_banco" value="<?php echo $numero_banco;?>" size="10" />
                  -
                    <input name="numero_banco_dac" type="text" class="formulario" id="numero_banco_dac" value="<?php echo $numero_banco_dac;?>" size="3" maxlength="5" /> 
                  ex: 341-7</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>                                          
              <tr>
                <td>Agência</td>
                <td><input name="agencia" type="text" class="formulario" id="agencia" value="<?php echo $agencia;?>" size="10" maxlength="20" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Conta Corrente / Dac</td>
                <td><input name="conta_corrente" type="text" class="formulario" id="conta_corrente" value="<?php echo $conta_corrente;?>" size="10" maxlength="20" />
                  -
                  <input name="conta_corrente_dac" type="text" class="formulario" id="conta_corrente_dac" value="<?php echo $conta_corrente_dac;?>" size="3" maxlength="5" /> 
                  ex: 10253-6</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Nosso Número</td>
                <td><input name="nosso_numero" type="text" class="formulario" id="nosso_numero" value="<?php echo $nosso_numero;?>" size="20" maxlength="20" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>No. Documento</td>
                <td><input name="numero_documento" type="text" class="formulario" id="numero_documento" value="<?php echo $numero_documento;?>" size="20" maxlength="20" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Carteira</td>
                <td><input name="carteira" type="text" class="formulario" id="carteira" value="<?php echo $carteira;?>" size="10" maxlength="5" /> 
                  ex: 175</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Convênio</td>
                <td><input name="convenio" type="text" class="formulario" id="convenio" value="<?php echo $convenio;?>" size="20" maxlength="20" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Taxa de Emissão</td>
                <td><input name="taxa" type="text" class="formulario" id="taxa" value="<?php echo $taxa;?>" size="20" maxlength="9" />
                  0,00</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Dias</td>
                <td><input name="dias" type="text" class="formulario" id="dias" value="<?php echo $dias;?>" size="5" maxlength="2" /> 
                  dias de vencimento após emissão</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              
              <tr>
                <td>Usuário</td>
                <td><?php echo $usuario;?></td>
                </tr>
              <tr>
                <td>Data Alteração</td>
                <td><?php echo $data_alteracao;?></td>
                </tr>
              <tr>
                <td>Data Cadastro</td>
                <td><?php echo $data_cadastro;?></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" id="btn-cmd-gravar" name="btn-cmd-gravar" value="Gravar" class="btn-gravar" /></td>
                </tr>
              </table>
          </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>