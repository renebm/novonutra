<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}



	if($_POST && $_REQUEST["action"]=="gravar"){
				
		$texto	=	$_REQUEST["editor1"];

		$ssql = "update tblconfiguracao set cvalor='{$texto}' where cparametro='email_produto_aviseme'";
		$result = mysql_query($ssql);
		if(!$result){
			$msg = "Erro ao gravar registro";
			$erro = 1;
		}
		else
		{
			$msg = "Registro atualizado com sucesso";
		}
		
	}


	/*----------------------------------------------------------------------------------
	carrega os dados
	-----------------------------------------------------------------------------------*/
	$ssql = "select cvalor from tblconfiguracao where cparametro='email_produto_aviseme'";
	$result = mysql_query($ssql);
	if(mysql_num_rows($result)==0){
		mysql_query("INSERT INTO `tblconfiguracao` (`configuracaoid`, `cparametro`, `cvalor`, `cdescricao`, `ccodusuario`, `cdata_alteracao`, `cdata_cadastro`) VALUES (NULL, 'email_produto_aviseme', '', 'E-mail que envia para o internautra que cadastrou dados no produto indisponivel para avisar quando disponível.', '1', '{$data_hoje}', '{$data_hoje}')");
	}
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$texto				=	$row["cvalor"];				
		}
		mysql_free_result($result);
	}	

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">

	$(document).ready(function() {
							   
		$("#data_inicio").mask("99/99/9999 99:99:99");					   	
		$("#data_termino").mask("99/99/9999 99:99:99");	   	
		
		$("#codconteudo").focus();
		
	});
    
</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form method="post" name="frm_conteudo" id="frm_conteudo" action="produto_aviseme_email.php">
    <input name="action" id="action" type="hidden" value="gravar" />  
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Produto Avise-me &raquo; E-mail</span> <a href="produto_aviseme_consulta.php">Consulta</a>
   	    </div>
            
            <div id="conteudo-interno">
				<table width="98%" border="0" cellspacing="0" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="130">&nbsp;</td>
                <td>&nbsp;</td>
                <td width="200" rowspan="5" align="center" valign="top"><?php echo $msg;?></td>
              </tr>
              <tr>
                <td>Texto</td>
                <td><textarea name="editor1" class="ckeditor" id="editor1"><?php echo $texto;?></textarea></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" id="btn-cmd-gravar" name="btn-cmd-gravar" value="Gravar" class="btn-gravar" /></td>
                </tr>
              </table>
          </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>