<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");


	//arquivo a exportar
	if(!file_exists("relatorio")){
		mkdir("relatorio",777);	
	}

	$arquivo = "relatorio/cadastro-".date("Ymd").".csv";
	
	if(file_exists($arquivo)){
		unlink($arquivo);	
	}
	
	
	$cabecalho = "ID;NOME;APELIDO;TIPO;CPF/CNPJ;RG/IE;EMAIL;SITE;NASCIMENTO;SEXO;TELEFONE;CELULAR;NEWS;SMS;DATA ALTERAÇÃO;DATA CADASTRO;\r\n";
	$cabecalho = utf8_decode($cabecalho);
	
	
	$fp = fopen($arquivo, "a"); // a=coloca o ponteiro no final do arquivo 
	$escreve = fwrite($fp, $cabecalho);
	fclose($fp);
	

	$string = addslashes($_REQUEST["string"]);
	$sexo = addslashes($_REQUEST["sexo"]);
  
  
	$ssql = "select cadastroid, crazao_social, cnome, capelido, ccodtipo, ccpf_cnpj, crg_ie, cemail, csite, cdata_nascimento, ccodsexo, ctelefone, ccelular, 
			cnews, csms, cdata_alteracao, cdata_cadastro
			from tblcadastro 	 
			where crazao_social like '%{$string}%' ";	
	
	if($sexo != 0 ){
			$ssql .= " and ccodsexo=$sexo ";	
	}


	$ssql .= " or cnome like '%{$string}%' ";	
	
	if($sexo != 0 ){
			$ssql .= " and ccodsexo=$sexo ";	
	}


	$ssql .= " or cemail like '%{$string}%' ";	
	
	if($sexo != 0 ){
			$ssql .= " and ccodsexo=$sexo ";	
	}
	
	$ssql .= " order by crazao_social , cnome  ";
	
	if(isset($_SESSION["cadastro_export_sql"])){
		$ssql = $_SESSION["cadastro_export_sql"];
	}
	
	$result = mysql_query($ssql);
	if($result){
	$count = 0;
		while($row=mysql_fetch_assoc($result)){
			$count++;
			$id				= $row["cadastroid"];
			$nome 			= $row["crazao_social"] . " " . $row["cnome"];
			$apelido		= $row["capelido"];
			$tipo			= ($row["ctipo"]==1) ? "PJ" : "PF";	//1 = pf 2 = pj
			$cpf 			= $row["ccpf_cnpj"];
			$cpf			= formata_cpf_cnpj_tela($cpf);
			
			$rg 			= $row["crg_ie"];							
			$email 			= $row["cemail"];
			
			$site 			= $row["csite"];
			$data_nascimento= $row["cdata_nascimento"];
			$data_nascimento= formata_data_tela($data_nascimento);
			
			$sexo 			= ($row["ccodsexo"]==1)?"MASC":"FEM";
			
			$telefone		= $row["ctelefone"];
			$celular		= $row["ccelular"];
			$news			= ($row["cnews"]==-1) ? "SIM" : "NÃO";
			$sms			= ($row["csms"]==-1) ? "SIM" : "NÃO";
			
			$data_alteracao = $row["cdata_alteracao"];
			$data_alteracao = formata_data_tela($data_alteracao);
			
			$data_cadastro	= $row["cdata_cadastro"];
			$data_cadastro  = formata_data_tela($data_cadastro);
			
			$linha = $id.";".$nome.";".$apelido.";".$tipo.";".$cpf.";".$rg.";".$email.";".$site.";".$data_nascimento.";".$sexo.";".$telefone.";".$celular.";".$news.";".$sms.";".$data_alteracao.";".$data_cadastro.";\r\n";
			$linha = utf8_decode($linha);
			
			$fp = fopen($arquivo, "a"); // a=coloca o ponteiro no final do arquivo 
			$escreve = fwrite($fp, $linha);
			fclose($fp);			
		}
		mysql_free_result($result);
	}
	
	header("location: $arquivo");
	exit();
	
?>