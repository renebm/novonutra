<?php
	include("../include/inc_conexao.php");	
	include("../include/canvas.php");
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie


	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}

	$id = $_REQUEST["id"];	
	if(!is_numeric($id)){
		$id = 0;	
	}
	
	
	

	/*-----------------------------------------------------------------------
	ajax de excluisão
	------------------------------------------------------------------------*/
	if($_POST && $_REQUEST["action"]=="excluir"){
		
		$ssql = "select produtoid from tblproduto where pcodmarca = '{$id}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)>0){
				echo "A marca não pode ser excluída pois existem produtos vinculados a mesma.";
				exit();
			}
		}
				
		$ssql = "select marcaid, mimagem from tblmarca where marcaid = '{$id}'";
		$result = mysql_query($ssql);
		if($result){
			while($row = mysql_fetch_assoc($result)){
				$arquivo = "../".$row["mimagem"];	
				if(file_exists($arquivo)){
					unlink($arquivo);
				}
				$arquivo = "../".$row["mimagem"];	
			mysql_free_result($result);
			}
		}
				
		$ssql = "delete from tblmarca where marcaid='{$id}'";
		mysql_query($ssql);
		echo "ok";
		exit();
		
	}
	
	
	/*-----------------------------------------------------------------------
	//define os tamanhos da imagem
	------------------------------------------------------------------------*/	

	if($config_marca_dimensao==""){
		$x=160;
		$y=80;
	}
	
	list($x,$y) = explode("x",$config_marca_dimensao);
	
	if($x==0 || $x=="" || $y==0 || $y==""){
		$x=160;
		$y=80;
	}


	/*-----------------------------------------------------------------------
	//grava
	------------------------------------------------------------------------*/
	if($_POST && $_REQUEST["action"]=="gravar"){
		$id					= 	addslashes($_REQUEST["id"]);				
		$marca				=	addslashes($_REQUEST["marca"]);
		$descricao			=	addslashes($_REQUEST["descricao"]);
		$link_seo			=	addslashes($_REQUEST["link_seo"]);
		$texto				=	$_REQUEST["editor1"];
		$imagem				=	"";
		
		
		if($id==0){
			//verifica se ja existe a marca
			$ssql = "select marcaid from tblmarca where mmarca='{$marca}'";
			$result = mysql_query($ssql);
			if($result){
				$num_rows = mysql_num_rows($result);
				if($num_rows>0){
					$msg = "Já existe um registro com os mesmos dados digitados";
					$erro = 1;
				}	
			}
			
			//insere o registro
			if($num_rows==0){
				$ssql = "insert into tblmarca (mmarca, mdescricao, mlink_seo, mguia, mimagem, mordem, mcodusuario, mdata_alteracao, mdata_cadastro) 
							values('{$marca}','{$descricao}','{$link_seo}','{$testo}', '{$imagem}', '0',  '{$usuario}', '{$data_hoje}','{$data_hoje}')";
				$result = mysql_query($ssql);
				if(!$result){
					$msg = "Erro ao gravar registro";
					$erro = 1;
				}
				else
				{
					$id = mysql_insert_id();
					$msg = "Registro salvo com sucesso";
				}
			}
									
		}
		else
		{
			$ssql = "update tblmarca set mmarca='{$marca}', mdescricao='{$descricao}', mlink_seo='{$link_seo}', mguia='{$texto}', 
						mcodusuario='{$usuario}', mdata_alteracao='{$data_hoje}'
						where marcaid='{$id}'";
			$result = mysql_query($ssql);
			if(!$result){
				$msg = "Erro ao atualizar registro";
				$erro = 1;
			}
			else
			{
				$msg = "Registro atualizado com sucesso";
			}
			
		}
		
		
		/*---------------------------------------------------------------------
		atualiza a imagem se foi postada
		-----------------------------------------------------------------------*/
	
		if($_FILES){						
			$file_name = $_FILES['arquivo']['name'];
			$file_type = $_FILES['arquivo']['type'];
			$file_size = $_FILES['arquivo']['size'];
			$file_tmp_name = $_FILES['arquivo']['tmp_name'];
			$error = $_FILES['arquivo']['error'];
						
			$file_size = number_format($file_size/1024,2);
			
			if($file_size>100){
				$msg .= "<br /><br />Erro ao gravar imagem: O tamanho do arquivo não pode ser maior que 100KB";
			}				
		}
	
		if($error==0){
			
			$arquivo = "imagem/marca/" . gera_url_seo($marca)."_clr";
			$arquivo .= right($file_name,4);	
			$arquivo = strtolower($arquivo);
			
			//echo $arquivo;
			//move para pasta TMP
			$move = move_uploaded_file($file_tmp_name,"../".$arquivo);
			
			$img = new canvas("../".$arquivo);
			$img->filter("grayscale")
				 ->save(str_replace("clr","pb","../".$arquivo));
			
			$ssql = "update tblmarca set mimagem='{$arquivo}' where marcaid='{$id}'";
			$result = mysql_query($ssql);
			if(!$result){
				$msg .= "<br />Ocorreu um erro ao atualizar a imagem tente enviar a imagem novamente";
			}
			
		}
	
		
	}




	/*-----------------------------------------------------------------------
	//carrega dados
	------------------------------------------------------------------------*/
	if($id>0){
	
		$ssql = "select marcaid, mmarca, mdescricao, mlink_seo, mguia, mordem, mimagem, mcodusuario, mdata_alteracao, mdata_cadastro
				from tblmarca where marcaid='{$id}'";
		$result = mysql_query($ssql);
		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				
				$marca				=	$row["mmarca"];
				$descricao			=	$row["mdescricao"];
				$link_seo			=	$row["mlink_seo"];
				$texto				=	$row["mguia"];
				$arquivo			=	$row["mimagem"];
				
				$usuario			=	get_usuario($row["mcodusuario"],"unome");		
				$data_alteracao		=	formata_data_tela($row["mdata_alteracao"]);
				$data_cadastro		=	formata_data_tela($row["mdata_cadastro"]);
				
			}
			mysql_free_result($result);
		}	
		
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$("#marca").focus();		
	});
</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form method="post" name="frm_conteudo" id="frm_conteudo" action="marca.php" enctype="multipart/form-data" onsubmit="return valida_marca();">
    <input name="action" id="action" type="hidden" value="gravar" />  
    <input name="id" id="id" type="hidden" value="<?php echo $id?>" />
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Marca &raquo;  <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='marca.php';"><?php echo ($id==0) ? "Novo Registro" : $marca;  ?></span> </span> <a href="marca_consulta.php">Consulta</a>
   	    </div>
            
            <div id="conteudo-interno">
				<table width="98%" border="0" cellspacing="0" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="130">ID</td>
                <td>#<?php echo $id;?></td>
                <td width="200" rowspan="19" align="center" valign="top">
				  <?php echo $msg;?>
                  <br />
                  <?php 				  
				  	if(file_exists("../".$arquivo) && strlen($arquivo)>4){		
						echo "<p align='center'>";
						echo "<img src='../".$arquivo."' width='".$x."' height='".$y."' border='0' />";
						echo "</p>";
					}
				  ?>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Marca</td>
                <td><input name="marca" type="text" class="formulario" id="marca" value="<?php echo $marca;?>" size="75" maxlength="100" onblur="javascript: gera_link_seo(this.value,'link_seo');"></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Descrição</td>
                <td><input name="descricao" class="formulario" id="descricao" type="text" size="75" maxlength="250" value="<?php echo $descricao;?>" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Link-Seo</td>
                <td><input name="link_seo" class="formulario" id="link_seo" type="text" size="75" maxlength="250" value="<?php echo $link_seo;?>" style="color:#CCC;" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Texto</td>
                <td><textarea name="editor1" class="ckeditor" id="editor1"><?php echo $texto;?></textarea></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Imagem</td>
                <td>
<input name="arquivo" type="file" id="arquivo" size="75" maxlength="200" />
Dimensão da imagem <?php echo $x . "x" .$y;?>pixel

                </td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>Tamanho máximo: 100KB<br />
				Formatos aceitos: .jpg, .gif, .png<br />
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Usuário</td>
                <td><?php echo $usuario;?></td>
                </tr>
              <tr>
                <td>Data Alteração</td>
                <td><?php echo $data_alteracao;?></td>
                </tr>
              <tr>
                <td>Data Cadastro</td>
                <td><?php echo $data_cadastro;?></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" id="btn-cmd-gravar" name="btn-cmd-gravar" value="Gravar" class="btn-gravar" /></td>
                </tr>
              </table>
          </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>