<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	
	$id = "0";
	$referencia = "";


	if(isset($_REQUEST["id"])){
		$id = intval($_REQUEST["id"]);
	}
	if(isset($_REQUEST["referencia"])){
		$referencia = addslashes($_REQUEST["referencia"]);
	}
	
	
	if($id>0 || $referencia != ""){
	
		$ssql = "select produtoid, preferencia, pcodigo, pcodigo_barras, pncm,  pproduto, psubtitulo, pcodigo_fabricante, pdescricao, pdescricao_detalhada, ppalavra_chave, 
				pvalor_unitario, pvalor_comparativo, pcodmarca, pdisponivel, pcontrola_estoque, pfrete_gratis, ppeso, plargura, paltura, pcomprimento, pminimo, 
				pmaximo, pvitrine, pdata_inicio, pdata_termino, plink_seo, pordem, pcodusuario, pdata_alteracao, pdata_cadastro, pficha_tecnica, ptop20, outlet, recomendados
				from tblproduto ";
		if($referencia!=""){
			$ssql .= " where preferencia='{$referencia}' limit 0,1";
		}		
		
		if($id>0){
			$ssql .= " where produtoid='{$id}'";
		}
		
		
		$result = mysql_query($ssql);
		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$referencia			=	$row["preferencia"];
				$codigo				=	$row["pcodigo"];
				$codigo_barras		=	$row["pcodigo_barras"];
				$ncm				=	$row["pncm"];
				$produto			=	$row["pproduto"];
				$subtitulo			=	$row["psubtitulo"];
				$codigo_fabricante	=	$row["pcodigo_fabricante"];
				
				$descricao			=	$row["pdescricao"];
				$descricao_detalhada=	$row["pdescricao_detalhada"];
				$ficha_tecnica		=	$row["pficha_tecnica"];
				$palavra_chave		=	$row["ppalavra_chave"];
				
				$valor_unitario		=	number_format($row["pvalor_unitario"],2,",",".");
				$valor_comparativo	=	number_format($row["pvalor_comparativo"],2,",",".");
				
				$marca				=	$row["pcodmarca"];
				
				$disponivel			=	$row["pdisponivel"];
				$controla_estoque	=	$row["pcontrola_estoque"];
				$frete_gratis		=	$row["pfrete_gratis"];
				$top20				=	$row["ptop20"];
				$peso				=	number_format($row["ppeso"],2,",",".");
				
				$largura			=	$row["plargura"];
				$altura				=	$row["paltura"];
				$comprimento		=	$row["pcomprimento"];
				
				$minimo				=	$row["pminimo"];
				$maximo				=	$row["pmaximo"];
				
				$vitrine			=	$row["pvitrine"];
				$data_inicio		=	formata_data_tela($row["pdata_inicio"]);
				$data_termino		=	formata_data_tela($row["pdata_termino"]);
				
				$link_seo			=	$row["plink_seo"];
				
				$ordem				=	$row["pordem"];
				
				$outlet             =   $row["outlet"];

				$recomendados		=   $row["recomendados"];
				
				$usuario			=	$row["pcodusuario"];		
				$data_alteracao		=	formata_data_tela($row["pdata_alteracao"]);
				$data_cadastro		=	formata_data_tela($row["pdata_cadastro"]);
				
			}
			mysql_free_result($result);
		}	
		
		
		$categoria;
		$i = 0;
		
		$ssql = "select pcodcategoria from tblproduto_categoria where pcodproduto='{$id}'";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$categoria[$i] = $row["pcodcategoria"];
				$i++;			
			}
			mysql_free_result($result);
		}
		
		
		$condicao;
		$i = 0;
		
		$ssql = "select pcodcondicao from tblproduto_condicao_pagamento where pcodproduto='{$id}'";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$condicao[$i] = $row["pcodcondicao"];
				$i++;			
			}
			mysql_free_result($result);
		}		
		
		
		$frete_condicao;
		$i = 0;
		
		$ssql = "select pcodfrete from tblproduto_frete where pcodproduto='{$id}'";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$frete_condicao[$i] = $row["pcodfrete"];
				$i++;			
			}
			mysql_free_result($result);
		}
		
		
		$propriedade;
		$i = 0;
		
		$ssql = "select ccodpropriedade from tblproduto_caracteristica where ccodproduto='{$id}'";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$propriedade[$i] = $row["ccodpropriedade"];
				$i++;			
			}
			mysql_free_result($result);
		}		
	
	}

	$img_dimensao		= get_configuracao("config_img_dimensao");	
	$img_dimensao = strtolower($img_dimensao);
	list($imagem_x, $imagem_y) = explode("x",$img_dimensao);
	
										

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - Loja Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - Loja Virtual" />
<meta name="description" content="Painel de administração da loja virtual" />
<meta name="keywords" content="loja virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">

	var dica = new Array();
	
	dica["referencia"] = 'Utilize este campo apenas se o produto for parte de uma coleçao de itens ou se for uma variação de cor de um produto já existente, caso contrário deixe em branco que o sistema irá preencher automaticamente.';
	dica["codigo"] = 'O campo código serve para informar o SKU (código único do produto exemplo PA3456789), normalmente este é um campo alpha numérico sem espaços.';
	dica["codigo_barras"] = 'Preencha com o código de barras, caso seu produto não tenha código de barras deixe em branco que o sistema irá preencher automaticamente.';
	dica["ncm"] = 'Preencha com o NCM (Nomenclatura Comum Mercosul), esta informação é obrigatória em caso de emissão de nota fiscal eletrônica pelo sistema da loja virtual. Caso não tenha essa informação consulte seu contador.';
	

	dica["produto"] = 'O nome do produto é uma das informações mais importantes pois ela que facilita a localicação do mesmo em sua loja e indexa o nome do produto me buscadores como google.';
	dica["subtitulo"] = 'Utilizado para cadastros de livros com subtitulos ou ainda para variação de cores de um mesmo produto.';

	dica["link_seo"] = 'Este campo é gerado automaticamente pelo sistema com a url amigável do nome do produto. O sistema irá gerar automaticamente este campo porem você poderá editar desde que não haja outro produto com a mesma url amigável.';

	dica["codigo_fabricante"] = 'Utilize este campo para inserir o código do produto junto a seu fornecedor.';

	dica["valor_comparativo"] = 'Este campo deve ser preenchido apenas se o produto tiver uma opção de desconto de valor por exemplo o produto custava R$ 100,00 e agora custa R$ 90,00. <br /><br />Este campo quando preenchido apresenta o valor comparativo no site riscado por cima o que significa que o precó do produto baixou.';

	dica["valor_unitario"] = 'Este campo deve ser preenchido com o valor final de venda do produto.';
	
	dica["peso"] = 'O peso deve ser informado em Kilos exemplo: 1 kilo deve ser usado 1,00 .';

	dica["largura"] = 'A largura deve ser informada em centimetros com números inteiros. A largura mínima aceita são 11 centímetros.';
	
	dica["altura"] = 'A altura deve ser informada em centimetros com números inteiros. A altura mínima aceita são 2 centímetros.';
	
	dica["comprimento"] = 'O cumprimento deve ser informada em centimetros com números inteiros. O comprimento mínimo aceita são 16 centímetros.';	
	
	dica["disponivel"] = 'Informa se o produto estará disponível para venda na loja virtual.<br /><br />O parâmetro data de início e data de término exclui esta regra.';	
	
	dica["controla_estoque"] = 'Informa se este produto terá controle de estoque ou não.<br />A informação de indisponível será setada automaticamente quando o produto tiver controle de estoque e seu estoque for igual ou menor que zero.';	

	dica["frete_gratis"] = 'Marque esta opção para definir que o produto terá frete gratis indiferente de local de entrega.<br /><br />Esta opção é muito usada em promoções e para produtos virtuais como downloads que não dependem de envio do produto físico.';	

	dica["minimo"] = 'Define a quantidade mínima de compra de um determinado produto, ou seja só poderão ser adquiridos se comprados na quantidade mínima.';	

	dica["maximo"] = 'Define a quantidade máxima de compra de um determinado produto, regra essa aplicada por carrinho e nao por cliente.';	

	dica["vitrine"] = 'Define se o produto será exibido na vitrine da loja. A regra de data de início e data de término exclui essa regra.';	

	dica["data_inicio"] = 'A data de início determina a data de início de exibição do produto na loja virtual.';	

	dica["data_termino"] = 'A data de término determina a data final de exibição do produto na loja virtual.';	

	dica["categoria"]	= 'Selecione todas as categorias que o produto deve aparecer no site.<br /><br /> Você pode escolher mais de uma categoria.';
	
	
	
	dica["descricao"]	= 'A descrição é muito importante pois esta informação será indexada aos sistemas de busca como google. Utilize uma brevre descrição que descreva o produto.';

	dica["palavra_chave"]	= 'As palavras chaves serão utilizadas no campo de busca do site como na indexação de buscadores como google.<br /><br />Utilize vírgula para separar as palavras chaves.<br /><br />Exemplo: Se o produto for uma geladeira você poderá incluir uma palavra chave <em>refrigerador, freezer</em> pois o sistema irá buscar por todas as palavras chaves além da descrição do produto.';

	dica["descricao_detalhada"]	= 'A descrição detalhada é a parte de informações sobre o produto. Utilize este campo para incluir o máximo de informações sobre o produto.';


	dica["tipo"]	= 'Selecione o tipo de arquivo que será enviado ao servidor.<br /><br />O sistema suporta arquivos de imagens: jpg, png e gif e cada arquivo de imagem deve ter no mínimo <?php echo $imagem_x."X".$imagem_y;?> pixel de dimensão para o efeito de zoom e seu tamanho não ser maior que 200KB.';
	
	dica["titulo"]	= 'Selecione o titulo/nome do arquivo. Para imagens utilize o nome do produto ou uma descrição da imagem. Para arquivos PDF utilize um título de descrição do arquivo.';
	
	dica["principal"]	= 'Selecione esta opção se a imagem for ser utilizada como imagem principal e capa do produto. Caso esta opção não esteja marcada a imagem será parte da galeria de fotos do produto.';
	
	dica["arquivo"]	= 'Selecione o arquivo e lembre-se que o sistema suporta arquivos de imagens nos formatos: jpg, png e gif e cada arquivo de imagem deve ter no mínimo <?php echo $imagem_x."X".$imagem_y;?> pixel de dimensão para o efeito de zoom e seu tamanho não ser maior que 200KB.';



	dica["propriedade"]	= 'Selecione as prorpiedades disponíveis para o produto. Não utilize <em>cor</em> como propriedade pois o sistema tem uma função exclusiva de referência de produto para essa propriedade.';

	dica["condicao"]	= 'A regra de condição de pagamento deve ser selecionada apenas quando um produto puder ser vendido somente nas opções selecionadas.<br /><br />Caso você não selecione nenhuma opção o sistema irá automaticamente disponibilizar todas as condições de pagamento disponíveis.';	
	
	dica["frete"]	= 'A regra de frete deve ser selecionada apenas quando um produto puder ser vendido somente nas opções de envio selecionadas.<br /><br />Caso você não selecione nenhuma opção o sistema irá automaticamente disponibilizar todas as condições de frete disponíveis.';



	$(document).ready(function() {
							   
		<?php
			if($id==0){
				echo '$("#tabs").tabs({disabled:[1,2,3,4,5,6,7]});';	
			}
		
		?>					   
							   
		$("#tabs").tabs({
			select: function(event, ui) {
				$("#tab").attr("value",ui.index+1);
				var guia = document.getElementById("tab").value;
				setTimeout('$("#btn-cmd-gravar-"+guia).attr("disabled",0)',100);
				if(guia==2){
					setTimeout('$("#peso").focus()',100);
				}
				if(guia==3){
					$("#dica"+guia).html(dica["categoria"]);	
				}
				
				if(guia==4){
					setTimeout('$("#descricao").focus()',100);
				}	


				if(guia==5){
					$("#dica"+guia).html(dica["propriedade"]);	
				}

				
				if(guia==6){
					setTimeout('$("#tipo").focus()',100);
				}	
				
				if(guia==7){
					$("#dica"+guia).html(dica["condicao"]);	
				}
				
				if(guia==8){
					$("#dica"+guia).html(dica["frete"]);	
				}				
				
			}
		});

		$("#referencia").focus();
		$("#data_inicio").mask("99/99/9999");
		$("#data_termino").mask("99/99/9999");		
		
		
		$(".numerico").keydown(function(event) { 
			// Allow: backspace, delete, tab, escape, and enter 
			if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||  
				 // Allow: Ctrl+A 
				(event.keyCode == 65 && event.ctrlKey === true) ||  
				 // Allow: home, end, left, right 
				(event.keyCode >= 35 && event.keyCode <= 39)) { 
					 // let it happen, don't do anything 
					 return; 
			} 
			else { 
				// Ensure that it is a number and stop the keypress 
				if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) { 
					event.preventDefault();  
				}    
			} 
		}); 
		
		
		$(".valor").keydown(function(event) { 
			// Allow: backspace, delete, tab, escape, and enter , virgula (,) , ponto(.)
			if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||  event.keyCode == 188 || event.keyCode == 190 ||    
				 // Allow: Ctrl+A 
				(event.keyCode == 65 && event.ctrlKey === true) ||  
				 // Allow: home, end, left, right 
				(event.keyCode >= 35 && event.keyCode <= 39)) { 
					 // let it happen, don't do anything 
					 return; 
			} 
			else { 
				// Ensure that it is a number and stop the keypress 
				if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) { 
					event.preventDefault();  
				}    
			} 
		}); 	

	
		//seta o valor da imagem id
		$('.imagem_produto').live('click', function(){
			var $this = $( this );  
			var id = $this.attr("rel");  
			var tipo = $this.attr("tipo");
			var principal = $this.attr("principal");			
			var titulo = $this.attr("title");

			$(".imagem_produto_on").removeClass("imagem_produto_on").addClass("imagem_produto");
			
			
			$("#tipo").attr("value",tipo);
			$("#titulo").attr("value",titulo);
			$("#principal").attr("value", principal );
			
			if($("#midiaid").attr("value")!=id){
				//seta o valor
				$("#midiaid").attr("value",id);
				$("#img_produto_"+id).addClass("imagem_produto_on");
				$("#btn-cmd-gravar-6").attr("value","Atualizar");
			}else{
				//seta o valor	
				$("#img_produto_"+id).removeClass("imagem_produto_on").addClass("imagem_produto");
				$("#midiaid").attr("value","0");
				$("#btn-cmd-gravar-6").attr("value","Enviar");
			}
			
			
		}); 	



	//exclui a imagem id
	$('.img_excluir').live('click', function(){
		var $this = $( this );  
		var midiaid = $this.attr("rel");  
		
		$("#midiaid").attr("value",midiaid);		
		var id = document.getElementById("id").value;
		
		if(confirm("Deseja realmente excluir o arquivo selecionado?")){
			produto_del_images(id, midiaid)
		}			
		
	}); 	


	
	});
  
	function seta_foco(id_name){
		var guia = document.getElementById("tab").value;
		$("#dica"+guia).html(dica[id_name.id]);	 
		setTimeout('$("#btn-cmd-gravar-"+guia).attr("disabled",0)',4000);
	}
  
  
</script>
</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form method="post" enctype="multipart/form-data" name="frm_produto" id="frm_produto" onsubmit="return valida_produto();">
    <input name="tab" id="tab" type="hidden" value="1" />  
    <input name="id" id="id" type="hidden" value="<?php echo $id?>" />
    <input name="midiaid" id="midiaid" type="hidden" value="0" onchange="javascript:produto_load_images(document.getElementById('id').value)" />
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Produto &raquo;  <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='produto.php';"><?php echo ($id==0) ? "Novo Registro" : $produto;  ?></span></span> <a href="produto_consulta.php">Consulta</a>
        	</div>
            
            <div id="conteudo-interno">
                
                  
              <div id="tabs">
                <ul>
                    <li><a href="#tab-1"><span>Principal</span></a></li>
                    <li><a href="#tab-2"><span>Propriedades</span></a></li>
                    <li><a href="#tab-3"><span>Categorias</span></a></li>
                    <li><a href="#tab-4"><span>Detalhes</span></a></li>
                    <li><a href="#tab-5"><span>Opções & Tamanhos</span></a></li>
                    <li><a href="#tab-6"><span>Imagens & Arquivos</span></a></li>
                    <li><a href="#tab-7"><span>Pagamentos</span></a></li>
                    <li><a href="#tab-8"><span>Fretes</span></a></li>
                </ul>

                <div id="tab-1">
                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>
                
                        <table width="98%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="120" height="25">ID</td>
                            <td><div id="produtoid"><?php echo $id?></div></td>
                          </tr>
                          <tr>
                            <td width="120" height="25">Referência</td>
                            <td><input name="referencia" class="formulario" id="referencia" type="text" size="35" maxlength="20" value="<?php echo $referencia;?>" onfocus="javascript:seta_foco(this);" />
                            <a href="javascript:void(0);" onclick="javascript:procura_produto_referencia();">
                            <img src="images/ico_lupa.gif" width="18" height="18" alt="Procurar"  border="0" />
                            </a>
                            </td>
                          </tr>
                          <tr>
                            <td height="25">Código*</td>
                            <td><input name="codigo" class="formulario" id="codigo" type="text" size="35" maxlength="50" value="<?php echo $codigo;?>" onfocus="javascript:seta_foco(this);" /></td>
                          </tr>
                          <tr>
                            <td height="25">EAN13</td>
                            <td><input name="codigo_barras" class="formulario" id="codigo_barras" type="text" size="35" maxlength="50" value="<?php echo $codigo_barras;?>" onfocus="javascript:seta_foco(this);" />
                            </td>
                          </tr>
                          <tr>
                            <td height="25">NCM</td>
                            <td><input name="ncm" class="formulario" id="ncm" type="text" size="35" maxlength="50" value="<?php echo $ncm;?>" onfocus="javascript:seta_foco(this);" />
                            </td>
                          </tr>                          
                          <tr>
                            <td height="25">&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>                          
                          <tr>
                            <td height="25">Nome do Produto*</td>
                            <td><input name="produto" class="formulario" id="produto" type="text" size="75" maxlength="100" value="<?php echo $produto;?>" onfocus="javascript:seta_foco(this);" onblur="javascript: gera_link_seo(this.value+'-'+document.frm_produto.subtitulo.value,'link_seo');" /></td>
                          </tr>
                          <tr>
                            <td height="25">Subtítulo</td>
                            <td><input name="subtitulo" class="formulario" id="subtitulo" type="text" size="75" maxlength="200" value="<?php echo $subtitulo;?>" onfocus="javascript:seta_foco(this);" onblur="javascript: gera_link_seo(document.frm_produto.produto.value +'-'+this.value,'link_seo');" /></td>
                          </tr>
                          <tr>
                            <td height="25">&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td height="25">Link-SEO*</td>
                            <td><input name="link_seo" class="formulario" id="link_seo" type="text" size="75" maxlength="250" value="<?php echo $link_seo;?>" onfocus="javascript:seta_foco(this);" style="color:#999;" /></td>
                          </tr>
                          <tr>
                            <td height="25">&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td height="25">Marca*</td>
                            <td>
                            <select name="marca" class="formulario" id="marca" onfocus="javascript:seta_foco(this);" >
                            <option value="0" >Selecione a marca &nbsp;&nbsp;&nbsp;</option>
                            <?php
                            	echo  monta_combo_marca($marca);
							?>
                            </select>
                            </td>
                          </tr>
                          <tr>
                            <td height="25">&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td height="25">Cod. Fabricante</td>
                            <td><input name="codigo_fabricante" class="formulario" id="codigo_fabricante" type="text" size="75" maxlength="20" value="<?php echo $codigo_fabricante;?>" onfocus="javascript:seta_foco(this);" /></td>
                          </tr>
                          <tr>
                            <td height="25">&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td height="25">Valor Comparativo*</td>
                            <td><input name="valor_comparativo" class="formulario valor" id="valor_comparativo" type="text" size="20" maxlength="20" value="<?php echo $valor_comparativo;?>" onfocus="javascript:seta_foco(this);" /> 
                              formato 0,00</td>
                          </tr>
                          <tr>
                            <td height="25">Valor Unitário*</td>
                            <td><input name="valor_unitario" class="formulario valor" id="valor_unitario" type="text" size="20" maxlength="20" value="<?php echo $valor_unitario;?>" onfocus="javascript:seta_foco(this);" /> 
                              formato 0,00</td>
                          </tr>
						  <tr>
                            <td height="25">Outlet</td>
							<td>
								<label><input type="radio" name="outlet" class="formulario valor" id="outlet" value="-1" <?php echo ($outlet == -1) ? "checked='checked'" : "";?> onfocus="javascript:seta_foco(this);" />Sim</label>
                                <label><input type="radio" class="formulario valor" name="recomendados" id="recomendados" value="0" <?php echo ($outlet == 0) ? "checked='checked'" : "";?> onfocus="javascript:seta_foco(this);" />Não</label>
							</td>
                          </tr>
                          <tr>
                            <td height="25">Recomendados</td>
							<td>
								<label><input type="radio" name="outlet" class="formulario valor" id="outlet" value="1" <?php echo ($recomendados == 1) ? "checked='checked'" : "";?> onfocus="javascript:seta_foco(this);" />Sim</label>
                                <label><input type="radio" class="formulario valor" name="outlet" id="outlet" value="0" <?php echo ($recomendados == 0) ? "checked='checked'" : "";?> onfocus="javascript:seta_foco(this);" />Não</label>
							</td>
                          </tr>
                          <tr>
                            <td height="25">&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td height="25">Data Alteração</td>
                            <td><?php echo $data_alteracao;?></td>
                          </tr>
                          <tr>
                            <td height="25">Data Cadastro</td>
                            <td><?php echo $data_cadastro;?></td>
                          </tr>
                          <tr>
                            <td height="25">&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td height="25">&nbsp;</td>
                            <td><input type="submit" id="btn-cmd-gravar-1" name="btn-cmd-gravar-1" value="Gravar" class="btn-gravar" /></td>
                          </tr>
                        </table>


                    </td>
                    <td width="250" id="td_dica1" align="left" valign="top" style="border:#CCC dotted 1px; text-align:justify; padding:0 10px 10px 10px;"><p><strong>Atenção:</strong></p>
                     <p id="dica1">&nbsp;</p>
                     <p>&nbsp;</p>
                     <p>* campos obrigatórios.</p>
                    </td>
                  </tr>
                </table>

				</div>
                


                
                <div id="tab-2">
                
                    <table width="98%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                    
                            <table width="98%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td height="25">Peso</td>
                                <td><input name="peso" class="formulario valor" id="peso" type="text" size="20" maxlength="20" value="<?php echo $peso;?>" onfocus="javascript:seta_foco(this);" />
                                  0,00 (peso em quilo)</td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>   
                              <tr>
                                <td height="25">Dimensões*</td>
                                <td>Largura
                                  <input name="largura" class="formulario numerico" id="largura" type="text" size="10" maxlength="6" value="<?php echo $largura;?>" onfocus="javascript:seta_foco(this);" />
                                    Altura
                                  <input name="altura" class="formulario numerico" id="altura" type="text" size="10" maxlength="6" value="<?php echo $altura;?>" onfocus="javascript:seta_foco(this);" />
                                    Comprimento
                                  <input name="comprimento" class="formulario numerico" id="comprimento" type="text" size="10" maxlength="6" value="<?php echo $comprimento;?>" onfocus="javascript:seta_foco(this);" /> 
                                    (medidas em centímetros)</td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>                                               
                              <tr>
                                <td width="120" height="25">Disponível</td>
                                <td>
                                    <label><input type="radio" name="disponivel" id="disponivel" value="-1" <?php echo ($disponivel == -1) ? "checked" : "" ;?> onfocus="javascript:seta_foco(this);" />Sim</label>
                                    <label><input type="radio" name="disponivel" id="disponivel" value="0" <?php echo ($disponivel == 0) ? "checked" : "" ;?> onfocus="javascript:seta_foco(this);" />Não</label>
                                 </td>
                              </tr>
                              <tr>
                                <td height="25">Controla Estoque</td>
                                <td>
                                    <label><input type="radio" name="controla_estoque" id="controla_estoque" value="-1" <?php echo ($controla_estoque == -1) ? "checked" : "" ;?> onfocus="javascript:seta_foco(this);" />Sim</label>
                                    <label><input type="radio" name="controla_estoque" id="controla_estoque" value="0" <?php echo ($controla_estoque == 0) ? "checked" : "" ;?> onfocus="javascript:seta_foco(this);" />Não</label>                            
                                </td>
                              </tr>
                              <tr>
                                <td height="25">Frete Grátis</td>
                                <td>
                                    <label><input type="radio" name="frete_gratis" id="frete_gratis" value="-1" <?php echo ($frete_gratis == -1) ? "checked" : "" ;?> onfocus="javascript:seta_foco(this);" />Sim</label>
                                    <label><input type="radio" name="frete_gratis" id="frete_gratis" value="0" <?php echo ($frete_gratis == 0) ? "checked" : "" ;?> onfocus="javascript:seta_foco(this);" />Não</label>                              
                                </td>
                              </tr>
							  <tr>
                                <td height="25">TOP 20</td>
                                <td>
                                    <label><input type="radio" name="top20" id="top20" value="-1" <?php echo ($top20 == -1) ? "checked" : "" ;?> onfocus="javascript:seta_foco(this);" />Sim</label>
                                    <label><input type="radio" name="top20" id="top20" value="0" <?php echo ($top20 == 0) ? "checked" : "" ;?> onfocus="javascript:seta_foco(this);" />Não</label>
                                </td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>                          
                              <tr>
                                <td height="25">Mínimo*</td>
                                <td><input name="minimo" class="formulario numerico" id="minimo" type="text" size="10" maxlength="6" value="<?php echo $minimo;?>" onfocus="javascript:seta_foco(this);" />
                                  número inteiro</td>
                              </tr>
                              <tr>
                                <td height="25">Máximo*</td>
                                <td><input name="maximo" class="formulario numerico" id="maximo" type="text" size="10" maxlength="6" value="<?php echo $maximo;?>" onfocus="javascript:seta_foco(this);" />
                                  número inteiro</td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25">Exibir na vitrine</td>
                                <td>
                                    <label><input type="radio" name="vitrine" id="vitrine" value="-1" <?php echo ($vitrine == -1) ? "checked" : "" ;?> onfocus="javascript:seta_foco(this);" />Sim</label>
                                    <label><input type="radio" name="vitrine" id="vitrine" value="0" <?php echo ($vitrine == 0) ? "checked" : "" ;?> onfocus="javascript:seta_foco(this);" />Não</label>                              
                                </td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25">Data de Inínio*</td>
                                <td><input name="data_inicio" class="formulario" id="data_inicio" type="text" size="20" maxlength="20" value="<?php echo $data_inicio;?>" onfocus="javascript:seta_foco(this);" />
                                  dd/mm/aaaa</td>
                              </tr>
                              <tr>
                                <td height="25">Data de Término*</td>
                                <td><input name="data_termino" class="formulario" id="data_termino" type="text" size="20" maxlength="20" value="<?php echo $data_termino;?>" onfocus="javascript:seta_foco(this);" />
                                dd/mm/aaaa</td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td><input type="submit" id="btn-cmd-gravar-2" name="btn-cmd-gravar-2" value="Gravar" class="btn-gravar" /></td>
                              </tr>
                            </table>
    
    
                        </td>
                        <td width="250" align="left" valign="top" style="border:#CCC dotted 1px; text-align:justify; padding:0 10px 10px 10px;"><p><strong>Atenção:</strong></p>
                         <p id="dica2">&nbsp;</p>
                         <p>&nbsp;</p>
                         <p>* campos obrigatórios.</p>
                        </td>
                      </tr>
                    </table>

				</div>
                
                
                
                <div id="tab-3">
                
                    <table width="98%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                    
                            <table width="98%" border="0" cellspacing="0" cellpadding="0">
                               <tr>
                                <td>
                                Selecione as categorias que o produto deve aparecer:
                                </td>
                                </tr>
                     			<tr>
                                <td>&nbsp;
                                
                                </td>
                                </tr>                                
                              <tr>
                                <td>
                                	<?php
										$i=0;
										
										//////////////////////////////////////////////////////
										//departamento
                                    	$ssql = "select categoriaid, ccodcategoria, ccategoria from tblcategoria where ccodcategoria=0 order by ccategoria";
										$result = mysql_query($ssql);
										if($result){
											while($row=mysql_fetch_assoc($result)){
												$i++;
												if($i==1){	
													echo '<ul style=" float:left; width:230px; padding:5px; margin:5px; border:#ccc dotted 1px;">';
												}
												
												
												$checked = '';
												for($i=0;$i<count($categoria);$i++){
													if($categoria[$i]==$row["categoriaid"]){
														$checked = ' checked="checked"';
														//break;
													}	
												}
												
												
												echo '<label><input type="checkbox" name="categoria[]" id="categoria[]" value="'.$row["categoriaid"].'" '.$checked.' class="categoria" />'.$row["ccategoria"].'</label>';	

												/////////////////////////////////////////////////////
												//categoria
												$ssql1 = "select categoriaid, ccodcategoria, ccategoria from tblcategoria where ccodcategoria=".$row["categoriaid"]." order by ccategoria";
												$result1 = mysql_query($ssql1);
												if($result1){
													while($row1=mysql_fetch_assoc($result1)){
														
														$checked = '';
														for($i=0;$i<count($categoria);$i++){
															if($categoria[$i]==$row1["categoriaid"]){
																$checked = ' checked="checked"';
																//break;
															}	
														}														
														
														echo "<li style='list-style:none; padding:5px 0 0 20px;'>";
														echo '<label><input type="checkbox" name="categoria[]" id="categoria[]" value="'.$row1["categoriaid"].'" '.$checked.' class="categoria" />'.$row1["ccategoria"].'</label>';	
														
														
														/////////////////////////////////////////////////////
														//subcategoria
														
															$ssql2 = "select categoriaid, ccodcategoria, ccategoria from tblcategoria where ccodcategoria=".$row1["categoriaid"]." order by ccategoria";
															$result2 = mysql_query($ssql2);
															if($result2){
																while($row2=mysql_fetch_assoc($result2)){		
																
																	$checked = '';
																	for($i=0;$i<count($categoria);$i++){
																		if($categoria[$i]==$row2["categoriaid"]){
																			$checked = ' checked="checked"';
																			//break;
																		}	
																	}																	
																
																	echo "<li style='list-style:none; padding:5px 0 0 40px;'>";
																	echo '<label><input type="checkbox" name="categoria[]" id="categoria[]" value="'.$row2["categoriaid"].'" '.$checked.' class="categoria" />'.$row2["ccategoria"].'</label>';	
																	echo '</li>';																
																}
																mysql_free_result($result2);
															}
														
														
														
														
														echo "</li>";
													}
													mysql_free_result($result1);
												}
												
												
												echo "</ul>";
												$i=0;
											}
											mysql_free_result($result);
										}
									
									?>
                                    
                                </td>
                              </tr>

                              <tr>
                                <td colspan="2"><input type="submit" id="btn-cmd-gravar-3" name="btn-cmd-gravar-3" value="Gravar" class="btn-gravar" /></td>
                              </tr>
                            </table>
    
    
                        </td>
                        <td width="250" align="left" valign="top" style="border:#CCC dotted 1px; text-align:justify; padding:0 10px 10px 10px;"><p><strong>Atenção:</strong></p>
                         <p id="dica3">&nbsp;</p>
                         <p>&nbsp;</p>
                         <p>* campos obrigatórios.</p>
                        </td>
                      </tr>
                    </table>

                </div>
                
                
                <div id="tab-4">
                
                  <table width="98%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                    
                            <table width="98%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="120" height="25">Descrição</td>
                                <td><textarea name="descricao" cols="75" rows="4" class="formulario" id="descricao" onfocus="javascript:seta_foco(this);"><?php echo $descricao;?></textarea></td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>   
                              <tr>
                                <td height="25">Palavras Chaves</td>
                                <td><input name="palavra_chave" class="formulario" id="palavra_chave" type="text" size="75" maxlength="100" value="<?php echo $palavra_chave;?>" onfocus="javascript:seta_foco(this);" /></td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25" align="left" valign="top" colspan="2">Descrição Detalhada</td>
                              </tr>
                              <tr>
                                <td colspan="2">
                                <textarea name="editor1" class="ckeditor" id="editor1"><?php echo $descricao_detalhada;?></textarea>
                                </td>
                              </tr>
							  <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25" align="left" valign="top" colspan="2">Ficha Técnica</td>
                              </tr>
                              <tr>
                                <td colspan="2">
                                <textarea name="editor2" class="ckeditor" id="editor2"><?php echo $ficha_tecnica;?></textarea>
                                </td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td><input type="submit" id="btn-cmd-gravar-4" name="btn-cmd-gravar-4" value="Gravar" class="btn-gravar" /></td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                            </table>
    
    
                        </td>
                        <td width="250" align="left" valign="top" style="border:#CCC dotted 1px; text-align:justify; padding:0 10px 10px 10px;"><p><strong>Atenção:</strong></p>
                         <p id="dica4">&nbsp;</p>
                         <p>&nbsp;</p>
                         <p>* campos obrigatórios.</p>
                        </td>
                      </tr>
                    </table>                
                
				</div>
                
                
                <div id="tab-5">
					<table width="98%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                    
                            <table width="98%" border="0" cellspacing="0" cellpadding="0">
                               <tr>
                                <td>
                                Selecione as opções e características do produto:
                                </td>
                              </tr>                            
                              <tr>
                                <td>
                                	<?php
										$i=0;
                                    	$ssql = "select propriedadeid, pcodpropriedade, ppropriedade from tblproduto_propriedade where pcodpropriedade=0 order by ppropriedade";
										$result = mysql_query($ssql);
										if($result){
											while($row=mysql_fetch_assoc($result)){
												$i++;
												if($i==1){	
													echo '<ul style=" float:left; width:650px; padding:15px; margin:15px; border:#ccc dotted 1px;">';
												}
																								
												
												echo $row["ppropriedade"];	


												$ssql1 = "select propriedadeid, pcodpropriedade, ppropriedade from tblproduto_propriedade where pcodpropriedade=".$row["propriedadeid"]." order by ppropriedade";
												$result1 = mysql_query($ssql1);
												if($result1){
													while($row1=mysql_fetch_assoc($result1)){
														
														$checked = '';
														for($i=0;$i<count($propriedade);$i++){
															if($propriedade[$i]==$row1["propriedadeid"]){
																$checked = ' checked="checked"';
																//break;
															}	
														}														
														
														echo "<li style='list-style:none; padding:5px 0 0 20px;'>";
														echo '<label><input type="checkbox" name="propriedade[]" id="propriedade[]" value="'.$row1["propriedadeid"].'" '.$checked.' class="propriedade" />'.$row1["ppropriedade"].'</label>';	
														echo "</li>";
													}
													mysql_free_result($result1);
												}
												
												
												echo "</ul>";
												$i=0;
											}
											mysql_free_result($result);
										}
									
									?>
                                	
                                    
                                </td>
                              </tr>

                              <tr>
                                <td colspan="2"><input type="submit" id="btn-cmd-gravar-5" name="btn-cmd-gravar-5" value="Gravar" class="btn-gravar" /></td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2"><p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p></td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                            </table>
    
    
                        </td>
                        <td width="250" align="left" valign="top" style="border:#CCC dotted 1px; text-align:justify; padding:0 10px 10px 10px;"><p><strong>Atenção:</strong></p>
                         <p id="dica5">&nbsp;</p>
                         <p>&nbsp;</p>
                         <p>* campos obrigatórios.</p>
                        </td>
                      </tr>
                    </table>
                </div>
                
                
                
                <div id="tab-6">
                  <table width="98%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                    
                            <table width="98%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="120" height="25">Formato</td>
                                <td>
                                <select name="tipo" size="1" class="formulario" id="tipo" onfocus="javascript:seta_foco(this);" >
                                  <option value="0">Selecione</option>
                                  <option value="1">Imagem</option>
                                  <option value="3">Arquivo PDF</option>
                                </select>
                                </td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25">Título</td>
                                <td><input name="titulo" class="formulario" id="titulo" type="text" size="75" maxlength="200" value="<?php echo $titulo;?>" onfocus="javascript:seta_foco(this);" /></td>
                              </tr>  
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>                                                                 
                              <tr>
                                <td height="25">Principal</td>
                                <td><select name="principal" size="1" class="formulario" id="principal" onfocus="javascript:seta_foco(this);" >
                                  <option value="0">Não</option>
                                  <option value="-1">Sim</option>
                                  <option value="2">Secundária</option>
                                </select></td>
                              </tr>
							  <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25" align="left" valign="top">Arquivo</td>
                                <td><input name="arquivo" type="file" class="formulario" id="arquivo" size="75" maxlength="250" onfocus="javascript:seta_foco(this);" /></td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td><input type="submit" id="btn-cmd-gravar-6" name="btn-cmd-gravar-6" value="Enviar" class="btn-gravar" /></td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td><iframe id="upload_target" name="upload_target" frameborder="0" scrolling="no" src="" style="width:500px;height:10px;border:0px solid #fff;"></iframe></td>
                              </tr>
                              <tr>
                                <td height="25">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25" colspan="2">
                                <div id="td-img-produto" style="width:95%; float:left; margin-bottom:15px;">                      
								<?php	
								
                                	$ssql = "select midiaid, mcodtipo, marquivo, mprincipal, mtitulo from tblproduto_midia where mcodproduto = $id and mcodtipo=1 order by mprincipal, midiaid";
									$result = mysql_query($ssql);
									if($result){
										while($row=mysql_fetch_assoc($result)){
											echo 
											'<div id="img_produto_'.$row["midiaid"].'" rel="'.$row["midiaid"].'" class="imagem_produto" principal="'.$row["mprincipal"].'" tipo="1" title="'.$row["mtitulo"].'">
												<img src="../'.$row["marquivo"].'" />												
												<div class="imagem_produto_excluir"><img src="images/ico_excluir.gif" class="img_excluir" rel="'.$row["midiaid"].'" /></div>
											 </div>';
										}
										mysql_free_result($result);
									}
								?>
                                </div>
                                <div id="td-arquivo-produto" style="width:95%; float:left; margin-bottom:15px;">                      
								<?php	
								
                                	$ssql = "select midiaid, mcodtipo, mtitulo, marquivo, mprincipal from tblproduto_midia where mcodproduto = $id and mcodtipo=3 order by mprincipal, midiaid";
									$result = mysql_query($ssql);
									if($result){
										while($row=mysql_fetch_assoc($result)){
											echo 
											'<div id="img_produto_'.$row["midiaid"].'" rel="'.$row["midiaid"].'" class="imagem_produto" principal="'.$row["mprincipal"].'" tipo="3" title="'.$row["mtitulo"].'">
												<img src="../images/ico-pdf.png" /><br />												
												'.$row["mtitulo"].'
												<div class="imagem_produto_excluir"><img src="images/ico_excluir.gif" class="img_excluir" rel="'.$row["midiaid"].'" /></div>
											 </div>';
										}
										mysql_free_result($result);
									}
								?>
                                </div>                                
                                </td>
                              </tr>
                              <tr>
                                <td height="25" colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25" colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td height="25" colspan="2">&nbsp;</td>
                              </tr>
                            </table>
    
    
                        </td>
                        <td width="250" align="left" valign="top" style="border:#CCC dotted 1px; text-align:justify; padding:0 10px 10px 10px;"><p><strong>Atenção:</strong></p>
                        <p id="dica6">&nbsp;</p>
                        </td>
                      </tr>
                    </table> 
                </div>
                
                
                <div id="tab-7">
					<table width="98%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                    
                            <table width="98%" border="0" cellspacing="0" cellpadding="0">
                               <tr>
                                <td>
                                Selecione as condições de pagamento para este produto:
                                </td>
                              </tr>                            
                              <tr>
                                <td>
                                	<?php
										$i=0;
                                    	$ssql = "select formapagamentoid, fforma_pagamento from tblforma_pagamento order by fforma_pagamento";
										$result = mysql_query($ssql);
										if($result){
											while($row=mysql_fetch_assoc($result)){
												$i++;
												if($i==1){	
													echo '<ul style=" float:left; width:650px; padding:15px; margin:15px; border:#ccc dotted 1px;">';
												}
																								
												
												echo $row["fforma_pagamento"];	


												$ssql1 = "select condicaoid, ccondicao from tblcondicao_pagamento where ccodforma_pagamento=".$row["formapagamentoid"]." order by cnumero_parcelas, ccondicao";
												$result1 = mysql_query($ssql1);
												if($result1){
													while($row1=mysql_fetch_assoc($result1)){
														
														$checked = '';
														for($i=0;$i<count($condicao);$i++){
															if($condicao[$i]==$row1["condicaoid"]){
																$checked = ' checked="checked"';
																//break;
															}	
														}														
														
														echo "<li style='list-style:none; padding:5px 0 0 20px;'>";
														echo '<label><input type="checkbox" name="condicao[]" id="condicao[]" value="'.$row1["condicaoid"].'" '.$checked.' class="condicao" />'.$row1["ccondicao"].'</label>';	
														echo "</li>";
													}
													mysql_free_result($result1);
												}
												
												
												echo "</ul>";
												$i=0;
											}
											mysql_free_result($result);
										}
									
									?>
                                    
                                </td>
                              </tr>

                              <tr>
                                <td colspan="2"><input type="submit" id="btn-cmd-gravar-7" name="btn-cmd-gravar-7" value="Gravar" class="btn-gravar" /></td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2"><p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p></td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                            </table>
    
    
                        </td>
                        <td width="250" align="left" valign="top" style="border:#CCC dotted 1px; text-align:justify; padding:0 10px 10px 10px;"><p><strong>Atenção:</strong></p>
                         <p id="dica7">&nbsp;</p>
                         <p>&nbsp;</p>
                         <p>* campos obrigatórios.</p>
                        </td>
                      </tr>
                    </table>                
                </div>  
                
                
                
                
                
                <div id="tab-8">
					<table width="98%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                    
                            <table width="98%" border="0" cellspacing="0" cellpadding="0">
                               <tr>
                                <td>
                                Selecione as condições frete e envio para este produto:
                                </td>
                              </tr>                            
                              <tr>
                                <td>
                                	<?php
										echo '<ul style=" float:left; width:650px; padding:15px; margin:15px; border:#ccc dotted 1px;">';
										$ssql1 = "select freteid, fcodigo, fdescricao from tblfrete_tipo order by fdescricao";
										$result1 = mysql_query($ssql1);
										if($result1){
											while($row1=mysql_fetch_assoc($result1)){
												
												$checked = '';
												for($i=0;$i<count($frete_condicao);$i++){
													if($frete_condicao[$i]==$row1["freteid"]){
														$checked = ' checked="checked"';
														//break;
													}	
												}														
												
												echo "<li style='list-style:none; padding:5px 0 0 20px;'>";
												echo '<label><input type="checkbox" name="frete_condicao[]" id="frete_condicao[]" value="'.$row1["freteid"].'" '.$checked.' class="frete" />'.$row1["fcodigo"].' - '.$row1["fdescricao"].'</label>';	
												echo "</li>";
											}
											mysql_free_result($result1);
										}
										echo "</ul>";
									
									?>
                                    
                                </td>
                              </tr>

                              <tr>
                                <td colspan="2"><input type="submit" id="btn-cmd-gravar-8" name="btn-cmd-gravar-8" value="Gravar" class="btn-gravar" /></td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2"><p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p></td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                            </table>
    
    
                        </td>
                        <td width="250" align="left" valign="top" style="border:#CCC dotted 1px; text-align:justify; padding:0 10px 10px 10px;"><p><strong>Atenção:</strong></p>
                         <p id="dica8">&nbsp;</p>
                         <p>&nbsp;</p>
                         <p>* campos obrigatórios.</p>
                        </td>
                      </tr>
                    </table>                
                </div>  
                                
                
                
                
                
                
                  
                
			</div>
            </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
</body>
</html>